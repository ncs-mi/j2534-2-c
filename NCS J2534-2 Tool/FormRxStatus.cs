﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace NCS_J2534_2_Tool
{
    public partial class FormRxStatus
    {
        public FormRxStatus()
        {
            InitializeComponent();
        }

        private uint localFlags;
        private J2534ProtocolId localProtocolId;

        protected override void OnLoad(EventArgs e)
        {
            SetControlArrays();
            SetCheckBoxCaptions();
            TextBoxFlags.Text = J2534Code.FlagsToText(localFlags);
            SetCheckBoxes();
            LabelDesc.Text = string.Empty;
            base.OnLoad(e);
        }

        private void SetCheckBoxCaptions()
        {
            CheckBoxFlag[0].Text = "Bit 0 - TX_MSG_TYPE";
            CheckBoxFlag[0].Tag = string.Concat("Receive Indication/Transmit Loopback", Environment.NewLine, "1 = transmitted message");
            CheckBoxFlag[1].Text = "Bit 1 - START_OF_MESSAGE";
            CheckBoxFlag[1].Tag = string.Concat("Indicates first byte of an ISO9141 or ISO14230 message or", Environment.NewLine, "the first frame of an ISO15765 multi-frame message");
            CheckBoxFlag[2].Text = "Bit 2 - RX_BREAK";
            CheckBoxFlag[2].Tag = string.Concat("Break Indication Received", Environment.NewLine, "SAE J2610 and J1850VPW only");
            CheckBoxFlag[3].Text = "Bit 3 - TX_INDICATION";
            CheckBoxFlag[3].Tag = string.Concat("ISO15765 TxDone", Environment.NewLine, "1 = TxDone");
            CheckBoxFlag[4].Text = "Bit 4 - ISO15765_PADDING_ERROR";
            CheckBoxFlag[4].Tag = "For ISO15765 a CAN frame was received with less than 8 data bytes.";
            CheckBoxFlag[5].Text = "Bit 5 - Reserved for SAE";
            CheckBoxFlag[6].Text = "Bit 6 - Reserved for SAE";
            CheckBoxFlag[7].Text = "Bit 7 - ISO15765_ADDR_TYPE";
            CheckBoxFlag[7].Tag = string.Concat("0 = no extended address", Environment.NewLine, "1 = extended address is first byte after the CAN ID");
            CheckBoxFlag[8].Text = "Bit 8 - CAN_29BIT_ID";
            CheckBoxFlag[8].Tag = string.Concat("0 = 11-bit", Environment.NewLine, "1 = 29-bit");
            for (int i = 9; i <= 15; i++)
            {
                CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Reserved for SAE");
            }

            if (J2534Code.IsSingleWire(localProtocolId))
            {
                CheckBoxFlag[16].Text = "Bit 16 - SW_CAN_HV_RX";
                CheckBoxFlag[16].Tag = string.Concat("Indicates that the Single Wire CAN message", Environment.NewLine,
                                                     "received was high-voltage message.", Environment.NewLine,
                                    "0 = Normal Message", Environment.NewLine,
                                    "1 = High-Voltage Message");

                CheckBoxFlag[17].Text = "Bit 17 - SW_CAN_HS_RX";
                CheckBoxFlag[17].Tag = string.Concat("Indicates that the Single Wire CAN bus has", Environment.NewLine,
                                    "transitioned to High Speed.", Environment.NewLine,
                                    "All communication after this event will", Environment.NewLine,
                                    "occur in highspeed mode. The message", Environment.NewLine,
                                    "data in this message is undefined.");

                CheckBoxFlag[18].Text = "Bit 18 - SW_CAN_NS_RX";
                CheckBoxFlag[18].Tag = string.Concat("Indicates that the Single Wire CAN bus has", Environment.NewLine,
                                    "transitioned to Normal Speed.", Environment.NewLine,
                                    "All communication after this event will", Environment.NewLine,
                                    "occur in normal-speed mode. The message", Environment.NewLine,
                                    "data in this message is undefined.");
            }
            else if (J2534Code.IsJ1939(localProtocolId))
            {
                CheckBoxFlag[16].Text = "Bit 16 - J1939_ADDRESS_CLAIMED";
                CheckBoxFlag[16].Tag = string.Concat("Address claimed status indication will be", Environment.NewLine,
                                    "sent when an Address and Name is successfully", Environment.NewLine,
                                    "claimed. The source address claimed will be in ", Environment.NewLine,
                                    "Data[0] of the message.", Environment.NewLine,
                                    "ExtraDataIndex = 0, DataSize = 1. ");

                CheckBoxFlag[17].Text = "Bit 17 - J1939_ADDRESS_LOST";
                CheckBoxFlag[17].Tag = string.Concat("Address Lost status indication will be sent when", Environment.NewLine,
                                    "a claimed Address and Name has been lost or when", Environment.NewLine,
                                    "an attempt to claim an Address and Name fails. ", Environment.NewLine,
                                    "The source address lost will be in Data[0] of the message.", Environment.NewLine,
                                    "ExtraDataIndex = 0, DataSize = 1.");

                CheckBoxFlag[18].Text = "Bit 18 - Reserved for SAE J2534-2";
                CheckBoxFlag[18].Tag = string.Empty;

            }
            else if (J2534Code.IsFaultTolerant(localProtocolId))
            {
                CheckBoxFlag[16].Text = "Bit 16 - Reserved for SAE J2534-2";
                CheckBoxFlag[16].Tag = string.Empty;

                CheckBoxFlag[17].Text = "Bit 17 - LINK_FAULT";
                CheckBoxFlag[17].Tag = string.Concat("Status bit in a received message that is", Environment.NewLine,
                                    "set when the transceiver has detected a", Environment.NewLine,
                                    "network fault but the device was still able", Environment.NewLine,
                                    "to correctly receive the message.");

                CheckBoxFlag[18].Text = "Bit 18 - Reserved for SAE J2534-2";
                CheckBoxFlag[18].Tag = string.Empty;

            }
            else if (J2534Code.IsAnalog(localProtocolId))
            {
                CheckBoxFlag[16].Text = "Bit 16 - OVERFLOW";
                CheckBoxFlag[16].Tag = string.Concat("Indicates that the input range of the A/D", Environment.NewLine,
                                    "has been exceeded on one or more", Environment.NewLine,
                                    "samples in the received message");

                CheckBoxFlag[17].Text = "Bit 17 - Reserved for SAE J2534-2";
                CheckBoxFlag[17].Tag = string.Empty;

                CheckBoxFlag[18].Text = "Bit 18 - Reserved for SAE J2534-2";
                CheckBoxFlag[18].Tag = string.Empty;

            }
            else if (J2534Code.IsTP2_0(localProtocolId))
            {
                CheckBoxFlag[16].Text = "Bit 16 - CONNECTION_ESTABLISHED";
                CheckBoxFlag[16].Tag = string.Concat("Connection established status", Environment.NewLine,
                                    "indication will be sent when a", Environment.NewLine,
                                    "Connection is successfully", Environment.NewLine,
                                    "established.");

                CheckBoxFlag[17].Text = "Bit 17 - CONNECTION_LOST";
                CheckBoxFlag[17].Tag = string.Concat("Connection Lost status indication will", Environment.NewLine,
                                    "be sent when a established connection has", Environment.NewLine,
                                    "been lost or when an attempt to establish", Environment.NewLine,
                                    "a connection fails.");

                CheckBoxFlag[18].Text = "Bit 18 - Reserved for SAE J2534-2";
                CheckBoxFlag[18].Tag = string.Empty;

            }
            else
            {
                for (int i = 16; i <= 18; i++)
                {
                    CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Reserved for SAE J2534-2");
                }
            }

            for (int i = 19; i <= 23; i++)
            {
                CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Reserved for SAE J2534-2");
            }
            for (int i = 24; i <= 31; i++)
            {
                CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Tool Manufacturer Specific");
            }
        }

        private void CheckBoxFlag_Click(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            SetCheckBoxes();
        }

        private void CheckBoxFlag_Enter(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.CheckBoxFlag, sender);
            LabelDesc.Text = string.Concat(CheckBoxFlag[index].Text, Environment.NewLine, CheckBoxFlag[index].Tag.ToString());
        }

        private void SetCheckBoxes()
        {
            uint mask = 0;
            for (int i = 0; i <= 31; i++)
            {
                mask = 1U << i;
                CheckBoxFlag[i].Checked = CheckBoxFlag[i].Enabled && (localFlags & mask) != 0;
            }
        }

        private void SetTextBox()
        {
            uint mask = 0;
            for (int i = 0; i <= 31; i++)
            {
                mask = 1U << i;
                if (CheckBoxFlag[i].Checked)
                {
                    localFlags |= mask;
                }
                else
                {
                    localFlags &= ~mask;
                }
            }
            TextBoxFlags.Text = J2534Code.FlagsToText(localFlags);
        }

        private void TextBoxFlags_TextChanged(object sender, EventArgs e)
        {
            if (!IsHandleCreated)
            {
                return;
            }
            localFlags = J2534Code.TextToFlags(TextBoxFlags.Text);
            SetCheckBoxes();
        }

        private void TextBoxFlags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void ContextMenuStripTextBox_Opening(object sender, CancelEventArgs e)
        {
            if (ContextMenuStripTextBox.SourceControl is TextBox thisTextBox)
            {
                int selectedLength = thisTextBox.SelectionLength;
                int textLength = thisTextBox.TextLength;
                MenuTextBoxPaste.Enabled = Clipboard.ContainsText();
                MenuTextBoxCut.Enabled = (selectedLength > 0);
                MenuTextBoxCopy.Enabled = MenuTextBoxCut.Enabled;
                MenuTextBoxDelete.Enabled = MenuTextBoxCut.Enabled;
                MenuTextBoxUndo.Enabled = thisTextBox.CanUndo;
                MenuTextBoxSelectAll.Enabled = selectedLength < textLength;
            }
        }

        private void MenuTextBoxCopy_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Copy();
            }
        }

        private void MenuTextBoxCut_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Cut();
            }
        }

        private void MenuTextBoxDelete_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Delete();
            }
        }

        private void MenuTextBoxPaste_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Paste();
            }
        }

        private void MenuTextBoxSelectAll_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).SelectAll();
            }
        }

        private void MenuTextBoxUndo_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Undo();
            }
        }

        private void SetControlArrays()
        {
            InitializeCheckBoxFlag();
            for (int i = 0; i < CheckBoxFlag.Length; i++)
            {
                CheckBoxFlag[i].Click += CheckBoxFlag_Click;
                CheckBoxFlag[i].Enter += CheckBoxFlag_Enter;
            }
        }

        public DialogResult ShowDialog(ref uint flags, J2534ProtocolId protocolId)
        {
            DialogResult result = 0;
            localFlags = flags;
            localProtocolId = protocolId;
            result = this.ShowDialog();
            if (result == DialogResult.OK)
            {
                flags = localFlags;
            }
            return result;
        }

    }
}