﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace NCS_J2534_2_Tool
{
    public partial class FormTxFlags
    {
        public FormTxFlags()
        {
            InitializeComponent();
        }

        private uint localFlags;
        private J2534Channel localChannel;

        protected override void OnLoad(EventArgs e)
        {
            SetControlArrays();
            SetCheckCaptions();
            TextBoxFlags.Text = J2534Code.FlagsToText(localFlags);
            SetChecks();
            LabelDesc.Text = string.Empty;
            base.OnLoad(e);
        }

        private void SetCheckCaptions()
        {
            for (int i = 0; i <= 5; i++)
            {
                CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Reserved for SAE");
            }
            CheckBoxFlag[6].Text = "Bit 6 - ISO15765_FRAME_PAD";
            CheckBoxFlag[6].Tag = string.Concat("0 = no padding", Environment.NewLine, "1 = pad all flow controlled messages to a full CAN frame using zeroes");
            CheckBoxFlag[7].Text = "Bit 7 - ISO15765_ADDR_TYPE";
            CheckBoxFlag[7].Tag = string.Concat("0 = no extended address", Environment.NewLine, "1 = extended address is first byte after the CAN ID");
            CheckBoxFlag[8].Text = "Bit 8 - CAN_29BIT_ID";
            CheckBoxFlag[8].Tag = string.Concat("0 = 11-bit", Environment.NewLine, "1 = 29-bit");
            CheckBoxFlag[9].Text = "Bit 9 - WAIT_P3_MIN_ONLY";
            CheckBoxFlag[9].Tag = string.Concat("0 = Message timing as specified in ISO 14230", Environment.NewLine,
                                                "1 = After a response is received for a physical request, the wait time shall be reduced to P3_MIN");
            CheckBoxFlag[10].Text = "Bit 10 - SW_CAN_HV_TX";
            CheckBoxFlag[10].Tag = string.Concat("Indicates that the Single Wire CAN message", Environment.NewLine,
                                                 "should be transmitted as a high voltage message");
            for (int i = 11; i <= 15; i++)
            {
                CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Reserved for SAE");
            }
            if (localChannel.IsJ1708)
            {
                for (int i = 16; i <= 19; i++)
                {
                    CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - MSG_PRIORITY_VALUE", " bit ", i - 16);
                    CheckBoxFlag[i].Tag = string.Concat("Message Priority", Environment.NewLine,
                                                        "Valid values 1 thru 8", Environment.NewLine,
                                                        "Value of 0 or greater than 8 will be treated as priority 8");
                }
            }
            else if (localChannel.IsTP2_0)
            {
                CheckBoxFlag[16].Text = "Bit 16 - TP2_0_BROADCAST_MSG";
                CheckBoxFlag[16].Tag = string.Concat("Send this message as a broadcast which means", Environment.NewLine,
                                                     "it will be sent 5 times with an interval of ", Environment.NewLine,
                                                     "TP2_0_T_BR_INT. The last 2 data bytes will", Environment.NewLine,
                                                     "alternate between 0x55 and 0xAA.");
                for (int i = 17; i <= 19; i++)
                {
                    CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Reserved for SAE J2534-2");
                }
            }
            else
            {
                for (int i = 16; i <= 19; i++)
                {
                    CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Reserved for SAE J2534-2");
                }
            }
            for (int i = 20; i <= 21; i++)
            {
                CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Reserved for SAE J2534-2");
            }
            CheckBoxFlag[22].Text = "Bit 22 - SCI_MODE";
            CheckBoxFlag[22].Tag = string.Concat("0 = Transmit using SCI full duplex mode", Environment.NewLine, "1 = Transmit using SCI half duplex mode");
            CheckBoxFlag[23].Text = "Bit 23 - SCI_TX_VOLTAGE";
            CheckBoxFlag[23].Tag = string.Concat("0 = no voltage after message transmit", Environment.NewLine, "1 = apply 20v after message transmit");
            for (int i = 24; i <= 31; i++)
            {
                CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Tool Manufacturer Specific");
            }
        }

        private void CheckBoxFlag_Click(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            SetText();
        }

        private void CheckBoxFlag_Enter(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.CheckBoxFlag, sender);
            LabelDesc.Text = string.Concat(CheckBoxFlag[index].Text, Environment.NewLine, Convert.ToString(CheckBoxFlag[index].Tag));
        }

        private void SetChecks()
        {
            uint mask = 0;
            for (int i = 0; i <= 31; i++)
            {
                mask = 1U << i;
                CheckBoxFlag[i].Checked = (localFlags & mask) != 0;
            }
        }

        private void SetText()
        {
            uint mask = 0;
            for (int i = 0; i <= 31; i++)
            {
                mask = 1U << i;
                if (CheckBoxFlag[i].Checked)
                {
                    localFlags |= mask;
                }
                else
                {
                    localFlags &= ~mask;
                }
            }
            TextBoxFlags.Text = J2534Code.FlagsToText(localFlags);
        }

        private void TextBoxFlags_TextChanged(object sender, EventArgs e)
        {
            if (!IsHandleCreated)
            {
                return;
            }
            localFlags = J2534Code.TextToFlags(TextBoxFlags.Text);
            SetChecks();
        }

        private void TextBoxFlags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void ContextMenuStripFlags_Opening(object sender, CancelEventArgs e)
        {
            if (ContextMenuStripFlags.SourceControl is TextBox thisTextBox)
            {
                if (thisTextBox.Enabled)
                {
                    thisTextBox.Focus();
                    int selectedLength = thisTextBox.SelectionLength;
                    int textLength = thisTextBox.Text.Length;
                    MenuFlagsPaste.Enabled = Clipboard.ContainsData(DataFormats.Text);
                    MenuFlagsCut.Enabled = selectedLength > 0;
                    MenuFlagsCopy.Enabled = selectedLength > 0;
                    MenuFlagsUndo.Enabled = thisTextBox.CanUndo;
                    MenuFlagsSelectAll.Enabled = selectedLength < textLength;
                    MenuFlagDefault.Available = localChannel.DefaultTxFlags != 0;
                }
            }
        }

        private void MenuFlagClear_Click(object sender, EventArgs e)
        {
            localFlags = 0;
            SetChecks();
            SetText();
        }

        private void MenuFlagDefault_Click(object sender, EventArgs e)
        {
            TextBoxFlags.Text = J2534Code.FlagsToText(localChannel.DefaultTxFlags);
        }

        private void MenuFlagsCopy_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Copy();
            }
        }

        private void MenuFlagsCut_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Cut();
            }
        }

        private void MenuFlagsPaste_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Paste();
            }
        }

        private void MenuFlagsSelectAll_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).SelectAll();
            }
        }

        private void MenuFlagsUndo_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Undo();
            }
        }

        private void SetControlArrays()
        {
            InitializeCheckBoxFlag();
            for (int i = 0; i < CheckBoxFlag.Length; i++)
            {
                CheckBoxFlag[i].Click += CheckBoxFlag_Click;
                CheckBoxFlag[i].Enter += CheckBoxFlag_Enter;
            }
        }

        public DialogResult ShowDialog(ref uint flags, J2534Channel channel)
        {
            DialogResult result = 0;
            localFlags = flags;
            localChannel = channel;
            result = this.ShowDialog();
            if (result == DialogResult.OK)
            {
                flags = localFlags;
            }
            return result;
        }

    }
}