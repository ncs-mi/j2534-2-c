﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Globalization;
using System.IO;

namespace NCS_J2534_2_Tool
{
    public static class PublicCode
    {


        public static string StripSpaces(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < value.Length; i++)
            {
                if (!char.IsWhiteSpace(value[i]))
                {
                    result.Append(value[i]);
                }
            }
            return result.ToString();
        }

        public static bool MustBeHex(string value)
        {
            bool result = false;
            if (string.IsNullOrWhiteSpace(value))
            {
                return result;
            }
            bool containsAtoF = false;
            bool containsInvalid = false;
            for (int i = 0; i < value.Length; i++)
            {
                char c = value[i];
                if ("AaBbCcDdEeFf".Contains(c.ToString()))
                {
                    containsAtoF = true;
                }
                if (!c.IsHex())
                {
                    containsInvalid = true;
                }
            }
            result = containsAtoF && !containsInvalid;
            return result;
        }

        public static int MSB(ulong value)
        {
            for (int i = 0; i <= 63; i++)
            {
                if ((value << i & 0x8000000000000000UL) != 0)
                {
                    return 63 - i;
                }
            }
            return -1;
        }

        public static int PopCount(ulong value)
        {
            const ulong m1 = 0x5555_5555_5555_5555;
            const ulong m2 = 0x3333_3333_3333_3333;
            const ulong m4 = 0x0F0F_0F0F_0F0F_0F0F;
            value -= (value >> 1) & m1;
            value = (value & m2) + ((value >> 2) & m2);
            value = (value + (value >> 4)) & m4;
            value += value >> 8;
            value += value >> 16;
            value += value >> 32;
            return (int)(value & 0x7FUL);
        }

#region ExtensionMethods
        private const uint EM_CANUNDO = 0xC6;
        private const uint EM_UNDO = 0xC7;
        private const uint EM_SETSEL = 0xB1;
        private const uint WM_COMMAND = 0x111;
        private const uint WM_CUT = 0x300;
        private const uint WM_COPY = 0x301;
        private const uint WM_PASTE = 0x302;
        private const uint WM_CLEAR = 0x303;
        private const uint WM_UNDO = 0x304;

        private static bool IsEditControl(IntPtr hWnd)
        {
            bool result = false;
            if (hWnd == IntPtr.Zero)
            {
                return result;
            }
            StringBuilder sb = new StringBuilder();
            sb.EnsureCapacity(80);
            int n = NativeMethods.GetClassName(hWnd, sb, 80);
            result = sb.ToString().ToLower().Contains("edit") && n > 0;
            return result;
        }

        private static readonly Action<TextBox, string, bool> SetSelectedTextInternal = (Action<TextBox, string, bool>)Delegate.CreateDelegate(typeof(Action<TextBox, string, bool>), typeof(TextBox).GetMethod("SetSelectedTextInternal", BindingFlags.Instance | BindingFlags.NonPublic));
        public static void Delete(this TextBox target)
        {
            if (string.IsNullOrEmpty(target?.Text))
            {
                return;
            }
            SetSelectedTextInternal(target, string.Empty, false);
        }

        public static void Undo(this ComboBox target)
        {
            if (target?.DropDownStyle != ComboBoxStyle.DropDown)
            {
                return;
            }
            IntPtr ptrTextBox = NativeMethods.FindWindowEx(target.Handle, IntPtr.Zero, null, null);
            if (ptrTextBox == IntPtr.Zero)
            {
                return;
            }
            NativeMethods.SendMessage(ptrTextBox, WM_UNDO, IntPtr.Zero, IntPtr.Zero);
        }

        public static void Cut(this ComboBox target)
        {
            if (target?.DropDownStyle != ComboBoxStyle.DropDown)
            {
                return;
            }
            NativeMethods.SendMessage(target.Handle, WM_CUT, IntPtr.Zero, IntPtr.Zero);
        }

        public static void Copy(this ComboBox target)
        {
            if (target?.DropDownStyle != ComboBoxStyle.DropDown)
            {
                return;
            }
            NativeMethods.SendMessage(target.Handle, WM_COPY, IntPtr.Zero, IntPtr.Zero);
        }

        public static void Paste(this ComboBox target)
        {
            if (target?.DropDownStyle != ComboBoxStyle.DropDown)
            {
                return;
            }
            NativeMethods.SendMessage(target.Handle, WM_PASTE, IntPtr.Zero, IntPtr.Zero);
        }

        public static void Delete(this ComboBox target)
        {
            if (target?.DropDownStyle != ComboBoxStyle.DropDown)
            {
                return;
            }
            target.SelectedText = string.Empty;
        }

        public static bool CanUndo(this ComboBox target)
        {
            if (target == null)
            {
                return false;
            }
            bool result = false;
            int i = 0;
            IntPtr textBoxHandle = NativeMethods.FindWindowEx(target.Handle, IntPtr.Zero, null, null);
            if (textBoxHandle == IntPtr.Zero)
            {
                return result;
            }
            if (IsEditControl(textBoxHandle))
            {
                i = NativeMethods.SendMessage(textBoxHandle, EM_CANUNDO, IntPtr.Zero, IntPtr.Zero);
            }
            result = i != 0;
            return result;
        }

        [StructLayout(LayoutKind.Explicit, Pack=1)]
        private struct Conversion
        {
            [FieldOffset(0)] public byte AsByte;
            [FieldOffset(0)] public sbyte AsSByte;
            [FieldOffset(0)] public Int16 AsInt16;
            [FieldOffset(0)] public UInt16 AsUInt16;
            [FieldOffset(0)] public Int32 AsInt32;
            [FieldOffset(0)] public UInt32 AsUInt32;
            [FieldOffset(0)] public Int64 AsInt64;
            [FieldOffset(0)] public UInt64 AsUInt64;
        }

        public static byte ToByte(this string target)
        {
            byte result = 0;
            bool isHex = false;
            target = StripSpaces(target);
            if (target.StartsWith("&h", PublicDeclarations.IgnoreCase) || target.StartsWith("0x", PublicDeclarations.IgnoreCase))
            {
                target = target.Substring(2);
                isHex = true;
            }
            else if (MustBeHex(target))
            {
                isHex = true;
            }
            if (isHex)
            {
                byte.TryParse(target, NumberStyles.HexNumber, null, out result);
            }
            else
            {
                byte.TryParse(target, out result);
            }
            return result;
        }

        public static UInt32 ToUint32(this string target)
        {
            uint result = 0;
            bool isHex = false;
            target = StripSpaces(target);
            if (target.StartsWith("&h", PublicDeclarations.IgnoreCase) || target.StartsWith("0x", PublicDeclarations.IgnoreCase))
            {
                target = target.Substring(2);
                isHex = true;
            }
            else if (MustBeHex(target))
            {
                isHex = true;
            }
            if (isHex)
            {
                uint.TryParse(target, NumberStyles.HexNumber, null, out result);
            }
            else
            {
                uint.TryParse(target, out result);
            }
            return result;
        }

        public static UInt16 ToUint16(this UInt32 value)
        {
            Conversion convert = new Conversion
            {
                AsUInt32 = value
            };
            return convert.AsUInt16;
        }

        public static Int32 ToInt32(this UInt32 value)
        {
            Conversion convert = new Conversion
            {
                AsUInt32 = value
            };
            return convert.AsInt32;
        }

        public static UInt32 ToUint32(this Int32 value)
        {
            Conversion convert = new Conversion
            {
                AsInt32 = value
            };
            return convert.AsUInt32;
        }

        public static void SelectAll(this ListView target)
        {
            if (target == null)
            {
                return;
            }
            foreach (ListViewItem item in target.Items)
            {
                item.Selected = true;
            }
        }

public static bool IsHex(this char target)
        {
            if ((target >= '0' && target <= '9') || 
                (target >= 'A' && target <= 'F') || 
                (target >= 'a' && target <= 'f'))
            {
                    return true;
            }
            return false;
        }

#endregion
    }

}