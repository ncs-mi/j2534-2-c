﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Win32;
using static NCS_J2534_2_Tool.J2534ProtocolId;
using static NCS_J2534_2_Tool.J2534IoCtlId;
using static NCS_J2534_2_Tool.J2534ConfigParameterId;
using static NCS_J2534_2_Tool.J2534Collections;

namespace NCS_J2534_2_Tool
{
    public static class J2534Code
    {
        public static int GetRegistryPassThruSupportInfo()
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey("Software\\PassThruSupport.04.04");
            if (key == null)
            {
                key = Registry.LocalMachine.OpenSubKey("Software\\Wow6432Node\\PassThruSupport.04.04");
            }
            if (key != null)
            {
                foreach (string keyname in key.GetSubKeyNames())
                {
                    RegistryKey subKey = key.OpenSubKey(keyname);
                    if (subKey != null)
                    {
                        J2534_API API = new J2534_API
                        {
                            KeyName = keyname,
                            DeviceName = subKey.GetValue("Name").ToString(),
                            LibraryName = subKey.GetValue("FunctionLibrary").ToString(),
                            VendorName = subKey.GetValue("Vendor").ToString(),
                            ConfigAppPath = subKey.GetValue("ConfigApplication").ToString()
                        };

                        switch (API.DeviceName.ToLower())
                        {
                            case "netbridge":
                                API.DeviceType = DeviceType.Netbridge;

                                break;
                            default:
                                API.DeviceType = DeviceType.Other;

                                break;
                        }

                        J2534_APIs.Add(API);
                        subKey.Close();
                    }
                }
                key.Close();
            }
            return J2534_APIs.Count;
        }

        public static void SetArrays()
        {
            for (int i = 0; i <= J2534Constants.MAX_FILTER; i++)
            {
                FilterEditBuffer[i] = new J2534Filter();
            }
            for (int i = 0; i <= J2534Constants.MAX_PM; i++)
            {
                PeriodicMessageEditBuffer[i] = new J2534PeriodicMessage();
            }
        }

        public static MessageCompareResults CompareMessages(PASSTHRU_MSG source, PASSTHRU_MSG compare)
        {
            MessageCompareResults result = 0;
            if (source.ProtocolId != compare.ProtocolId)
            {
                result |= MessageCompareResults.ProtocolMismatch;
            }
            if (source.RxStatus != compare.RxStatus)
            {
                result |= MessageCompareResults.RxStatusMismatch;
            }
            if (source.TxFlags != compare.TxFlags)
            {
                result |= MessageCompareResults.TxFlagsMismatch;
            }
            if (source.Timestamp != compare.Timestamp)
            {
                result |= MessageCompareResults.TimestampMismatch;
            }
            if (source.DataSize != compare.DataSize)
            {
                result |= MessageCompareResults.DataSizeMismatch;
            }
            if (source.ExtraDataIndex != compare.ExtraDataIndex)
            {
                result |= MessageCompareResults.ExtraDataMismatch;
            }
            if (source.DataSize != compare.DataSize)
            {
                result |= MessageCompareResults.DataMismatch;
            }
            else if (source.Data == null || compare.Data == null)
            {
                result |= MessageCompareResults.DataMismatch;
            }
            else if (source.Data.Length < 4128 || compare.Data.Length < 4128)
            {
                result |= MessageCompareResults.DataMismatch;
            }
            else
            {
                int n = source.DataSize.ToInt32() - 1;
                if (n > 4127)
                {
                    n = 4127;
                }
                for (int i = 0; i <= n; i++)
                {
                    if (source.Data[i] != compare.Data[i])
                    {
                        result |= MessageCompareResults.DataMismatch;
                        break;
                    }
                }
            }
            return result;
        }

        public static void FlagsKeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox thisTextBox))
            {
                return;
            }
            string unSelected = thisTextBox.Text.Remove(thisTextBox.SelectionStart, thisTextBox.SelectionLength);
            switch (e.KeyChar)
            {
                case 'X':
                case 'x':
                    if (unSelected.IndexOfAny("&HhXx".ToCharArray()) >= 0)
                    {
                        e.Handled = true;
                    }
                    if (!(thisTextBox.Text.StartsWith("0") && thisTextBox.SelectionStart == 1))
                    {
                        e.Handled = true;
                    }

                    break;
                case '&':
                    if (unSelected.Contains('x'.ToString()) || unSelected.Contains('X'.ToString()) || thisTextBox.SelectionStart != 0)
                    {
                        e.Handled = true;
                    }

                    break;
                case 'H':
                case 'h':
                    if (unSelected.IndexOfAny("HhXx".ToCharArray()) >= 0 || !(thisTextBox.Text.StartsWith("&") && thisTextBox.SelectionStart == 1))
                    {
                        e.Handled = true;
                    }
                    break;
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:
                break;
                default:
                    if (!(e.KeyChar.IsHex() || e.KeyChar == ' '))
                    {
                        e.Handled = true;
                    }
                    break;
            }
        }

        public static bool CanMakePeriodicMessage(J2534Device device, string protocolName)
        {
            bool result = false;
            if (device == null)
            {
                return result;
            }
            J2534ProtocolId protocolId = NameToProtocolId(protocolName);
            J2534Channel channel = device.Channel(protocolId);
            if (channel == null)
            {
                return false;
            }
            J2534Channel channelISO = null;
            if (channel.IsCAN && !channel.Connected)
            {
                channelISO = CAN_ChannelToISO_Channel(device, channel);
            }
            if (channel != null && channel.Connected)
            {
                result = true;
            }
            else if (channelISO != null && channelISO.Connected && channelISO.MixedMode)
            {
                result = true;
            }
            return result;
        }

        public static J2534FilterType NameToFilterType(string name)
        {
            switch (name.ToLower())
            {
                case "pass":
                    return J2534FilterType.PASS_FILTER;
                case "block":
                    return J2534FilterType.BLOCK_FILTER;
                case "flow":
                    return J2534FilterType.FLOW_CONTROL_FILTER;
                default:
                    return J2534FilterType.NO_FILTER;
            }
        }

        public static string FilterTypeToName(J2534FilterType filterType)
        {
            switch (filterType)
            {
                case J2534FilterType.PASS_FILTER:
                    return "Pass";
                case J2534FilterType.BLOCK_FILTER:
                    return "Block";
                case J2534FilterType.FLOW_CONTROL_FILTER:
                    return "Flow";
                default:
                    return "None";
            }
        }

        public static bool IsISO15765(J2534ProtocolId protocolId)
        {
            bool result = false;
            if ((protocolId == ISO15765) || 
                (protocolId == ISO15765_PS) || 
                (protocolId == SW_ISO15765_PS) || 
                (protocolId == FT_ISO15765_PS) || 
                (protocolId >= ISO15765_CH1 && protocolId <= ISO15765_CH128) || 
                (protocolId >= SW_CAN_ISO15765_CH1 && protocolId <= SW_CAN_ISO15765_CH128) || 
                (protocolId >= FT_ISO15765_CH1 && protocolId <= FT_ISO15765_CH128))
            {
                result = true;
            }
            return result;
        }

        public static bool IsCAN(J2534ProtocolId protocolId)
        {
            bool result = false;
            if ((protocolId == CAN) || 
                (protocolId == CAN_PS) || 
                (protocolId == SW_CAN_PS) || 
                (protocolId == FT_CAN_PS) || 
                (protocolId >= CAN_CH1 && protocolId <= CAN_CH128) || 
                (protocolId >= SW_CAN_CAN_CH1 && protocolId <= SW_CAN_CAN_CH128) || 
                (protocolId >= FT_CAN_CH1 && protocolId <= FT_CAN_CH128))
            {
                result = true;
            }
            return result;
        }

        public static bool IsJ1708(J2534ProtocolId protocolId)
        {
            bool result = false;
            if ((protocolId == J1708_PS) || 
                (protocolId >= J1708_CH1 && protocolId <= J1708_CH128))
            {
                result = true;
            }
            return result;
        }

        public static bool IsJ1939(J2534ProtocolId protocolId)
        {
            bool result = false;
            if ((protocolId == J1939_PS) || 
                (protocolId >= J1939_CH1 && protocolId <= J1939_CH128))
            {
                result = true;
            }
            return result;
        }

        public static bool IsISO9141(J2534ProtocolId protocolId)
        {
            bool result = false;
            if ((protocolId == ISO9141) || 
                (protocolId == ISO9141_PS) || 
                (protocolId >= ISO9141_CH1 && protocolId <= ISO9141_CH128))
            {
                result = true;
            }
            return result;
        }

        public static bool IsISO14230(J2534ProtocolId protocolId)
        {
            bool result = false;
            if ((protocolId == ISO14230) || 
                (protocolId == ISO14230_PS) || 
                (protocolId >= ISO14230_CH1 && protocolId <= ISO14230_CH128))
            {
                result = true;
            }
            return result;
        }

        public static bool IsJ1850VPW(J2534ProtocolId protocolId)
        {
            bool result = false;
            if ((protocolId == J1850VPW) || 
                (protocolId == J1850VPW_PS) || 
                (protocolId >= J1850VPW_CH1 && protocolId <= J1850VPW_CH128))
            {
                result = true;
            }
            return result;
        }

        public static bool IsJ1850PWM(J2534ProtocolId protocolId)
        {
            bool result = false;
            if ((protocolId == J1850PWM) || 
                (protocolId == J1850PWM_PS) || 
                (protocolId >= J1850PWM_CH1 && protocolId <= J1850PWM_CH128))
            {
                result = true;
            }
            return result;
        }

        public static bool IsTP2_0(J2534ProtocolId protocolId)
        {
            bool result = false;
            if ((protocolId == TP2_0_PS) || 
                (protocolId >= TP2_0_CH1 && protocolId <= TP2_0_CH128))
            {
                result = true;
            }
            return result;
        }

        public static bool IsSingleWire(J2534ProtocolId protocolId)
        {
            bool result = false;
            if ((protocolId == SW_CAN_PS) || 
                (protocolId >= SW_CAN_CAN_CH1 && protocolId <= SW_CAN_CAN_CH128) || 
                (protocolId == SW_ISO15765_PS) || 
                (protocolId >= SW_CAN_ISO15765_CH1 && protocolId <= SW_CAN_ISO15765_CH128))
            {
                result = true;
            }
            return result;
        }

        public static bool IsFaultTolerant(J2534ProtocolId protocolId)
        {
            bool result = false;
            if ((protocolId == FT_CAN_PS) || 
                (protocolId >= FT_CAN_CH1 && protocolId <= FT_CAN_CH128) || 
                (protocolId == FT_ISO15765_PS) || 
                (protocolId >= FT_ISO15765_CH1 && protocolId <= FT_ISO15765_CH128))
            {
                result = true;
            }
            return result;
        }

        public static bool IsPinSwitched(J2534ProtocolId protocolId)
        {
            bool result = false;
            if (protocolId >= J1850VPW_PS && protocolId <= FT_ISO15765_PS)
            {
                result = true;
            }
            return result;
        }

        public static bool IsCHx(J2534ProtocolId protocolId)
        {
            bool result = false;
            if (protocolId >= CAN_CH1 && protocolId <= TP2_0_CH128)
            {
                result = true;
            }
            return result;
        }

        public static bool IsAnalog(J2534ProtocolId protocolId)
        {
            bool result = false;
            if (protocolId >= ANALOG_IN_1 && protocolId <= ANALOG_IN_32)
            {
                result = true;
            }
            return result;
        }

        public static void ProcessAnalogMessage(J2534Device device, J2534Channel channel, PASSTHRU_MSG message)
        {
            if (channel == null)
            {
                return;
            }
            GetConfigParameter(device, channel, ACTIVE_CHANNELS);
            GetConfigParameter(device, channel, INPUT_RANGE_LOW);
            GetConfigParameter(device, channel, INPUT_RANGE_HIGH);
            GetConfigParameter(device, channel, READINGS_PER_MSG);

            int openChannels = PublicCode.PopCount(channel.AnalogSubsystem.ActiveChannels);
            int iterations = (int)channel.AnalogSubsystem.ReadingsPerMessage;
            if (openChannels * iterations > 1032)
            {
                iterations = 1032 / openChannels;
            }
            int u = 0;
            //the outer 'j' loop doesn't do anything, but it's how you would get all the messages
            //with READINGS_PER_MESSAGE set higher than 1 if you needed them
            for (int j = 1; j <= iterations; j++)
            {
                for (int i = 0; i <= 31; i++)
                {
                    if ((channel.AnalogSubsystem.ActiveChannels & 1L << i) != 0)
                    {
                        channel.AnalogSubsystem.AnalogIn[i] = BitConverter.ToInt32(message.Data, u);
                        u += 4;
                    }
                }
            }
        }

        public static J2534MessageText ToMessageText(this PASSTHRU_MSG target)
        {
            J2534MessageText result = new J2534MessageText();
            StringBuilder dataBuilder = new StringBuilder();
            StringBuilder ExtraDataBuilder = new StringBuilder();
            StringBuilder AsciiBuilder = new StringBuilder();

            result.ProtocolName = ProtocolIdToName(target.ProtocolId);
            result.RxStatus = FlagsToText(target.RxStatus);
            result.TxFlags = FlagsToText(target.TxFlags);
            result.Timestamp = TimestampToString(target.Timestamp);
            int size = (int)(target.DataSize - 1);
            for (int i = 0; i <= size; i++)
            {
                byte c = target.Data[i];
                string hex = c.ToString("X2");
                if (i < target.ExtraDataIndex)
                {
                    dataBuilder.Append(hex);
                    dataBuilder.Append(" ");
                    if (c > 31 && c < 127)
                    {
                        AsciiBuilder.Append("'");
                        AsciiBuilder.Append(Microsoft.VisualBasic.Strings.Chr(c));
                        AsciiBuilder.Append("' ");
                    }
                    else
                    {
                        AsciiBuilder.Append(hex);
                        AsciiBuilder.Append(" ");
                    }
                }
                else
                {
                    ExtraDataBuilder.Append(hex);
                    ExtraDataBuilder.Append(" ");
                }
            }
            result.Data = dataBuilder.ToString().TrimEnd();
            result.ExtraData = ExtraDataBuilder.ToString().TrimEnd();
            result.DataASCII = AsciiBuilder.ToString().TrimEnd();
            result.Tx = (target.RxStatus & J2534RxStatusFlags.TX_MSG_TYPE) != 0;
            return result;
        }

        public static PASSTHRU_MSG ToMessage(this J2534MessageText target)
        {
            if (target == null)
            {
                return new PASSTHRU_MSG();
            }
            PASSTHRU_MSG result = PASSTHRU_MSG.CreateInstance();
            if (string.IsNullOrWhiteSpace(target.ProtocolName))
            {
                result.ProtocolId = 0;
            }
            else
            {
                result.ProtocolId = NameToProtocolId(target.ProtocolName);
            }
            result.TxFlags = (J2534TxFlags)TextToFlags(target.TxFlags);
            string msgData = PublicCode.StripSpaces(target.Data);
            msgData = msgData.Replace("[", "");
            msgData = msgData.Replace("]", "");
            int dataIndex = 0;
            byte dataByte = 0;
            for (int i = 0; i < msgData.Length; i += 2)
            {
                string hexPair = msgData.Substring(i, Math.Min(2, msgData.Length - i));
                byte.TryParse(hexPair, NumberStyles.HexNumber, null, out dataByte);
                result.Data[dataIndex] = dataByte;
                dataIndex++;
            }
            result.DataSize = dataIndex.ToUint32();
            result.ExtraDataIndex = result.DataSize;
            return result;
        }

        public static string FlagsToText(J2534ConnectFlags flags)
        {
            return FlagsToText((uint)flags);
        }
        public static string FlagsToText(J2534RxStatusFlags flags)
        {
            return FlagsToText((uint)flags);
        }
        public static string FlagsToText(J2534TxFlags flags)
        {
            return FlagsToText((uint)flags);
        }
        public static string FlagsToText(uint flags)
        {
            return string.Concat("0x", flags.ToString("X8"));
        }

        public static uint TextToFlags(string text)
        {
            uint result = 0;
            if (string.IsNullOrWhiteSpace(text))
            {
                result = 0;
            }
            else
            {
                if (text.StartsWith("&h", PublicDeclarations.IgnoreCase) || text.StartsWith("0x", PublicDeclarations.IgnoreCase))
                {
                    text = text.Substring(2);
                }
                if (text.StartsWith("&"))
                {
                    text = text.Substring(1);
                }
                uint.TryParse(text, NumberStyles.HexNumber, null, out result);
            }
            return result;
        }

        public static PASSTHRU_MSG GetMessageMask(PASSTHRU_MSG message)
        {
            PASSTHRU_MSG result = message.Clone();
            int size = (int)(message.DataSize - 1);
            for (int i = 0; i <= size; i++)
            {
                result.Data[i] = byte.MaxValue;
            }
            return result;
        }

        public static string TimestampToString(uint timestamp)
        {
            uint uSec = timestamp % 1000000U;
            uint sec = timestamp / 1000000U % 60U;
            uint min = timestamp / 60000000U;
            return string.Concat(min.ToString("00"), ":", sec.ToString("00"), ".", uSec.ToString("000000"));
        }

        public static string ProtocolIdToName(J2534ProtocolId protocolId)
        {
            string result = "none";
            bool isCHxId = IsCHx(protocolId);
            bool isAnalogId = IsAnalog(protocolId);
            J2534ProtocolId channel = protocolId & (J2534ProtocolId)0x7FU;
            J2534ProtocolId rootProtocolId = (isCHxId || isAnalogId) ? protocolId & (J2534ProtocolId)0xFF80U: protocolId;
            if (Enum.IsDefined(typeof(J2534ProtocolId), rootProtocolId))
            {
                result = rootProtocolId.ToString(); 
            }
            if (isCHxId)
            {
                result = result.Replace("_CH1", string.Concat("_CH", (uint)(channel + 1)));
            }
            else if (isAnalogId)
            {
                result = result.Replace("_IN_1", string.Concat("_IN_", (uint)(channel + 1)));
            }
            return result;
        }

        public static J2534ProtocolId ProtocolIdToChannelNo(J2534ProtocolId protocolId)
        {
            J2534ProtocolId result = 0;
            if (IsCHx(protocolId) || IsAnalog(protocolId))
            {
                result = (protocolId & (J2534ProtocolId)0x7FU) + 1U;
            }
            return result;
        }

        public static J2534ProtocolId NameToProtocolId(string protocolName)
        {
            J2534ProtocolId result = 0;
            if (string.IsNullOrWhiteSpace(protocolName))
            {
                return result;
            }
            uint offset = uint.MaxValue;
            string lookup = string.Empty;
            int chLoc = protocolName.LastIndexOf("_CH");
            int inLoc = protocolName.LastIndexOf("_IN_");
            if (chLoc > 0)
            {
                lookup = string.Concat(protocolName.Substring(0, chLoc + 3), "1");
                uint.TryParse(protocolName.Substring(chLoc + 3), out offset);
                if (offset < J2534Constants.MIN_CH || offset > J2534Constants.MAX_CH)
                {
                    return result;
                }
                offset --;
            }
            else if (inLoc > 0)
            {
                lookup = string.Concat(protocolName.Substring(0, inLoc + 4), "1");
                uint.TryParse(protocolName.Substring(inLoc + 4), out offset);
                if (offset < J2534Constants.MIN_CH || offset > J2534Constants.MAX_IN)
                {
                    return result;
                }
                offset --;
            }
            else
            {
                lookup = protocolName;
                offset = 0;
            }

            if (Enum.TryParse(lookup, true, out J2534ProtocolId test))
            {
                result = test + offset;
            }


            return result;
        }

        public static J2534Channel CAN_ChannelToISO_Channel(J2534Device device, J2534Channel channel)
        {
            J2534Channel result = null;
            if (device == null || channel == null)
            {
                return result;
            }
            if (channel.ProtocolId == CAN)
            {
                result = device.Channel(ISO15765);
            }
            else if (channel.ProtocolId == CAN_PS)
            {
                result = device.Channel(ISO15765_PS);
            }
            else if (channel.ProtocolId == SW_CAN_PS)
            {
                result = device.Channel(SW_ISO15765_PS);
            }
            else if (channel.ProtocolId == FT_CAN_PS)
            {
                result = device.Channel(FT_ISO15765_PS);
            }
            else if (channel.ProtocolId >= CAN_CH1 && channel.ProtocolId <= CAN_CH128)
            {
                result = device.Channel((uint)ISO15765_CH1 + (uint)channel.ProtocolId - CAN_CH1);
            }
            else if (channel.ProtocolId >= SW_CAN_CAN_CH1 && channel.ProtocolId <= SW_CAN_CAN_CH128)
            {
                result = device.Channel((uint)SW_CAN_ISO15765_CH1 + (uint)channel.ProtocolId - SW_CAN_CAN_CH1);
            }
            else if (channel.ProtocolId >= FT_CAN_CH1 && channel.ProtocolId <= FT_CAN_CH128)
            {
                result = device.Channel((uint)FT_ISO15765_CH1 + (uint)channel.ProtocolId - FT_CAN_CH1);
            }

            return result;
        }

        public static J2534Channel ISO_ChannelToCAN_Channel(J2534Device device, J2534Channel channel)
        {
            J2534Channel result = null;
            if (device == null || channel == null)
            {
                return result;
            }

            if (channel.ProtocolId == ISO15765)
            {
                result = device.Channel(CAN);
            }
            else if (channel.ProtocolId == ISO15765_PS)
            {
                result = device.Channel(CAN_PS);
            }
            else if (channel.ProtocolId == SW_ISO15765_PS)
            {
                result = device.Channel(SW_CAN_PS);
            }
            else if (channel.ProtocolId == FT_ISO15765_PS)
            {
                result = device.Channel(FT_CAN_PS);
            }
            else if (channel.ProtocolId >= ISO15765_CH1 && channel.ProtocolId <= ISO15765_CH128)
            {
                result = device.Channel((uint)CAN_CH1 + (uint)channel.ProtocolId - ISO15765_CH1);
            }
            else if (channel.ProtocolId >= SW_CAN_ISO15765_CH1 && channel.ProtocolId <= SW_CAN_ISO15765_CH128)
            {
                result = device.Channel((uint)SW_CAN_CAN_CH1 + (uint)channel.ProtocolId - SW_CAN_ISO15765_CH1);
            }
            else if (channel.ProtocolId >= FT_ISO15765_CH1 && channel.ProtocolId <= FT_ISO15765_CH128)
            {
                result = device.Channel((uint)FT_CAN_CH1 + (uint)channel.ProtocolId - FT_ISO15765_CH1);
            }
            return result;
        }

        public static J2534ProtocolId CAN_IdToISO_Id(J2534ProtocolId protocolId)
        {
            J2534ProtocolId result = 0;
            if (protocolId == CAN)
            {
                result = ISO15765;
            }
            else if (protocolId == CAN_PS)
            {
                result = ISO15765_PS;
            }
            else if (protocolId == SW_CAN_PS)
            {
                result = SW_ISO15765_PS;
            }
            else if (protocolId == FT_CAN_PS)
            {
                result = FT_ISO15765_PS;
            }
            else if (protocolId >= CAN_CH1 && protocolId <= CAN_CH128)
            {
                result = (uint)ISO15765_CH1 + (uint)protocolId - CAN_CH1;
            }
            else if (protocolId >= SW_CAN_CAN_CH1 && protocolId <= SW_CAN_CAN_CH128)
            {
                result = (uint)SW_CAN_ISO15765_CH1 + (uint)protocolId - SW_CAN_CAN_CH1;
            }
            else if (protocolId >= FT_ISO15765_CH1 && protocolId <= FT_ISO15765_CH128)
            {
                result = (uint)FT_ISO15765_CH1 + (uint)protocolId - FT_CAN_CH1;
            }
            return result;
        }

        public static J2534ProtocolId ISO_IdToCAN_Id(J2534ProtocolId protocolId)
        {
            J2534ProtocolId result = 0;
            if (protocolId == ISO15765)
            {
                result = CAN;
            }
            else if (protocolId == ISO15765_PS)
            {
                result = CAN_PS;
            }
            else if (protocolId == SW_ISO15765_PS)
            {
                result = SW_CAN_PS;
            }
            else if (protocolId == FT_ISO15765_PS)
            {
                result = FT_CAN_PS;
            }
            else if (protocolId >= ISO15765_CH1 && protocolId <= ISO15765_CH128)
            {
                result = (uint)CAN_CH1 + (uint)protocolId - ISO15765_CH1;
            }
            else if (protocolId >= SW_CAN_ISO15765_CH1 && protocolId <= SW_CAN_ISO15765_CH128)
            {
                result = (uint)SW_CAN_CAN_CH1 + (uint)protocolId - SW_CAN_ISO15765_CH1;
            }
            else if (protocolId >= FT_ISO15765_CH1 && protocolId <= FT_ISO15765_CH128)
            {
                result = (uint)FT_CAN_CH1 + (uint)protocolId - FT_ISO15765_CH1;
            }
            return result;
        }

        public static J2534Device NameToDevice(string name)
        {
            J2534Device result = null;
            foreach (KeyValuePair<int, J2534Device> Box in J2534Devices)
            {
                if (Box.Value.Name == name)
                {
                    result = Box.Value;
                    break;
                }
            }
            return result;
        }

        public static void FixFilterMask(PASSTHRU_MSG mask, PASSTHRU_MSG pattern)
        {
            if (mask.DataSize == 0 && pattern.DataSize > 0)
            {
                mask = GetMessageMask(pattern);
            }
            int n = (int)mask.DataSize - 1;
            for (int i = (int)pattern.DataSize; i <= n; i++)
            {
                pattern.Data[i] = 0;
            }
            for (int i = 0; i <= n; i++)
            {
                pattern.Data[i] &= mask.Data[i];
            }
            n = (int)pattern.DataSize - 1;
            for (int i = (int)mask.DataSize; i <= n; i++)
            {
                pattern.Data[i] = 0;
            }
            pattern.DataSize = mask.DataSize;
            pattern.ExtraDataIndex = pattern.DataSize;
        }

        public static void DisplayMessage(J2534Device device, J2534MessageText messageText)
        {
            if (device == null)
            {
                return;
            }
            string data = messageText.Data;
            if ((messageText?.ExtraData?.Length ?? 0) > 0)
            {
                if ((data?.Length ?? 0) > 0)
                {
                    data = string.Concat(data, " ");
                }
                data = string.Concat(data, "[", messageText.ExtraData, "]");
            }

            ListViewItem item = device.ListViewMessageIn.Items.Add(messageText.Timestamp);
            if (messageText.Tx)
            {
                item.ImageIndex = 1; //out
                if (device.MessageOutCount == ulong.MaxValue)
                {
                    device.MessageOutCount = 0;
                }
                device.MessageOutCount += 1U;
            }
            else
            {
                item.ImageIndex = 0; //in
                if (device.MessageInCount == ulong.MaxValue)
                {
                    device.MessageInCount = 0;
                }
                device.MessageInCount += 1U;
            }

            item.SubItems.Add(messageText.ProtocolName);
            item.SubItems.Add(messageText.RxStatus);
            item.SubItems.Add(data);
            if (!string.IsNullOrWhiteSpace(messageText.DataASCII))
            {
                item.SubItems[3].Tag = messageText.DataASCII;
            }
            if (device.ListViewMessageIn.Items.Count > 10000)
            {
                device.ListViewMessageIn.Items.RemoveAt(0);
            }
            if (device.ListViewMessageOut.Items.Count > 10000)
            {
                device.ListViewMessageOut.Items.RemoveAt(0);
            }

        }

        public static J2534Device GetListViewParentDevice(ListView thisListView)
        {
            foreach (KeyValuePair<int, J2534Device> kvp in J2534Devices)
            {
                if (thisListView == kvp.Value.ListViewMessageIn || thisListView == kvp.Value.ListViewMessageOut)
                {
                    return kvp.Value;
                }
            }
            return null;
        }

        public static J2534Device GetTimerParentDevice(Timer thisTimer)
        {
            foreach (KeyValuePair<int, J2534Device> kvp in J2534Devices)
            {
                if (thisTimer == kvp.Value.TimerMessageReceive)
                {
                    return kvp.Value;
                }
            }
            return null;
        }

        public static string GetVersionNumericPart(string value)
        {
            string result = string.Empty;
            int n = value?.Length ?? 0;
            for (int i = 0; i < n; i++)
            {
                char c = value[i];
                if (c == '.' || char.IsDigit(c)) //this is a kludge for a box that returns a story about its FW version
                {
                    result = string.Concat(result, c);
                }
                else
                {
                    break;
                }
            }
            string[] versions = result.Split('.');
            result = string.Empty;
            double d = 0;
            foreach (string version in versions)
            {
                double.TryParse(version, out d);
                result = string.Concat(result, d, ".");
            }
            return result.TrimEnd('.');
        }

        public static bool IsSendable(J2534Device device, J2534Channel channel)
        {
            if (channel == null)
            {
                return false;
            }
            if (channel.Connected)
            {
                return true;
            }
            if (channel.IsCAN)
            {
                J2534Channel channelISO = CAN_ChannelToISO_Channel(device, channel);
                if (channelISO != null && channelISO.MixedMode)
                {
                    return true;
                }
            }
            return false;
        }

        public static J2534Result SetConfigParameter(J2534Device device, J2534Channel channel, J2534ConfigParameterId parameterId, uint value)
        {
            J2534Result result = (J2534Result)(-1);
            if (device?.API?.PassThruIoctl == null || channel == null)
            {
                return result;
            }
            int parameterIndex = channel.ParameterIndex(parameterId);
            if (parameterIndex < 0)
            {
                return result;
            }
            SCONFIG config = new SCONFIG();
            SCONFIG_LIST configList = new SCONFIG_LIST();
            config.Parameter = (uint)parameterId;
            config.Value = value;
            configList.NumOfParams = 1;

            IntPtr configPointer = Marshal.AllocHGlobal(Marshal.SizeOf(config));
            Marshal.StructureToPtr(config, configPointer, true);
            configList.ConfigPtr = configPointer.ToInt32();
            IntPtr configListPointer = Marshal.AllocHGlobal(Marshal.SizeOf(configList));
            Marshal.StructureToPtr(configList, configListPointer, true);
            result = device.API.PassThruIoctl(channel.ChannelId, SET_CONFIG, configListPointer.ToInt32(), 0);
            config = (SCONFIG)Marshal.PtrToStructure(configPointer, typeof(SCONFIG));
            Marshal.FreeHGlobal(configListPointer);
            Marshal.FreeHGlobal(configPointer);
            if (result == J2534Result.STATUS_NOERROR && parameterId == CAN_MIXED_FORMAT && channel.IsISO15765)
            {
                channel.MixedMode = config.Value != 0;
                ClearAllFilters(device, channel);
            }

            return result;
        }

        public static J2534Result ClearAllFilters(J2534Device device, J2534Channel channel)
        {
            if (device?.API?.PassThruIoctl == null || channel == null)
            {
                return (J2534Result)(-1);
            }
            J2534Result result = device.API.PassThruIoctl(channel.ChannelId, CLEAR_MSG_FILTERS, 0, 0);
            FormJ2534.DefaultInstance.ShowResult(device, result, "PassThruIoctl - CLEAR_MSG_FILTERS");
            for (int i = 0; i <= J2534Constants.MAX_FILTER; i++)
            {
                channel.Filter[i].FilterType = 0;
                channel.Filter[i].MaskMessage.Clear();
                channel.Filter[i].PatternMessage.Clear();
                channel.Filter[i].FlowMessage.Clear();
                channel.Filter[i].FlowMessage.TxFlags = channel.DefaultTxFlags;
                channel.Filter[i].MessageId = 0;
                channel.Filter[i].Enabled = false;
            }
            FormJ2534.DefaultInstance.SetFilterControls();
            FormJ2534.DefaultInstance.UpdateFilterEdit();
            return result;
        }

        public static J2534Result ClearAllPeriodicMessages(J2534Device device, J2534Channel channel)
        {
            if (device?.API?.PassThruIoctl == null || channel == null)
            {
                return (J2534Result)(-1);
            }
            J2534Result result = device.API.PassThruIoctl(channel.ChannelId, CLEAR_PERIODIC_MSGS, 0, 0);
            FormJ2534.DefaultInstance.ShowResult(device, result, "PassThruIoctl - CLEAR_PERIODIC_MSGS");
            for (int i = 0; i <= J2534Constants.MAX_PM; i++)
            {
                channel.PeriodicMessage[i].Message.Clear();
                channel.PeriodicMessage[i].Message.TxFlags = channel.DefaultTxFlags;
                channel.PeriodicMessage[i].Interval = 100;
                channel.PeriodicMessage[i].MessageId = 0;
                channel.PeriodicMessage[i].Enabled = false;
            }
            FormJ2534.DefaultInstance.SetPeriodicMessageControls(device);
            FormJ2534.DefaultInstance.UpdatePeriodicMessageEdit();
            return result;
        }

        public static J2534Result ReadBatteryVoltage(J2534Device device)
        {
            if (device?.API?.PassThruIoctl == null || !device.Connected)
            {
                return (J2534Result)(-1);
            }
            GCHandle gh = GCHandle.Alloc(device.Analog.BatteryReading, GCHandleType.Pinned);
            IntPtr readingPointer = gh.AddrOfPinnedObject();
            J2534Result result = device.API.PassThruIoctl(device.DeviceId, READ_VBATT, 0, readingPointer.ToInt32());
            device.Analog.BatteryReading = (uint)gh.Target;
            gh.Free();
            return result;
        }

        public static J2534Result ReadProgrammingVoltage(J2534Device device)
        {
            if (device?.API?.PassThruIoctl == null || !device.Connected)
            {
                return (J2534Result)(-1);
            }
            GCHandle gh = GCHandle.Alloc(device.Analog.PinReading, GCHandleType.Pinned);
            IntPtr readingPointer = gh.AddrOfPinnedObject();
            J2534Result result = device.API.PassThruIoctl(device.DeviceId, READ_PROG_VOLTAGE, 0, readingPointer.ToInt32());
            device.Analog.PinReading = (uint)gh.Target;
            gh.Free();
            return result;
        }

        public static J2534Result GetConfigParameter(J2534Device device, J2534Channel channel, J2534ConfigParameterId parameterId)
        {
            if (device?.API?.PassThruIoctl == null || channel == null)
            {
                return (J2534Result)(-1);
            }
            int parameterIndex = channel.ParameterIndex(parameterId);
            J2534Result result = (J2534Result)(-1);
            if (parameterIndex < 0)
            {
                return result;
            }
            SCONFIG config = new SCONFIG();
            SCONFIG_LIST configList = new SCONFIG_LIST();
            config.Parameter = (uint)parameterId;
            configList.NumOfParams = 1;

            IntPtr configPointer = Marshal.AllocHGlobal(Marshal.SizeOf(config));
            Marshal.StructureToPtr(config, configPointer, true);
            configList.ConfigPtr = configPointer.ToInt32();
            IntPtr configListPointer = Marshal.AllocHGlobal(Marshal.SizeOf(configList));
            Marshal.StructureToPtr(configList, configListPointer, true);
            result = device.API.PassThruIoctl(channel.ChannelId, GET_CONFIG, configListPointer.ToInt32(), 0);
            config = (SCONFIG)Marshal.PtrToStructure(configPointer, typeof(SCONFIG));
            Marshal.FreeHGlobal(configListPointer);
            Marshal.FreeHGlobal(configPointer);
            if (result == J2534Result.STATUS_NOERROR && parameterId == CAN_MIXED_FORMAT && channel.IsISO15765)
            {
                channel.MixedMode = config.Value != 0;
            }
            if (result == J2534Result.STATUS_NOERROR)
            {
                channel.ConfParameter[parameterIndex].Value = config.Value;
                switch ((J2534ConfigParameterId)config.Parameter)
                {
                    case DATA_RATE:
                        channel.BaudRate = config.Value;

                        break;
                    case ACTIVE_CHANNELS:
                        channel.AnalogSubsystem.ActiveChannels = config.Value;

                        break;
                    case INPUT_RANGE_LOW:
                        channel.AnalogSubsystem.RangeLow = config.Value.ToInt32();

                        break;
                    case INPUT_RANGE_HIGH:
                        channel.AnalogSubsystem.RangeHigh = config.Value.ToInt32();

                        break;
                    case READINGS_PER_MSG:
                        channel.AnalogSubsystem.ReadingsPerMessage = config.Value;

                        break;
                }
            }

            return result;
        }

        public static byte[] PrepFunctionalAddressArray(byte[] values)
        {
            if (values == null)
            {
                return new byte[0];
            }
            List<byte> result = new List<byte> {Capacity = 64};
            foreach (byte value in values)
            {
                if (value != 0 && !result.Contains(value))
                {
                    result.Add(value);
                }
            }
            return result.ToArray();
        }

        public static byte[] BuildJ1939Name(J1939AddressClaimSetup setup)
        {
            if (setup == null)
            {
                return new byte[0];
            }
            uint mfgCodeId = 0;
            byte[] result = new byte[9];
            result[0] = setup.Address;
            mfgCodeId = setup.MfgCode << 21;
            mfgCodeId |= setup.Identity & J1939AddressClaimSetup.J1939_NAME_MASK_IDENTITY;
            Array.Copy(BitConverter.GetBytes(mfgCodeId), 0, result, 1, 4);
            result[5] = (byte)(setup.FunctionInstance << 3);
            result[5] |= (byte)(setup.ECUInstance & J1939AddressClaimSetup.J1939_NAME_MASK_ECU_INST);
            result[6] = setup.FunctionId;
            result[7] = (byte)(setup.VehicleSystemId << 1);
            result[8] = (byte)(setup.IndustryGroup << 4 & J1939AddressClaimSetup.J1939_NAME_MASK_IND_GRP);
            result[8] |= (byte)(setup.VehicleSystemInstance & J1939AddressClaimSetup.J1939_NAME_MASK_VEH_SYS_INST);
            if (setup.ArbitraryCapable)
            {
                result[8] |= J1939AddressClaimSetup.J1939_NAME_MASK_ARB;
            }
            return result;
        }

        public static J2534Result ClaimJ1939Address(J2534Device device, J2534Channel channel, J1939AddressClaimSetup setup)
        {
            J2534Result result = (J2534Result)(-1);
            byte[] bytes = BuildJ1939Name(setup);
            if (device?.API?.PassThruIoctl == null || channel == null)
            {
                return result;
            }
            SBYTE_ARRAY inputMsg = new SBYTE_ARRAY
            {
                NumOfBytes = 9
            };
            IntPtr bytesPointer = Marshal.AllocHGlobal(Marshal.SizeOf(bytes[0]) * bytes.Length);
            Marshal.Copy(bytes, 0, bytesPointer, bytes.Length);
            inputMsg.BytePtr = bytesPointer.ToInt32();
            IntPtr inputMsgPointer = Marshal.AllocHGlobal(Marshal.SizeOf(inputMsg));
            Marshal.StructureToPtr(inputMsg, inputMsgPointer, true);
            result = device.API.PassThruIoctl(channel.ChannelId, PROTECT_J1939_ADDR, inputMsgPointer.ToInt32(), 0);
            Marshal.FreeHGlobal(bytesPointer);
            Marshal.FreeHGlobal(inputMsgPointer);
            FormJ2534.DefaultInstance.ShowResult(device, result, "PassThruIoctl - PROTECT_J1939_ADDR");
            return result;
        }
    }
}