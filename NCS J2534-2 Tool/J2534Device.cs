﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Text;
using static NCS_J2534_2_Tool.J2534ProtocolId;
using static NCS_J2534_2_Tool.J2534IoCtlId;
using static NCS_J2534_2_Tool.J2534ConfigParameterId;
using static NCS_J2534_2_Tool.J2534GetDeviceInfoParameter;
using static NCS_J2534_2_Tool.J2534GetProtocolInfoParameter;

namespace NCS_J2534_2_Tool
{
    public sealed class J2534Device : IDisposable
    {
        public J2534Channel[] ChannelSet {get; set;} = {};
        public J2534Setup Setup {get; set;} = new J2534Setup {MessageReadRate = 10, TextBoxOutFlags = "0x00000000", TextBoxMessageOut = "00 00"};
        public J2534Analog Analog {get; set;} = new J2534Analog();
        public J2534_API API {get; set;}
        public string Name {get; set;}
        public uint DeviceId { get; private set; }
        public string Version {get; set;}
        public string Description {get; set;}
        public bool Connected {get; set;}
        public bool Online {get; set;}
        public ulong MessageInCount {get; set;}
        public ulong MessageOutCount {get; set;}
        public uint SerialNo {get; set;}
        public string SupportedProtocols
        {
            get
            {
                StringBuilder result = new StringBuilder();
                int n = ChannelSet.Length - 1;
                for (int index = 0; index <= n; index++)
                {
                    result.Append(ChannelSet[index].Name);
                    J2534ProtocolId protocolId = ChannelSet[index].ProtocolId;
                    J2534ProtocolId first = J2534Code.ProtocolIdToChannelNo(protocolId);
                    if (first > 0)
                    {
                        J2534ProtocolId last = 0;
                        J2534ProtocolId root = protocolId & (J2534ProtocolId)0xFF80U;
                        uint j = 0;
                        int i = index;
                        while (i <= n && (ChannelSet[i].ProtocolId & (J2534ProtocolId)0xFF80U) == root)
                        {
                            last = first + j;
                            j += 1U;
                            i++;
                        }
                        if (last > first)
                        {
                            index = i;
                            result.Append(" to ");
                            result.Append((uint)last);
                        }
                    }
                    if (index < n)
                    {
                        result.Append(", ");
                    }
                }
                return result.ToString();
            }
        }
        public int MaxChannel
        {
            get
            {
                return ChannelSet.Length - 1;
            }
        }
        public J2534Channel Channel(int index)
        {
            if (index >= 0 && index < ChannelSet.Length)
            {
                return ChannelSet[index];
            }
            return null;
        }
        public J2534Channel Channel(J2534ProtocolId protocolId)
        {
            for (int index = 0; index <= MaxChannel; index++)
            {
                if (ChannelSet[index].ProtocolId == protocolId)
                {
                    return ChannelSet[index];
                }
            }
            return null;
        }
        public J2534Channel Channel(string name)
        {
            J2534Channel result = null;
            for (int index = 0; index <= MaxChannel; index++)
            {
                if (ChannelSet[index].Name == name)
                {
                    return ChannelSet[index];
                }
            }
            return result;
        }
        public int ChannelIndex(J2534ProtocolId protocolId)
        {
            for (int index = 0; index <= MaxChannel; index++)
            {
                if (ChannelSet[index].ProtocolId == protocolId)
                {
                    return index;
                }
            }
            return -1;
        }
        public int ChannelIndex(string name)
        {
            for (int index = 0; index <= MaxChannel; index++)
            {
                if (ChannelSet[index].Name == name)
                {
                    return index;
                }
            }
            return -1;
        }
        public int ChannelIndex(J2534Channel channel)
        {
            for (int index = 0; index <= MaxChannel; index++)
            {
                if (ChannelSet[index] == channel)
                {
                    return index;
                }
            }
            return -1;
        }
        public bool HasChannel(string name)
        {
            for (int i = 0; i <= MaxChannel; i++)
            {
                if (ChannelSet[i].Name == name)
                {
                    return true;
                }
            }
            return false;
        }
        public bool HasChannel(J2534ProtocolId protocolId)
        {
            for (int i = 0; i <= MaxChannel; i++)
            {
                if (ChannelSet[i].ProtocolId == protocolId)
                {
                    return true;
                }
            }
            return false;
        }

        public ListView ListViewMessageIn = new ListView();
        public ListView ListViewMessageOut = new ListView();
        public Timer TimerMessageReceive = new Timer();

        public void SetDeviceId(uint value)
        {
            this.DeviceId = value;
        }

        private void BuildListViewObjects()
        {
            ListViewMessageIn.Font = new Font("Courier New", 9);
            ListViewMessageIn.Name = "ListViewMessageIn";
            ListViewMessageIn.UseCompatibleStateImageBehavior = false;
            ListViewMessageIn.View = View.Details;
            ListViewMessageIn.FullRowSelect = true;
            ListViewMessageIn.HideSelection = false;
            ListViewMessageIn.SmallImageList = new ImageList();
            ListViewMessageIn.SmallImageList.Images.Add(Properties.Resources.inArrow);
            ListViewMessageIn.SmallImageList.Images.Add(Properties.Resources.outArrow);
            if (ListViewMessageIn.Columns.Count == 0)
            {
                ListViewMessageIn.Columns.Add("Timestamp", "Timestamp", 120);
                ListViewMessageIn.Columns.Add("Network ID", "Network ID", 103);
                ListViewMessageIn.Columns.Add("Rx Status", "Rx Status", 82);
                ListViewMessageIn.Columns.Add("Data Bytes", "Data Bytes");
            }

            ListViewMessageOut.Font = new Font("Courier New", 9);
            ListViewMessageOut.Name = "ListViewMessageOut";
            ListViewMessageOut.UseCompatibleStateImageBehavior = false;
            ListViewMessageOut.View = View.Details;
            ListViewMessageOut.FullRowSelect = true;
            ListViewMessageOut.HideSelection = false;
            ListViewMessageOut.MultiSelect = false;
            if (ListViewMessageOut.Columns.Count == 0)
            {
                ListViewMessageOut.Columns.Add("Network ID", "Network ID", 103);
                ListViewMessageOut.Columns.Add("Tx Flags", "Tx Flags", 82);
                ListViewMessageOut.Columns.Add("Data Bytes", "Data Bytes");
                ListViewMessageOut.Columns.Add("Comments", "Comments");
            }
        }

        public void BuildChannels()
        {
            BuildChannels(this);
        }
        private static void BuildChannels(J2534Device device)
        {
            if (device == null)
            {
                return;
            }
            List<J2534Channel> ChannelSet = new List<J2534Channel>();

            SPARAM deviceInfo = GetDeviceInfo(device, SERIAL_NUMBER);
            if (deviceInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
            {
                device.SerialNo = deviceInfo.Value;
            }

            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.J1850VPW));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.J1850PWM));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.ISO9141));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.ISO14230));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.CAN));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SW_CAN));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.FT_CAN));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.ISO15765));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SW_ISO15765));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.FT_ISO15765));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SCI_A_ENGINE));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SCI_A_TRANS));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SCI_B_ENGINE));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.SCI_B_TRANS));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.J2610));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.GM_UART));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.ECHO_BYTE));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.HONDA_DIAGH));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.J1939));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.J1708));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.TP2_0));
            ChannelSet.AddRange(GetProtocolSupport(device, ChannelSetup.Analog));

            if (device.API.DeviceType == DeviceType.Netbridge)
            {
                J2534Channel channel = new J2534Channel(LIN);
                GetChannelInfo(device, ref channel);
                ChannelSet.Add(channel);
            }

            device.ChannelSet = ChannelSet.ToArray();
            device.Analog.BatteryReading = 0;
            device.Analog.PinReading = 0;
            device.Analog.PinSetting = 5000;
            for (int i = 0; i <= 15; i++)
            {
                device.Analog.Pin[i] = new PinInfo
                {
                    Mode = PinMode.Open,
                    Selected = false,
                    CanBeSet = false,
                    CanBeShorted = false
                };
            }
            GetProgVoltagePinInfo(device);

        }

        private static J2534Channel[] GetProtocolSupport(J2534Device device, ChannelSetupParameters builder)
        {
            List<J2534Channel> result = new List<J2534Channel>();
            if (device == null)
            {
                return new J2534Channel[0];
            }

            SPARAM deviceInfo = GetDeviceInfo(device, builder.SupportParameter);
            if (deviceInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
            {
                if (deviceInfo.SS > 0 && builder.BaseId.HasValue) //Base Channel
                {
                    J2534Channel channel = new J2534Channel(builder.BaseId.Value);
                    GetChannelInfo(device, ref channel);
                    result.Add(channel);
                }

                if (deviceInfo.RR > 0 && builder.PinSwitchedId.HasValue) //Pin Switched Channels
                {
                    J2534Channel channel = new J2534Channel(builder.PinSwitchedId.Value);
                    List<J2534ConfigParameter> parameters = new List<J2534ConfigParameter>();
                    parameters.AddRange(channel.ConfParameter);
                    if (builder.J1962PinsParameter.HasValue)
                    {
                        channel.J1962Pins = GetPinInfo(device, builder.J1962PinsParameter.Value);
                        if (channel.J1962Pins.Length > 0)
                        {
                            parameters.Add(new J2534ConfigParameter(J1962_PINS));
                        }
                    }
                    if (builder.J1939PinsParameter.HasValue)
                    {
                        channel.J1939Pins = GetPinInfo(device, builder.J1939PinsParameter.Value);
                        if (channel.J1939Pins.Length > 0)
                        {
                            parameters.Add(new J2534ConfigParameter(J1939_PINS));
                        }
                    }
                    if (builder.J1708PinsParameter.HasValue)
                    {
                        channel.J1708Pins = GetPinInfo(device, builder.J1708PinsParameter.Value);
                        if (channel.J1708Pins.Length > 0)
                        {
                            parameters.Add(new J2534ConfigParameter(J1708_PINS));
                        }
                    }
                    channel.ConfParameter = parameters.ToArray();
                    GetChannelInfo(device, ref channel);
                    result.Add(channel);
                }

                int maxChannel = deviceInfo.QQ - 1; //CHx Channels
                if (maxChannel >= 0 && builder.FirstCHxId.HasValue)
                {
                    for (int i = 0; i <= maxChannel; i++)
                    {
                        J2534ProtocolId protocolId = builder.FirstCHxId.Value + i.ToUint32();
                        J2534Channel channel = new J2534Channel(protocolId);
                        GetChannelInfo(device, ref channel);
                        result.Add(channel);
                    }
                }
            }
            return result.ToArray();
        }
        private static J2534Channel[] GetProtocolSupport(J2534Device device, ChannelSetupParametersK builder)
        {
            List<J2534Channel> result = new List<J2534Channel>();
            if (device == null)
            {
                return new J2534Channel[0];
            }

            SPARAM deviceInfo = GetDeviceInfo(device, builder.SupportParameter);
            if (deviceInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
            {
                if (deviceInfo.SS > 0 && builder.BaseId.HasValue) //Base Channel
                {
                    J2534Channel channel = new J2534Channel(builder.BaseId.Value);
                    GetChannelInfo(device, ref channel);
                    result.Add(channel);
                }

                if (deviceInfo.RR > 0 && builder.PinSwitchedId.HasValue) //Pin Switched Channels
                {
                    J2534Channel channel = new J2534Channel(builder.PinSwitchedId.Value);
                    List<J2534ConfigParameter> parameters = new List<J2534ConfigParameter>();
                    if (builder.J1962_K_PinsParameter.HasValue && builder.J1962_L_PinsParameter.HasValue)
                    {
                        channel.J1962Pins = GetPinInfo(device, builder.J1962_K_PinsParameter.Value, builder.J1962_L_PinsParameter.Value);
                        if (channel.J1962Pins.Length > 0)
                        {
                            parameters.Add(new J2534ConfigParameter(J1962_PINS));
                        }
                    }
                    if (builder.J1939_K_PinsParameter.HasValue && builder.J1939_L_PinsParameter.HasValue)
                    {
                        channel.J1939Pins = GetPinInfo(device, builder.J1939_K_PinsParameter.Value, builder.J1939_L_PinsParameter.Value);
                        if (channel.J1939Pins.Length > 0)
                        {
                            parameters.Add(new J2534ConfigParameter(J1939_PINS));
                        }
                    }
                    channel.ConfParameter = parameters.ToArray();
                    GetChannelInfo(device, ref channel);
                    result.Add(channel);
                }

                int maxChannel = deviceInfo.QQ - 1; //CHx Channels
                if (maxChannel >= 0 && builder.FirstCHxId.HasValue)
                {
                    for (int i = 0; i <= maxChannel; i++)
                    {
                        J2534ProtocolId protocolId = builder.FirstCHxId.Value + i.ToUint32();
                        J2534Channel channel = new J2534Channel(protocolId);
                        GetChannelInfo(device, ref channel);
                        result.Add(channel);
                    }
                }
            }
            return result.ToArray();
        }
        private static J2534Channel[] GetProtocolSupport(J2534Device device, ChannelSetupParametersAnalog builder)
        {
            List<J2534Channel> result = new List<J2534Channel>();
            SPARAM deviceInfo = GetDeviceInfo(device, ANALOG_IN_SUPPORTED);
            if (deviceInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
            {
                int maxChannel = deviceInfo.QQ - 1;
                if (maxChannel >= 0)
                {
                    SPARAM protocolInfo = new SPARAM();
                    for (int i = 0; i <= maxChannel; i++)
                    {
                        J2534ProtocolId protocolId = builder.FirstCHxId.Value + i.ToUint32();
                        J2534Channel channel = new J2534Channel(protocolId) {AnalogSubsystem = new J2534AnalogSubsystem()};
                        protocolInfo = GetProtocolInfo(device, protocolId, MAX_AD_ACTIVE_CHANNELS);
                        if (protocolInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
                        {
                            //this is a kludge to support a box that reports MAX_AD_ACTIVE_CHANNELS
                            //as an integer rather than a bitmap. The kludge will fail if the box reports an integer 7
                            //for example, where this will assume it is a bitmap and return 3 for the 3 bits
                            if ((protocolInfo.Value == 1) || 
                                (protocolInfo.Value == 3) || 
                                (protocolInfo.Value == 7) || 
                                (protocolInfo.Value == 15) || 
                                (protocolInfo.Value > 31)) //the bits are packed or it's not valid as an integer
                            {
                                    channel.AnalogSubsystem.MaxChannel = PublicCode.PopCount(protocolInfo.Value).ToUint32();
                            }
                            else
                            {
                                    channel.AnalogSubsystem.MaxChannel = protocolInfo.Value;
                            }
                        }
                        protocolInfo = GetProtocolInfo(device, protocolId, MAX_AD_SAMPLE_RATE);
                        if (protocolInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
                        {
                            channel.AnalogSubsystem.MaxSampleRate = protocolInfo.Value;
                        }
                        GetChannelInfo(device, ref channel);
                        result.Add(channel);
                    }
                }
            }
            return result.ToArray();
        }

        private static void GetChannelInfo(J2534Device device, ref J2534Channel channel)
        {
            //Most of this is unused, but it's here to be expanded and made use of
            SPARAM protocolInfo = GetProtocolInfo(device, channel.ProtocolId, MAX_RX_BUFFER_SIZE);
            if (protocolInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
            {
                channel.MaxRXBufferSize = protocolInfo.Value;
            }

            protocolInfo = GetProtocolInfo(device, channel.ProtocolId, MAX_PASS_FILTER);
            if (protocolInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
            {
                channel.MaxPassFilter = protocolInfo.Value;
            }

            protocolInfo = GetProtocolInfo(device, channel.ProtocolId, MAX_BLOCK_FILTER);
            if (protocolInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
            {
                channel.MaxBlockFilter = protocolInfo.Value;
            }

            protocolInfo = GetProtocolInfo(device, channel.ProtocolId, MAX_FLOW_CONTROL_FILTER);
            if (protocolInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
            {
                channel.MaxFlowFilter = protocolInfo.Value;
            }

            protocolInfo = GetProtocolInfo(device, channel.ProtocolId, MAX_PERIODIC_MSGS);
            if (protocolInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
            {
                channel.MaxPeriodicMessage = protocolInfo.Value;
            }

            protocolInfo = GetProtocolInfo(device, channel.ProtocolId, FIVE_BAUD_MOD_SUPPORTED);
            if (protocolInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
            {
                channel.FiveBaudMod = protocolInfo.Value;
            }

            //Different boxes report suppordated data rates differently. I didn't find the below code
            //useful but chose to leave it as a comment.

            //for (int i = 0; i <= 1000000; i++)
            //{
            //    protocolInfo = GetProtocolInfo(device, channel.ProtocolId, DESIRED_DATA_RATE, (uint)i);
            //    if (protocolInfo.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
            //    {
            //        //Do something with the info
            //    }
            //}
        }

        private static int GetProgVoltagePinInfo(J2534Device device)
        {
            if (device == null || device.API.PassThruIoctl == null)
            {
                return -1;
            }
            SPARAM[] paramStruct = new SPARAM[2];
            SPARAM_LIST paramList = new SPARAM_LIST();
            PinMap pinTest = new PinMap();
            device.Analog.Pin[0].CanBeSet = true;
            for (int pinNo = 1; pinNo <= 15; pinNo++)
            {
                paramList.NumOfParams = 2;
                paramStruct[0].Parameter = (uint)SHORT_TO_GND_J1962;
                pinTest.LLLL =  (ushort)(1 << pinNo - 1);
                pinTest.HHHH = 0;
                paramStruct[0].Value = pinTest.Value;
                paramStruct[1].Parameter = (uint)PGM_VOLTAGE_J1962;
                pinTest.HHHH = pinTest.LLLL;
                pinTest.LLLL = 0;
                paramStruct[1].Value = pinTest.Value;
                int pinsInfoSize = Marshal.SizeOf(typeof(SPARAM));
                IntPtr pinsInfoSizePointer = Marshal.AllocHGlobal(pinsInfoSize * paramStruct.Length);
                for (int i = 0; i < paramStruct.Length; i++)
                {
                    Marshal.StructureToPtr(paramStruct[i], pinsInfoSizePointer + i * pinsInfoSize, true);
                }
                paramList.ParamPtr = pinsInfoSizePointer.ToInt32();
                IntPtr paramListPointer = Marshal.AllocHGlobal(Marshal.SizeOf(paramList));
                Marshal.StructureToPtr(paramList, paramListPointer, true);
                J2534Result result = device.API.PassThruIoctl(device.DeviceId, GET_DEVICE_INFO, 0, paramListPointer.ToInt32());
                for (int i = 0; i < paramStruct.Length; i++)
                {
                    paramStruct[i] = (SPARAM)Marshal.PtrToStructure(pinsInfoSizePointer + i * pinsInfoSize, typeof(SPARAM));
                }
                if (result == J2534Result.STATUS_NOERROR)
                {
                    device.Analog.Pin[pinNo].CanBeShorted = paramStruct[0].Supported == (uint)J2534DiscoveryResult.SUPPORTED;
                    device.Analog.Pin[pinNo].CanBeSet = paramStruct[1].Supported == (uint)J2534DiscoveryResult.SUPPORTED;
                }
                Marshal.FreeHGlobal(paramListPointer);
                Marshal.FreeHGlobal(pinsInfoSizePointer);
            }
            return 0;
        }

        private static SPARAM GetDeviceInfo(J2534Device device, J2534GetDeviceInfoParameter parameter)
        {
            SPARAM result = new SPARAM
            {
                Parameter = (uint)parameter
            };
            SPARAM_LIST paramList = new SPARAM_LIST();
            if (device == null || device.API.PassThruIoctl == null)
            {
                return result;
            }
            paramList.NumOfParams = 1;
            IntPtr resultPointer = Marshal.AllocHGlobal(Marshal.SizeOf(result));
            Marshal.StructureToPtr(result, resultPointer, true);
            paramList.ParamPtr = resultPointer.ToInt32();
            IntPtr paramListPointer = Marshal.AllocHGlobal(Marshal.SizeOf(paramList));
            Marshal.StructureToPtr(paramList, paramListPointer, true);
            device.API.PassThruIoctl(device.DeviceId, GET_DEVICE_INFO, 0, paramListPointer.ToInt32());
            result = (SPARAM)Marshal.PtrToStructure(resultPointer, typeof(SPARAM));
            Marshal.FreeHGlobal(paramListPointer);
            Marshal.FreeHGlobal(resultPointer);
            return result;
        }

        private static SPARAM GetProtocolInfo(J2534Device device, J2534ProtocolId protocolId, J2534GetProtocolInfoParameter parameter)
        {
            return GetProtocolInfo(device, protocolId, parameter, 0);
        }
        private static SPARAM GetProtocolInfo(J2534Device device, J2534ProtocolId protocolId, J2534GetProtocolInfoParameter parameter, uint input)
        {
            SPARAM result = new SPARAM
            {
                Parameter = (uint)parameter,
                Value = input
            };
            SPARAM_LIST paramList = new SPARAM_LIST();
            if (device == null || device.API.PassThruIoctl == null)
            {
                return result;
            }
            paramList.NumOfParams = 1;
            IntPtr resultPointer = Marshal.AllocHGlobal(Marshal.SizeOf(result));
            Marshal.StructureToPtr(result, resultPointer, true);
            paramList.ParamPtr = resultPointer.ToInt32();
            IntPtr paramListPointer = Marshal.AllocHGlobal(Marshal.SizeOf(paramList));
            Marshal.StructureToPtr(paramList, paramListPointer, true);
            GCHandle gh = GCHandle.Alloc((uint)protocolId, GCHandleType.Pinned);
            IntPtr ProtocolIdPointer = gh.AddrOfPinnedObject();
            device.API.PassThruIoctl(device.DeviceId, GET_PROTOCOL_INFO, ProtocolIdPointer.ToInt32(), paramListPointer.ToInt32());
            result = (SPARAM)Marshal.PtrToStructure(resultPointer, typeof(SPARAM));
            Marshal.FreeHGlobal(paramListPointer);
            Marshal.FreeHGlobal(resultPointer);
            gh.Free();
            return result;
        }

        private static uint[] GetPinInfo(J2534Device device, J2534GetDeviceInfoParameter parameter)
        {
            SPARAM paramStruct = new SPARAM
            {
                Parameter = (uint)parameter
            };
            SPARAM_LIST paramList = new SPARAM_LIST();
            uint[] pins = {};
            PinMap pinTest = new PinMap();
            PinMap pin = new PinMap();
            if (device == null || device.API.PassThruIoctl == null)
            {
                return pins;
            }
            for (pin.HiPin = 1; pin.HiPin <= 15; pin.HiPin++)
            {
                for (pin.LoPin = 0; pin.LoPin <= 15; pin.LoPin++) //inner loop from zero for single wire
                {
                    pinTest.HHHH = (1U << pin.HiPin - 1).ToUint16();
                    pinTest.LLLL = (1U << pin.LoPin - 1).ToUint16();
                    paramStruct.Value = pinTest.Value;
                    paramList.NumOfParams = 1;
                    IntPtr paramStructPointer = Marshal.AllocHGlobal(Marshal.SizeOf(paramStruct));
                    Marshal.StructureToPtr(paramStruct, paramStructPointer, true);
                    paramList.ParamPtr = paramStructPointer.ToInt32();
                    IntPtr paramListPointer = Marshal.AllocHGlobal(Marshal.SizeOf(paramList));
                    Marshal.StructureToPtr(paramList, paramListPointer, true);
                    J2534Result result = device.API.PassThruIoctl(device.DeviceId, GET_DEVICE_INFO, 0, paramListPointer.ToInt32());
                    paramStruct = (SPARAM)Marshal.PtrToStructure(paramStructPointer, typeof(SPARAM));
                    if (result == J2534Result.STATUS_NOERROR)
                    {
                        if (paramStruct.Supported == (uint)J2534DiscoveryResult.SUPPORTED)
                        {
                            int n = pins.Length;
                            Array.Resize(ref pins, n + 1);
                            pins[n] = pin.Value;
                        }
                    }
                    Marshal.FreeHGlobal(paramListPointer);
                    Marshal.FreeHGlobal(paramStructPointer);
                }
            }
            return pins;
        }
        private static uint[] GetPinInfo(J2534Device device, J2534GetDeviceInfoParameter parameterK, J2534GetDeviceInfoParameter parameterL)
        {
            SPARAM[] paramStruct = new SPARAM[2];
            paramStruct[0].Parameter = (uint)parameterK;
            paramStruct[0].Value = 0;
            paramStruct[1].Parameter = (uint)parameterL;
            paramStruct[1].Value = 0;
            SPARAM_LIST paramList = new SPARAM_LIST();
            uint[] pins = {};
            PinMap pinTest = new PinMap();
            PinMap pin = new PinMap();
            uint pinK = 0;
            uint pinL = 0;
            if (device == null || device.API.PassThruIoctl == null)
            {
                return pins;
            }
            for (pin.HiPin = 1; pin.HiPin <= 15; pin.HiPin++)
            {
                for (pin.LoPin = 0; pin.LoPin <= 15; pin.LoPin++) //inner loop from zero for single wire
                {
                    pinTest.HHHH = (1U << pin.HiPin - 1).ToUint16();
                    pinTest.LLLL = (1U << pin.LoPin - 1).ToUint16();
                    paramStruct[0].Value = pinTest.Value;
                    paramStruct[1].Value = pinTest.Value;
                    paramList.NumOfParams = 2;
                    int pinsInfoSize = Marshal.SizeOf(typeof(SPARAM));
                    IntPtr pinsInfoSizePointer = Marshal.AllocHGlobal(pinsInfoSize * paramStruct.Length);
                    for (int i = 0; i < paramStruct.Length; i++)
                    {
                        Marshal.StructureToPtr(paramStruct[i], pinsInfoSizePointer + i * pinsInfoSize, true);
                    }
                    paramList.ParamPtr = pinsInfoSizePointer.ToInt32();
                    IntPtr paramListPointer = Marshal.AllocHGlobal(Marshal.SizeOf(paramList));
                    Marshal.StructureToPtr(paramList, paramListPointer, true);
                    J2534Result result = device.API.PassThruIoctl(device.DeviceId, GET_DEVICE_INFO, 0, paramListPointer.ToInt32());
                    for (int i = 0; i < paramStruct.Length; i++)
                    {
                        paramStruct[i] = (SPARAM)Marshal.PtrToStructure(pinsInfoSizePointer + i * pinsInfoSize, typeof(SPARAM));
                    }
                    if (result == J2534Result.STATUS_NOERROR)
                    {
                        if (paramStruct[0].Supported == (uint)J2534DiscoveryResult.SUPPORTED || 
                            paramStruct[1].Supported == (uint)J2534DiscoveryResult.SUPPORTED)
                        {
                            if (paramStruct[0].Supported == (uint)J2534DiscoveryResult.SUPPORTED)
                            {
                                pinK = pin.Value & 0xFF00U;
                            }
                            if (paramStruct[1].Supported == (uint)J2534DiscoveryResult.SUPPORTED)
                            {
                                pinL = pin.Value & 0xFFU;
                            }
                            int n = pins.Length;
                            Array.Resize(ref pins, n + 1);
                            pins[n] = pinK | pinL;
                        }
                    }
                    Marshal.FreeHGlobal(paramListPointer);
                    Marshal.FreeHGlobal(pinsInfoSizePointer);
                }
            }
            return pins;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return this.Name;
        }

        public J2534Device()
        {
            BuildListViewObjects();
        }

        private bool disposedValue;
        protected void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    ListViewMessageIn.Dispose();
                    ListViewMessageOut.Dispose();
                    TimerMessageReceive.Dispose();
                    //Don't dispose of the API here
                }
            }
            disposedValue = true;
        }
        public void Dispose()
        {
            Dispose(true);
        }

    }

}