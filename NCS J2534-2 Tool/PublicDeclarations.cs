﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    internal static class PublicDeclarations
    {
        public const int betaVersion = 0;
        public const StringComparison IgnoreCase = StringComparison.OrdinalIgnoreCase;
    }

    public sealed class KeyChars
    {
        public const char Back = '\b';
        public const char Copy = (char)3;
        public const char Paste = (char)22;
        public const char Cut = (char)24;
        public const char Undo = (char)26;
        public const char Escape = (char)27;
        public const char Cr = '\r';
        public const char Lf = '\n';
        private KeyChars()
        {
        }
    }

    public class InputBoxResponse
    {
        public string Response {get; set;} = string.Empty;
        public DialogResult Result {get; set;}
    }
}