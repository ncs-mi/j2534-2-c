﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901


using System.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class FormInputBox : System.Windows.Forms.Form
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ButtonOK = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.LabelPrompt = new System.Windows.Forms.Label();
            this.TextBoxInput = new System.Windows.Forms.TextBox();
            this.ContextMenuStripTextBox = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuTextBoxUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuTextBoxCut = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuTextBoxSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenuStripTextBox.SuspendLayout();
            this.SuspendLayout();
            //
            //ButtonOK
            //
            this.ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ButtonOK.Location = new System.Drawing.Point(283, 12);
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.Size = new System.Drawing.Size(75, 23);
            this.ButtonOK.TabIndex = 0;
            this.ButtonOK.Text = "OK";
            this.ButtonOK.UseVisualStyleBackColor = true;
            //
            //ButtonCancel
            //
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(283, 41);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.ButtonCancel.TabIndex = 1;
            this.ButtonCancel.Text = "Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            //
            //LabelPrompt
            //
            this.LabelPrompt.AutoSize = true;
            this.LabelPrompt.Location = new System.Drawing.Point(6, 9);
            this.LabelPrompt.Name = "LabelPrompt";
            this.LabelPrompt.Size = new System.Drawing.Size(66, 13);
            this.LabelPrompt.TabIndex = 2;
            this.LabelPrompt.Text = "LabelPrompt";
            //
            //TextBoxInput
            //
            this.TextBoxInput.Location = new System.Drawing.Point(9, 87);
            this.TextBoxInput.Name = "TextBoxInput";
            this.TextBoxInput.Size = new System.Drawing.Size(349, 20);
            this.TextBoxInput.TabIndex = 3;
            //
            //ContextMenuStripTextBox
            //
            this.ContextMenuStripTextBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuTextBoxUndo, this.MenuTextBoxSeparator1, this.MenuTextBoxCut, this.MenuTextBoxCopy, this.MenuTextBoxPaste, this.MenuTextBoxDelete, this.MenuTextBoxSeparator2, this.MenuTextBoxSelectAll});
            this.ContextMenuStripTextBox.Name = "ContextMenuStripTextBox";
            this.ContextMenuStripTextBox.Size = new System.Drawing.Size(123, 148);
            //
            //MenuTextBoxUndo
            //
            this.MenuTextBoxUndo.Name = "MenuTextBoxUndo";
            this.MenuTextBoxUndo.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxUndo.Text = "Undo";
            //
            //MenuTextBoxSeparator1
            //
            this.MenuTextBoxSeparator1.Name = "MenuTextBoxSeparator1";
            this.MenuTextBoxSeparator1.Size = new System.Drawing.Size(119, 6);
            //
            //MenuTextBoxCut
            //
            this.MenuTextBoxCut.Name = "MenuTextBoxCut";
            this.MenuTextBoxCut.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxCut.Text = "Cut";
            //
            //MenuTextBoxCopy
            //
            this.MenuTextBoxCopy.Name = "MenuTextBoxCopy";
            this.MenuTextBoxCopy.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxCopy.Text = "Copy";
            //
            //MenuTextBoxPaste
            //
            this.MenuTextBoxPaste.Name = "MenuTextBoxPaste";
            this.MenuTextBoxPaste.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxPaste.Text = "Paste";
            //
            //MenuTextBoxDelete
            //
            this.MenuTextBoxDelete.Name = "MenuTextBoxDelete";
            this.MenuTextBoxDelete.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxDelete.Text = "Delete";
            //
            //MenuTextBoxSeparator2
            //
            this.MenuTextBoxSeparator2.Name = "MenuTextBoxSeparator2";
            this.MenuTextBoxSeparator2.Size = new System.Drawing.Size(119, 6);
            //
            //MenuTextBoxSelectAll
            //
            this.MenuTextBoxSelectAll.Name = "MenuTextBoxSelectAll";
            this.MenuTextBoxSelectAll.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxSelectAll.Text = "Select All";
            //
            //FormInputBox
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ButtonCancel;
            this.ClientSize = new System.Drawing.Size(370, 119);
            this.Controls.Add(this.TextBoxInput);
            this.Controls.Add(this.LabelPrompt);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.ButtonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormInputBox";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormInputBox";
            this.ContextMenuStripTextBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

            ContextMenuStripTextBox.Opening += new System.ComponentModel.CancelEventHandler(ContextMenuStripTextBox_Opening);
            MenuTextBoxCopy.Click += new System.EventHandler(MenuTextBoxCopy_Click);
            MenuTextBoxCut.Click += new System.EventHandler(MenuTextBoxCut_Click);
            MenuTextBoxDelete.Click += new System.EventHandler(MenuTextBoxDelete_Click);
            MenuTextBoxPaste.Click += new System.EventHandler(MenuTextBoxPaste_Click);
            MenuTextBoxSelectAll.Click += new System.EventHandler(MenuTextBoxSelectAll_Click);
            MenuTextBoxUndo.Click += new System.EventHandler(MenuTextBoxUndo_Click);
        }

        private Button ButtonOK;
        private Button ButtonCancel;
        private Label LabelPrompt;
        private TextBox TextBoxInput;
        private ContextMenuStrip ContextMenuStripTextBox;
        private ToolStripMenuItem MenuTextBoxUndo;
        private ToolStripSeparator MenuTextBoxSeparator1;
        private ToolStripMenuItem MenuTextBoxCut;
        private ToolStripMenuItem MenuTextBoxCopy;
        private ToolStripMenuItem MenuTextBoxPaste;
        private ToolStripMenuItem MenuTextBoxDelete;
        private ToolStripSeparator MenuTextBoxSeparator2;
        private ToolStripMenuItem MenuTextBoxSelectAll;

        private static FormInputBox _DefaultInstance;
        public static FormInputBox DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new FormInputBox();

                return _DefaultInstance;
            }
        }
    }

}