﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901


using System.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class FormRxStatus : System.Windows.Forms.Form
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRxStatus));
            this.TextBoxFlags = new System.Windows.Forms.TextBox();
            this.ContextMenuStripTextBox = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuTextBoxUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuTextBoxCut = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuTextBoxSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.DoneButton = new System.Windows.Forms.Button();
            this.CheckBoxFlag0 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag1 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag2 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag3 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag4 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag5 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag6 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag7 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag8 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag9 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag10 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag11 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag12 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag13 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag14 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag15 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag16 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag17 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag18 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag19 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag20 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag21 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag22 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag23 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag24 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag25 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag26 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag27 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag28 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag29 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag30 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag31 = new System.Windows.Forms.CheckBox();
            this.LabelDesc = new System.Windows.Forms.Label();
            this.ContextMenuStripTextBox.SuspendLayout();
            this.SuspendLayout();
            //
            //TextBoxFlags
            //
            this.TextBoxFlags.AcceptsReturn = true;
            this.TextBoxFlags.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFlags.Location = new System.Drawing.Point(319, 298);
            this.TextBoxFlags.MaxLength = 0;
            this.TextBoxFlags.Name = "TextBoxFlags";
            this.TextBoxFlags.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFlags.TabIndex = 33;
            //
            //ContextMenuStripTextBox
            //
            this.ContextMenuStripTextBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuTextBoxUndo, this.MenuTextBoxSeparator1, this.MenuTextBoxCut, this.MenuTextBoxCopy, this.MenuTextBoxPaste, this.MenuTextBoxDelete, this.MenuTextBoxSeparator2, this.MenuTextBoxSelectAll});
            this.ContextMenuStripTextBox.Name = "ContextMenuStripTextBox";
            this.ContextMenuStripTextBox.Size = new System.Drawing.Size(123, 148);
            //
            //MenuTextBoxUndo
            //
            this.MenuTextBoxUndo.Name = "MenuTextBoxUndo";
            this.MenuTextBoxUndo.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxUndo.Text = "Undo";
            //
            //MenuTextBoxSeparator1
            //
            this.MenuTextBoxSeparator1.Name = "MenuTextBoxSeparator1";
            this.MenuTextBoxSeparator1.Size = new System.Drawing.Size(119, 6);
            //
            //MenuTextBoxCut
            //
            this.MenuTextBoxCut.Name = "MenuTextBoxCut";
            this.MenuTextBoxCut.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxCut.Text = "Cut";
            //
            //MenuTextBoxCopy
            //
            this.MenuTextBoxCopy.Name = "MenuTextBoxCopy";
            this.MenuTextBoxCopy.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxCopy.Text = "Copy";
            //
            //MenuTextBoxPaste
            //
            this.MenuTextBoxPaste.Name = "MenuTextBoxPaste";
            this.MenuTextBoxPaste.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxPaste.Text = "Paste";
            //
            //MenuTextBoxDelete
            //
            this.MenuTextBoxDelete.Name = "MenuTextBoxDelete";
            this.MenuTextBoxDelete.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxDelete.Text = "Delete";
            //
            //MenuTextBoxSeparator2
            //
            this.MenuTextBoxSeparator2.Name = "MenuTextBoxSeparator2";
            this.MenuTextBoxSeparator2.Size = new System.Drawing.Size(119, 6);
            //
            //MenuTextBoxSelectAll
            //
            this.MenuTextBoxSelectAll.Name = "MenuTextBoxSelectAll";
            this.MenuTextBoxSelectAll.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxSelectAll.Text = "Select All";
            //
            //DoneButton
            //
            this.DoneButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.DoneButton.Location = new System.Drawing.Point(319, 324);
            this.DoneButton.Name = "DoneButton";
            this.DoneButton.Size = new System.Drawing.Size(65, 25);
            this.DoneButton.TabIndex = 0;
            this.DoneButton.Text = "Done";
            this.DoneButton.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag0
            //
            this.CheckBoxFlag0.AutoSize = true;
            this.CheckBoxFlag0.Location = new System.Drawing.Point(24, 24);
            this.CheckBoxFlag0.Name = "CheckBoxFlag0";
            this.CheckBoxFlag0.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag0.TabIndex = 1;
            this.CheckBoxFlag0.Text = "CheckBoxFlag(0)";
            this.CheckBoxFlag0.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag1
            //
            this.CheckBoxFlag1.AutoSize = true;
            this.CheckBoxFlag1.Location = new System.Drawing.Point(24, 48);
            this.CheckBoxFlag1.Name = "CheckBoxFlag1";
            this.CheckBoxFlag1.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag1.TabIndex = 2;
            this.CheckBoxFlag1.Text = "CheckBoxFlag(1)";
            this.CheckBoxFlag1.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag2
            //
            this.CheckBoxFlag2.AutoSize = true;
            this.CheckBoxFlag2.Location = new System.Drawing.Point(24, 72);
            this.CheckBoxFlag2.Name = "CheckBoxFlag2";
            this.CheckBoxFlag2.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag2.TabIndex = 3;
            this.CheckBoxFlag2.Text = "CheckBoxFlag(2)";
            this.CheckBoxFlag2.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag3
            //
            this.CheckBoxFlag3.AutoSize = true;
            this.CheckBoxFlag3.Location = new System.Drawing.Point(24, 96);
            this.CheckBoxFlag3.Name = "CheckBoxFlag3";
            this.CheckBoxFlag3.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag3.TabIndex = 4;
            this.CheckBoxFlag3.Text = "CheckBoxFlag(3)";
            this.CheckBoxFlag3.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag4
            //
            this.CheckBoxFlag4.AutoSize = true;
            this.CheckBoxFlag4.Location = new System.Drawing.Point(24, 120);
            this.CheckBoxFlag4.Name = "CheckBoxFlag4";
            this.CheckBoxFlag4.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag4.TabIndex = 5;
            this.CheckBoxFlag4.Text = "CheckBoxFlag(4)";
            this.CheckBoxFlag4.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag5
            //
            this.CheckBoxFlag5.AutoSize = true;
            this.CheckBoxFlag5.Location = new System.Drawing.Point(24, 144);
            this.CheckBoxFlag5.Name = "CheckBoxFlag5";
            this.CheckBoxFlag5.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag5.TabIndex = 6;
            this.CheckBoxFlag5.Text = "CheckBoxFlag(5)";
            this.CheckBoxFlag5.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag6
            //
            this.CheckBoxFlag6.AutoSize = true;
            this.CheckBoxFlag6.Location = new System.Drawing.Point(24, 168);
            this.CheckBoxFlag6.Name = "CheckBoxFlag6";
            this.CheckBoxFlag6.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag6.TabIndex = 7;
            this.CheckBoxFlag6.Text = "CheckBoxFlag(6)";
            this.CheckBoxFlag6.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag7
            //
            this.CheckBoxFlag7.AutoSize = true;
            this.CheckBoxFlag7.Location = new System.Drawing.Point(24, 192);
            this.CheckBoxFlag7.Name = "CheckBoxFlag7";
            this.CheckBoxFlag7.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag7.TabIndex = 8;
            this.CheckBoxFlag7.Text = "CheckBoxFlag(7)";
            this.CheckBoxFlag7.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag8
            //
            this.CheckBoxFlag8.AutoSize = true;
            this.CheckBoxFlag8.Location = new System.Drawing.Point(24, 216);
            this.CheckBoxFlag8.Name = "CheckBoxFlag8";
            this.CheckBoxFlag8.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag8.TabIndex = 9;
            this.CheckBoxFlag8.Text = "CheckBoxFlag(8)";
            this.CheckBoxFlag8.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag9
            //
            this.CheckBoxFlag9.AutoSize = true;
            this.CheckBoxFlag9.Location = new System.Drawing.Point(24, 240);
            this.CheckBoxFlag9.Name = "CheckBoxFlag9";
            this.CheckBoxFlag9.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag9.TabIndex = 10;
            this.CheckBoxFlag9.Text = "CheckBoxFlag(9)";
            this.CheckBoxFlag9.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag10
            //
            this.CheckBoxFlag10.AutoSize = true;
            this.CheckBoxFlag10.Location = new System.Drawing.Point(231, 24);
            this.CheckBoxFlag10.Name = "CheckBoxFlag10";
            this.CheckBoxFlag10.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag10.TabIndex = 11;
            this.CheckBoxFlag10.Text = "CheckBoxFlag(10)";
            this.CheckBoxFlag10.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag11
            //
            this.CheckBoxFlag11.AutoSize = true;
            this.CheckBoxFlag11.Location = new System.Drawing.Point(231, 48);
            this.CheckBoxFlag11.Name = "CheckBoxFlag11";
            this.CheckBoxFlag11.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag11.TabIndex = 12;
            this.CheckBoxFlag11.Text = "CheckBoxFlag(11)";
            this.CheckBoxFlag11.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag12
            //
            this.CheckBoxFlag12.AutoSize = true;
            this.CheckBoxFlag12.Location = new System.Drawing.Point(231, 72);
            this.CheckBoxFlag12.Name = "CheckBoxFlag12";
            this.CheckBoxFlag12.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag12.TabIndex = 13;
            this.CheckBoxFlag12.Text = "CheckBoxFlag(12)";
            this.CheckBoxFlag12.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag13
            //
            this.CheckBoxFlag13.AutoSize = true;
            this.CheckBoxFlag13.Location = new System.Drawing.Point(231, 96);
            this.CheckBoxFlag13.Name = "CheckBoxFlag13";
            this.CheckBoxFlag13.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag13.TabIndex = 14;
            this.CheckBoxFlag13.Text = "CheckBoxFlag(13)";
            this.CheckBoxFlag13.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag14
            //
            this.CheckBoxFlag14.AutoSize = true;
            this.CheckBoxFlag14.Location = new System.Drawing.Point(231, 120);
            this.CheckBoxFlag14.Name = "CheckBoxFlag14";
            this.CheckBoxFlag14.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag14.TabIndex = 15;
            this.CheckBoxFlag14.Text = "CheckBoxFlag(14)";
            this.CheckBoxFlag14.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag15
            //
            this.CheckBoxFlag15.AutoSize = true;
            this.CheckBoxFlag15.Location = new System.Drawing.Point(231, 144);
            this.CheckBoxFlag15.Name = "CheckBoxFlag15";
            this.CheckBoxFlag15.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag15.TabIndex = 16;
            this.CheckBoxFlag15.Text = "CheckBoxFlag(15)";
            this.CheckBoxFlag15.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag16
            //
            this.CheckBoxFlag16.AutoSize = true;
            this.CheckBoxFlag16.Location = new System.Drawing.Point(231, 168);
            this.CheckBoxFlag16.Name = "CheckBoxFlag16";
            this.CheckBoxFlag16.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag16.TabIndex = 17;
            this.CheckBoxFlag16.Text = "CheckBoxFlag(16)";
            this.CheckBoxFlag16.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag17
            //
            this.CheckBoxFlag17.AutoSize = true;
            this.CheckBoxFlag17.Location = new System.Drawing.Point(231, 192);
            this.CheckBoxFlag17.Name = "CheckBoxFlag17";
            this.CheckBoxFlag17.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag17.TabIndex = 18;
            this.CheckBoxFlag17.Text = "CheckBoxFlag(17)";
            this.CheckBoxFlag17.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag18
            //
            this.CheckBoxFlag18.AutoSize = true;
            this.CheckBoxFlag18.Location = new System.Drawing.Point(231, 216);
            this.CheckBoxFlag18.Name = "CheckBoxFlag18";
            this.CheckBoxFlag18.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag18.TabIndex = 19;
            this.CheckBoxFlag18.Text = "CheckBoxFlag(18)";
            this.CheckBoxFlag18.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag19
            //
            this.CheckBoxFlag19.AutoSize = true;
            this.CheckBoxFlag19.Location = new System.Drawing.Point(231, 240);
            this.CheckBoxFlag19.Name = "CheckBoxFlag19";
            this.CheckBoxFlag19.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag19.TabIndex = 20;
            this.CheckBoxFlag19.Text = "CheckBoxFlag(19)";
            this.CheckBoxFlag19.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag20
            //
            this.CheckBoxFlag20.AutoSize = true;
            this.CheckBoxFlag20.Location = new System.Drawing.Point(438, 24);
            this.CheckBoxFlag20.Name = "CheckBoxFlag20";
            this.CheckBoxFlag20.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag20.TabIndex = 21;
            this.CheckBoxFlag20.Text = "CheckBoxFlag(20)";
            this.CheckBoxFlag20.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag21
            //
            this.CheckBoxFlag21.AutoSize = true;
            this.CheckBoxFlag21.Location = new System.Drawing.Point(438, 48);
            this.CheckBoxFlag21.Name = "CheckBoxFlag21";
            this.CheckBoxFlag21.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag21.TabIndex = 22;
            this.CheckBoxFlag21.Text = "CheckBoxFlag(21)";
            this.CheckBoxFlag21.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag22
            //
            this.CheckBoxFlag22.AutoSize = true;
            this.CheckBoxFlag22.Location = new System.Drawing.Point(438, 72);
            this.CheckBoxFlag22.Name = "CheckBoxFlag22";
            this.CheckBoxFlag22.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag22.TabIndex = 23;
            this.CheckBoxFlag22.Text = "CheckBoxFlag(22)";
            this.CheckBoxFlag22.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag23
            //
            this.CheckBoxFlag23.AutoSize = true;
            this.CheckBoxFlag23.Location = new System.Drawing.Point(438, 96);
            this.CheckBoxFlag23.Name = "CheckBoxFlag23";
            this.CheckBoxFlag23.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag23.TabIndex = 24;
            this.CheckBoxFlag23.Text = "CheckBoxFlag(23)";
            this.CheckBoxFlag23.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag24
            //
            this.CheckBoxFlag24.AutoSize = true;
            this.CheckBoxFlag24.Location = new System.Drawing.Point(438, 120);
            this.CheckBoxFlag24.Name = "CheckBoxFlag24";
            this.CheckBoxFlag24.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag24.TabIndex = 25;
            this.CheckBoxFlag24.Text = "CheckBoxFlag(24)";
            this.CheckBoxFlag24.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag25
            //
            this.CheckBoxFlag25.AutoSize = true;
            this.CheckBoxFlag25.Location = new System.Drawing.Point(438, 144);
            this.CheckBoxFlag25.Name = "CheckBoxFlag25";
            this.CheckBoxFlag25.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag25.TabIndex = 26;
            this.CheckBoxFlag25.Text = "CheckBoxFlag(25)";
            this.CheckBoxFlag25.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag26
            //
            this.CheckBoxFlag26.AutoSize = true;
            this.CheckBoxFlag26.Location = new System.Drawing.Point(438, 168);
            this.CheckBoxFlag26.Name = "CheckBoxFlag26";
            this.CheckBoxFlag26.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag26.TabIndex = 27;
            this.CheckBoxFlag26.Text = "CheckBoxFlag(26)";
            this.CheckBoxFlag26.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag27
            //
            this.CheckBoxFlag27.AutoSize = true;
            this.CheckBoxFlag27.Location = new System.Drawing.Point(438, 192);
            this.CheckBoxFlag27.Name = "CheckBoxFlag27";
            this.CheckBoxFlag27.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag27.TabIndex = 28;
            this.CheckBoxFlag27.Text = "CheckBoxFlag(27)";
            this.CheckBoxFlag27.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag28
            //
            this.CheckBoxFlag28.AutoSize = true;
            this.CheckBoxFlag28.Location = new System.Drawing.Point(438, 216);
            this.CheckBoxFlag28.Name = "CheckBoxFlag28";
            this.CheckBoxFlag28.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag28.TabIndex = 29;
            this.CheckBoxFlag28.Text = "CheckBoxFlag(28)";
            this.CheckBoxFlag28.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag29
            //
            this.CheckBoxFlag29.AutoSize = true;
            this.CheckBoxFlag29.Location = new System.Drawing.Point(438, 240);
            this.CheckBoxFlag29.Name = "CheckBoxFlag29";
            this.CheckBoxFlag29.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag29.TabIndex = 30;
            this.CheckBoxFlag29.Text = "CheckBoxFlag(29)";
            this.CheckBoxFlag29.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag30
            //
            this.CheckBoxFlag30.AutoSize = true;
            this.CheckBoxFlag30.Location = new System.Drawing.Point(438, 264);
            this.CheckBoxFlag30.Name = "CheckBoxFlag30";
            this.CheckBoxFlag30.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag30.TabIndex = 31;
            this.CheckBoxFlag30.Text = "CheckBoxFlag(30)";
            this.CheckBoxFlag30.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag31
            //
            this.CheckBoxFlag31.AutoSize = true;
            this.CheckBoxFlag31.Location = new System.Drawing.Point(438, 288);
            this.CheckBoxFlag31.Name = "CheckBoxFlag31";
            this.CheckBoxFlag31.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag31.TabIndex = 32;
            this.CheckBoxFlag31.Text = "CheckBoxFlag(31)";
            this.CheckBoxFlag31.UseVisualStyleBackColor = true;
            //
            //LabelDesc
            //
            this.LabelDesc.BackColor = System.Drawing.SystemColors.Window;
            this.LabelDesc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelDesc.Location = new System.Drawing.Point(24, 264);
            this.LabelDesc.Name = "LabelDesc";
            this.LabelDesc.Size = new System.Drawing.Size(289, 85);
            this.LabelDesc.TabIndex = 34;
            //
            //FormRxStatus
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.DoneButton;
            this.ClientSize = new System.Drawing.Size(654, 358);
            this.Controls.Add(this.TextBoxFlags);
            this.Controls.Add(this.DoneButton);
            this.Controls.Add(this.CheckBoxFlag0);
            this.Controls.Add(this.CheckBoxFlag1);
            this.Controls.Add(this.CheckBoxFlag2);
            this.Controls.Add(this.CheckBoxFlag3);
            this.Controls.Add(this.CheckBoxFlag4);
            this.Controls.Add(this.CheckBoxFlag5);
            this.Controls.Add(this.CheckBoxFlag6);
            this.Controls.Add(this.CheckBoxFlag7);
            this.Controls.Add(this.CheckBoxFlag8);
            this.Controls.Add(this.CheckBoxFlag9);
            this.Controls.Add(this.CheckBoxFlag10);
            this.Controls.Add(this.CheckBoxFlag11);
            this.Controls.Add(this.CheckBoxFlag12);
            this.Controls.Add(this.CheckBoxFlag13);
            this.Controls.Add(this.CheckBoxFlag14);
            this.Controls.Add(this.CheckBoxFlag15);
            this.Controls.Add(this.CheckBoxFlag16);
            this.Controls.Add(this.CheckBoxFlag17);
            this.Controls.Add(this.CheckBoxFlag18);
            this.Controls.Add(this.CheckBoxFlag19);
            this.Controls.Add(this.CheckBoxFlag20);
            this.Controls.Add(this.CheckBoxFlag21);
            this.Controls.Add(this.CheckBoxFlag22);
            this.Controls.Add(this.CheckBoxFlag23);
            this.Controls.Add(this.CheckBoxFlag24);
            this.Controls.Add(this.CheckBoxFlag25);
            this.Controls.Add(this.CheckBoxFlag26);
            this.Controls.Add(this.CheckBoxFlag27);
            this.Controls.Add(this.CheckBoxFlag28);
            this.Controls.Add(this.CheckBoxFlag29);
            this.Controls.Add(this.CheckBoxFlag30);
            this.Controls.Add(this.CheckBoxFlag31);
            this.Controls.Add(this.LabelDesc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(110, 108);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormRxStatus";
            this.Text = "Rx Status Flags";
            this.ContextMenuStripTextBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

            TextBoxFlags.TextChanged += new System.EventHandler(TextBoxFlags_TextChanged);
            TextBoxFlags.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxFlags_KeyPress);
            ContextMenuStripTextBox.Opening += new System.ComponentModel.CancelEventHandler(ContextMenuStripTextBox_Opening);
            MenuTextBoxCopy.Click += new System.EventHandler(MenuTextBoxCopy_Click);
            MenuTextBoxCut.Click += new System.EventHandler(MenuTextBoxCut_Click);
            MenuTextBoxDelete.Click += new System.EventHandler(MenuTextBoxDelete_Click);
            MenuTextBoxPaste.Click += new System.EventHandler(MenuTextBoxPaste_Click);
            MenuTextBoxSelectAll.Click += new System.EventHandler(MenuTextBoxSelectAll_Click);
            MenuTextBoxUndo.Click += new System.EventHandler(MenuTextBoxUndo_Click);
        }

        public void InitializeCheckBoxFlag()
        {
            CheckBoxFlag = new System.Windows.Forms.CheckBox[32];
            this.CheckBoxFlag[0] = CheckBoxFlag0;
            this.CheckBoxFlag[1] = CheckBoxFlag1;
            this.CheckBoxFlag[2] = CheckBoxFlag2;
            this.CheckBoxFlag[3] = CheckBoxFlag3;
            this.CheckBoxFlag[4] = CheckBoxFlag4;
            this.CheckBoxFlag[5] = CheckBoxFlag5;
            this.CheckBoxFlag[6] = CheckBoxFlag6;
            this.CheckBoxFlag[7] = CheckBoxFlag7;
            this.CheckBoxFlag[8] = CheckBoxFlag8;
            this.CheckBoxFlag[9] = CheckBoxFlag9;
            this.CheckBoxFlag[10] = CheckBoxFlag10;
            this.CheckBoxFlag[11] = CheckBoxFlag11;
            this.CheckBoxFlag[12] = CheckBoxFlag12;
            this.CheckBoxFlag[13] = CheckBoxFlag13;
            this.CheckBoxFlag[14] = CheckBoxFlag14;
            this.CheckBoxFlag[15] = CheckBoxFlag15;
            this.CheckBoxFlag[16] = CheckBoxFlag16;
            this.CheckBoxFlag[17] = CheckBoxFlag17;
            this.CheckBoxFlag[18] = CheckBoxFlag18;
            this.CheckBoxFlag[19] = CheckBoxFlag19;
            this.CheckBoxFlag[20] = CheckBoxFlag20;
            this.CheckBoxFlag[21] = CheckBoxFlag21;
            this.CheckBoxFlag[22] = CheckBoxFlag22;
            this.CheckBoxFlag[23] = CheckBoxFlag23;
            this.CheckBoxFlag[24] = CheckBoxFlag24;
            this.CheckBoxFlag[25] = CheckBoxFlag25;
            this.CheckBoxFlag[26] = CheckBoxFlag26;
            this.CheckBoxFlag[27] = CheckBoxFlag27;
            this.CheckBoxFlag[28] = CheckBoxFlag28;
            this.CheckBoxFlag[29] = CheckBoxFlag29;
            this.CheckBoxFlag[30] = CheckBoxFlag30;
            this.CheckBoxFlag[31] = CheckBoxFlag31;
        }

        private System.Windows.Forms.TextBox TextBoxFlags;
        private System.Windows.Forms.Button DoneButton;
        private System.Windows.Forms.CheckBox[] CheckBoxFlag;
        private System.Windows.Forms.CheckBox CheckBoxFlag0;
        private System.Windows.Forms.CheckBox CheckBoxFlag1;
        private System.Windows.Forms.CheckBox CheckBoxFlag2;
        private System.Windows.Forms.CheckBox CheckBoxFlag3;
        private System.Windows.Forms.CheckBox CheckBoxFlag4;
        private System.Windows.Forms.CheckBox CheckBoxFlag5;
        private System.Windows.Forms.CheckBox CheckBoxFlag6;
        private System.Windows.Forms.CheckBox CheckBoxFlag7;
        private System.Windows.Forms.CheckBox CheckBoxFlag8;
        private System.Windows.Forms.CheckBox CheckBoxFlag9;
        private System.Windows.Forms.CheckBox CheckBoxFlag10;
        private System.Windows.Forms.CheckBox CheckBoxFlag11;
        private System.Windows.Forms.CheckBox CheckBoxFlag12;
        private System.Windows.Forms.CheckBox CheckBoxFlag13;
        private System.Windows.Forms.CheckBox CheckBoxFlag14;
        private System.Windows.Forms.CheckBox CheckBoxFlag15;
        private System.Windows.Forms.CheckBox CheckBoxFlag16;
        private System.Windows.Forms.CheckBox CheckBoxFlag17;
        private System.Windows.Forms.CheckBox CheckBoxFlag18;
        private System.Windows.Forms.CheckBox CheckBoxFlag19;
        private System.Windows.Forms.CheckBox CheckBoxFlag20;
        private System.Windows.Forms.CheckBox CheckBoxFlag21;
        private System.Windows.Forms.CheckBox CheckBoxFlag22;
        private System.Windows.Forms.CheckBox CheckBoxFlag23;
        private System.Windows.Forms.CheckBox CheckBoxFlag24;
        private System.Windows.Forms.CheckBox CheckBoxFlag25;
        private System.Windows.Forms.CheckBox CheckBoxFlag26;
        private System.Windows.Forms.CheckBox CheckBoxFlag27;
        private System.Windows.Forms.CheckBox CheckBoxFlag28;
        private System.Windows.Forms.CheckBox CheckBoxFlag29;
        private System.Windows.Forms.CheckBox CheckBoxFlag30;
        private System.Windows.Forms.CheckBox CheckBoxFlag31;
        private System.Windows.Forms.Label LabelDesc;
        private ContextMenuStrip ContextMenuStripTextBox;
        private ToolStripMenuItem MenuTextBoxUndo;
        private ToolStripSeparator MenuTextBoxSeparator1;
        private ToolStripMenuItem MenuTextBoxCut;
        private ToolStripMenuItem MenuTextBoxCopy;
        private ToolStripMenuItem MenuTextBoxPaste;
        private ToolStripMenuItem MenuTextBoxDelete;
        private ToolStripSeparator MenuTextBoxSeparator2;
        private ToolStripMenuItem MenuTextBoxSelectAll;

        private static FormRxStatus _DefaultInstance;
        public static FormRxStatus DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new FormRxStatus();

                return _DefaultInstance;
            }
        }
    }
}