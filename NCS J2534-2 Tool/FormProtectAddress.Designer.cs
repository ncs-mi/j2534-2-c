﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class FormProtectAddress : System.Windows.Forms.Form
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LabelAddress = new System.Windows.Forms.Label();
            this.TextBoxAddress = new System.Windows.Forms.TextBox();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.ButtonSet = new System.Windows.Forms.Button();
            this.CheckBox1 = new System.Windows.Forms.CheckBox();
            this.TextBoxIndGroup = new System.Windows.Forms.TextBox();
            this.TextBoxVehSysInst = new System.Windows.Forms.TextBox();
            this.TextBoxVehSys = new System.Windows.Forms.TextBox();
            this.TextBoxFunc = new System.Windows.Forms.TextBox();
            this.TextBoxFuncInst = new System.Windows.Forms.TextBox();
            this.TextBoxECUInst = new System.Windows.Forms.TextBox();
            this.TextBoxMfgCode = new System.Windows.Forms.TextBox();
            this.TextBoxIdent = new System.Windows.Forms.TextBox();
            this.LabelIG = new System.Windows.Forms.Label();
            this.LabelSystemInstance = new System.Windows.Forms.Label();
            this.LabelSystem = new System.Windows.Forms.Label();
            this.LabelFunction = new System.Windows.Forms.Label();
            this.LabelFunctionInstance = new System.Windows.Forms.Label();
            this.LabelECUInstance = new System.Windows.Forms.Label();
            this.LabelMfgCode = new System.Windows.Forms.Label();
            this.LabelIdentityNo = new System.Windows.Forms.Label();
            this.LabelIGSize = new System.Windows.Forms.Label();
            this.LabelVehSysInstSize = new System.Windows.Forms.Label();
            this.LabelVehSysSize = new System.Windows.Forms.Label();
            this.LabelFuncSize = new System.Windows.Forms.Label();
            this.LabelFuncInstSize = new System.Windows.Forms.Label();
            this.LabelECUInstSize = new System.Windows.Forms.Label();
            this.LabelMfgCodeSize = new System.Windows.Forms.Label();
            this.LabelIdentSize = new System.Windows.Forms.Label();
            this.LabelResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            //
            //LabelAddress
            //
            this.LabelAddress.AutoSize = true;
            this.LabelAddress.Location = new System.Drawing.Point(89, 13);
            this.LabelAddress.Name = "LabelAddress";
            this.LabelAddress.Size = new System.Drawing.Size(48, 13);
            this.LabelAddress.TabIndex = 11;
            this.LabelAddress.Text = "Address:";
            this.LabelAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //TextBoxAddress
            //
            this.TextBoxAddress.Location = new System.Drawing.Point(139, 10);
            this.TextBoxAddress.Name = "TextBoxAddress";
            this.TextBoxAddress.Size = new System.Drawing.Size(80, 20);
            this.TextBoxAddress.TabIndex = 2;
            //
            //ButtonCancel
            //
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(333, 294);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(65, 25);
            this.ButtonCancel.TabIndex = 0;
            this.ButtonCancel.Text = "Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            //
            //ButtonSet
            //
            this.ButtonSet.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ButtonSet.Location = new System.Drawing.Point(262, 294);
            this.ButtonSet.Name = "ButtonSet";
            this.ButtonSet.Size = new System.Drawing.Size(65, 25);
            this.ButtonSet.TabIndex = 1;
            this.ButtonSet.Text = "Set";
            this.ButtonSet.UseVisualStyleBackColor = true;
            //
            //CheckBox1
            //
            this.CheckBox1.AutoSize = true;
            this.CheckBox1.Location = new System.Drawing.Point(225, 12);
            this.CheckBox1.Name = "CheckBox1";
            this.CheckBox1.Size = new System.Drawing.Size(106, 17);
            this.CheckBox1.TabIndex = 20;
            this.CheckBox1.Text = "Arbitrary Capable";
            this.CheckBox1.UseVisualStyleBackColor = true;
            //
            //TextBoxIndGroup
            //
            this.TextBoxIndGroup.Location = new System.Drawing.Point(139, 36);
            this.TextBoxIndGroup.Name = "TextBoxIndGroup";
            this.TextBoxIndGroup.Size = new System.Drawing.Size(80, 20);
            this.TextBoxIndGroup.TabIndex = 3;
            //
            //TextBoxVehSysInst
            //
            this.TextBoxVehSysInst.Location = new System.Drawing.Point(139, 62);
            this.TextBoxVehSysInst.Name = "TextBoxVehSysInst";
            this.TextBoxVehSysInst.Size = new System.Drawing.Size(80, 20);
            this.TextBoxVehSysInst.TabIndex = 4;
            //
            //TextBoxVehSys
            //
            this.TextBoxVehSys.Location = new System.Drawing.Point(139, 88);
            this.TextBoxVehSys.Name = "TextBoxVehSys";
            this.TextBoxVehSys.Size = new System.Drawing.Size(80, 20);
            this.TextBoxVehSys.TabIndex = 5;
            //
            //TextBoxFunc
            //
            this.TextBoxFunc.Location = new System.Drawing.Point(139, 114);
            this.TextBoxFunc.Name = "TextBoxFunc";
            this.TextBoxFunc.Size = new System.Drawing.Size(80, 20);
            this.TextBoxFunc.TabIndex = 6;
            //
            //TextBoxFuncInst
            //
            this.TextBoxFuncInst.Location = new System.Drawing.Point(139, 140);
            this.TextBoxFuncInst.Name = "TextBoxFuncInst";
            this.TextBoxFuncInst.Size = new System.Drawing.Size(80, 20);
            this.TextBoxFuncInst.TabIndex = 7;
            //
            //TextBoxECUInst
            //
            this.TextBoxECUInst.Location = new System.Drawing.Point(139, 166);
            this.TextBoxECUInst.Name = "TextBoxECUInst";
            this.TextBoxECUInst.Size = new System.Drawing.Size(80, 20);
            this.TextBoxECUInst.TabIndex = 8;
            //
            //TextBoxMfgCode
            //
            this.TextBoxMfgCode.Location = new System.Drawing.Point(139, 192);
            this.TextBoxMfgCode.Name = "TextBoxMfgCode";
            this.TextBoxMfgCode.Size = new System.Drawing.Size(80, 20);
            this.TextBoxMfgCode.TabIndex = 9;
            //
            //TextBoxIdent
            //
            this.TextBoxIdent.Location = new System.Drawing.Point(139, 218);
            this.TextBoxIdent.Name = "TextBoxIdent";
            this.TextBoxIdent.Size = new System.Drawing.Size(80, 20);
            this.TextBoxIdent.TabIndex = 10;
            //
            //LabelIG
            //
            this.LabelIG.AutoSize = true;
            this.LabelIG.Location = new System.Drawing.Point(58, 39);
            this.LabelIG.Name = "LabelIG";
            this.LabelIG.Size = new System.Drawing.Size(79, 13);
            this.LabelIG.TabIndex = 12;
            this.LabelIG.Text = "Industry Group:";
            this.LabelIG.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //LabelSystemInstance
            //
            this.LabelSystemInstance.AutoSize = true;
            this.LabelSystemInstance.Location = new System.Drawing.Point(11, 65);
            this.LabelSystemInstance.Name = "LabelSystemInstance";
            this.LabelSystemInstance.Size = new System.Drawing.Size(126, 13);
            this.LabelSystemInstance.TabIndex = 13;
            this.LabelSystemInstance.Text = "Vehicle System Instance:";
            this.LabelSystemInstance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //LabelSystem
            //
            this.LabelSystem.AutoSize = true;
            this.LabelSystem.Location = new System.Drawing.Point(55, 91);
            this.LabelSystem.Name = "LabelSystem";
            this.LabelSystem.Size = new System.Drawing.Size(82, 13);
            this.LabelSystem.TabIndex = 14;
            this.LabelSystem.Text = "Vehicle System:";
            this.LabelSystem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //LabelFunction
            //
            this.LabelFunction.AutoSize = true;
            this.LabelFunction.Location = new System.Drawing.Point(86, 117);
            this.LabelFunction.Name = "LabelFunction";
            this.LabelFunction.Size = new System.Drawing.Size(51, 13);
            this.LabelFunction.TabIndex = 15;
            this.LabelFunction.Text = "Function:";
            this.LabelFunction.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //LabelFunctionInstance
            //
            this.LabelFunctionInstance.AutoSize = true;
            this.LabelFunctionInstance.Location = new System.Drawing.Point(42, 143);
            this.LabelFunctionInstance.Name = "LabelFunctionInstance";
            this.LabelFunctionInstance.Size = new System.Drawing.Size(95, 13);
            this.LabelFunctionInstance.TabIndex = 16;
            this.LabelFunctionInstance.Text = "Function Instance:";
            this.LabelFunctionInstance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //LabelECUInstance
            //
            this.LabelECUInstance.AutoSize = true;
            this.LabelECUInstance.Location = new System.Drawing.Point(61, 169);
            this.LabelECUInstance.Name = "LabelECUInstance";
            this.LabelECUInstance.Size = new System.Drawing.Size(76, 13);
            this.LabelECUInstance.TabIndex = 17;
            this.LabelECUInstance.Text = "ECU Instance:";
            this.LabelECUInstance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //LabelMfgCode
            //
            this.LabelMfgCode.AutoSize = true;
            this.LabelMfgCode.Location = new System.Drawing.Point(36, 195);
            this.LabelMfgCode.Name = "LabelMfgCode";
            this.LabelMfgCode.Size = new System.Drawing.Size(101, 13);
            this.LabelMfgCode.TabIndex = 18;
            this.LabelMfgCode.Text = "Manufacturer Code:";
            this.LabelMfgCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //LabelIdentityNo
            //
            this.LabelIdentityNo.AutoSize = true;
            this.LabelIdentityNo.Location = new System.Drawing.Point(53, 221);
            this.LabelIdentityNo.Name = "LabelIdentityNo";
            this.LabelIdentityNo.Size = new System.Drawing.Size(84, 13);
            this.LabelIdentityNo.TabIndex = 19;
            this.LabelIdentityNo.Text = "Identity Number:";
            this.LabelIdentityNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            //LabelIGSize
            //
            this.LabelIGSize.AutoSize = true;
            this.LabelIGSize.Enabled = false;
            this.LabelIGSize.Location = new System.Drawing.Point(225, 39);
            this.LabelIGSize.Name = "LabelIGSize";
            this.LabelIGSize.Size = new System.Drawing.Size(56, 13);
            this.LabelIGSize.TabIndex = 21;
            this.LabelIGSize.Text = "3 bits (0-7)";
            //
            //LabelVehSysInstSize
            //
            this.LabelVehSysInstSize.AutoSize = true;
            this.LabelVehSysInstSize.Enabled = false;
            this.LabelVehSysInstSize.Location = new System.Drawing.Point(225, 65);
            this.LabelVehSysInstSize.Name = "LabelVehSysInstSize";
            this.LabelVehSysInstSize.Size = new System.Drawing.Size(105, 13);
            this.LabelVehSysInstSize.TabIndex = 22;
            this.LabelVehSysInstSize.Text = "4 bits (0-15, 0x0-0xF)";
            //
            //LabelVehSysSize
            //
            this.LabelVehSysSize.AutoSize = true;
            this.LabelVehSysSize.Enabled = false;
            this.LabelVehSysSize.Location = new System.Drawing.Point(225, 91);
            this.LabelVehSysSize.Name = "LabelVehSysSize";
            this.LabelVehSysSize.Size = new System.Drawing.Size(123, 13);
            this.LabelVehSysSize.TabIndex = 23;
            this.LabelVehSysSize.Text = "7 bits (0-127, 0x00-0x7F)";
            //
            //LabelFuncSize
            //
            this.LabelFuncSize.AutoSize = true;
            this.LabelFuncSize.Enabled = false;
            this.LabelFuncSize.Location = new System.Drawing.Point(225, 117);
            this.LabelFuncSize.Name = "LabelFuncSize";
            this.LabelFuncSize.Size = new System.Drawing.Size(123, 13);
            this.LabelFuncSize.TabIndex = 24;
            this.LabelFuncSize.Text = "8 bits (0-255, 0x00-0xFF)";
            //
            //LabelFuncInstSize
            //
            this.LabelFuncInstSize.AutoSize = true;
            this.LabelFuncInstSize.Enabled = false;
            this.LabelFuncInstSize.Location = new System.Drawing.Point(225, 143);
            this.LabelFuncInstSize.Name = "LabelFuncInstSize";
            this.LabelFuncInstSize.Size = new System.Drawing.Size(117, 13);
            this.LabelFuncInstSize.TabIndex = 25;
            this.LabelFuncInstSize.Text = "5 bits (0-31, 0x00-0x1F)";
            //
            //LabelECUInstSize
            //
            this.LabelECUInstSize.AutoSize = true;
            this.LabelECUInstSize.Enabled = false;
            this.LabelECUInstSize.Location = new System.Drawing.Point(225, 169);
            this.LabelECUInstSize.Name = "LabelECUInstSize";
            this.LabelECUInstSize.Size = new System.Drawing.Size(56, 13);
            this.LabelECUInstSize.TabIndex = 26;
            this.LabelECUInstSize.Text = "3 bits (0-7)";
            //
            //LabelMfgCodeSize
            //
            this.LabelMfgCodeSize.AutoSize = true;
            this.LabelMfgCodeSize.Enabled = false;
            this.LabelMfgCodeSize.Location = new System.Drawing.Point(225, 195);
            this.LabelMfgCodeSize.Name = "LabelMfgCodeSize";
            this.LabelMfgCodeSize.Size = new System.Drawing.Size(147, 13);
            this.LabelMfgCodeSize.TabIndex = 27;
            this.LabelMfgCodeSize.Text = "11 bits (0-2047, 0x000-0x7FF)";
            //
            //LabelIdentSize
            //
            this.LabelIdentSize.AutoSize = true;
            this.LabelIdentSize.Enabled = false;
            this.LabelIdentSize.Location = new System.Drawing.Point(225, 221);
            this.LabelIdentSize.Name = "LabelIdentSize";
            this.LabelIdentSize.Size = new System.Drawing.Size(177, 13);
            this.LabelIdentSize.TabIndex = 28;
            this.LabelIdentSize.Text = "21 bits (0-2097151, 0x00-0x1FFFFF)";
            //
            //LabelResult
            //
            this.LabelResult.AutoSize = true;
            this.LabelResult.Location = new System.Drawing.Point(92, 257);
            this.LabelResult.Name = "LabelResult";
            this.LabelResult.Size = new System.Drawing.Size(212, 13);
            this.LabelResult.TabIndex = 29;
            this.LabelResult.Text = "0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00";
            //
            //FormProtectAddress
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ButtonCancel;
            this.ClientSize = new System.Drawing.Size(410, 331);
            this.Controls.Add(this.LabelResult);
            this.Controls.Add(this.LabelIdentSize);
            this.Controls.Add(this.LabelMfgCodeSize);
            this.Controls.Add(this.LabelECUInstSize);
            this.Controls.Add(this.LabelFuncInstSize);
            this.Controls.Add(this.LabelFuncSize);
            this.Controls.Add(this.LabelVehSysSize);
            this.Controls.Add(this.LabelVehSysInstSize);
            this.Controls.Add(this.LabelIGSize);
            this.Controls.Add(this.LabelIdentityNo);
            this.Controls.Add(this.LabelMfgCode);
            this.Controls.Add(this.LabelECUInstance);
            this.Controls.Add(this.LabelFunctionInstance);
            this.Controls.Add(this.LabelFunction);
            this.Controls.Add(this.LabelSystem);
            this.Controls.Add(this.LabelSystemInstance);
            this.Controls.Add(this.LabelIG);
            this.Controls.Add(this.TextBoxIdent);
            this.Controls.Add(this.TextBoxMfgCode);
            this.Controls.Add(this.TextBoxECUInst);
            this.Controls.Add(this.TextBoxFuncInst);
            this.Controls.Add(this.TextBoxFunc);
            this.Controls.Add(this.TextBoxVehSys);
            this.Controls.Add(this.TextBoxVehSysInst);
            this.Controls.Add(this.TextBoxIndGroup);
            this.Controls.Add(this.CheckBox1);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.ButtonSet);
            this.Controls.Add(this.TextBoxAddress);
            this.Controls.Add(this.LabelAddress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormProtectAddress";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "J1939 Address Claim";
            this.ResumeLayout(false);
            this.PerformLayout();

            TextBoxAddress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxAddress_KeyPress);
            TextBoxIndGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxAddress_KeyPress);
            TextBoxVehSysInst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxAddress_KeyPress);
            TextBoxVehSys.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxAddress_KeyPress);
            TextBoxFunc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxAddress_KeyPress);
            TextBoxFuncInst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxAddress_KeyPress);
            TextBoxECUInst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxAddress_KeyPress);
            TextBoxMfgCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxAddress_KeyPress);
            TextBoxIdent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxAddress_KeyPress);
            TextBoxAddress.TextChanged += new System.EventHandler(TextBoxAddress_TextChanged);
            TextBoxIndGroup.TextChanged += new System.EventHandler(TextBoxIndGroup_TextChanged);
            TextBoxVehSysInst.TextChanged += new System.EventHandler(TextBoxVehSysInst_TextChanged);
            TextBoxVehSys.TextChanged += new System.EventHandler(TextBoxVehSys_TextChanged);
            TextBoxFunc.TextChanged += new System.EventHandler(TextBoxFunc_TextChanged);
            TextBoxFuncInst.TextChanged += new System.EventHandler(TextBoxFuncInst_TextChanged);
            TextBoxECUInst.TextChanged += new System.EventHandler(TextBoxECUInst_TextChanged);
            TextBoxMfgCode.TextChanged += new System.EventHandler(TextBoxMfgCode_TextChanged);
            TextBoxIdent.TextChanged += new System.EventHandler(TextBoxIdent_TextChanged);
            CheckBox1.Click += new System.EventHandler(CheckBox1_Click);
        }

        private Label LabelAddress;
        private TextBox TextBoxAddress;
        private Button ButtonCancel;
        private Button ButtonSet;
        private CheckBox CheckBox1;
        private TextBox TextBoxIndGroup;
        private TextBox TextBoxVehSysInst;
        private TextBox TextBoxVehSys;
        private TextBox TextBoxFunc;
        private TextBox TextBoxFuncInst;
        private TextBox TextBoxECUInst;
        private TextBox TextBoxMfgCode;
        private TextBox TextBoxIdent;
        private Label LabelIG;
        private Label LabelSystemInstance;
        private Label LabelSystem;
        private Label LabelFunction;
        private Label LabelFunctionInstance;
        private Label LabelECUInstance;
        private Label LabelMfgCode;
        private Label LabelIdentityNo;
        private Label LabelIGSize;
        private Label LabelVehSysInstSize;
        private Label LabelVehSysSize;
        private Label LabelFuncSize;
        private Label LabelFuncInstSize;
        private Label LabelECUInstSize;
        private Label LabelMfgCodeSize;
        private Label LabelIdentSize;
        private Label LabelResult;

        private static FormProtectAddress _DefaultInstance;
        public static FormProtectAddress DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new FormProtectAddress();

                return _DefaultInstance;
            }
        }
    }

}