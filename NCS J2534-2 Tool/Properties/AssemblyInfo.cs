﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

// Review the values of the assembly attributes

[assembly: AssemblyTitle("NCS J2534-2 Tool")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("National Control Systems, Inc.")]
[assembly: AssemblyProduct("NCS J2534-2 Tool")]
[assembly: AssemblyCopyright("Copyright © 2017, 2018 National Control Systems, Inc.")]
[assembly: AssemblyTrademark("")]

[assembly: ComVisible(false)]

//The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("733e01ae-4fbf-4e78-8d91-95e8f1f8f924")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// <Assembly: AssemblyVersion("1.0.*")>

[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]


[assembly: AssemblyConfiguration("")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]
