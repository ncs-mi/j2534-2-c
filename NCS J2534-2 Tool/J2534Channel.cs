﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NCS_J2534_2_Tool.J2534ProtocolId;

namespace NCS_J2534_2_Tool
{
    public class J2534Channel
    {
        public uint ChannelId { get; set; }
        public bool Connected { get; set; }
        public uint BaudRate { get; set; }
        public J2534ConnectFlags ConnectFlags { get; set; }
        public bool MixedMode { get; set; }
        public uint MaxRXBufferSize { get; set; }
        public uint MaxPassFilter { get; set; }
        public uint MaxBlockFilter { get; set; }
        public uint MaxFlowFilter { get; set; }
        public uint MaxPeriodicMessage { get; set; }
        public uint FiveBaudMod { get; set; }
        public string Name { get; set; }
        public J2534ProtocolId ProtocolId { get; set; }
        public uint[] BaudRates { get; set; } = { };
        public int DefaultBaudRateIndex { get; set; }
        public J2534TxFlags DefaultTxFlags { get; set; }
        public J2534ConnectFlags DefaultConnectFlags { get; set; }
        public uint SelectedPins { get; set; }
        public uint Connector { get; set; }
        public uint[] J1962Pins { get; set; } = { };
        public uint[] J1939Pins { get; set; } = { };
        public uint[] J1708Pins { get; set; } = { };
        public J2534ConfigParameter[] ConfParameter { get; set; } = { };
        public J2534AnalogSubsystem AnalogSubsystem { get; set; }
        public J2534FunctionalMessage[] FunctionalMessage { get; set; }
        public J2534Filter[] Filter { get; set; }
        public J2534PeriodicMessage[] PeriodicMessage { get; set; }
        public bool PeriodicMessagesExist
        {
            get
            {
                bool result = false;
                if (PeriodicMessage == null)
                {
                    return result;
                }
                for (int i = 0; i <= J2534Constants.MAX_PM; i++)
                {
                    if (!PeriodicMessage[i].IsEmpty())
                    {
                        result = true;
                    }
                }
                return result;
            }
        }
        public bool FiltersExist
        {
            get
            {
                bool result = false;
                if (Filter == null)
                {
                    return result;
                }
                for (int i = 0; i <= J2534Constants.MAX_FILTER; i++)
                {
                    if (!(Filter[i].IsEmpty()))
                    {
                        result = true;
                    }
                }
                return result;
            }
        }
        public int ParameterIndex(J2534ConfigParameterId parameterId)
        {
            int result = -1;
            for (int i = 0; i < ConfParameter.Length; i++)
            {
                if (parameterId == ConfParameter[i].Id)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }
        public int NextFilter
        {
            get
            {
                int result = -1;
                if (IsAnalog)
                {
                    return result;
                }
                for (int i = 0; i <= J2534Constants.MAX_FILTER; i++)
                {
                    if (!Filter[i].Enabled)
                    {
                        if (Filter[i].PatternMessage.DataSize == 0 && Filter[i].MaskMessage.DataSize == 0 && Filter[i].FlowMessage.DataSize == 0 && Filter[i].FilterType == 0)
                        {
                            result = i;
                            break;
                        }
                    }
                }
                return result;
            }
        }
        public int NextPeriodicMessage
        {
            get
            {
                int result = -1;
                if (IsAnalog)
                {
                    return result;
                }
                for (int i = 0; i <= J2534Constants.MAX_PM; i++)
                {
                    if (!PeriodicMessage[i].Enabled)
                    {
                        if (PeriodicMessage[i].Message.DataSize == 0)
                        {
                            result = i;
                            break;
                        }
                    }
                }
                return result;
            }
        }
        public bool IsISO15765 { get; }
        public bool IsCAN { get; }
        public bool IsJ1708 { get; }
        public bool IsJ1939 { get; }
        public bool IsISO9141 { get; }
        public bool IsISO14230 { get; }
        public bool IsJ1850VPW { get; }
        public bool IsJ1850PWM { get; }
        public bool IsTP2_0 { get; }
        public bool IsSingleWire { get; }
        public bool IsFaultTolerant { get; }
        public bool IsPinSwitched { get; }
        public bool IsCHx { get; }
        public bool IsAnalog { get; }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return Name;
        }

        public J2534Channel(J2534ProtocolId id)
        {
            ProtocolId = id;
            Name = J2534Code.ProtocolIdToName(ProtocolId);
            if ((ProtocolId == J1850VPW) ||
                (ProtocolId == J1850VPW_PS) ||
                (ProtocolId >= J1850VPW_CH1 && ProtocolId <= J1850VPW_CH128))
            {
                BaudRates = J2534BaudRates.J1850VPW();
                DefaultBaudRateIndex = 0;
                ConfParameter = J2534ConfigParameters.J1850VPW();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
                IsJ1850VPW = true;
            }
            else if ((ProtocolId == ISO9141) ||
                     (ProtocolId == ISO9141_PS) ||
                     (ProtocolId >= ISO9141_CH1 && ProtocolId <= ISO9141_CH128))
            {
                BaudRates = J2534BaudRates.ISO9141_14230();
                DefaultBaudRateIndex = 5;
                ConfParameter = J2534ConfigParameters.ISO9141();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
                IsISO9141 = true;
            }
            else if ((ProtocolId == ISO14230) ||
                     (ProtocolId == ISO14230_PS) ||
                     (ProtocolId >= ISO14230_CH1 && ProtocolId <= ISO14230_CH128))
            {
                BaudRates = J2534BaudRates.ISO9141_14230();
                DefaultBaudRateIndex = 5;
                ConfParameter = J2534ConfigParameters.ISO14230();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
                IsISO14230 = true;
            }
            else if ((ProtocolId == J1850PWM) ||
                     (ProtocolId == J1850PWM_PS) ||
                     (ProtocolId >= J1850PWM_CH1 && ProtocolId <= J1850PWM_CH128))
            {
                BaudRates = J2534BaudRates.J1850PWM();
                DefaultBaudRateIndex = 0;
                ConfParameter = J2534ConfigParameters.J1850PWM();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
                IsJ1850PWM = true;
                FunctionalMessage = new NCS_J2534_2_Tool.J2534FunctionalMessage[J2534Constants.MAX_FUNC_MSG + 1];
                for (int i = 0; i <= J2534Constants.MAX_FUNC_MSG; i++)
                {
                    FunctionalMessage[i] = new J2534FunctionalMessage();
                }
            }
            else if ((ProtocolId == CAN) ||
                     (ProtocolId == CAN_PS) ||
                     (ProtocolId >= CAN_CH1 && ProtocolId <= CAN_CH128))
            {
                BaudRates = J2534BaudRates.CAN_ISO();
                DefaultBaudRateIndex = 7;
                ConfParameter = J2534ConfigParameters.CAN();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
                IsCAN = true;
            }
            else if ((ProtocolId == SW_CAN_PS) ||
                     (ProtocolId >= SW_CAN_CAN_CH1 && ProtocolId <= SW_CAN_CAN_CH128))
            {
                BaudRates = J2534BaudRates.SW_CAN_ISO();
                DefaultBaudRateIndex = 1;
                ConfParameter = J2534ConfigParameters.SW_CAN();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
                IsCAN = true;
                IsSingleWire = true;
            }
            else if ((ProtocolId == FT_CAN_PS) ||
                     (ProtocolId >= FT_CAN_CH1 && ProtocolId <= FT_CAN_CH128))
            {
                BaudRates = J2534BaudRates.FT_CAN_ISO();
                DefaultBaudRateIndex = 2;
                ConfParameter = J2534ConfigParameters.FT_CAN();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
                IsCAN = true;
                IsFaultTolerant = true;
            }
            else if ((ProtocolId == ISO15765) ||
                     (ProtocolId == ISO15765_PS) ||
                     (ProtocolId >= ISO15765_CH1 && ProtocolId <= ISO15765_CH128))
            {
                BaudRates = J2534BaudRates.CAN_ISO();
                DefaultBaudRateIndex = 7;
                ConfParameter = J2534ConfigParameters.ISO15765();
                DefaultTxFlags = J2534TxFlags.ISO15765_FRAME_PAD;
                DefaultConnectFlags = 0x0;
                IsISO15765 = true;
            }
            else if ((ProtocolId == SW_ISO15765_PS) ||
                     (ProtocolId >= SW_CAN_ISO15765_CH1 && ProtocolId <= SW_CAN_ISO15765_CH128))
            {
                BaudRates = J2534BaudRates.SW_CAN_ISO();
                DefaultBaudRateIndex = 1;
                ConfParameter = J2534ConfigParameters.SW_ISO15765();
                DefaultTxFlags = J2534TxFlags.ISO15765_FRAME_PAD;
                DefaultConnectFlags = 0x0;
                IsISO15765 = true;
                IsSingleWire = true;
            }
            else if ((ProtocolId == FT_ISO15765_PS) ||
                     (ProtocolId >= FT_ISO15765_CH1 && ProtocolId <= FT_ISO15765_CH128))
            {
                BaudRates = J2534BaudRates.FT_CAN_ISO();
                DefaultBaudRateIndex = 2;
                ConfParameter = J2534ConfigParameters.FT_ISO15765();
                DefaultTxFlags = J2534TxFlags.ISO15765_FRAME_PAD;
                DefaultConnectFlags = 0x0;
                IsISO15765 = true;
                IsFaultTolerant = true;
            }
            else if ((ProtocolId == SCI_A_ENGINE) ||
                     (ProtocolId == SCI_A_TRANS) ||
                     (ProtocolId == SCI_B_ENGINE) ||
                     (ProtocolId == SCI_B_TRANS))
            {
                BaudRates = J2534BaudRates.J2610();
                DefaultBaudRateIndex = 2;
                ConfParameter = J2534ConfigParameters.J2610();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
            }
            else if ((ProtocolId == J2610_PS) ||
                     (ProtocolId >= J2610_CH1 && ProtocolId <= J2610_CH128))
            {
                BaudRates = J2534BaudRates.J2610();
                DefaultBaudRateIndex = 2;
                ConfParameter = J2534ConfigParameters.J2610();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
            }
            else if ((ProtocolId == GM_UART_PS) ||
                     (ProtocolId >= GM_UART_CH1 && ProtocolId <= GM_UART_CH128))
            {
                BaudRates = J2534BaudRates.GM_UART();
                DefaultBaudRateIndex = 0;
                ConfParameter = J2534ConfigParameters.GM_UART();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
            }
            else if ((ProtocolId == UART_ECHO_BYTE_PS) ||
                     (ProtocolId >= ECHO_BYTE_CH1 && ProtocolId <= ECHO_BYTE_CH128))
            {
                BaudRates = J2534BaudRates.UART_ECHO_BYTE();
                DefaultBaudRateIndex = 0;
                ConfParameter = J2534ConfigParameters.UART_ECHO_BYTE();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
            }
            else if ((ProtocolId == HONDA_DIAGH_PS) ||
                     (ProtocolId >= HONDA_DIAGH_CH1 && ProtocolId <= HONDA_DIAGH_CH128))
            {
                BaudRates = J2534BaudRates.HONDA_DIAGH();
                DefaultBaudRateIndex = 0;
                ConfParameter = J2534ConfigParameters.HONDA_DIAGH();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
            }
            else if ((ProtocolId == J1939_PS) ||
                     (ProtocolId >= J1939_CH1 && ProtocolId <= J1939_CH128))
            {
                BaudRates = J2534BaudRates.J1939();
                DefaultBaudRateIndex = 1;
                ConfParameter = J2534ConfigParameters.J1939();
                DefaultTxFlags = J2534TxFlags.CAN_29BIT_ID;
                DefaultConnectFlags = J2534ConnectFlags.CAN_29BIT_ID;
                IsJ1939 = true;
            }
            else if ((ProtocolId == J1708_PS) ||
                     (ProtocolId >= J1708_CH1 && ProtocolId <= J1708_CH128))
            {
                BaudRates = J2534BaudRates.J1708();
                DefaultBaudRateIndex = 0;
                ConfParameter = J2534ConfigParameters.J1708();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
                IsJ1708 = true;
            }
            else if ((ProtocolId == TP2_0_PS) ||
                     (ProtocolId >= TP2_0_CH1 && ProtocolId <= TP2_0_CH128))
            {
                BaudRates = J2534BaudRates.TP2_0();
                DefaultBaudRateIndex = 0;
                ConfParameter = J2534ConfigParameters.TP2_0();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
                IsTP2_0 = true;
            }
            else if (ProtocolId >= ANALOG_IN_1 && ProtocolId <= ANALOG_IN_32)
            {
                BaudRates = new uint[0];
                DefaultBaudRateIndex = 0;
                ConfParameter = J2534ConfigParameters.Analog();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
                IsAnalog = true;
            }
            else if (ProtocolId == LIN)
            {
                BaudRates = J2534BaudRates.LIN();
                DefaultBaudRateIndex = 0;
                ConfParameter = J2534ConfigParameters.LIN();
                DefaultTxFlags = 0x0;
                DefaultConnectFlags = 0x0;
            }

            if (ProtocolId >= J1850VPW_PS && ProtocolId <= FT_ISO15765_PS)
            {
                IsPinSwitched = true;
            }
            else if (ProtocolId >= CAN_CH1 && ProtocolId <= TP2_0_CH128)
            {
                IsCHx = true;
            }

            Filter = new J2534Filter[J2534Constants.MAX_FILTER + 1];
            for (int i = 0; i <= J2534Constants.MAX_FILTER; i++)
            {
                Filter[i] = new J2534Filter();
                Filter[i].FlowMessage.TxFlags = DefaultTxFlags;
            }

            PeriodicMessage = new J2534PeriodicMessage[J2534Constants.MAX_PM + 1];
            for (int i = 0; i <= J2534Constants.MAX_PM; i++)
            {
                PeriodicMessage[i] = new J2534PeriodicMessage();
                PeriodicMessage[i].Message.TxFlags = DefaultTxFlags;
            }

        }
    }
}
