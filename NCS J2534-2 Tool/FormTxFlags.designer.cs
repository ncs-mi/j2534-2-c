﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901


using System.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class FormTxFlags : System.Windows.Forms.Form
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTxFlags));
            this.TextBoxFlags = new System.Windows.Forms.TextBox();
            this.Cancel_Button = new System.Windows.Forms.Button();
            this.CheckBoxFlag0 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag1 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag2 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag3 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag4 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag5 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag6 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag7 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag8 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag9 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag10 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag11 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag12 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag13 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag14 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag15 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag16 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag17 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag18 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag19 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag20 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag21 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag22 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag23 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag24 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag25 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag26 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag27 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag28 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag29 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag30 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFlag31 = new System.Windows.Forms.CheckBox();
            this.SetButton = new System.Windows.Forms.Button();
            this.LabelDesc = new System.Windows.Forms.Label();
            this.ContextMenuStripFlags = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuFlagsUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsSpace1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuFlagsCut = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsSpace2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuFlagsSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsSpace3 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuFlagClear = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagDefault = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenuStripFlags.SuspendLayout();
            this.SuspendLayout();
            //
            //TextBoxFlags
            //
            this.TextBoxFlags.AcceptsReturn = true;
            this.TextBoxFlags.Location = new System.Drawing.Point(312, 288);
            this.TextBoxFlags.MaxLength = 0;
            this.TextBoxFlags.Name = "TextBoxFlags";
            this.TextBoxFlags.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFlags.TabIndex = 35;
            //
            //Cancel_Button
            //
            this.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel_Button.Location = new System.Drawing.Point(367, 312);
            this.Cancel_Button.Name = "Cancel_Button";
            this.Cancel_Button.Size = new System.Drawing.Size(65, 25);
            this.Cancel_Button.TabIndex = 0;
            this.Cancel_Button.Text = "Cancel";
            this.Cancel_Button.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag0
            //
            this.CheckBoxFlag0.AutoSize = true;
            this.CheckBoxFlag0.Location = new System.Drawing.Point(8, 24);
            this.CheckBoxFlag0.Name = "CheckBoxFlag0";
            this.CheckBoxFlag0.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag0.TabIndex = 2;
            this.CheckBoxFlag0.Text = "CheckBoxFlag(0)";
            this.CheckBoxFlag0.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag1
            //
            this.CheckBoxFlag1.AutoSize = true;
            this.CheckBoxFlag1.Location = new System.Drawing.Point(8, 40);
            this.CheckBoxFlag1.Name = "CheckBoxFlag1";
            this.CheckBoxFlag1.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag1.TabIndex = 3;
            this.CheckBoxFlag1.Text = "CheckBoxFlag(1)";
            this.CheckBoxFlag1.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag2
            //
            this.CheckBoxFlag2.AutoSize = true;
            this.CheckBoxFlag2.Location = new System.Drawing.Point(8, 56);
            this.CheckBoxFlag2.Name = "CheckBoxFlag2";
            this.CheckBoxFlag2.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag2.TabIndex = 4;
            this.CheckBoxFlag2.Text = "CheckBoxFlag(2)";
            this.CheckBoxFlag2.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag3
            //
            this.CheckBoxFlag3.AutoSize = true;
            this.CheckBoxFlag3.Location = new System.Drawing.Point(8, 72);
            this.CheckBoxFlag3.Name = "CheckBoxFlag3";
            this.CheckBoxFlag3.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag3.TabIndex = 5;
            this.CheckBoxFlag3.Text = "CheckBoxFlag(3)";
            this.CheckBoxFlag3.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag4
            //
            this.CheckBoxFlag4.AutoSize = true;
            this.CheckBoxFlag4.Location = new System.Drawing.Point(8, 88);
            this.CheckBoxFlag4.Name = "CheckBoxFlag4";
            this.CheckBoxFlag4.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag4.TabIndex = 6;
            this.CheckBoxFlag4.Text = "CheckBoxFlag(4)";
            this.CheckBoxFlag4.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag5
            //
            this.CheckBoxFlag5.AutoSize = true;
            this.CheckBoxFlag5.Location = new System.Drawing.Point(8, 104);
            this.CheckBoxFlag5.Name = "CheckBoxFlag5";
            this.CheckBoxFlag5.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag5.TabIndex = 7;
            this.CheckBoxFlag5.Text = "CheckBoxFlag(5)";
            this.CheckBoxFlag5.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag6
            //
            this.CheckBoxFlag6.AutoSize = true;
            this.CheckBoxFlag6.Location = new System.Drawing.Point(8, 120);
            this.CheckBoxFlag6.Name = "CheckBoxFlag6";
            this.CheckBoxFlag6.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag6.TabIndex = 8;
            this.CheckBoxFlag6.Text = "CheckBoxFlag(6)";
            this.CheckBoxFlag6.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag7
            //
            this.CheckBoxFlag7.AutoSize = true;
            this.CheckBoxFlag7.Location = new System.Drawing.Point(8, 136);
            this.CheckBoxFlag7.Name = "CheckBoxFlag7";
            this.CheckBoxFlag7.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag7.TabIndex = 9;
            this.CheckBoxFlag7.Text = "CheckBoxFlag(7)";
            this.CheckBoxFlag7.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag8
            //
            this.CheckBoxFlag8.AutoSize = true;
            this.CheckBoxFlag8.Location = new System.Drawing.Point(8, 152);
            this.CheckBoxFlag8.Name = "CheckBoxFlag8";
            this.CheckBoxFlag8.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag8.TabIndex = 10;
            this.CheckBoxFlag8.Text = "CheckBoxFlag(8)";
            this.CheckBoxFlag8.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag9
            //
            this.CheckBoxFlag9.AutoSize = true;
            this.CheckBoxFlag9.Location = new System.Drawing.Point(8, 168);
            this.CheckBoxFlag9.Name = "CheckBoxFlag9";
            this.CheckBoxFlag9.Size = new System.Drawing.Size(107, 17);
            this.CheckBoxFlag9.TabIndex = 11;
            this.CheckBoxFlag9.Text = "CheckBoxFlag(9)";
            this.CheckBoxFlag9.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag10
            //
            this.CheckBoxFlag10.AutoSize = true;
            this.CheckBoxFlag10.Location = new System.Drawing.Point(8, 184);
            this.CheckBoxFlag10.Name = "CheckBoxFlag10";
            this.CheckBoxFlag10.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag10.TabIndex = 12;
            this.CheckBoxFlag10.Text = "CheckBoxFlag(10)";
            this.CheckBoxFlag10.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag11
            //
            this.CheckBoxFlag11.AutoSize = true;
            this.CheckBoxFlag11.Location = new System.Drawing.Point(8, 200);
            this.CheckBoxFlag11.Name = "CheckBoxFlag11";
            this.CheckBoxFlag11.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag11.TabIndex = 13;
            this.CheckBoxFlag11.Text = "CheckBoxFlag(11)";
            this.CheckBoxFlag11.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag12
            //
            this.CheckBoxFlag12.AutoSize = true;
            this.CheckBoxFlag12.Location = new System.Drawing.Point(8, 216);
            this.CheckBoxFlag12.Name = "CheckBoxFlag12";
            this.CheckBoxFlag12.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag12.TabIndex = 14;
            this.CheckBoxFlag12.Text = "CheckBoxFlag(12)";
            this.CheckBoxFlag12.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag13
            //
            this.CheckBoxFlag13.AutoSize = true;
            this.CheckBoxFlag13.Location = new System.Drawing.Point(8, 232);
            this.CheckBoxFlag13.Name = "CheckBoxFlag13";
            this.CheckBoxFlag13.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag13.TabIndex = 15;
            this.CheckBoxFlag13.Text = "CheckBoxFlag(13)";
            this.CheckBoxFlag13.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag14
            //
            this.CheckBoxFlag14.AutoSize = true;
            this.CheckBoxFlag14.Location = new System.Drawing.Point(8, 248);
            this.CheckBoxFlag14.Name = "CheckBoxFlag14";
            this.CheckBoxFlag14.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag14.TabIndex = 16;
            this.CheckBoxFlag14.Text = "CheckBoxFlag(14)";
            this.CheckBoxFlag14.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag15
            //
            this.CheckBoxFlag15.AutoSize = true;
            this.CheckBoxFlag15.Location = new System.Drawing.Point(8, 264);
            this.CheckBoxFlag15.Name = "CheckBoxFlag15";
            this.CheckBoxFlag15.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag15.TabIndex = 17;
            this.CheckBoxFlag15.Text = "CheckBoxFlag(15)";
            this.CheckBoxFlag15.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag16
            //
            this.CheckBoxFlag16.AutoSize = true;
            this.CheckBoxFlag16.Location = new System.Drawing.Point(228, 24);
            this.CheckBoxFlag16.Name = "CheckBoxFlag16";
            this.CheckBoxFlag16.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag16.TabIndex = 18;
            this.CheckBoxFlag16.Text = "CheckBoxFlag(16)";
            this.CheckBoxFlag16.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag17
            //
            this.CheckBoxFlag17.AutoSize = true;
            this.CheckBoxFlag17.Location = new System.Drawing.Point(228, 40);
            this.CheckBoxFlag17.Name = "CheckBoxFlag17";
            this.CheckBoxFlag17.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag17.TabIndex = 19;
            this.CheckBoxFlag17.Text = "CheckBoxFlag(17)";
            this.CheckBoxFlag17.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag18
            //
            this.CheckBoxFlag18.AutoSize = true;
            this.CheckBoxFlag18.Location = new System.Drawing.Point(228, 56);
            this.CheckBoxFlag18.Name = "CheckBoxFlag18";
            this.CheckBoxFlag18.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag18.TabIndex = 20;
            this.CheckBoxFlag18.Text = "CheckBoxFlag(18)";
            this.CheckBoxFlag18.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag19
            //
            this.CheckBoxFlag19.AutoSize = true;
            this.CheckBoxFlag19.Location = new System.Drawing.Point(228, 72);
            this.CheckBoxFlag19.Name = "CheckBoxFlag19";
            this.CheckBoxFlag19.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag19.TabIndex = 21;
            this.CheckBoxFlag19.Text = "CheckBoxFlag(19)";
            this.CheckBoxFlag19.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag20
            //
            this.CheckBoxFlag20.AutoSize = true;
            this.CheckBoxFlag20.Location = new System.Drawing.Point(228, 88);
            this.CheckBoxFlag20.Name = "CheckBoxFlag20";
            this.CheckBoxFlag20.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag20.TabIndex = 22;
            this.CheckBoxFlag20.Text = "CheckBoxFlag(20)";
            this.CheckBoxFlag20.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag21
            //
            this.CheckBoxFlag21.AutoSize = true;
            this.CheckBoxFlag21.Location = new System.Drawing.Point(228, 104);
            this.CheckBoxFlag21.Name = "CheckBoxFlag21";
            this.CheckBoxFlag21.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag21.TabIndex = 23;
            this.CheckBoxFlag21.Text = "CheckBoxFlag(21)";
            this.CheckBoxFlag21.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag22
            //
            this.CheckBoxFlag22.AutoSize = true;
            this.CheckBoxFlag22.Location = new System.Drawing.Point(228, 120);
            this.CheckBoxFlag22.Name = "CheckBoxFlag22";
            this.CheckBoxFlag22.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag22.TabIndex = 24;
            this.CheckBoxFlag22.Text = "CheckBoxFlag(22)";
            this.CheckBoxFlag22.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag23
            //
            this.CheckBoxFlag23.AutoSize = true;
            this.CheckBoxFlag23.Location = new System.Drawing.Point(228, 136);
            this.CheckBoxFlag23.Name = "CheckBoxFlag23";
            this.CheckBoxFlag23.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag23.TabIndex = 25;
            this.CheckBoxFlag23.Text = "CheckBoxFlag(23)";
            this.CheckBoxFlag23.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag24
            //
            this.CheckBoxFlag24.AutoSize = true;
            this.CheckBoxFlag24.Location = new System.Drawing.Point(228, 152);
            this.CheckBoxFlag24.Name = "CheckBoxFlag24";
            this.CheckBoxFlag24.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag24.TabIndex = 26;
            this.CheckBoxFlag24.Text = "CheckBoxFlag(24)";
            this.CheckBoxFlag24.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag25
            //
            this.CheckBoxFlag25.AutoSize = true;
            this.CheckBoxFlag25.Location = new System.Drawing.Point(228, 168);
            this.CheckBoxFlag25.Name = "CheckBoxFlag25";
            this.CheckBoxFlag25.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag25.TabIndex = 27;
            this.CheckBoxFlag25.Text = "CheckBoxFlag(25)";
            this.CheckBoxFlag25.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag26
            //
            this.CheckBoxFlag26.AutoSize = true;
            this.CheckBoxFlag26.Location = new System.Drawing.Point(228, 184);
            this.CheckBoxFlag26.Name = "CheckBoxFlag26";
            this.CheckBoxFlag26.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag26.TabIndex = 28;
            this.CheckBoxFlag26.Text = "CheckBoxFlag(26)";
            this.CheckBoxFlag26.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag27
            //
            this.CheckBoxFlag27.AutoSize = true;
            this.CheckBoxFlag27.Location = new System.Drawing.Point(228, 200);
            this.CheckBoxFlag27.Name = "CheckBoxFlag27";
            this.CheckBoxFlag27.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag27.TabIndex = 29;
            this.CheckBoxFlag27.Text = "CheckBoxFlag(27)";
            this.CheckBoxFlag27.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag28
            //
            this.CheckBoxFlag28.AutoSize = true;
            this.CheckBoxFlag28.Location = new System.Drawing.Point(228, 216);
            this.CheckBoxFlag28.Name = "CheckBoxFlag28";
            this.CheckBoxFlag28.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag28.TabIndex = 30;
            this.CheckBoxFlag28.Text = "CheckBoxFlag(28)";
            this.CheckBoxFlag28.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag29
            //
            this.CheckBoxFlag29.AutoSize = true;
            this.CheckBoxFlag29.Location = new System.Drawing.Point(228, 232);
            this.CheckBoxFlag29.Name = "CheckBoxFlag29";
            this.CheckBoxFlag29.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag29.TabIndex = 31;
            this.CheckBoxFlag29.Text = "CheckBoxFlag(29)";
            this.CheckBoxFlag29.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag30
            //
            this.CheckBoxFlag30.AutoSize = true;
            this.CheckBoxFlag30.Location = new System.Drawing.Point(228, 248);
            this.CheckBoxFlag30.Name = "CheckBoxFlag30";
            this.CheckBoxFlag30.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag30.TabIndex = 32;
            this.CheckBoxFlag30.Text = "CheckBoxFlag(30)";
            this.CheckBoxFlag30.UseVisualStyleBackColor = true;
            //
            //CheckBoxFlag31
            //
            this.CheckBoxFlag31.AutoSize = true;
            this.CheckBoxFlag31.Location = new System.Drawing.Point(228, 264);
            this.CheckBoxFlag31.Name = "CheckBoxFlag31";
            this.CheckBoxFlag31.Size = new System.Drawing.Size(113, 17);
            this.CheckBoxFlag31.TabIndex = 33;
            this.CheckBoxFlag31.Text = "CheckBoxFlag(31)";
            this.CheckBoxFlag31.UseVisualStyleBackColor = true;
            //
            //SetButton
            //
            this.SetButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SetButton.Location = new System.Drawing.Point(312, 312);
            this.SetButton.Name = "SetButton";
            this.SetButton.Size = new System.Drawing.Size(49, 25);
            this.SetButton.TabIndex = 1;
            this.SetButton.Text = "Set";
            this.SetButton.UseVisualStyleBackColor = true;
            //
            //LabelDesc
            //
            this.LabelDesc.BackColor = System.Drawing.SystemColors.Window;
            this.LabelDesc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelDesc.Location = new System.Drawing.Point(8, 288);
            this.LabelDesc.Name = "LabelDesc";
            this.LabelDesc.Size = new System.Drawing.Size(298, 57);
            this.LabelDesc.TabIndex = 34;
            //
            //ContextMenuStripFlags
            //
            this.ContextMenuStripFlags.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuFlagsUndo, this.MenuFlagsSpace1, this.MenuFlagsCut, this.MenuFlagsCopy, this.MenuFlagsPaste, this.MenuFlagsSpace2, this.MenuFlagsSelectAll, this.MenuFlagsSpace3, this.MenuFlagClear, this.MenuFlagDefault});
            this.ContextMenuStripFlags.Name = "ContextMenuStripFlags";
            this.ContextMenuStripFlags.Size = new System.Drawing.Size(162, 176);
            //
            //MenuFlagsUndo
            //
            this.MenuFlagsUndo.Name = "MenuFlagsUndo";
            this.MenuFlagsUndo.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsUndo.Text = "Undo";
            //
            //MenuFlagsSpace1
            //
            this.MenuFlagsSpace1.Name = "MenuFlagsSpace1";
            this.MenuFlagsSpace1.Size = new System.Drawing.Size(158, 6);
            //
            //MenuFlagsCut
            //
            this.MenuFlagsCut.Name = "MenuFlagsCut";
            this.MenuFlagsCut.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsCut.Text = "Cut";
            //
            //MenuFlagsCopy
            //
            this.MenuFlagsCopy.Name = "MenuFlagsCopy";
            this.MenuFlagsCopy.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsCopy.Text = "Copy";
            //
            //MenuFlagsPaste
            //
            this.MenuFlagsPaste.Name = "MenuFlagsPaste";
            this.MenuFlagsPaste.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsPaste.Text = "Paste";
            //
            //MenuFlagsSpace2
            //
            this.MenuFlagsSpace2.Name = "MenuFlagsSpace2";
            this.MenuFlagsSpace2.Size = new System.Drawing.Size(158, 6);
            //
            //MenuFlagsSelectAll
            //
            this.MenuFlagsSelectAll.Name = "MenuFlagsSelectAll";
            this.MenuFlagsSelectAll.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsSelectAll.Text = "Select All";
            //
            //MenuFlagsSpace3
            //
            this.MenuFlagsSpace3.Name = "MenuFlagsSpace3";
            this.MenuFlagsSpace3.Size = new System.Drawing.Size(158, 6);
            //
            //MenuFlagClear
            //
            this.MenuFlagClear.Name = "MenuFlagClear";
            this.MenuFlagClear.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagClear.Text = "Clear Flags";
            //
            //MenuFlagDefault
            //
            this.MenuFlagDefault.Name = "MenuFlagDefault";
            this.MenuFlagDefault.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagDefault.Text = "Set Default Flags";
            //
            //FormTxFlags
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancel_Button;
            this.ClientSize = new System.Drawing.Size(444, 352);
            this.Controls.Add(this.TextBoxFlags);
            this.Controls.Add(this.Cancel_Button);
            this.Controls.Add(this.CheckBoxFlag0);
            this.Controls.Add(this.CheckBoxFlag1);
            this.Controls.Add(this.CheckBoxFlag2);
            this.Controls.Add(this.CheckBoxFlag3);
            this.Controls.Add(this.CheckBoxFlag4);
            this.Controls.Add(this.CheckBoxFlag5);
            this.Controls.Add(this.CheckBoxFlag6);
            this.Controls.Add(this.CheckBoxFlag7);
            this.Controls.Add(this.CheckBoxFlag8);
            this.Controls.Add(this.CheckBoxFlag9);
            this.Controls.Add(this.CheckBoxFlag10);
            this.Controls.Add(this.CheckBoxFlag11);
            this.Controls.Add(this.CheckBoxFlag12);
            this.Controls.Add(this.CheckBoxFlag13);
            this.Controls.Add(this.CheckBoxFlag14);
            this.Controls.Add(this.CheckBoxFlag15);
            this.Controls.Add(this.CheckBoxFlag16);
            this.Controls.Add(this.CheckBoxFlag17);
            this.Controls.Add(this.CheckBoxFlag18);
            this.Controls.Add(this.CheckBoxFlag19);
            this.Controls.Add(this.CheckBoxFlag20);
            this.Controls.Add(this.CheckBoxFlag21);
            this.Controls.Add(this.CheckBoxFlag22);
            this.Controls.Add(this.CheckBoxFlag23);
            this.Controls.Add(this.CheckBoxFlag24);
            this.Controls.Add(this.CheckBoxFlag25);
            this.Controls.Add(this.CheckBoxFlag26);
            this.Controls.Add(this.CheckBoxFlag27);
            this.Controls.Add(this.CheckBoxFlag28);
            this.Controls.Add(this.CheckBoxFlag29);
            this.Controls.Add(this.CheckBoxFlag30);
            this.Controls.Add(this.CheckBoxFlag31);
            this.Controls.Add(this.SetButton);
            this.Controls.Add(this.LabelDesc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormTxFlags";
            this.Text = "TxFlags";
            this.ContextMenuStripFlags.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

            TextBoxFlags.TextChanged += new System.EventHandler(TextBoxFlags_TextChanged);
            TextBoxFlags.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxFlags_KeyPress);
            ContextMenuStripFlags.Opening += new System.ComponentModel.CancelEventHandler(ContextMenuStripFlags_Opening);
            MenuFlagClear.Click += new System.EventHandler(MenuFlagClear_Click);
            MenuFlagDefault.Click += new System.EventHandler(MenuFlagDefault_Click);
            MenuFlagsCopy.Click += new System.EventHandler(MenuFlagsCopy_Click);
            MenuFlagsCut.Click += new System.EventHandler(MenuFlagsCut_Click);
            MenuFlagsPaste.Click += new System.EventHandler(MenuFlagsPaste_Click);
            MenuFlagsSelectAll.Click += new System.EventHandler(MenuFlagsSelectAll_Click);
            MenuFlagsUndo.Click += new System.EventHandler(MenuFlagsUndo_Click);
        }

        public void InitializeCheckBoxFlag()
        {
            CheckBoxFlag = new System.Windows.Forms.CheckBox[32];
            this.CheckBoxFlag[0] = CheckBoxFlag0;
            this.CheckBoxFlag[1] = CheckBoxFlag1;
            this.CheckBoxFlag[2] = CheckBoxFlag2;
            this.CheckBoxFlag[3] = CheckBoxFlag3;
            this.CheckBoxFlag[4] = CheckBoxFlag4;
            this.CheckBoxFlag[5] = CheckBoxFlag5;
            this.CheckBoxFlag[6] = CheckBoxFlag6;
            this.CheckBoxFlag[7] = CheckBoxFlag7;
            this.CheckBoxFlag[8] = CheckBoxFlag8;
            this.CheckBoxFlag[9] = CheckBoxFlag9;
            this.CheckBoxFlag[10] = CheckBoxFlag10;
            this.CheckBoxFlag[11] = CheckBoxFlag11;
            this.CheckBoxFlag[12] = CheckBoxFlag12;
            this.CheckBoxFlag[13] = CheckBoxFlag13;
            this.CheckBoxFlag[14] = CheckBoxFlag14;
            this.CheckBoxFlag[15] = CheckBoxFlag15;
            this.CheckBoxFlag[16] = CheckBoxFlag16;
            this.CheckBoxFlag[17] = CheckBoxFlag17;
            this.CheckBoxFlag[18] = CheckBoxFlag18;
            this.CheckBoxFlag[19] = CheckBoxFlag19;
            this.CheckBoxFlag[20] = CheckBoxFlag20;
            this.CheckBoxFlag[21] = CheckBoxFlag21;
            this.CheckBoxFlag[22] = CheckBoxFlag22;
            this.CheckBoxFlag[23] = CheckBoxFlag23;
            this.CheckBoxFlag[24] = CheckBoxFlag24;
            this.CheckBoxFlag[25] = CheckBoxFlag25;
            this.CheckBoxFlag[26] = CheckBoxFlag26;
            this.CheckBoxFlag[27] = CheckBoxFlag27;
            this.CheckBoxFlag[28] = CheckBoxFlag28;
            this.CheckBoxFlag[29] = CheckBoxFlag29;
            this.CheckBoxFlag[30] = CheckBoxFlag30;
            this.CheckBoxFlag[31] = CheckBoxFlag31;
        }

        private System.Windows.Forms.TextBox TextBoxFlags;
        private System.Windows.Forms.Button Cancel_Button;
        private System.Windows.Forms.CheckBox[] CheckBoxFlag;
        private System.Windows.Forms.CheckBox CheckBoxFlag0;
        private System.Windows.Forms.CheckBox CheckBoxFlag1;
        private System.Windows.Forms.CheckBox CheckBoxFlag2;
        private System.Windows.Forms.CheckBox CheckBoxFlag3;
        private System.Windows.Forms.CheckBox CheckBoxFlag4;
        private System.Windows.Forms.CheckBox CheckBoxFlag5;
        private System.Windows.Forms.CheckBox CheckBoxFlag6;
        private System.Windows.Forms.CheckBox CheckBoxFlag7;
        private System.Windows.Forms.CheckBox CheckBoxFlag8;
        private System.Windows.Forms.CheckBox CheckBoxFlag9;
        private System.Windows.Forms.CheckBox CheckBoxFlag10;
        private System.Windows.Forms.CheckBox CheckBoxFlag11;
        private System.Windows.Forms.CheckBox CheckBoxFlag12;
        private System.Windows.Forms.CheckBox CheckBoxFlag13;
        private System.Windows.Forms.CheckBox CheckBoxFlag14;
        private System.Windows.Forms.CheckBox CheckBoxFlag15;
        private System.Windows.Forms.CheckBox CheckBoxFlag16;
        private System.Windows.Forms.CheckBox CheckBoxFlag17;
        private System.Windows.Forms.CheckBox CheckBoxFlag18;
        private System.Windows.Forms.CheckBox CheckBoxFlag19;
        private System.Windows.Forms.CheckBox CheckBoxFlag20;
        private System.Windows.Forms.CheckBox CheckBoxFlag21;
        private System.Windows.Forms.CheckBox CheckBoxFlag22;
        private System.Windows.Forms.CheckBox CheckBoxFlag23;
        private System.Windows.Forms.CheckBox CheckBoxFlag24;
        private System.Windows.Forms.CheckBox CheckBoxFlag25;
        private System.Windows.Forms.CheckBox CheckBoxFlag26;
        private System.Windows.Forms.CheckBox CheckBoxFlag27;
        private System.Windows.Forms.CheckBox CheckBoxFlag28;
        private System.Windows.Forms.CheckBox CheckBoxFlag29;
        private System.Windows.Forms.CheckBox CheckBoxFlag30;
        private System.Windows.Forms.CheckBox CheckBoxFlag31;
        private System.Windows.Forms.Label LabelDesc;
        private System.Windows.Forms.Button SetButton;
        private ContextMenuStrip ContextMenuStripFlags;
        private ToolStripMenuItem MenuFlagsUndo;
        private ToolStripSeparator MenuFlagsSpace1;
        private ToolStripMenuItem MenuFlagsCut;
        private ToolStripMenuItem MenuFlagsCopy;
        private ToolStripMenuItem MenuFlagsPaste;
        private ToolStripSeparator MenuFlagsSpace2;
        private ToolStripMenuItem MenuFlagsSelectAll;
        private ToolStripSeparator MenuFlagsSpace3;
        private ToolStripMenuItem MenuFlagClear;
        private ToolStripMenuItem MenuFlagDefault;

        private static FormTxFlags _DefaultInstance;
        public static FormTxFlags DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new FormTxFlags();

                return _DefaultInstance;
            }
        }
    }
}