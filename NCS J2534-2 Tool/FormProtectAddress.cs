﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    public partial class FormProtectAddress
    {
        public FormProtectAddress()
        {
            InitializeComponent();
        }

        private J1939AddressClaimSetup localSetup;
        private byte[] resultBytes = new byte[9];

        protected override void OnLoad(EventArgs e)
        {
            UpdateResult();
            UpdateLabel();
            base.OnLoad(e);
        }

        private void UpdateResult()
        {
            resultBytes = J2534Code.BuildJ1939Name(localSetup);
            UpdateLabel();
        }

        private void UpdateLabel()
        {
            LabelResult.Text = string.Concat("0x", resultBytes[8].ToString("X2"),
                                            " 0x", resultBytes[7].ToString("X2"),
                                            " 0x", resultBytes[6].ToString("X2"),
                                            " 0x", resultBytes[5].ToString("X2"),
                                            " 0x", resultBytes[4].ToString("X2"),
                                            " 0x", resultBytes[3].ToString("X2"),
                                            " 0x", resultBytes[2].ToString("X2"),
                                            " 0x", resultBytes[1].ToString("X2"),
                                            " 0x", resultBytes[0].ToString("X2"));
        }

        private void TextBoxAddress_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void TextBoxAddress_TextChanged(object sender, EventArgs e)
        {
            localSetup.Address = TextBoxAddress.Text.ToByte();
            UpdateResult();
        }

        private void TextBoxIndGroup_TextChanged(object sender, EventArgs e)
        {
            localSetup.IndustryGroup = TextBoxIndGroup.Text.ToByte();
            UpdateResult();
        }

        private void TextBoxVehSysInst_TextChanged(object sender, EventArgs e)
        {
            localSetup.VehicleSystemInstance = TextBoxVehSysInst.Text.ToByte();
            UpdateResult();
        }

        private void TextBoxVehSys_TextChanged(object sender, EventArgs e)
        {
            localSetup.VehicleSystemId = TextBoxVehSys.Text.ToByte();
            UpdateResult();
        }

        private void TextBoxFunc_TextChanged(object sender, EventArgs e)
        {
            localSetup.FunctionId = TextBoxFunc.Text.ToByte();
            UpdateResult();
        }

        private void TextBoxFuncInst_TextChanged(object sender, EventArgs e)
        {
            localSetup.FunctionInstance = TextBoxFuncInst.Text.ToByte();
            UpdateResult();
        }

        private void TextBoxECUInst_TextChanged(object sender, EventArgs e)
        {
            localSetup.ECUInstance = TextBoxECUInst.Text.ToByte();
            UpdateResult();
        }

        private void TextBoxMfgCode_TextChanged(object sender, EventArgs e)
        {
            localSetup.MfgCode = TextBoxMfgCode.Text.ToUint32();
            UpdateResult();
        }

        private void TextBoxIdent_TextChanged(object sender, EventArgs e)
        {
            localSetup.Identity = TextBoxIdent.Text.ToUint32();
            UpdateResult();
        }

        private void CheckBox1_Click(object sender, EventArgs e)
        {
            localSetup.ArbitraryCapable = CheckBox1.Checked;
            UpdateResult();
        }

        public DialogResult ShowDialog(J1939AddressClaimSetup setup)
        {
            localSetup = setup;
            return this.ShowDialog();
        }

    }


}