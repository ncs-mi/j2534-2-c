﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901


using System.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class FormMessageEdit : System.Windows.Forms.Form
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }


        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMessageEdit));
            this.MenuFlagsUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsSpace1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuFlagsCut = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsSpace2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuFlagsSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsSpace3 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuFlagsEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsClear = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsDefault = new System.Windows.Forms.ToolStripMenuItem();
            this.ButtonOK = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.TextBoxComment = new System.Windows.Forms.TextBox();
            this.ContextMenuStripFlags = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TextBoxData = new System.Windows.Forms.TextBox();
            this.TextBoxFlags = new System.Windows.Forms.TextBox();
            this.ComboBoxProtocol = new System.Windows.Forms.ComboBox();
            this.LabelComment = new System.Windows.Forms.Label();
            this.LabelDataBytes = new System.Windows.Forms.Label();
            this.LabelTxFlags = new System.Windows.Forms.Label();
            this.LabelNetWorkId = new System.Windows.Forms.Label();
            this.ContextMenuStripFlags.SuspendLayout();
            this.SuspendLayout();
            //
            //MenuFlagsUndo
            //
            this.MenuFlagsUndo.Name = "MenuFlagsUndo";
            this.MenuFlagsUndo.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsUndo.Text = "Undo";
            //
            //MenuFlagsSpace1
            //
            this.MenuFlagsSpace1.Name = "MenuFlagsSpace1";
            this.MenuFlagsSpace1.Size = new System.Drawing.Size(158, 6);
            //
            //MenuFlagsCut
            //
            this.MenuFlagsCut.Name = "MenuFlagsCut";
            this.MenuFlagsCut.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsCut.Text = "Cut";
            //
            //MenuFlagsCopy
            //
            this.MenuFlagsCopy.Name = "MenuFlagsCopy";
            this.MenuFlagsCopy.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsCopy.Text = "Copy";
            //
            //MenuFlagsPaste
            //
            this.MenuFlagsPaste.Name = "MenuFlagsPaste";
            this.MenuFlagsPaste.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsPaste.Text = "Paste";
            //
            //MenuFlagsSpace2
            //
            this.MenuFlagsSpace2.Name = "MenuFlagsSpace2";
            this.MenuFlagsSpace2.Size = new System.Drawing.Size(158, 6);
            //
            //MenuFlagsSelectAll
            //
            this.MenuFlagsSelectAll.Name = "MenuFlagsSelectAll";
            this.MenuFlagsSelectAll.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsSelectAll.Text = "Select All";
            //
            //MenuFlagsSpace3
            //
            this.MenuFlagsSpace3.Name = "MenuFlagsSpace3";
            this.MenuFlagsSpace3.Size = new System.Drawing.Size(158, 6);
            //
            //MenuFlagsEdit
            //
            this.MenuFlagsEdit.Name = "MenuFlagsEdit";
            this.MenuFlagsEdit.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsEdit.Text = "Edit Flags";
            //
            //MenuFlagsClear
            //
            this.MenuFlagsClear.Name = "MenuFlagsClear";
            this.MenuFlagsClear.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsClear.Text = "Clear Flags";
            //
            //MenuFlagsDefault
            //
            this.MenuFlagsDefault.Name = "MenuFlagsDefault";
            this.MenuFlagsDefault.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsDefault.Text = "Set Default Flags";
            //
            //ButtonOK
            //
            this.ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ButtonOK.Location = new System.Drawing.Point(336, 69);
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.Size = new System.Drawing.Size(65, 25);
            this.ButtonOK.TabIndex = 1;
            this.ButtonOK.Text = "OK";
            this.ButtonOK.UseVisualStyleBackColor = true;
            //
            //ButtonCancel
            //
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(408, 69);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(65, 25);
            this.ButtonCancel.TabIndex = 0;
            this.ButtonCancel.Text = "Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            //
            //TextBoxComment
            //
            this.TextBoxComment.AcceptsReturn = true;
            this.TextBoxComment.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxComment.Location = new System.Drawing.Point(8, 69);
            this.TextBoxComment.MaxLength = 0;
            this.TextBoxComment.Name = "TextBoxComment";
            this.TextBoxComment.Size = new System.Drawing.Size(322, 20);
            this.TextBoxComment.TabIndex = 5;
            //
            //ContextMenuStripFlags
            //
            this.ContextMenuStripFlags.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuFlagsUndo, this.MenuFlagsSpace1, this.MenuFlagsCut, this.MenuFlagsCopy, this.MenuFlagsPaste, this.MenuFlagsSpace2, this.MenuFlagsSelectAll, this.MenuFlagsSpace3, this.MenuFlagsEdit, this.MenuFlagsClear, this.MenuFlagsDefault});
            this.ContextMenuStripFlags.Name = "ContextMenuStripFlags";
            this.ContextMenuStripFlags.Size = new System.Drawing.Size(162, 198);
            //
            //TextBoxData
            //
            this.TextBoxData.AcceptsReturn = true;
            this.TextBoxData.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxData.Location = new System.Drawing.Point(208, 21);
            this.TextBoxData.MaxLength = 0;
            this.TextBoxData.Name = "TextBoxData";
            this.TextBoxData.Size = new System.Drawing.Size(265, 20);
            this.TextBoxData.TabIndex = 4;
            //
            //TextBoxFlags
            //
            this.TextBoxFlags.AcceptsReturn = true;
            this.TextBoxFlags.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFlags.Location = new System.Drawing.Point(120, 21);
            this.TextBoxFlags.MaxLength = 0;
            this.TextBoxFlags.Name = "TextBoxFlags";
            this.TextBoxFlags.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFlags.TabIndex = 3;
            //
            //ComboBoxProtocol
            //
            this.ComboBoxProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxProtocol.Location = new System.Drawing.Point(8, 21);
            this.ComboBoxProtocol.Name = "ComboBoxProtocol";
            this.ComboBoxProtocol.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxProtocol.TabIndex = 2;
            //
            //LabelComment
            //
            this.LabelComment.Location = new System.Drawing.Point(8, 53);
            this.LabelComment.Name = "LabelComment";
            this.LabelComment.Size = new System.Drawing.Size(89, 17);
            this.LabelComment.TabIndex = 9;
            this.LabelComment.Text = "Comment:";
            //
            //LabelDataBytes
            //
            this.LabelDataBytes.Location = new System.Drawing.Point(208, 5);
            this.LabelDataBytes.Name = "LabelDataBytes";
            this.LabelDataBytes.Size = new System.Drawing.Size(89, 17);
            this.LabelDataBytes.TabIndex = 8;
            this.LabelDataBytes.Text = "Data Bytes:";
            //
            //LabelTxFlags
            //
            this.LabelTxFlags.Location = new System.Drawing.Point(120, 5);
            this.LabelTxFlags.Name = "LabelTxFlags";
            this.LabelTxFlags.Size = new System.Drawing.Size(65, 17);
            this.LabelTxFlags.TabIndex = 7;
            this.LabelTxFlags.Text = "Tx Flags:";
            //
            //LabelNetWorkId
            //
            this.LabelNetWorkId.Location = new System.Drawing.Point(8, 5);
            this.LabelNetWorkId.Name = "LabelNetWorkId";
            this.LabelNetWorkId.Size = new System.Drawing.Size(89, 17);
            this.LabelNetWorkId.TabIndex = 6;
            this.LabelNetWorkId.Text = "Network ID:";
            //
            //FormMessageEdit
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ButtonCancel;
            this.ClientSize = new System.Drawing.Size(479, 99);
            this.Controls.Add(this.ButtonOK);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.TextBoxComment);
            this.Controls.Add(this.TextBoxData);
            this.Controls.Add(this.TextBoxFlags);
            this.Controls.Add(this.ComboBoxProtocol);
            this.Controls.Add(this.LabelComment);
            this.Controls.Add(this.LabelDataBytes);
            this.Controls.Add(this.LabelTxFlags);
            this.Controls.Add(this.LabelNetWorkId);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMessageEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Message";
            this.ContextMenuStripFlags.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

            ComboBoxProtocol.SelectedIndexChanged += new System.EventHandler(ComboBoxProtocol_SelectedIndexChanged);
            TextBoxComment.TextChanged += new System.EventHandler(TextBoxComment_TextChanged);
            TextBoxComment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxComment_KeyPress);
            TextBoxComment.Leave += new System.EventHandler(TextBoxComment_Leave);
            TextBoxData.TextChanged += new System.EventHandler(TextBoxData_TextChanged);
            TextBoxData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxData_KeyPress);
            TextBoxData.Leave += new System.EventHandler(TextBoxData_Leave);
            TextBoxFlags.TextChanged += new System.EventHandler(TextBoxFlags_TextChanged);
            TextBoxFlags.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxFlags_KeyPress);
            TextBoxFlags.Leave += new System.EventHandler(TextFlags_Leave);
            MenuFlagsClear.Click += new System.EventHandler(MenuFlagClear_Click);
            MenuFlagsDefault.Click += new System.EventHandler(MenuFlagDefault_Click);
            MenuFlagsEdit.Click += new System.EventHandler(MenuFlagEdit_Click);
            MenuFlagsCopy.Click += new System.EventHandler(MenuFlagsCopy_Click);
            MenuFlagsCut.Click += new System.EventHandler(MenuFlagsCut_Click);
            MenuFlagsPaste.Click += new System.EventHandler(MenuFlagsPaste_Click);
            MenuFlagsSelectAll.Click += new System.EventHandler(MenuFlagsSelectAll_Click);
            MenuFlagsUndo.Click += new System.EventHandler(MenuFlagsUndo_Click);
            ContextMenuStripFlags.Opening += new CancelEventHandler(ContextMenuStripFlags_Opening);
        }

        private System.Windows.Forms.ToolStripMenuItem MenuFlagsUndo;
        private System.Windows.Forms.ToolStripSeparator MenuFlagsSpace1;
        private System.Windows.Forms.ToolStripMenuItem MenuFlagsCut;
        private System.Windows.Forms.ToolStripMenuItem MenuFlagsCopy;
        private System.Windows.Forms.ToolStripMenuItem MenuFlagsPaste;
        private System.Windows.Forms.ToolStripSeparator MenuFlagsSpace2;
        private System.Windows.Forms.ToolStripMenuItem MenuFlagsSelectAll;
        private System.Windows.Forms.ToolStripSeparator MenuFlagsSpace3;
        private System.Windows.Forms.ToolStripMenuItem MenuFlagsEdit;
        private System.Windows.Forms.ToolStripMenuItem MenuFlagsClear;
        private System.Windows.Forms.ToolStripMenuItem MenuFlagsDefault;
        private System.Windows.Forms.ContextMenuStrip ContextMenuStripFlags;
        private Button ButtonOK;
        private Button ButtonCancel;
        private TextBox TextBoxComment;
        private TextBox TextBoxData;
        private TextBox TextBoxFlags;
        private ComboBox ComboBoxProtocol;
        private Label LabelComment;
        private Label LabelDataBytes;
        private Label LabelTxFlags;
        private Label LabelNetWorkId;


        private static FormMessageEdit _DefaultInstance;
        public static FormMessageEdit DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new FormMessageEdit();

                return _DefaultInstance;
            }
        }
    }
}