﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using static NCS_J2534_2_Tool.J2534ProtocolId;
using static NCS_J2534_2_Tool.J2534IoCtlId;
using static NCS_J2534_2_Tool.J2534ConfigParameterId;
using static NCS_J2534_2_Tool.J2534Collections;

namespace NCS_J2534_2_Tool
{
    public partial class FormJ2534
    {
        public FormJ2534()
        {
            InitializeComponent();
        }

        private J2534Device selectedDevice;
        private bool settingPeriodicMessages;
        private bool settingFilters;
        private bool settingTextBoxAnalogRate;
        private bool settingTextBoxActiveChannelConfig;
        [System.Runtime.CompilerServices.AccessedThroughProperty(nameof(TextBoxAnalogRateConfig))]
        private TextBox _TextBoxAnalogRateConfig;
        private TextBox TextBoxAnalogRateConfig
        {
            [DebuggerNonUserCode]
            get
            {
                return _TextBoxAnalogRateConfig;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized), System.Diagnostics.DebuggerNonUserCode]
            set
            {
                if (_TextBoxAnalogRateConfig != null)
                {
                    _TextBoxAnalogRateConfig.TextChanged -= TextBoxAnalogRateConfig_TextChanged;
                    _TextBoxAnalogRateConfig.KeyPress -= TextBoxAnalogRateConfig_KeyPress;
                }

                _TextBoxAnalogRateConfig = value;

                if (value != null)
                {
                    _TextBoxAnalogRateConfig.TextChanged += TextBoxAnalogRateConfig_TextChanged;
                    _TextBoxAnalogRateConfig.KeyPress += TextBoxAnalogRateConfig_KeyPress;
                }
            }
        }
        [System.Runtime.CompilerServices.AccessedThroughProperty(nameof(TextBoxActiveChannelConfig))]
        private TextBox _TextBoxActiveChannelConfig;
        private TextBox TextBoxActiveChannelConfig
        {
            [DebuggerNonUserCode]
            get
            {
                return _TextBoxActiveChannelConfig;
            }
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized), System.Diagnostics.DebuggerNonUserCode]
            set
            {
                if (_TextBoxActiveChannelConfig != null)
                {
                    _TextBoxActiveChannelConfig.TextChanged -= TextBoxActiveChannelConfig_TextChanged;
                    _TextBoxActiveChannelConfig.KeyPress -= TextBoxActiveChannelConfig_KeyPress;
                }

                _TextBoxActiveChannelConfig = value;

                if (value != null)
                {
                    _TextBoxActiveChannelConfig.TextChanged += TextBoxActiveChannelConfig_TextChanged;
                    _TextBoxActiveChannelConfig.KeyPress += TextBoxActiveChannelConfig_KeyPress;
                }
            }
        }
        private uint configChannels;
        private uint configRate;
        private bool[] PeriodicMessageCheckBoxHit = new bool[J2534Constants.MAX_PM + 1];

        private Point ListViewMessageIn_MouseMove_position = new Point(-1, -1);

        protected override void OnLoad(EventArgs e)
        {
            string title = string.Concat(My.MyApplication.Application.Info.ProductName,
                                         " version ",
                                         My.MyApplication.Application.Info.Version.Major,
                                         ".",
                                         My.MyApplication.Application.Info.Version.Minor.ToString("00"));
             if (PublicDeclarations.betaVersion > 0)
            {
                title = string.Concat(title, " (ß", PublicDeclarations.betaVersion, ") ");
            }
            this.Text = title;
            SetControlArrays();
            J2534Code.SetArrays();
            SetMiscControls();
            ClearFilterControls();
            ClearPeriodicMessageControls();
            SetConfigControls();
            SetInitControls();
            SetFunctionalMessageLookupTableControls();
            InitComboBoxAPI();
            InitializeEditMenu();
            FileMenuInit();
            UpdateComboBoxAvailableDevice();
            SetVoltControls();
            UpdateTabs();
            ComboBoxDevice.Enabled = false;
            LabelComboDevice.Enabled = ComboBoxDevice.Enabled;
            TabControl1.SelectedIndex = 0;
            base.OnLoad(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            foreach (J2534Device device in J2534Devices.Values)
            {
                if (device.Connected)
                {
                    CloseJ2534Device(device);
                }
            }
            foreach (J2534_API API in J2534_APIs)
            {
                API.Dispose();
            }
            base.OnClosed(e);
        }

        private void SetMiscControls()
        {
            ComboBoxDevice.Visible = true;
            ComboBoxDevice.Text = string.Empty;
            TextBoxReadRate.Text = string.Empty;
            TextBoxTimeOut.Text = "0";
            TextBoxMessageOut.Text = string.Empty;
            TextBoxOutFlags.Text = string.Empty;
            TextBoxConnectFlags.Text = string.Empty;
            TextBoxConnectFlags.Enabled = false;
            LabelConnectFlags.Enabled = false;
            for (int i = 0; i <= J2534Constants.MAX_FILTER; i++)
            {
                ComboBoxFilterType[i].Items.Add("None");
                ComboBoxFilterType[i].Items.Add("Pass");
                ComboBoxFilterType[i].Items.Add("Block");
                ComboBoxFilterType[i].Items.Add("Flow");
                ComboBoxFilterType[i].SelectedIndex = 0;
            }
            SetMessageControls();
            LabelJ2534Info.Text = string.Empty;
            LabelProtSupport.Text = string.Empty;
        }

        private void UpdateDevice()
        {
            J2534Device newDevice = ComboBoxAvailableDevice.SelectedItem as J2534Device;
            if (selectedDevice != null && selectedDevice != newDevice)
            {
                HandleOutgoingDevice(selectedDevice);
            }
            UpdateDeviceSetup(selectedDevice);
            if (newDevice != null && selectedDevice != newDevice)
            {
                HandleIncomingDevice(newDevice);
            }
            selectedDevice = newDevice;
            if (newDevice != null)
            {
                ToolStripStatusLabel1.Text = string.Concat("In: ", newDevice.MessageInCount, "     Out: ", newDevice.MessageOutCount);
            }
            else
            {
                ToolStripStatusLabel1.Text = string.Empty;
            }
            UpdateMessageControls();
            UpdateConnectControls();
            UpdateComboBoxAvailableChannel(newDevice.Setup.SelectedChannelName);
        }

        private void HandleOutgoingDevice(J2534Device device)
        {
            if (device != null)
            {
                device.ListViewMessageIn.Visible = false;
                device.ListViewMessageIn.MouseMove -= ListViewMessageIn_MouseMove;
                device.ListViewMessageIn.MouseLeave -= ListViewMessageIn_MouseLeave;
                device.ListViewMessageIn.SizeChanged -= ListViewMessageIn_SizeChanged;
                device.ListViewMessageIn.ContextMenuStrip = null;
                if (device.ListViewMessageIn.Parent == TabPageMessages)
                {
                    TabPageMessages.Controls.Remove(device.ListViewMessageIn);
                }

                device.ListViewMessageOut.Visible = false;
                device.ListViewMessageOut.Leave -= ListViewMessageOut_Leave;
                device.ListViewMessageOut.SelectedIndexChanged -= ListViewMessageOut_SelectedIndexChanged;
                device.ListViewMessageOut.Enter -= ListViewMessageOut_Enter;
                device.ListViewMessageOut.SizeChanged -= ListViewMessageOut_SizeChanged;
                device.ListViewMessageOut.ContextMenuStrip = null;
                if (device.ListViewMessageOut.Parent == TabPageMessages)
                {
                    TabPageMessages.Controls.Remove(device.ListViewMessageOut);
                }
            }
            UpdateDeviceInfo(device);
            UpdateComboBoxConnectChannel(null);
        }

        private void UpdateDeviceSetup(J2534Device device)
        {
            if (device != null)
            {
                int.TryParse(TextBoxReadRate.Text, out int value);
                device.Setup.MessageReadRate = value;
                device.Setup.SelectedChannelName = ComboBoxAvailableChannel.Text;
                device.Setup.MessageChannelName = ComboBoxMessageChannel.Text;
                device.Setup.ConnectChannelName = ComboBoxConnectChannel.Text;
                device.Setup.TextBoxOutFlags = TextBoxOutFlags.Text;
                device.Setup.TextBoxMessageOut = TextBoxMessageOut.Text;
            }
        }

        private void HandleIncomingDevice(J2534Device device)
        {
            if (device.ListViewMessageIn.Parent == null)
            {
                TabPageMessages.Controls.Add(device.ListViewMessageIn);
            }
            device.ListViewMessageIn.Visible = true;
            device.ListViewMessageIn.Enabled = true;
            device.ListViewMessageIn.MouseMove += ListViewMessageIn_MouseMove;
            device.ListViewMessageIn.MouseLeave += ListViewMessageIn_MouseLeave;
            device.ListViewMessageIn.SizeChanged += ListViewMessageIn_SizeChanged;
            device.ListViewMessageIn.Location = LvInLocator.Location;
            device.ListViewMessageIn.Size = LvInLocator.Size;
            device.ListViewMessageIn.BringToFront();
            device.ListViewMessageIn.ContextMenuStrip = ContextMenuStripMessageIn;

            if (device.ListViewMessageOut.Parent == null)
            {
                TabPageMessages.Controls.Add(device.ListViewMessageOut);
            }
            device.ListViewMessageOut.Visible = true;
            device.ListViewMessageOut.Enabled = true;
            device.ListViewMessageOut.Leave += ListViewMessageOut_Leave;
            device.ListViewMessageOut.SelectedIndexChanged += ListViewMessageOut_SelectedIndexChanged;
            device.ListViewMessageOut.Enter += ListViewMessageOut_Enter;
            device.ListViewMessageOut.SizeChanged += ListViewMessageOut_SizeChanged;
            device.ListViewMessageOut.Location = LvOutLocator.Location;
            device.ListViewMessageOut.Size = LvOutLocator.Size;
            device.ListViewMessageOut.BringToFront();
            device.ListViewMessageOut.ContextMenuStrip = ContextMenuStripMessageOut;

            TextBoxReadRate.Text = device.Setup.MessageReadRate.ToString("000");
            UpdateComboBoxAvailableChannel(device.Setup.SelectedChannelName);
            UpdateComboBoxMessageChannel(device.Setup.MessageChannelName);
            TextBoxOutFlags.Text = device.Setup.TextBoxOutFlags;
            TextBoxMessageOut.Text = device.Setup.TextBoxMessageOut;
            for (int i = 0; i < RadioButtonPin.Length; i++)
            {
                if (RadioButtonPin[i] != null)
                {
                    RadioButtonPin[i].Tag = device.Analog.Pin[i].Mode;
                    RadioButtonPin[i].Checked = device.Analog.Pin[i].Selected;
                }
            }
            UpdateComboBoxConnectChannel(device);
            UpdateDeviceInfo(device);
            UpdateDeviceLabels();
            SetVoltControls();
            foreach (KeyValuePair<int, J2534Device> box in J2534Devices)
            {
                if (box.Value != device)
                {
                    box.Value.ListViewMessageIn.Visible = false;
                    box.Value.ListViewMessageOut.Visible = false;
                }
            }
        }

        private static int GetAvailableDeviceIndex()
        {
            int result = int.MaxValue;
            for (int i = 1; i <= int.MaxValue; i++)
            {
                if (!J2534Devices.ContainsKey(i))
                {
                    result = i;
                    break;
                }
                if (i == int.MaxValue)
                {
                    break;
                }
            }
            return result;
        }

        private static void RemoveBox(J2534Device device)
        {
            if (!J2534Devices.Values.Contains(device))
            {
                return;
            }
            int index = -1;
            foreach (KeyValuePair<int, J2534Device> kvp in J2534Devices)
            {
                if (kvp.Value == device)
                {
                    index = kvp.Key;
                    break;
                }
            }
            J2534Devices.TryRemove(index, out J2534Device temp);
            device.Dispose();
        }

        private void ButtonOpenBox_Click(object sender, EventArgs e)
        {
            if (ComboBoxAPI.SelectedItem == null)
            {
                return;
            }
            if (!(ComboBoxAPI.SelectedItem is J2534_API item))
            {
                return;
            }
            if (ComboBoxDevice.Text.Length > 0)
            {
                int index = GetAvailableDeviceIndex();
                J2534Device device = new J2534Device
                {
                    Name = ComboBoxDevice.Text,
                    API = item
                };
                if (J2534Devices.TryAdd(index, device))
                {
                    if (OpenJ2534Device(device))
                    {
                        UpdateComboBoxAvailableDevice();
                        UpdateComboBoxDevice();
                        ComboBoxAvailableDevice.Text = ComboBoxDevice.Text;
                        UpdateDevice();
                    }
                }
            }
        }

        private void ButtonCloseBox_Click(object sender, EventArgs e)
        {
            J2534Device device = J2534Code.NameToDevice(ComboBoxDevice.Text);
            if (device != null)
            {
                CloseJ2534Device(device);
            }
            UpdateComboBoxAvailableDevice();
            UpdateComboBoxDevice();
        }

        private void ButtonConnect_Click(object sender, EventArgs e)
        {
            J2534ConnectOptions options = new J2534ConnectOptions();
            int pinsetIndex = 0;
            options.Channel = ComboBoxConnectChannel.SelectedItem as J2534Channel;
            if (options.Channel == null)
            {
                return;
            }
            uint.TryParse(ComboBoxBaudRate.Text, out uint temp);
            options.BaudRate = temp;
            options.ConnectFlags = (J2534ConnectFlags)J2534Code.TextToFlags(TextBoxConnectFlags.Text);
            options.MixedMode = CheckBoxMixedMode.Checked;
            if (options.Channel.IsPinSwitched)
            {
                pinsetIndex = ComboBoxPins.SelectedIndex;
                switch (ComboBoxConnector.Text)
                {
                    case "J1962":
                        options.Connector = (uint)J1962_PINS;
                        if (pinsetIndex >= 0)
                        {
                            options.Pins = options.Channel.J1962Pins[pinsetIndex];
                        }

                        break;
                    case "J1939":
                        options.Connector = (uint)J1939_PINS;
                        if (pinsetIndex >= 0)
                        {
                            options.Pins = options.Channel.J1939Pins[pinsetIndex];
                        }

                        break;
                    case "J1708":
                        options.Connector = (uint)J1708_PINS;
                        if (pinsetIndex >= 0)
                        {
                            options.Pins = options.Channel.J1708Pins[pinsetIndex];
                        }

                        break;
                }

            }
            ConnectChannel(selectedDevice, options);
            ComboBoxConnectChannel.Focus();
        }

        private void ButtonDisconnect_Click(object sender, EventArgs e)
        {
            J2534Channel channel = ComboBoxConnectChannel.SelectedItem as J2534Channel;
            DisconnectChannel(selectedDevice, channel);
        }

        private void ButtonClearRx_Click(object sender, EventArgs e)
        {
            if (selectedDevice?.API?.PassThruIoctl == null)
            {
                return;
            }
            foreach (J2534Channel channel in selectedDevice.ChannelSet)
            {
                if (channel.Connected)
                {
                    J2534Result result = selectedDevice.API.PassThruIoctl(channel.ChannelId, CLEAR_RX_BUFFER, 0, 0);
                    ShowResult(selectedDevice, result, "PassThruIoctl - CLEAR_RX_BUFFER");
                }
            }
        }

        private void ButtonClearTx_Click(object sender, EventArgs e)
        {
            if (selectedDevice?.API?.PassThruIoctl == null)
            {
                return;
            }
            foreach (J2534Channel channel in selectedDevice.ChannelSet)
            {
                if (channel.Connected)
                {
                    J2534Result result = selectedDevice.API.PassThruIoctl(channel.ChannelId, CLEAR_TX_BUFFER, 0, 0);
                    ShowResult(selectedDevice, result, "PassThruIoctl - CLEAR_TX_BUFFER");
                }
            }
        }

        private void ButtonExecute5BInit_Click(object sender, EventArgs e)
        {
            if (selectedDevice?.API?.PassThruIoctl == null)
            {
                return;
            }
            SBYTE_ARRAY inputMessage = new SBYTE_ARRAY();
            SBYTE_ARRAY outputMessage = new SBYTE_ARRAY();
            byte[] ecuAddress = new byte[1];
            byte[] keyWord = new byte[2];
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || !(channel.IsISO9141 || channel.IsISO14230))
            {
                SetInitControls();
                return;
            }
            ecuAddress[0] = TextBox5BInitECU.Text.ToByte();
            inputMessage.NumOfBytes = 1;
            IntPtr ecuAddressPointer = Marshal.AllocHGlobal(Marshal.SizeOf(ecuAddress[0]) * ecuAddress.Length);
            Marshal.Copy(ecuAddress, 0, ecuAddressPointer, ecuAddress.Length);
            inputMessage.BytePtr = ecuAddressPointer.ToInt32();
            IntPtr inputMessagePointer = Marshal.AllocHGlobal(Marshal.SizeOf(inputMessage));
            Marshal.StructureToPtr(inputMessage, inputMessagePointer, true);
            outputMessage.NumOfBytes = 2;
            IntPtr keyWordPointer = Marshal.AllocHGlobal(Marshal.SizeOf(keyWord[0]) * keyWord.Length);
            Marshal.Copy(keyWord, 0, keyWordPointer, keyWord.Length);
            outputMessage.BytePtr = keyWordPointer.ToInt32();
            IntPtr outputMessagePointer = Marshal.AllocHGlobal(Marshal.SizeOf(outputMessage));
            Marshal.StructureToPtr(outputMessage, outputMessagePointer, true);
            J2534Result result = selectedDevice.API.PassThruIoctl(channel.ChannelId, FIVE_BAUD_INIT, inputMessagePointer.ToInt32(), outputMessagePointer.ToInt32());
            Marshal.Copy(keyWordPointer, keyWord, 0, keyWord.Length);
            Marshal.FreeHGlobal(ecuAddressPointer);
            Marshal.FreeHGlobal(inputMessagePointer);
            Marshal.FreeHGlobal(keyWordPointer);
            Marshal.FreeHGlobal(outputMessagePointer);

            ShowResult(selectedDevice, result, "PassThruIoctl - FIVE_BAUD_INIT");
            if (result == J2534Result.STATUS_NOERROR)
            {
                Label5BKeyWord0.Text = string.Concat("0x", keyWord[0].ToString("X2"));
                Label5BKeyWord1.Text = string.Concat("0x", keyWord[1].ToString("X2"));
            }
            else
            {
                Label5BKeyWord0.Text = string.Empty;
                Label5BKeyWord1.Text = string.Empty;
            }
        }

        private void ButtonExecuteFastInit_Click(object sender, EventArgs e)
        {
            if (selectedDevice?.API?.PassThruIoctl == null)
            {
                return;
            }
            J2534MessageText initMessageText = new J2534MessageText();

            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || !(channel.IsISO9141 || channel.IsISO14230))
            {
                SetInitControls();
                return;
            }
            initMessageText.Data = TextBoxFIMessage.Text;
            initMessageText.ProtocolName = ComboBoxAvailableChannel.Text;
            initMessageText.TxFlags = TextBoxFIFlags.Text;

            PASSTHRU_MSG inputMessage = initMessageText.ToMessage();
            IntPtr inputPointer = Marshal.AllocHGlobal(Marshal.SizeOf(inputMessage));
            Marshal.StructureToPtr(inputMessage, inputPointer, true);

            PASSTHRU_MSG outputMessage = PASSTHRU_MSG.CreateInstance();
            IntPtr outputPointer = Marshal.AllocHGlobal(Marshal.SizeOf(outputMessage));
            Marshal.StructureToPtr(outputMessage, outputPointer, true);

            J2534Result result = selectedDevice.API.PassThruIoctl(channel.ChannelId, FAST_INIT, inputPointer.ToInt32(), outputPointer.ToInt32());

            outputMessage = (PASSTHRU_MSG)Marshal.PtrToStructure(outputPointer, typeof(PASSTHRU_MSG));

            Marshal.FreeHGlobal(inputPointer);
            Marshal.FreeHGlobal(outputPointer);

            ShowResult(selectedDevice, result, "PassThruIoctl - FAST_INIT");
            if (result == J2534Result.STATUS_NOERROR)
            {
                J2534MessageText respMessageText = outputMessage.ToMessageText();
                LabelFIResponse.Text = respMessageText.Data;
                LabelFIRxStatus.Text = respMessageText.RxStatus;
            }
            else
            {
                LabelFIResponse.Text = string.Empty;
                LabelFIRxStatus.Text = string.Empty;
            }

        }

        private void ButtonApplyFilter_Click(object sender, EventArgs e)
        {
            if (selectedDevice?.API?.PassThruStopMsgFilter == null)
            {
                return;
            }
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel))
            {
                return;
            }
            for (int i = 0; i <= J2534Constants.MAX_FILTER; i++)
            {
                if (FilterEditBuffer[i].FilterType == J2534FilterType.PASS_FILTER || FilterEditBuffer[i].FilterType == J2534FilterType.BLOCK_FILTER)
                {
                    if (J2534Code.IsISO15765(FilterEditBuffer[i].MaskMessage.ProtocolId))
                    {
                        FilterEditBuffer[i].MaskMessage.ProtocolId = J2534Code.ISO_IdToCAN_Id(FilterEditBuffer[i].MaskMessage.ProtocolId);
                        FilterEditBuffer[i].PatternMessage.ProtocolId = FilterEditBuffer[i].MaskMessage.ProtocolId;
                    }
                }
                J2534Code.FixFilterMask(FilterEditBuffer[i].MaskMessage, FilterEditBuffer[i].PatternMessage);
                if (channel.Connected)
                {
                    if (FilterEditBuffer[i].Enabled)
                    {
                        if (FilterChangesPresent(i))
                        {
                            J2534Result result = selectedDevice.API.PassThruStopMsgFilter(channel.ChannelId, FilterEditBuffer[i].MessageId);
                            if (result == J2534Result.STATUS_NOERROR)
                            {
                                FilterEditBuffer[i].Enabled = false;
                            }
                            ShowResult(selectedDevice, result, "PassThruStopMsgFilter");
                        }
                    }
                    if ((!(FilterEditBuffer[i].Enabled)) && FilterEditBuffer[i].FilterType != J2534FilterType.NO_FILTER)
                    {
                        IntPtr maskPointer = Marshal.AllocHGlobal(Marshal.SizeOf(FilterEditBuffer[i].MaskMessage));
                        Marshal.StructureToPtr(FilterEditBuffer[i].MaskMessage, maskPointer, true);
                        IntPtr patternPointer = Marshal.AllocHGlobal(Marshal.SizeOf(FilterEditBuffer[i].PatternMessage));
                        Marshal.StructureToPtr(FilterEditBuffer[i].PatternMessage, patternPointer, true);
                        IntPtr flowPointer = IntPtr.Zero;
                        if (FilterEditBuffer[i].FilterType == J2534FilterType.FLOW_CONTROL_FILTER)
                        {
                            flowPointer = Marshal.AllocHGlobal(Marshal.SizeOf(FilterEditBuffer[i].FlowMessage));
                            Marshal.StructureToPtr(FilterEditBuffer[i].FlowMessage, flowPointer, true);
                        }
                        uint value = 0;
                        J2534Result result = selectedDevice.API.PassThruStartMsgFilter(channel.ChannelId, FilterEditBuffer[i].FilterType, maskPointer.ToInt32(), patternPointer.ToInt32(), flowPointer.ToInt32(), ref value);
                        FilterEditBuffer[i].MessageId = value;
                        Marshal.FreeHGlobal(maskPointer);
                        Marshal.FreeHGlobal(patternPointer);
                        Marshal.FreeHGlobal(flowPointer);
                        if (result == J2534Result.STATUS_NOERROR)
                        {
                            FilterEditBuffer[i].Enabled = true;
                        }
                        else
                        {
                            FilterEditBuffer[i].FilterType = J2534FilterType.NO_FILTER;
                        }
                        ShowResult(selectedDevice, result, "PassThruStartMsgFilter");
                    }
                }
                channel.Filter[i] = FilterEditBuffer[i].Clone();
            }
            SetFilterControls();
            UpdateFilterEdit();
        }

        private void ButtonCancelFilter_Click(object sender, EventArgs e)
        {
            SetFilterControls();
            UpdateFilterEdit();
        }

        private void ButtonClearAllFilter_Click(object sender, EventArgs e)
        {
            J2534Channel channel = ComboBoxAvailableChannel.SelectedItem as J2534Channel;
            J2534Code.ClearAllFilters(selectedDevice, channel);
        }

        private void ButtonCreatePassFilter_Click(object sender, EventArgs e)
        {
            J2534Channel channel = ComboBoxAvailableChannel.SelectedItem as J2534Channel;
            J2534Channel channelForFlags = channel;
            if (channel == null)
            {
                return;
            }
            if (channel.IsISO15765)
            {
                channelForFlags = J2534Code.ISO_ChannelToCAN_Channel(selectedDevice, channel);
            }

            int nextFilter = channel.NextFilter;
            if (nextFilter < 0)
            {
                return;
            }
            if ((channel.ProtocolId == J1850VPW) || 
                (channel.ProtocolId == J1850VPW_PS) || 
                (channel.ProtocolId >= J1850VPW_CH1 && channel.ProtocolId <= J1850VPW_CH128) || 
                (channel.ProtocolId == ISO9141) || 
                (channel.ProtocolId == ISO9141_PS) || 
                (channel.ProtocolId >= ISO9141_CH1 && channel.ProtocolId <= ISO9141_CH128) || 
                (channel.ProtocolId == ISO14230) || 
                (channel.ProtocolId == ISO14230_PS) || 
                (channel.ProtocolId >= ISO14230_CH1 && channel.ProtocolId <= ISO14230_CH128) || 
                (channel.ProtocolId == SCI_A_ENGINE) || 
                (channel.ProtocolId == SCI_A_TRANS) || 
                (channel.ProtocolId == SCI_B_ENGINE) || 
                (channel.ProtocolId == SCI_B_TRANS) || 
                (channel.ProtocolId == J1708_PS) || 
                (channel.ProtocolId >= J1708_CH1 && channel.ProtocolId <= J1708_CH128))
            {
                TextBoxFilterMask[nextFilter].Text = "00";
                TextBoxFilterPatt[nextFilter].Text = "00";
            }
            else if ((channel.ProtocolId == J1850PWM) || 
                     (channel.ProtocolId == J1850PWM_PS) || 
                     (channel.ProtocolId >= J1850PWM_CH1 && channel.ProtocolId <= J1850PWM_CH128) || 
                     (channel.ProtocolId == HONDA_DIAGH_PS) || 
                     (channel.ProtocolId >= HONDA_DIAGH_CH1 && channel.ProtocolId <= HONDA_DIAGH_CH128))
            {
                TextBoxFilterMask[nextFilter].Text = "00 00 00";
                TextBoxFilterPatt[nextFilter].Text = "00 00 00";
            }
            else if ((channel.ProtocolId == J1939_PS) || 
                     (channel.ProtocolId >= J1939_CH1 && channel.ProtocolId <= J1939_CH128))
            {
                TextBoxFilterMask[nextFilter].Text = "00 00 00 00 00";
                TextBoxFilterPatt[nextFilter].Text = "00 00 00 00 00";

            }
            else
            {
                TextBoxFilterMask[nextFilter].Text = "00 00 00 00";
                TextBoxFilterPatt[nextFilter].Text = "00 00 00 00";

            }
            if (channelForFlags != null)
            {
                TextBoxFilterFlags[nextFilter].Text = J2534Code.FlagsToText(channelForFlags.DefaultTxFlags);
            }
            else
            {
                TextBoxFilterFlags[nextFilter].Text = "0x00000000";
            }
            ComboBoxFilterType[nextFilter].Text = "Pass";
            SetFilterEditBuffer(nextFilter);
            UpdateFilterEdit();
        }

        private void ButtonApplyFunctionalMessages_Click(object sender, EventArgs e)
        {
            if (selectedDevice?.API?.PassThruIoctl == null)
            {
                return;
            }
            byte[] functionalAddresses = new byte[J2534Constants.MAX_FUNC_MSG + 1];
            byte addrByte = 0;

            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || !channel.IsJ1850PWM)
            {
                SetFunctionalMessageLookupTableControls();
                return;
            }
            for (int i = 0; i <= J2534Constants.MAX_FUNC_MSG; i++)
            {
                addrByte = TextBoxFunctionalMessage[i].Text.ToByte();
                if (channel.FunctionalMessage[i].Value != 0)
                {
                    if (channel.FunctionalMessage[i].Value != addrByte)
                    {
                        DeleteFromFunctionalMessageLookupTable(channel.FunctionalMessage[i].Value);
                        CheckBoxFunctionalMessageDelete[i].Checked = false;
                    }
                    if (CheckBoxFunctionalMessageDelete[i].Checked)
                    {
                        TextBoxFunctionalMessage[i].Text = string.Empty;
                        addrByte = 0;
                        DeleteFromFunctionalMessageLookupTable(channel.FunctionalMessage[i].Value);
                    }
                }
                functionalAddresses[i] = addrByte;
            }

            functionalAddresses = J2534Code.PrepFunctionalAddressArray(functionalAddresses);

            J2534Result result = J2534Result.STATUS_NOERROR;
            if (functionalAddresses.Length > 0)
            {
                SBYTE_ARRAY message = new SBYTE_ARRAY
                {
                    NumOfBytes = functionalAddresses.Length.ToUint32()
                };
                IntPtr funcAddrPointer = Marshal.AllocHGlobal(Marshal.SizeOf(functionalAddresses[0]) * functionalAddresses.Length);
                Marshal.Copy(functionalAddresses, 0, funcAddrPointer, functionalAddresses.Length);
                message.BytePtr = funcAddrPointer.ToInt32();

                IntPtr pointer = Marshal.AllocHGlobal(Marshal.SizeOf(message));
                Marshal.StructureToPtr(message, pointer, true);

                result = selectedDevice.API.PassThruIoctl(channel.ChannelId, ADD_TO_FUNCT_MSG_LOOKUP_TABLE, pointer.ToInt32(), 0);

                Marshal.Copy(funcAddrPointer, functionalAddresses, 0, functionalAddresses.Length);
                Marshal.FreeHGlobal(funcAddrPointer);
                Marshal.FreeHGlobal(pointer);
            }

            if (result == J2534Result.STATUS_NOERROR)
            {
                for (int i = 0; i <= J2534Constants.MAX_FUNC_MSG; i++)
                {
                    channel.FunctionalMessage[i].Value = (i < functionalAddresses.Length) ? functionalAddresses[i] : (byte)0;
                    if (channel.FunctionalMessage[i].Value != 0)
                    {
                        channel.FunctionalMessage[i].NewText = string.Concat("0x", functionalAddresses[i].ToString("X2"));
                    }
                    else
                    {
                        channel.FunctionalMessage[i].NewText = string.Empty;
                    }
                }
            }
            ShowResult(selectedDevice, result, "PassThruIoctl - ADD_TO_FUNCT_MSG_LOOKUP_TABLE");
            SetFunctionalMessageLookupTableControls();
        }

        private void ButtonCancelFunctionalMessages_Click(object sender, EventArgs e)
        {
            SetFunctionalMessageLookupTableControls();
        }

        private void ButtonClearAllFunctionalMessagesClick(object sender, EventArgs e)
        {
            if (selectedDevice?.API?.PassThruIoctl == null)
            {
                return;
            }
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || !channel.IsJ1850PWM)
            {
                SetFunctionalMessageLookupTableControls();
                return;
            }
            J2534Result result = selectedDevice.API.PassThruIoctl(channel.ChannelId, CLEAR_FUNCT_MSG_LOOKUP_TABLE, 0, 0);
            ShowResult(selectedDevice, result, "PassThruIoctl - CLEAR_FUNCT_MSG_LOOKUP_TABLE");
            if (result == J2534Result.STATUS_NOERROR)
            {
                for (int i = 0; i <= J2534Constants.MAX_FUNC_MSG; i++)
                {
                    channel.FunctionalMessage[i].Value = 0;
                    channel.FunctionalMessage[i].NewText = string.Empty;
                }
            }
            SetFunctionalMessageLookupTableControls();
        }

        private void ButtonLoadDLL_Click(object sender, EventArgs e)
        {
            if (ComboBoxAPI.SelectedItem is J2534_API API)
            {
                API.LoadJ2534dll();
                ButtonLoadDLL.Enabled = !API.DllIsLoaded;
            }
            UpdateComboBoxDevice();
        }

        private void ButtonApplyPeriodicMessages_Click(object sender, EventArgs e)
        {
            if (selectedDevice?.API?.PassThruStopPeriodicMsg == null)
            {
                return;
            }
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel))
            {
                return;
            }
            for (int index = 0; index <= J2534Constants.MAX_PM; index++)
            {
                if (channel.Connected)
                {
                    if (PeriodicMessageEditBuffer[index].Enabled)
                    {
                        if (PeriodicMessageChangesPresent(index))
                        {
                            J2534Result result = selectedDevice.API.PassThruStopPeriodicMsg(channel.ChannelId, PeriodicMessageEditBuffer[index].MessageId);
                            if (result == J2534Result.STATUS_NOERROR)
                            {
                                PeriodicMessageEditBuffer[index].Enabled = false;
                            }
                            ShowResult(selectedDevice, result, "PassThruStopPeriodicMsg");
                        }
                    }
                    if (!(PeriodicMessageEditBuffer[index].Enabled))
                    {
                        if (CheckBoxPeriodicMessageEnable[index].Checked)
                        {
                            IntPtr pointer = Marshal.AllocHGlobal(Marshal.SizeOf(PeriodicMessageEditBuffer[index].Message));
                            Marshal.StructureToPtr(PeriodicMessageEditBuffer[index].Message, pointer, true);
                            uint value = 0;
                            J2534Result result = selectedDevice.API.PassThruStartPeriodicMsg(channel.ChannelId, pointer.ToInt32(), ref value, PeriodicMessageEditBuffer[index].Interval);
                            PeriodicMessageEditBuffer[index].MessageId = value;
                            Marshal.FreeHGlobal(pointer);
                            if (result == J2534Result.STATUS_NOERROR)
                            {
                                PeriodicMessageEditBuffer[index].Enabled = true;
                            }
                            ShowResult(selectedDevice, result, "PassThruStartPeriodicMsg");
                        }
                    }
                }
                channel.PeriodicMessage[index] = PeriodicMessageEditBuffer[index].Clone();
            }
            SetPeriodicMessageControls(selectedDevice);
            UpdatePeriodicMessageEdit();
        }

        private void ButtonCancelPeriodicMessages_Click(object sender, EventArgs e)
        {
            SetPeriodicMessageControls(selectedDevice);
            UpdatePeriodicMessageEdit();
        }

        private void ButtonClearAllPeriodicMessages_Click(object sender, EventArgs e)
        {
            J2534Channel channel = ComboBoxAvailableChannel.SelectedItem as J2534Channel;
            J2534Code.ClearAllPeriodicMessages(selectedDevice, channel);
        }

        private void ButtonReceive_Click(object sender, EventArgs e)
        {
            if (selectedDevice == null)
            {
                return;
            }
            selectedDevice.Setup.GetMessages = !selectedDevice.Setup.GetMessages;
            selectedDevice.TimerMessageReceive.Enabled = selectedDevice.Setup.GetMessages;
            GetMessages(selectedDevice);
            UpdateMessageControls();
        }

        private void ButtonClearList_Click(object sender, EventArgs e)
        {
            selectedDevice.ListViewMessageIn.Items.Clear();
            UpdateMessageControls();
        }

        private void ButtonSend_Click(object sender, EventArgs e)
        {
            PASSTHRU_MSG message = new PASSTHRU_MSG();
            if (selectedDevice?.API?.PassThruWriteMsgs == null)
            {
                UpdateButtonSend();
                return;
            }
            if (ButtonSend.Tag is ListView)
            {
                message = GetListViewOutMessageText().ToMessage();
            }
            else
            {
                J2534MessageText messageText = new J2534MessageText
                {
                    Data = TextBoxMessageOut.Text,
                    ProtocolName = ComboBoxMessageChannel.Text,
                    TxFlags = TextBoxOutFlags.Text
                };
                message = messageText.ToMessage();
            }
            J2534Channel channel = selectedDevice.Channel(message.ProtocolId);
            if (channel == null)
            {
                return;
            }
            if (channel.IsCAN && CheckBoxPadMessage.Checked)
            {
                if (message.DataSize < 12)
                {
                    for (int i = (int)message.DataSize; i <= 11; i++)
                    {
                        message.Data[i] = 0;
                    }
                    message.DataSize = 12;
                    message.ExtraDataIndex = 12;
                }
            }
            uint channelId = channel.ChannelId;
            if (channel.IsCAN && !channel.Connected)
            {
                J2534Channel channelISO = J2534Code.CAN_ChannelToISO_Channel(selectedDevice, channel);
                if (channelISO != null)
                {
                    if (channelISO.Connected && channelISO.MixedMode)
                    {
                        channelId = channelISO.ChannelId;
                    }
                }
            }

            IntPtr pointer = Marshal.AllocHGlobal(Marshal.SizeOf(message));
            Marshal.StructureToPtr(message, pointer, true);
            uint.TryParse(TextBoxTimeOut.Text, out uint timeout);
            int count = 1;
            J2534Result result = selectedDevice.API.PassThruWriteMsgs(channelId, pointer.ToInt32(), ref count, timeout);
            Marshal.FreeHGlobal(pointer);
            ShowResult(selectedDevice, result, "PassThruWriteMsgs");

            UpdateButtonSend();
        }

        private void ButtonClaimJ1939Address_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(ComboBoxAvailableChannel.Text) || selectedDevice == null)
            {
                return;
            }
            J2534Channel channel = ComboBoxAvailableChannel.SelectedItem as J2534Channel;
            J1939AddressClaimSetup setup = new J1939AddressClaimSetup();
            if (FormProtectAddress.DefaultInstance.ShowDialog(setup) == DialogResult.OK)
            {
                J2534Code.ClaimJ1939Address(selectedDevice, channel, setup);
            }
            FormProtectAddress.DefaultInstance.Dispose();
        }

        private void ButtonSetConfig_Click(object sender, EventArgs e)
        {
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel))
            {
                return;
            }

            for (int i = 0; i < channel.ConfParameter.Length; i++)
            {
                if (!channel.ConfParameter[i].IsReadOnly)
                {
                    J2534ConfigParameterId parameterId = channel.ConfParameter[i].Id;
                    if (ConfigChangesPresent(i) != 0)
                    {
                        uint value = GetConfigValue(channel, parameterId);
                        J2534Result result = J2534Code.SetConfigParameter(selectedDevice, channel, parameterId, value);
                        ShowResult(selectedDevice, result, string.Concat("PassThruIoctl - SET_CONFIG ", channel.ConfParameter[i].Id.ToString()));
                    }
                }
            }

            SetConfigControls();
        }

        private void ButtonClearConfig_Click(object sender, EventArgs e)
        {
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel))
            {
                return;
            }
            for (int i = 0; i < TextBoxParamVal.Length; i++)
            {
                if (TextBoxParamVal[i] != null)
                {
                    TextBoxParamVal[i].Text = string.Empty;
                    if (i < channel.ConfParameter.Length)
                    {
                        channel.ConfParameter[i].NewText = string.Empty;
                    }
                }
            }
            SetConfigControls();
        }

        private void ButtonSetVoltage_Click(object sender, EventArgs e)
        {
            SetProgrammingVoltage(selectedDevice);
        }

        private void ButtonReadVolt_Click(object sender, EventArgs e)
        {
            J2534Result result = J2534Code.ReadProgrammingVoltage(selectedDevice);
            if (selectedDevice.Analog.PinReading < 1000)
            {
                LabelVoltRead.Text = string.Concat(selectedDevice.Analog.PinReading, " mVDC");
            }
            else
            {
                LabelVoltRead.Text = string.Concat(selectedDevice.Analog.PinReading / 1000.0, " VDC");
            }
            ShowResult(selectedDevice, result, "PassThruIoctl - READ_PROG_VOLTAGE");
        }

        private void ButtonReadBatt_Click(object sender, EventArgs e)
        {
            J2534Result result = J2534Code.ReadBatteryVoltage(selectedDevice);
            if (selectedDevice.Analog.BatteryReading < 1000)
            {
                LabelBattRead.Text = string.Concat(selectedDevice.Analog.BatteryReading, " mVDC");
            }
            else
            {
                LabelBattRead.Text = string.Concat(selectedDevice.Analog.BatteryReading / 1000.0, " VDC");
            }
            ShowResult(selectedDevice, result, "PassThruIoctl - READ_VBATT");
        }

        private void CheckBoxFunctionalMessageDelete_Click(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            UpdateFunctionalMessageLookupTableEdit();
        }

        private void CheckBoxPeriodicMessageEnable_Click(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.CheckBoxPeriodicMessageEnable, sender);
            if (index < 0)
            {
                return;
            }
            PeriodicMessageCheckBoxHit[index] = true;
            SetPeriodicMessageEditBuffer(index);
            UpdatePeriodicMessageEdit();
        }

        private void CheckBoxCH_Click(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            SetChannelText();
        }

        private void ComboBoxAnalogChannel_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ShowAnalogInputs(selectedDevice);
        }

        private void ComboBoxAvailableChannel_SelectionChangeCommitted(object sender, EventArgs e)
        {
            UpdateTabs();
            if (TabControl1.SelectedTab != TabPageConnect)
            {
                ComboBoxAvailableChannel.Focus();
            }
            ComboBoxMessageChannel.Text = ComboBoxAvailableChannel.Text;
        }

        private void ComboBoxAvailableDevice_SelectionChangeCommitted(object sender, EventArgs e)
        {
            UpdateDevice();
            UpdateTabs();
            if (TabControl1.SelectedTab != TabPageConnect)
            {
                ComboBoxAvailableDevice.Focus();
            }
        }

        private void ComboBoxMessageChannel_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateButtonSend();
            UpdateButtonClaimJ1939Address();
            ComboBoxAvailableChannel.Text = ComboBoxMessageChannel.Text;
            if (ComboBoxMessageChannel.SelectedItem is J2534Channel channel)
            {
                TextBoxOutFlags.Text = J2534Code.FlagsToText(channel.DefaultTxFlags);
            }
        }

        private void ComboBoxDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateDeviceLabels();
        }

        private void ComboBoxDevice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == KeyChars.Cr)
            {
                UpdateDeviceLabels();
            }
        }

        private void ComboBoxDevice_TextChanged(object sender, EventArgs e)
        {
            UpdateDeviceLabels();
        }

        private void ComboBoxAPI_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateAPILabels();
            UpdateComboBoxDevice();
        }

        private void ComboBoxBaudRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:

                break;
                default:
                    e.Handled = !char.IsDigit(e.KeyChar);
                    break;
            }
        }

        private void ComboBoxFilterType_SelectionChangeCommitted(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.ComboBoxFilterType, sender);
            if (index < 0)
            {
                return;
            }
            SetFilterEditBuffer(index);
            UpdateFilterEdit();
        }

        private void ComboBoxConnectChannel_SelectionChangeCommitted(object sender, EventArgs e)
        {
            UpdateConnectControls();
        }

        private void ComboBoxConnector_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateComboBoxPins(ComboBoxConnectChannel.SelectedItem as J2534Channel);
        }

        private void ComboBoxPeriodicMessageChannel_SelectionChangeCommitted(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.ComboBoxPeriodicMessageChannel, sender);
            if (index < 0)
            {
                return;
            }
            SetPeriodicMessageEditBuffer(index);
            UpdatePeriodicMessageEdit();
            UpdatePeriodicMessageCheck(index);
        }

        private void ComboBoxPeriodicMessageChannel_Leave(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.ComboBoxPeriodicMessageChannel, sender);
            if (index < 0)
            {
                return;
            }
            SetPeriodicMessageEditBuffer(index);
            UpdatePeriodicMessageEdit();
            if (ActiveControl != CheckBoxPeriodicMessageEnable[index])
            {
                UpdatePeriodicMessageCheck(index);
            }
        }

        private void ComboBoxAnalogRateMode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetRateText();
        }

        private void ListViewMessageIn_SizeChanged(object sender, EventArgs e) //Event Handler added in HandleIncomingDevice
        {
            ListView thisListView = (ListView)sender;
            try
            {
                thisListView.Columns["Data Bytes"].Width = thisListView.Width - 320; // started As 292
            }
            catch
            {
            }
        }

        private void ListViewMessageIn_MouseMove(object sender, MouseEventArgs e) //Event Handler added in HandleIncomingDevice
        {
            ListView thisListView = (ListView)sender;
            if (ListViewMessageIn_MouseMove_position == MousePosition) // to avoid annoying flickering
            {
                return;
            }
            ListViewHitTestInfo info = thisListView.HitTest(thisListView.PointToClient(MousePosition));
            if (info.SubItem != null && info.SubItem.Tag != null && !string.IsNullOrWhiteSpace(info.SubItem.Text))
            {
                ToolTipMessageIn.Show(info.SubItem.Tag.ToString(), info.Item.ListView, info.Item.ListView.PointToClient(Cursor.Position + new Size(1, 25)));
            }
            else
            {
                ToolTipMessageIn.Hide(this);
            }
            ListViewMessageIn_MouseMove_position = MousePosition;
        }

        private void ListViewMessageIn_MouseLeave(object sender, EventArgs e) //Event Handler added in HandleIncomingDevice
        {
            ToolTipMessageIn.Hide(this);
        }

        private void ListViewMessageOut_Enter(object sender, EventArgs e) //Event Handler added in HandleIncomingDevice
        {
            UpdateButtonSend();
        }

        private void ListViewMessageOut_SelectedIndexChanged(object sender, EventArgs e) //Event Handler added in HandleIncomingDevice
        {
            UpdateButtonSend();
        }

        private void ListViewMessageOut_Leave(object sender, EventArgs e) //Event Handler added in HandleIncomingDevice
        {
            UpdateButtonSend();
        }

        private void ListViewMessageOut_SizeChanged(object sender, EventArgs e) //Event Handler added in HandleIncomingDevice
        {
            ListView thisListView = (ListView)sender;
            try
            {
                thisListView.Columns["Data Bytes"].Width = (thisListView.Width - 190) * 2 / 3; //started as 185
                thisListView.Columns["Comments"].Width = (thisListView.Width - 190) / 3;
            }
            catch
            {
            }
        }

        private void InitializeEditMenu()
        {
            MenuEditUndo.Enabled = false;
            MenuEditCut.Enabled = false;
            MenuEditCopy.Enabled = false;
            MenuEditCopy.Available = true;
            MenuEditCopyLine.Available = false;
            MenuEditMsgInCopyData.Available = false;
            MenuEditPaste.Enabled = false;
            MenuEditDelete.Enabled = false;
            MenuEditMsgInMakeFilter.Available = false;
            MenuEditMsgOutEditMessage.Available = false;
            MenuEditMsgOutAddMessage.Available = false;
            MenuEditMsgOutCopyToScratchPad.Available = false;
            MenuEditScratchAddToOutgoingMessageSet.Available = false;
            MenuEditMsgOutMakePeriodicMessage.Available = false;
            MenuEditSelectAll.Enabled = false;
            MenuEditClear.Enabled = false;
            MenuEditFlagsEdit.Available = false;
            MenuEditFlagsClear.Available = false;
            MenuEditFlagsSetDefault.Available = false;
            MenuEditRxStatus.Available = false;
        }

        private void ConfigureEditMenu()
        {
            if (ActiveControl is TextBox thisTextBox)
            {
                int selectedLength = thisTextBox.SelectionLength;
                int textLength = thisTextBox.Text.Length;
                MenuEditUndo.Enabled = thisTextBox.CanUndo;
                MenuEditCut.Enabled = selectedLength > 0;
                MenuEditCopy.Enabled = MenuEditCut.Enabled;
                MenuEditCopyLine.Enabled = false;
                MenuEditMsgInCopyData.Enabled = false;
                MenuEditPaste.Enabled = Clipboard.ContainsText();
                MenuEditDelete.Enabled = MenuEditCut.Enabled;
                MenuEditMsgInMakeFilter.Enabled = false;
                MenuEditMsgOutEditMessage.Enabled = false;
                MenuEditMsgOutAddMessage.Enabled = false;
                MenuEditSelectAll.Enabled = selectedLength < textLength;
                MenuEditClear.Enabled = textLength > 0;
                if (thisTextBox == TextBoxMessageOut)
                {
                    MenuEditScratchAddToOutgoingMessageSet.Available = ComboBoxMessageChannel.SelectedIndex >= 0;
                }
                else if (thisTextBox == TextBoxOutFlags)
                {
                    MenuEditFlagsEdit.Available = true;
                    MenuEditFlagsClear.Available = true;
                }
                else if (thisTextBox == TextBoxConnectFlags)
                {
                    MenuEditFlagsEdit.Available = true;
                    MenuEditFlagsClear.Available = true;
                    MenuEditFlagsSetDefault.Available = false;
                }
                else if (thisTextBox.Name.StartsWith("_TextBoxFilterFlags_") || thisTextBox.Name.StartsWith("_TextBoxPeriodicMessageFlags_"))
                {
                    MenuEditFlagsEdit.Available = true;
                    MenuEditFlagsClear.Available = true;
                }
                uint flags = GetDefaultFlags(selectedDevice, thisTextBox);
                MenuEditFlagsSetDefault.Available = flags != 0;
            }
            else if (ActiveControl is ListView thisListView)
            {
                J2534Device device = J2534Code.GetListViewParentDevice(thisListView);
                MenuEditUndo.Enabled = false;
                MenuEditCut.Enabled = false;
                MenuEditPaste.Enabled = false;
                MenuEditCopy.Enabled = false;
                int itemCount = thisListView.Items.Count;
                int selectedCount = thisListView.SelectedItems.Count;
                if (thisListView == ListViewResults)
                {
                    MenuEditCopyLine.Enabled = selectedCount > 0;
                    MenuEditCopyLine.Text = (selectedCount > 1) ? "Copy Selected" : "Copy Line";
                    MenuEditCopyLine.Available = true;
                    MenuEditDelete.Enabled = selectedCount > 0;
                    MenuEditSelectAll.Enabled = selectedCount < itemCount;
                    MenuEditClear.Enabled = itemCount > 0;
                    MenuEditCopy.Available = false;
                }
                else if (device != null && thisListView == device.ListViewMessageIn)
                {
                    MenuEditCopyLine.Enabled = selectedCount > 0;
                    MenuEditCopyLine.Text = (selectedCount > 1) ? "Copy Selected" : "Copy Line";
                    MenuEditCopyLine.Available = true;
                    MenuEditMsgInCopyData.Enabled = selectedCount == 1;
                    MenuEditMsgInCopyData.Available = true;
                    J2534Channel selectedChannel = ComboBoxMessageChannel.SelectedItem as J2534Channel;
                    J2534Channel channel = null;
                    if (selectedCount > 0)
                    {
                        channel = device.Channel(thisListView.SelectedItems[0].SubItems[1].Text);
                    }
                    int nextFilter = channel?.NextFilter ?? -1;
                    MenuEditDelete.Enabled = selectedCount > 0;
                    MenuEditMsgInMakeFilter.Enabled = selectedCount == 1 && (nextFilter >= 0) && selectedChannel == channel;
                    MenuEditSelectAll.Enabled = selectedCount < itemCount;
                    MenuEditClear.Enabled = itemCount > 0;
                    MenuEditRxStatus.Enabled = selectedCount == 1;
                    MenuEditCopy.Available = false;
                    MenuEditMsgInMakeFilter.Available = true;
                    MenuEditRxStatus.Available = true;
                }
                else if (device != null && thisListView == selectedDevice.ListViewMessageOut)
                {
                    MenuEditMsgOutEditMessage.Enabled = selectedCount > 0;
                    MenuEditMsgOutEditMessage.Available = true;
                    MenuEditDelete.Enabled = MenuEditMsgOutEditMessage.Enabled;
                    MenuEditMsgOutCopyToScratchPad.Enabled = MenuEditMsgOutEditMessage.Enabled;
                    J2534Channel selectedChannel = ComboBoxMessageChannel.SelectedItem as J2534Channel;
                    J2534Channel channel = null;
                    if (selectedCount > 0)
                    {
                        channel = device.Channel(thisListView.SelectedItems[0].Text);
                    }
                    int nextPM = channel?.NextPeriodicMessage ?? -1;
                    MenuEditMsgOutMakePeriodicMessage.Enabled = MenuEditMsgOutEditMessage.Enabled && nextPM >= 0 && J2534Code.CanMakePeriodicMessage(device, thisListView.SelectedItems[0].Text) && selectedChannel == channel;
                    MenuEditSelectAll.Enabled = false;
                    MenuEditClear.Enabled = itemCount > 0;
                    MenuEditMsgOutAddMessage.Enabled = true;
                    MenuEditMsgOutAddMessage.Available = true;
                    MenuEditMsgOutCopyToScratchPad.Available = true;
                    MenuEditMsgOutMakePeriodicMessage.Available = true;
                }
            }
            else if (ActiveControl is ComboBox thisCombo)
            {
                if (thisCombo.DropDownStyle != ComboBoxStyle.DropDown)
                {
                    return;
                }
                int selectedLength = thisCombo.SelectionLength;
                int textLength = thisCombo.Text.Length;
                MenuEditUndo.Enabled = thisCombo.CanUndo();
                MenuEditCut.Enabled = selectedLength > 0;
                MenuEditCopy.Enabled = MenuEditCut.Enabled;
                MenuEditCopyLine.Enabled = false;
                MenuEditMsgInCopyData.Enabled = false;
                MenuEditPaste.Enabled = Clipboard.ContainsText();
                MenuEditDelete.Enabled = MenuEditCut.Enabled;
                MenuEditMsgInMakeFilter.Enabled = false;
                MenuEditMsgOutEditMessage.Enabled = false;
                MenuEditMsgOutAddMessage.Enabled = false;
                MenuEditSelectAll.Enabled = selectedLength < textLength;
                MenuEditClear.Enabled = textLength > 0;
            }
        }

        private void MenuEdit_DropDownOpening(object sender, EventArgs e)
        {
            InitializeEditMenu();
            ConfigureEditMenu();
        }

        private void MenuEditClear_Click(object sender, EventArgs e)
        {
            if (ActiveControl is TextBox)
            {
                ((TextBox)ActiveControl).Clear();
            }
            else if (ActiveControl is ComboBox)
            {
                ((ComboBox)ActiveControl).SelectAll();
                ((ComboBox)ActiveControl).Delete();
            }
            else if (ActiveControl is ListView)
            {
                ((ListView)ActiveControl).Items.Clear();
            }
        }

        private void MenuEditCopy_Click(object sender, EventArgs e)
        {
            if (ActiveControl is TextBox)
            {
                ((TextBox)ActiveControl).Copy();
            }
            else if (ActiveControl is ComboBox)
            {
                ((ComboBox)ActiveControl).Copy();
            }
        }

        private void MenuEditCut_Click(object sender, EventArgs e)
        {
            if (ActiveControl is TextBox)
            {
                ((TextBox)ActiveControl).Cut();
            }
            else if (ActiveControl is ComboBox)
            {
                ((ComboBox)ActiveControl).Cut();
            }
        }

        private void MenuEditDelete_Click(object sender, EventArgs e)
        {
            if (ActiveControl is TextBox)
            {
                ((TextBox)ActiveControl).Delete();
            }
            else if (ActiveControl is ComboBox)
            {
                ((ComboBox)ActiveControl).Delete();
            }
            else if (ActiveControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase) || thisListView == ListViewResults)
                {
                    foreach (ListViewItem item in thisListView.SelectedItems)
                    {
                        thisListView.Items.Remove(item);
                    }
                }
            }
        }

        private void MenuEditFlagsClear_Click(object sender, EventArgs e)
        {
            if (ActiveControl is TextBox)
            {
                ActiveControl.Text = "0x00000000";
            }
        }

        private void MenuEditFlagsSetDefault_Click(object sender, EventArgs e)
        {
            if (ActiveControl is TextBox thisTextBox)
            {
                thisTextBox.Text = J2534Code.FlagsToText(GetDefaultFlags(selectedDevice, thisTextBox));
            }
        }

        private void MenuEditFlagsEdit_Click(object sender, EventArgs e)
        {
            if (ActiveControl is TextBox thisTextBox)
            {
                uint flags = J2534Code.TextToFlags(thisTextBox.Text);
                if (thisTextBox == TextBoxConnectFlags)
                {
                    if (FormConnectFlags.DefaultInstance.ShowDialog(ref flags) == DialogResult.OK)
                    {
                        thisTextBox.Text = J2534Code.FlagsToText(flags);
                    }
                    FormConnectFlags.DefaultInstance.Dispose();
                }
                else
                {
                    J2534Channel channel = null;
                    if (thisTextBox == TextBoxOutFlags)
                    {
                        channel = ComboBoxMessageChannel.SelectedItem as J2534Channel;
                    }
                    else
                    {
                        channel = ComboBoxAvailableChannel.SelectedItem as J2534Channel;
                    }
                    if (channel != null)
                    {
                        if (FormTxFlags.DefaultInstance.ShowDialog(ref flags, channel) == DialogResult.OK)
                        {
                            thisTextBox.Text = J2534Code.FlagsToText(flags);
                        }
                        FormTxFlags.DefaultInstance.Dispose();
                    }
                }
            }
        }

        private void MenuEditMsgInCopyData_Click(object sender, EventArgs e)
        {
            if (ActiveControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageIn", PublicDeclarations.IgnoreCase))
                {
                    if (thisListView.SelectedItems.Count > 0)
                    {
                        Clipboard.SetText(thisListView.SelectedItems[0].SubItems[3].Text);
                    }
                }
            }
        }

        private void MenuEditCopyLine_Click(object sender, EventArgs e)
        {
            if (ActiveControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageIn", PublicDeclarations.IgnoreCase))
                {
                    MsgInCopyLine(thisListView);
                }
                else if (thisListView == ListViewResults)
                {
                    ResultsCopyLine(thisListView);
                }
            }
        }

        private void MenuEditMsgInMakeFilter_Click(object sender, EventArgs e)
        {
            if (ActiveControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageIn", PublicDeclarations.IgnoreCase))
                {
                    MakeFilterFromIncomingMessage(thisListView);
                }
            }
        }

        private void MenuEditMsgOutAddMessage_Click(object sender, EventArgs e)
        {
            if (ActiveControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    J2534MessageText messageText = GetNewMessageTextFromDialog();
                    if (!string.IsNullOrWhiteSpace(messageText.ProtocolName))
                    {
                        AddMessageToOutgoingListView(thisListView, messageText);
                    }
                }
            }
        }

        private void MenuEditMsgOutEditMessage_Click(object sender, EventArgs e)
        {
            if (ActiveControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    ModifySelectedOutgoingMessage(thisListView);
                }
            }
        }

        private void MenuEditMsgOutCopyToScratchPad_Click(object sender, EventArgs e)
        {
            if (ActiveControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    CopyOutgoingMessageToScratchPad(thisListView);
                }
            }
        }

        private void MenuEditMsgOutMakePeriodicMessage_Click(object sender, EventArgs e)
        {
            if (ActiveControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    MakePeriodicFromOutgoingMessage(thisListView);
                }
            }
        }

        private void MenuEditScratchAddToOutgoingMessageSet_Click(object sender, EventArgs e)
        {
            if (selectedDevice != null && selectedDevice.ListViewMessageOut != null)
            {
                J2534MessageText messageText = GetScratchPadMessageText();
                AddMessageToOutgoingListView(selectedDevice.ListViewMessageOut, messageText);
            }
        }

        private void MenuEditPaste_Click(object sender, EventArgs e)
        {
            if (!Clipboard.ContainsText())
            {
                return;
            }
            if (ActiveControl is TextBox)
            {
                ((TextBox)ActiveControl).Paste();
            }
            else if (ActiveControl is ComboBox)
            {
                ((ComboBox)ActiveControl).Paste();
            }
        }

        private void MenuEditRxStatus_Click(object sender, EventArgs e)
        {
            if (ActiveControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageIn", PublicDeclarations.IgnoreCase) && thisListView.SelectedItems.Count > 0)
                {
                    J2534ProtocolId protocolId = J2534Code.NameToProtocolId(thisListView.SelectedItems[0].SubItems[1].Text);
                    uint flags = J2534Code.TextToFlags(thisListView.SelectedItems[0].SubItems[2].Text);
                    FormRxStatus.DefaultInstance.ShowDialog(ref flags, protocolId);
                    FormRxStatus.DefaultInstance.Dispose();
                }
            }
        }

        private void MenuEditSelectAll_Click(object sender, EventArgs e)
        {
            if (ActiveControl is TextBox)
            {
                ((TextBox)ActiveControl).SelectAll();
            }
            else if (ActiveControl is ComboBox)
            {
                ((ComboBox)ActiveControl).SelectAll();
            }
            else if (ActiveControl is ListView thisListView)
            {
                if (string.Equals(thisListView.Name, "ListViewMessageIn", PublicDeclarations.IgnoreCase) || thisListView == ListViewResults)
                {
                    thisListView.SelectAll();
                }
            }
        }

        private void MenuEditUndo_Click(object sender, EventArgs e)
        {
            if (ActiveControl is TextBox)
            {
                ((TextBox)ActiveControl).Undo();
            }
            else if (ActiveControl is ComboBox)
            {
                ((ComboBox)ActiveControl).Undo();
            }
        }

        private void FileMenuInit()
        {
            MenuFileMsgOutLoad.Available = false;
            MenuFilePeriodicMessageLoad.Available = false;
            MenuFileFilterLoad.Available = false;
            MenuFileSpace1.Available = false;

            MenuFileMsgInSaveSelected.Available = false;
            MenuFileMsgInSaveAll.Available = false;
            MenuFileMsgOutSave.Available = false;
            MenuFilePeriodicMessageSave.Available = false;
            MenuFileFilterSave.Available = false;
            MenuFileSpace2.Available = false;
        }

        private void SetFileMenus()
        {
            bool boxExists = selectedDevice != null;
            if (TabControl1.SelectedTab == TabPageMessages)
            {
                MenuFileMsgInSaveAll.Available = true;
                MenuFileMsgInSaveAll.Enabled = boxExists && selectedDevice.ListViewMessageIn.Items.Count > 0;
                MenuFileMsgInSaveSelected.Available = true;
                MenuFileMsgInSaveSelected.Enabled = boxExists && selectedDevice.ListViewMessageIn.SelectedItems.Count > 0;
                MenuFileMsgInSaveSelected.Text = (boxExists && selectedDevice.ListViewMessageIn.SelectedItems.Count > 1) ? "Save Selected Incoming Messages" : "Save Selected Incoming Message";
                MenuFileMsgOutLoad.Available = true;
                MenuFileMsgOutLoad.Enabled = boxExists;
                MenuFileMsgOutSave.Available = true;
                MenuFileMsgOutSave.Enabled = boxExists && selectedDevice.ListViewMessageOut.Items.Count > 0;
            }
            else if (TabControl1.SelectedTab == TabPagePeriodicMessages)
            {
                MenuFilePeriodicMessageLoad.Available = true;
                MenuFilePeriodicMessageLoad.Enabled = TextBoxPeriodicMessage[0].Enabled;
                J2534Channel channel = ComboBoxAvailableChannel.SelectedItem as J2534Channel;
                MenuFilePeriodicMessageSave.Available = true;
                MenuFilePeriodicMessageSave.Enabled = channel != null && channel.PeriodicMessagesExist;
            }
            else if (TabControl1.SelectedTab == TabPageFilters)
            {
                MenuFileFilterLoad.Available = true;
                MenuFileFilterLoad.Enabled = TextBoxFilterMask[0].Enabled;
                J2534Channel channel = ComboBoxAvailableChannel.SelectedItem as J2534Channel;
                MenuFileFilterSave.Available = true;
                MenuFileFilterSave.Available = channel != null && channel.FiltersExist;
            }
            MenuFileSpace1.Available = MenuFileFilterLoad.Available || MenuFilePeriodicMessageLoad.Available || MenuFileMsgOutLoad.Available;
            MenuFileSpace2.Available = MenuFileFilterSave.Available || MenuFilePeriodicMessageSave.Available || MenuFileMsgInSaveAll.Available || MenuFileMsgInSaveSelected.Available || MenuFileMsgOutSave.Available;
        }

        private void MenuFile_DropDownOpening(object sender, EventArgs e)
        {
            FileMenuInit();
            SetFileMenus();
        }

        private void MenuFileMsgInSaveAll_Click(object sender, EventArgs e)
        {
            if (selectedDevice == null)
            {
                return;
            }
            ListViewItem[] itemList = new ListViewItem[selectedDevice.ListViewMessageIn.Items.Count];
            selectedDevice.ListViewMessageIn.Items.CopyTo(itemList, 0);
            SaveListViewItemListToFile(itemList);
        }

        private void MenuFileMsgInSaveSelected_Click(object sender, EventArgs e)
        {
            if (selectedDevice == null)
            {
                return;
            }
            ListViewItem[] itemList = new ListViewItem[selectedDevice.ListViewMessageIn.SelectedItems.Count];
            selectedDevice.ListViewMessageIn.SelectedItems.CopyTo(itemList, 0);
            SaveListViewItemListToFile(itemList);
        }

        private void MenuFileMsgOutLoad_Click(object sender, EventArgs e)
        {
            if (selectedDevice == null)
            {
                return;
            }
            LoadOutgoingMessageListFromFile(selectedDevice.ListViewMessageOut);
        }

        private void MenuFileMsgOutSave_Click(object sender, EventArgs e)
        {
            if (selectedDevice == null)
            {
                return;
            }
            SaveOutgoingMessageListToFile(selectedDevice.ListViewMessageOut);
        }

        private void MenuFileFilterLoad_Click(object sender, EventArgs e)
        {
            LoadFiltersFromFile();
        }

        private void MenuFileFilterSave_Click(object sender, EventArgs e)
        {
            SaveFiltersToFile();
        }

        private void MenuFilePeriodicMessageLoad_Click(object sender, EventArgs e)
        {
            LoadPeriodicMessagesFromFile();
        }

        private void MenuFilePeriodicMessageSave_Click(object sender, EventArgs e)
        {
            SavePeriodicMessagesToFile();
        }

        private void MenuFileExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ContextMenuStripFlags_Opening(object sender, CancelEventArgs e)
        {
            if (ContextMenuStripFlags.SourceControl is TextBox thisTextBox)
            {
                uint flags = 0;
                if (selectedDevice != null)
                {
                    flags = GetDefaultFlags(selectedDevice, thisTextBox);
                }
                if (thisTextBox.Enabled)
                {
                    thisTextBox.Focus();
                    MenuFlagsCut.Enabled = thisTextBox.SelectionLength > 0;
                    MenuFlagsCopy.Enabled = MenuFlagsCut.Enabled;
                    MenuFlagsUndo.Enabled = thisTextBox.CanUndo;
                    MenuFlagsPaste.Enabled = Clipboard.ContainsText();
                    MenuFlagsDelete.Enabled = MenuFlagsCut.Enabled;
                    MenuFlagsSelectAll.Enabled = thisTextBox.SelectionLength < thisTextBox.Text.Length;
                    MenuFlagsEdit.Enabled = true;
                    MenuFlagsClear.Enabled = true;
                    MenuFlagsSetDefault.Available = flags != 0;
                }
            }
        }

        private void MenuFlagsClear_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Text = "0x00000000";
            }
        }

        private void MenuFlagsSetDefault_Click(object sender, EventArgs e)
        {
            if (selectedDevice == null)
            {
                return;
            }
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox thisTextBox)
            {
                thisTextBox.Text = J2534Code.FlagsToText(GetDefaultFlags(selectedDevice, thisTextBox));
            }
        }

        private void MenuFlagsEdit_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox thisTextBox)
            {
                uint flags = J2534Code.TextToFlags(thisTextBox.Text);
                if (thisTextBox == TextBoxConnectFlags)
                {
                    if (FormConnectFlags.DefaultInstance.ShowDialog(ref flags) == DialogResult.OK)
                    {
                        thisTextBox.Text = J2534Code.FlagsToText(flags);
                    }
                    FormConnectFlags.DefaultInstance.Dispose();
                }
                else
                {
                    J2534Channel channel = null;
                    if (thisTextBox == TextBoxOutFlags)
                    {
                        channel = ComboBoxMessageChannel.SelectedItem as J2534Channel;
                    }
                    else
                    {
                        channel = ComboBoxMessageChannel.SelectedItem as J2534Channel;
                    }
                    if (FormTxFlags.DefaultInstance.ShowDialog(ref flags, channel) == DialogResult.OK)
                    {
                        thisTextBox.Text = J2534Code.FlagsToText(flags);
                    }
                    FormTxFlags.DefaultInstance.Dispose();
                }
            }
        }

        private void MenuFlagsCopy_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Copy();
            }
        }

        private void MenuFlagsCut_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Cut();
            }
        }

        private void MenuFlagsDelete_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Delete();
            }
        }

        private void MenuFlagsPaste_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Paste();
            }
        }

        private void MenuFlagsSelectAll_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).SelectAll();
            }
        }

        private void MenuFlagsUndo_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Undo();
            }
        }

        private void MenuHelpAbout_Click(object sender, EventArgs e)
        {
            FormSplash.DefaultInstance.Show();
            FormSplash.DefaultInstance.Timer1.Enabled = false;
            FormSplash.DefaultInstance.ButtonOK.Visible = true;
        }

        private void ContextMenuStripMsgIn_Opening(object sender, CancelEventArgs e)
        {
            if (ContextMenuStripMessageIn.SourceControl is ListView thisListView)
            {
                J2534Device device = J2534Code.GetListViewParentDevice(thisListView);
                if (device == null)
                {
                    return;
                }
                int selectedCount = thisListView.SelectedItems.Count;
                int itemCount = thisListView.Items.Count;
                MenuMsgInSelectAll.Enabled = selectedCount < itemCount;
                MenuMsgInCopyLine.Enabled = selectedCount > 0;
                MenuMsgInCopyLine.Text = (selectedCount > 1) ? "Copy Selected" : "Copy Line";
                MenuMsgInCopyData.Enabled = selectedCount == 1;
                MenuMsgInRxStatus.Enabled = selectedCount == 1;
                J2534Channel channel = null;
                J2534Channel selectedChannel = ComboBoxMessageChannel.SelectedItem as J2534Channel;
                if (selectedCount > 0)
                {
                    channel = device.Channel(thisListView.SelectedItems[0].SubItems[1].Text);
                }
                int nextFilter = channel?.NextFilter ?? -1;
                MenuMsgInMakeFilter.Enabled = selectedCount == 1 && nextFilter >= 0 && selectedChannel == channel;
                MenuMsgInSaveAll.Enabled = itemCount > 0;
                MenuMsgInSaveSelected.Enabled = selectedCount > 0;
                MenuMsgInClear.Enabled = itemCount > 0;
            }
        }

        private void MenuMsgInClear_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView)
            {
                ((ListView)thisMenuStrip.SourceControl).Items.Clear();
            }
        }

        private void MenuMsgInCopyData_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.SelectedItems.Count == 1)
                {
                    Clipboard.SetText(thisListView.SelectedItems[0].SubItems[3].Text);
                }
            }
        }

        private void MenuMsgInCopyLine_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageIn", PublicDeclarations.IgnoreCase))
                {
                    MsgInCopyLine(thisListView);
                }
            }
        }

        private void MenuMsgInMakeFilter_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageIn", PublicDeclarations.IgnoreCase))
                {
                    MakeFilterFromIncomingMessage(thisListView);
                }
            }
        }

        private void MenuMsgInRxStatus_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageIn", PublicDeclarations.IgnoreCase) && thisListView.SelectedItems.Count > 0)
                {
                    J2534ProtocolId protocolId = J2534Code.NameToProtocolId(thisListView.SelectedItems[0].SubItems[1].Text);
                    uint flags = J2534Code.TextToFlags(thisListView.SelectedItems[0].SubItems[2].Text);
                    FormRxStatus.DefaultInstance.ShowDialog(ref flags, protocolId);
                    FormRxStatus.DefaultInstance.Dispose();
                }
            }
        }

        private void MenuMsgInSaveAll_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageIn", PublicDeclarations.IgnoreCase))
                {
                    ListViewItem[] itemList = new ListViewItem[thisListView.Items.Count];
                    thisListView.Items.CopyTo(itemList, 0);
                    SaveListViewItemListToFile(itemList);
                }
            }
        }

        private void MenuMsgInSaveSelected_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageIn", PublicDeclarations.IgnoreCase))
                {
                    ListViewItem[] itemList = new ListViewItem[thisListView.SelectedItems.Count];
                    thisListView.SelectedItems.CopyTo(itemList, 0);
                    SaveListViewItemListToFile(itemList);
                }
            }
        }

        private void MenuMsgInSelectAll_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageIn", PublicDeclarations.IgnoreCase))
                {
                    thisListView.SelectAll();
                }
            }
        }

        private void ContextMenuStripMsgOut_Opening(object sender, CancelEventArgs e)
        {
            if (!(ContextMenuStripMessageOut.SourceControl is ListView))
            {
                return;
            }
            ListView thisListView = (ListView)ContextMenuStripMessageOut.SourceControl;
            J2534Device device = J2534Code.GetListViewParentDevice(thisListView);
            if (device == null)
            {
                return;
            }
            int itemCount = thisListView.Items.Count;
            int selectedCount = thisListView.SelectedItems.Count;
            bool selectionOK = selectedCount == 1;
            MenuMsgOutEditMessage.Enabled = selectionOK;
            MenuMsgOutDelete.Enabled = selectedCount > 0;
            MenuMsgOutCopyToScratchPad.Enabled = selectionOK;
            J2534Channel selectedChannel = ComboBoxMessageChannel.SelectedItem as J2534Channel;
            J2534Channel channel = null;
            if (selectionOK)
            {
                channel = device.Channel(thisListView.SelectedItems[0].Text);
            }
            int nextPM = channel?.NextPeriodicMessage ?? -1;
            MenuMsgOutMakePeriodicMessage.Enabled = selectionOK && nextPM >= 0 && J2534Code.CanMakePeriodicMessage(device, thisListView.SelectedItems[0].Text) && selectedChannel == channel;
            MenuMsgOutAddMessage.Enabled = true;
            MenuMsgOutSave.Enabled = itemCount > 0;
            MenuMsgOutLoad.Enabled = true;
            MenuMsgOutClear.Enabled = itemCount > 0;
        }

        private void MenuMsgOutClear_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    thisListView.Items.Clear();
                }
            }
        }

        private void MenuMsgOutDelete_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    foreach (ListViewItem item in thisListView.SelectedItems)
                    {
                        thisListView.Items.Remove(item);
                    }
                }
            }
        }

        private void MenuMsgOutAddMessage_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    J2534MessageText messageText = GetNewMessageTextFromDialog();
                    if (!string.IsNullOrWhiteSpace(messageText.ProtocolName))
                    {
                        AddMessageToOutgoingListView(thisListView, messageText);
                    }
                }
            }
        }

        private void MenuMsgOutLoad_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    LoadOutgoingMessageListFromFile(thisListView);
                }
            }
        }

        private void MenuMsgOutEditMessage_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    ModifySelectedOutgoingMessage(thisListView);
                }
            }
        }

        private void MenuMsgOutMakePeriodicMessage_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    MakePeriodicFromOutgoingMessage(thisListView);
                }
            }
        }

        private void MenuMsgOutSave_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    SaveOutgoingMessageListToFile(thisListView);
                }
            }
        }

        private void MenuMsgOutCopyToScratchPad_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView.Name.Equals("ListViewMessageOut", PublicDeclarations.IgnoreCase))
                {
                    CopyOutgoingMessageToScratchPad(thisListView);
                }
            }
        }

        private void ContextMenuStripScratchPad_Opening(object sender, CancelEventArgs e)
        {
            if (!TextBoxMessageOut.Enabled)
            {
                return;
            }
            int selectedLenth = TextBoxMessageOut.SelectionLength;
            int textLength = TextBoxMessageOut.TextLength;
            MenuScratchPadPaste.Enabled = Clipboard.ContainsText();
            MenuScratchPadCut.Enabled = selectedLenth > 0;
            MenuScratchPadCopy.Enabled = MenuScratchPadCut.Enabled;
            MenuScratchPadDelete.Enabled = MenuScratchPadCut.Enabled;
            MenuScratchPadUndo.Enabled = TextBoxMessageOut.CanUndo;
            MenuScratchPadSelectAll.Enabled = selectedLenth < textLength;
            MenuScratchPadAddToOutgoingMessageSet.Available = ComboBoxMessageChannel.SelectedIndex >= 0;
        }

        private void MenuScratchPadCopy_Click(object sender, EventArgs e)
        {
            TextBoxMessageOut.Copy();
        }

        private void MenuScratchPadCut_Click(object sender, EventArgs e)
        {
            TextBoxMessageOut.Cut();
        }

        private void MenuScratchPadDelete_Click(object sender, EventArgs e)
        {
            TextBoxMessageOut.Delete();
        }

        private void MenuScratchPadPaste_Click(object sender, EventArgs e)
        {
            TextBoxMessageOut.Paste();
        }

        private void MenuScratchPadSelectAll_Click(object sender, EventArgs e)
        {
            TextBoxMessageOut.SelectAll();
        }

        private void MenuScratchPadUndo_Click(object sender, EventArgs e)
        {
            TextBoxMessageOut.Undo();
        }

        private void MenuScratchPadAddToOutgoingMessageSet_Click(object sender, EventArgs e)
        {
            if (selectedDevice != null && selectedDevice.ListViewMessageOut != null)
            {
                J2534MessageText messageText = GetScratchPadMessageText();
                AddMessageToOutgoingListView(selectedDevice.ListViewMessageOut, messageText);
            }
        }

        private void ContextMenuStripTextBox_Opening(object sender, CancelEventArgs e)
        {
            if (ContextMenuStripTextBox.SourceControl is TextBox thisTextBox)
            {
                int selectedLength = thisTextBox.SelectionLength;
                int textLength = thisTextBox.TextLength;
                MenuTextBoxPaste.Enabled = Clipboard.ContainsText() && !thisTextBox.ReadOnly;
                MenuTextBoxCopy.Enabled = (selectedLength > 0);
                MenuTextBoxCut.Enabled = MenuTextBoxCopy.Enabled && !thisTextBox.ReadOnly;
                MenuTextBoxDelete.Enabled = MenuTextBoxCut.Enabled;
                MenuTextBoxUndo.Enabled = thisTextBox.CanUndo;
                MenuTextBoxSelectAll.Enabled = (selectedLength < textLength);
            }
            else if (ContextMenuStripTextBox.SourceControl is ComboBox thisComboBox)
            {
                if (thisComboBox.DropDownStyle != ComboBoxStyle.DropDown)
                {
                    e.Cancel = true;
                    return;
                }
                int selectedLength = thisComboBox.SelectionLength;
                int textLength = thisComboBox.Text.Length;
                MenuTextBoxPaste.Enabled = Clipboard.ContainsText();
                MenuTextBoxCut.Enabled = (selectedLength > 0);
                MenuTextBoxCopy.Enabled = MenuTextBoxCut.Enabled;
                MenuTextBoxDelete.Enabled = MenuTextBoxCut.Enabled;
                MenuTextBoxUndo.Enabled = thisComboBox.CanUndo();
                MenuTextBoxSelectAll.Enabled = (selectedLength < textLength);
            }
        }

        private void MenuTextBoxCopy_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Copy();
            }
            else if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).Copy();
            }
        }

        private void MenuTextBoxCut_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Cut();
            }
            else if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).Cut();
            }
        }

        private void MenuTextBoxDelete_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Delete();
            }
            else if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).Delete();
            }
        }

        private void MenuTextBoxPaste_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Paste();
            }
            else if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).Paste();
            }
        }

        private void MenuTextBoxSelectAll_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).SelectAll();
            }
            else if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).SelectAll();
            }
        }

        private void MenuTextBoxUndo_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Undo();
            }
            else if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).Undo();
            }
        }

        private void RadioButtonPin_CheckedChanged(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            if (!(sender is RadioButton pinButton))
            {
                return;
            }
            if (pinButton.Checked)
            {
                UpdatePinControls(selectedDevice);
            }
        }

        private void MenuDeviceUndo_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).Undo();
            }
        }

        private void MenuDeviceCut_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).Cut();
            }
        }

        private void MenuDeviceCopy_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).Copy();
            }
        }

        private void MenuDevicePaste_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).Paste();
            }
        }

        private void MenuDeviceDelete_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).Delete();
            }
        }

        private void MenuDeviceSelectAll_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ComboBox)
            {
                ((ComboBox)thisMenuStrip.SourceControl).SelectAll();
            }
        }

        private void ContextMenuStripResults_Opening(object sender, CancelEventArgs e)
        {
            if (!(ContextMenuStripResults.SourceControl is ListView))
            {
                return;
            }
            ListView thisListView = (ListView)ContextMenuStripResults.SourceControl;
            int selectedCount = thisListView.SelectedItems.Count;
            int itemCount = thisListView.Items.Count;
            MenuResultsSelectAll.Enabled = selectedCount < itemCount;
            MenuResultsCopyLine.Enabled = selectedCount > 0;
            MenuResultsCopyLine.Text = (selectedCount > 1) ? "Copy Selected" : "Copy Line";
            MenuResultsDelete.Enabled = selectedCount > 0;
            MenuResultsClear.Enabled = itemCount > 0;
        }

        private void MenuResultsCopyLine_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                if (thisListView == ListViewResults)
                {
                    ResultsCopyLine(thisListView);
                }
            }
        }

        private void MenuResultsDelete_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView thisListView)
            {
                foreach (ListViewItem item in thisListView.SelectedItems)
                {
                    thisListView.Items.Remove(item);
                }
            }
        }

        private void MenuResultsSelectAll_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView)
            {
                ((ListView)thisMenuStrip.SourceControl).SelectAll();
            }
        }

        private void MenuResultsClear_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is ListView)
            {
                ((ListView)thisMenuStrip.SourceControl).Items.Clear();
            }
        }

        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTabs();
        }

        private void TimerMessageReceive_Tick(object sender, EventArgs e) //Eventhandler set in OpenJ2534Device
        {
            J2534Device device = J2534Code.GetTimerParentDevice((Timer)sender);
            if (device != null && device.Setup.GetMessages)
            {
                GetMessages(device);
            }
        }

        private void TextBox5BInit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void TextBoxFIFlags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void TextBoxFilterFlags_TextChanged(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            if (settingFilters)
            {
                return;
            }
            int index = Array.IndexOf(TextBoxFilterFlags, sender);
            if (index < 0)
            {
                return;
            }
            SetFilterEditBuffer(index);
            UpdateFilterEdit();
        }

        private void TextBoxFilterFlags_KeyPress(object sender, KeyPressEventArgs e) //Event Handler added in SetControlArrays
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                int index = Array.IndexOf(TextBoxPeriodicMessageFlags, sender);
                if (index < 0)
                {
                    return;
                }
                SetFilterEditBuffer(index);
                UpdateFilterEdit();
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void TextBoxFilterFlags_Leave(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(TextBoxFilterFlags, sender);
            if (index < 0)
            {
                return;
            }
            SetFilterEditBuffer(index);
            UpdateFilterEdit();
        }

        private void TextBoxFilterFlow_TextChanged(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            if (settingFilters)
            {
                return;
            }
            int index = Array.IndexOf(TextBoxFilterFlow, sender);
            if (index < 0)
            {
                return;
            }
            SetFilterEditBuffer(index);
            UpdateFilterEdit();
        }

        private void TextBoxFilterFlow_KeyPress(object sender, KeyPressEventArgs e) //Event Handler added in SetControlArrays
        {
            switch (e.KeyChar)
            {
                case KeyChars.Cr:
                    int index = Array.IndexOf(TextBoxFilterFlow, sender);
                    SetFilterEditBuffer(index);
                    UpdateFilterEdit();
                    break;
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:

                break;
                default:
                    if (!e.KeyChar.IsHex())
                    {
                        e.Handled = true;
                    }
                    break;
            }
        }

        private void TextBoxFilterFlow_Leave(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(TextBoxFilterFlow, sender);
            if (index < 0)
            {
                return;
            }
            SetFilterEditBuffer(index);
            UpdateFilterEdit();
        }

        private void TextBoxFilterMask_TextChanged(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            if (settingFilters)
            {
                return;
            }
            int index = Array.IndexOf(TextBoxFilterMask, sender);
            if (index < 0)
            {
                return;
            }
            SetFilterEditBuffer(index);
            UpdateFilterEdit();
        }

        private void TextBoxFilterMask_KeyPress(object sender, KeyPressEventArgs e) //Event Handler added in SetControlArrays
        {
            switch (e.KeyChar)
            {
                case KeyChars.Cr:
                    int index = Array.IndexOf(TextBoxFilterMask, sender);
                    SetFilterEditBuffer(index);
                    UpdateFilterEdit();
                    break;
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:

                break;
                default:
                    if (!e.KeyChar.IsHex())
                    {
                        e.Handled = true;
                    }
                    break;
            }
        }

        private void TextBoxFilterMask_Leave(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(TextBoxFilterMask, sender);
            if (index < 0)
            {
                return;
            }
            SetFilterEditBuffer(index);
            UpdateFilterEdit();
        }

        private void TextBoxFilterPatt_TextChanged(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            if (settingFilters)
            {
                return;
            }
            int index = Array.IndexOf(TextBoxFilterPatt, sender);
            if (index < 0)
            {
                return;
            }
            SetFilterEditBuffer(index);
            UpdateFilterEdit();
        }

        private void TextBoxFilterPatt_KeyPress(object sender, KeyPressEventArgs e) //Event Handler added in SetControlArrays
        {
            switch (e.KeyChar)
            {
                case KeyChars.Cr:
                    int index = Array.IndexOf(TextBoxFilterPatt, sender);
                    SetFilterEditBuffer(index);
                    UpdateFilterEdit();
                    break;
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:

                break;
                default:
                    if (!e.KeyChar.IsHex())
                    {
                        e.Handled = true;
                    }
                    break;
            }
        }

        private void TextBoxFilterPatt_Leave(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.TextBoxFilterPatt, sender);
            if (index < 0)
            {
                return;
            }
            SetFilterEditBuffer(index);
            UpdateFilterEdit();
        }

        private void TextBoxFIMessage_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case KeyChars.Cr:
                    int index = Array.IndexOf(TextBoxFilterPatt, sender);
                    SetFilterEditBuffer(index);
                    UpdateFilterEdit();

                    break;
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:

                break;
                default:
                    if (!e.KeyChar.IsHex())
                    {
                        e.Handled = true;
                    }

                    break;
            }
        }

        private void TextBoxConnectFlags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void TextBoxOutFlags_Enter(object sender, EventArgs e)
        {
            UpdateButtonSend();
        }

        private void TextBoxOutFlags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void TextBoxOutFlags_Leave(object sender, EventArgs e)
        {
            UpdateButtonSend();
        }

        private void TextBoxFunctionalMessage_TextChanged(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            UpdateFunctionalMessageLookupTableEdit();
        }

        private void TextBoxFunctionalMessage_KeyPress(object sender, KeyPressEventArgs e) //Event Handler added in SetControlArrays
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                UpdateFunctionalMessageLookupTableEdit();
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void TextBoxFunctionalMessage_Leave(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            UpdateFunctionalMessageLookupTableEdit();
        }

        private void TextBoxMessageOut_Enter(object sender, EventArgs e)
        {
            UpdateButtonSend();
        }

        private void TextBoxMessageOut_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case KeyChars.Cr:
                    UpdateButtonSend();
                    break;
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:

                break;
                default:
                    if (!(e.KeyChar.IsHex() || e.KeyChar == ' '))
                    {
                        e.Handled = true;
                    }
                    break;
            }
        }

        private void TextBoxMessageOut_Leave(object sender, EventArgs e)
        {
            UpdateButtonSend();
        }

        private void TextBoxParamVal_TextChanged(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.TextBoxParamVal, sender);
            if (index < 0)
            {
                return;
            }
            SetEditConfig(index);
            UpdateConfigEdit();
        }

        private void TextBoxParamVal_KeyPress(object sender, KeyPressEventArgs e) //Event Handler added in SetControlArrays
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                int index = Array.IndexOf(this.TextBoxParamVal, sender);
                SetEditConfig(index);
                UpdateConfigEdit();
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void TextBoxParamVal_Leave(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.TextBoxParamVal, sender);
            if (index < 0)
            {
                return;
            }
            SetEditConfig(index);
            UpdateConfigEdit();
        }

        private void TextBoxAnalogRateConfig_TextChanged(object sender, EventArgs e)
        {
            UpdateRateControls();
        }

        private void TextBoxAnalogRate_TextChanged(object sender, EventArgs e)
        {
            if (settingTextBoxAnalogRate)
            {
                return;
            }
            SetRateText();
        }

        private void TextBoxAnalogRateConfig_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                UpdateRateControls();
                SetRateText();
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void TextBoxActiveChannelConfig_TextChanged(object sender, EventArgs e)
        {
            configChannels = TextBoxActiveChannelConfig.Text.ToUint32();
            if (!settingTextBoxActiveChannelConfig)
            {
                UpdateChannelCheckBoxes();
            }
        }

        private void TextBoxActiveChannelConfig_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                UpdateChannelCheckBoxes();
                SetChannelText();
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void TextBoxPeriodicMessageFlags_TextChanged(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            if (settingPeriodicMessages)
            {
                return;
            }
            int index = Array.IndexOf(this.TextBoxPeriodicMessageFlags, sender);
            if (index < 0)
            {
                return;
            }
            SetPeriodicMessageEditBuffer(index);
            UpdatePeriodicMessageEdit();
        }

        private void TextBoxPeriodicMessageFlags_KeyPress(object sender, KeyPressEventArgs e) //Event Handler added in SetControlArrays
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                int index = Array.IndexOf(TextBoxPeriodicMessageFlags, sender);
                SetPeriodicMessageEditBuffer(index);
                UpdatePeriodicMessageEdit();
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void TextBoxPeriodicMessageFlags_Leave(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.TextBoxPeriodicMessageFlags, sender);
            if (index < 0)
            {
                return;
            }
            SetPeriodicMessageEditBuffer(index);
            UpdatePeriodicMessageEdit();
        }

        private void TextBoxPeriodicMessageInterval_TextChanged(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            if (settingPeriodicMessages)
            {
                return;
            }
            int index = Array.IndexOf(this.TextBoxPeriodicMessageInterval, sender);
            if (index < 0)
            {
                return;
            }
            SetPeriodicMessageEditBuffer(index);
            UpdatePeriodicMessageEdit();
        }

        private void TextBoxPeriodicMessageInterval_KeyPress(object sender, KeyPressEventArgs e) //Event Handler added in SetControlArrays
        {
            switch (e.KeyChar)
            {
                case KeyChars.Cr:
                    int index = Array.IndexOf(TextBoxPeriodicMessageInterval, sender);
                    SetPeriodicMessageEditBuffer(index);
                    UpdatePeriodicMessageEdit();

                    break;
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:

                break;
                default:
                    e.Handled = !char.IsDigit(e.KeyChar);

                    break;
            }
        }

        private void TextBoxPeriodicMessageInterval_Leave(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.TextBoxPeriodicMessageInterval, sender);
            if (index < 0)
            {
                return;
            }
            SetPeriodicMessageEditBuffer(index);
            UpdatePeriodicMessageEdit();
        }

        private void TextBoxPeriodicMessage_TextChanged(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            if (settingPeriodicMessages)
            {
                return;
            }
            int index = Array.IndexOf(this.TextBoxPeriodicMessage, sender);
            if (index < 0)
            {
                return;
            }
            SetPeriodicMessageEditBuffer(index);
            UpdatePeriodicMessageEdit();
            UpdatePeriodicMessageCheck(index);
        }

        private void TextBoxPeriodicMessage_KeyPress(object sender, KeyPressEventArgs e) //Event Handler added in SetControlArrays
        {
            switch (e.KeyChar)
            {
                case KeyChars.Cr:
                    int index = Array.IndexOf(this.TextBoxPeriodicMessage, sender);
                    SetPeriodicMessageEditBuffer(index);
                    UpdatePeriodicMessageEdit();
                    UpdatePeriodicMessageCheck(index);

                    break;
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:

                break;
                default:
                    if (!e.KeyChar.IsHex())
                    {
                        e.Handled = true;
                    }

                    break;
            }
        }

        private void TextBoxPeriodicMessage_Leave(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.TextBoxPeriodicMessage, sender);
            if (index < 0)
            {
                return;
            }
            SetPeriodicMessageEditBuffer(index);
            UpdatePeriodicMessageEdit();
            if (ActiveControl != CheckBoxPeriodicMessageEnable[index])
            {
                UpdatePeriodicMessageCheck(index);
            }
        }

        private void TextBoxReadRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case KeyChars.Cr:
                    int readRate = 0;
                    int.TryParse(TextBoxReadRate.Text, out readRate);
                    if (selectedDevice != null)
                    {
                        if (readRate != 0)
                        {
                            selectedDevice.TimerMessageReceive.Interval = readRate;
                        }
                    }

                    break;
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:

                break;
                default:
                    e.Handled = !char.IsDigit(e.KeyChar);

                    break;
            }
        }

        private void TextBoxTimeOut_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case KeyChars.Cr:

                break;
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:

                break;
                default:
                    e.Handled = !char.IsDigit(e.KeyChar);

                    break;
            }
        }

        private void TextBoxVoltSetting_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == KeyChars.Cr)
            {
                UpdatePinVoltage();
            }
            e.Handled = !char.IsDigit(e.KeyChar);
        }

        private void TextBoxVoltSetting_Leave(object sender, EventArgs e)
        {
            UpdatePinVoltage();
        }

        private void UpdatePinVoltage()
        {
            uint.TryParse(TextBoxVoltSetting.Text, out uint pinVoltage);
            if (pinVoltage < 5000)
            {
                pinVoltage = 5000;
            }
            if (pinVoltage > 20000)
            {
                pinVoltage = 20000;
            }
            TextBoxVoltSetting.Text = pinVoltage.ToString();
            if (selectedDevice != null)
            {
                selectedDevice.Analog.PinSetting = pinVoltage;
            }
        }

        private void UpdateComboBoxAvailableChannel()
        {
            UpdateComboBoxAvailableChannel(null);
        }
        private void UpdateComboBoxAvailableChannel(string name)
        {
            int m = -1;
            if (selectedDevice != null)
            {
                m = selectedDevice.MaxChannel;
            }
            string selectedChannel = ComboBoxAvailableChannel.Text;
            ComboBoxAvailableChannel.Items.Clear();
            for (int index = 0; index <= m; index++)
            {
                if (selectedDevice.Channel(index).Connected)
                {
                    ComboBoxAvailableChannel.Items.Add(selectedDevice.Channel(index));
                }
            }
            if (ComboBoxAvailableChannel.Items.Count > 0)
            {
                ComboBoxAvailableChannel.Text = selectedChannel;
                if (!string.IsNullOrWhiteSpace(name))
                {
                    ComboBoxAvailableChannel.Text = name;
                }
                if (string.IsNullOrWhiteSpace(ComboBoxAvailableChannel.Text))
                {
                    ComboBoxAvailableChannel.SelectedIndex = 0;
                }
            }
            ComboBoxAvailableChannel.Enabled = ComboBoxAvailableChannel.Items.Count > 1;
            for (int i = 1; i < LabelChannel.Length; i++)
            {
                LabelChannel[i].Enabled = ComboBoxAvailableChannel.Enabled;
            }
        }

        private void UpdateComboBoxMessageChannel()
        {
            UpdateComboBoxMessageChannel(null);
        }
        private void UpdateComboBoxMessageChannel(string name)
        {
            int m = -1;
            if (selectedDevice != null)
            {
                m = selectedDevice.MaxChannel;
            }
            string selectedChannel = ComboBoxMessageChannel.Text;
            ComboBoxMessageChannel.Items.Clear();
            for (int index = 0; index <= m; index++)
            {
                J2534Channel channel = selectedDevice.Channel(index);
                if (channel.Connected && !channel.IsAnalog)
                {
                    ComboBoxMessageChannel.Items.Add(channel);
                }
                else if (channel.IsCAN)
                {
                    J2534Channel channelISO = J2534Code.CAN_ChannelToISO_Channel(selectedDevice, channel);
                    if (channelISO != null)
                    {
                        if (channelISO.Connected && channelISO.MixedMode)
                        {
                            ComboBoxMessageChannel.Items.Add(channel);
                        }
                    }
                }
            }
            if (ComboBoxMessageChannel.Items.Count > 0)
            {
                ComboBoxMessageChannel.Text = selectedChannel;
                if (!string.IsNullOrWhiteSpace(name))
                {
                    ComboBoxMessageChannel.Text = name;
                }
                if (string.IsNullOrWhiteSpace(ComboBoxMessageChannel.Text))
                {
                    ComboBoxMessageChannel.SelectedIndex = 0;
                }
            }
            ComboBoxMessageChannel.Enabled = ComboBoxMessageChannel.Items.Count > 1;
        }

        private void UpdateComboBoxConnectChannel(J2534Device device)
        {
            string selectedChannel = ComboBoxConnectChannel.Text;
            ComboBoxConnectChannel.Items.Clear();
            if (device?.ChannelSet != null)
            {
                ComboBoxConnectChannel.Items.AddRange(device.ChannelSet);
            }
            if (ComboBoxConnectChannel.Items.Count > 0)
            {
                ComboBoxConnectChannel.SelectedIndex = 0;
            }
            ComboBoxConnectChannel.Text = selectedChannel;
            if (!string.IsNullOrWhiteSpace(device?.Setup?.ConnectChannelName))
            {
                ComboBoxConnectChannel.Text = device.Setup.ConnectChannelName;
            }
        }

        private void UpdateConnectControls()
        {
            ComboBoxConnectChannel.Enabled = selectedDevice != null && selectedDevice.Connected;
            J2534Channel channel = ComboBoxConnectChannel.SelectedItem as J2534Channel;
            bool connected = ComboBoxConnectChannel.Enabled && channel != null && channel.Connected;
            LabelConnectChannel.Enabled = ComboBoxConnectChannel.Enabled;
            if (channel != null)
            {
                ButtonConnect.Enabled = ComboBoxConnectChannel.Enabled && !connected;
                if (channel.IsISO15765)
                {
                    CheckBoxMixedMode.Enabled = ButtonConnect.Enabled;
                    CheckBoxMixedMode.Checked = channel.MixedMode;
                }
                else
                {
                    CheckBoxMixedMode.Enabled = false;
                    CheckBoxMixedMode.Checked = false;
                }
                UpdateComboBoxBaudRate(channel);
                UpdateComboBoxConnector(channel);
                UpdateComboBoxPins(channel);
            }
            else
            {
                ButtonConnect.Enabled = false;
                CheckBoxMixedMode.Enabled = false;
                CheckBoxMixedMode.Checked = false;
                ComboBoxBaudRate.Items.Clear();
                ComboBoxBaudRate.Text = string.Empty;
                ComboBoxBaudRate.Enabled = false;
                TextBoxConnectFlags.Text = string.Empty;
                TextBoxConnectFlags.Enabled = false;
                LabelConnectFlags.Enabled = false;
                ComboBoxConnector.Items.Clear();
                ComboBoxConnector.Enabled = false;
                LabelConn.Enabled = false;
                ComboBoxPins.Items.Clear();
                ComboBoxPins.Enabled = false;
                LabelPins.Enabled = false;
            }
            LabelBaud.Enabled = ComboBoxBaudRate.Enabled;
            ButtonDisconnect.Enabled = connected && ComboBoxConnectChannel.Enabled;
        }

        private void UpdateMessageControls()
        {
            UpdateComboBoxMessageChannel();
            SetMessageControls();
            if (selectedDevice != null)
            {
                int.TryParse(TextBoxReadRate.Text, out int i);
                if (i == 0)
                {
                    selectedDevice.TimerMessageReceive.Enabled = false;
                }
                else
                {
                    selectedDevice.TimerMessageReceive.Interval = i;
                }
                if (selectedDevice.Setup.GetMessages)
                {
                    ButtonReceive.Text = "Stop";
                }
                else
                {
                    ButtonReceive.Text = "Start";
                }
            }
            else
            {
                ButtonReceive.Text = "Start";
            }
            UpdateButtonSend();
            UpdateButtonClaimJ1939Address();
            ButtonReceive.Enabled = ComboBoxAvailableChannel.Items.Count > 0;
            ButtonClearList.Enabled = ButtonReceive.Enabled;
        }

        private void UpdateButtonClaimJ1939Address()
        {
            if (ComboBoxMessageChannel.SelectedItem is J2534Channel channel)
            {
                ButtonClaimJ1939Address.Visible = channel.IsJ1939;
                ButtonClaimJ1939Address.Enabled = ButtonClaimJ1939Address.Visible && channel.Connected;
            }
            else
            {
                ButtonClaimJ1939Address.Visible = false;
                ButtonClaimJ1939Address.Enabled = ButtonClaimJ1939Address.Visible;
            }
        }

        private void UpdateComboBoxBaudRate(J2534Channel channel)
        {
            ComboBoxBaudRate.Items.Clear();
            ComboBoxBaudRate.Text = string.Empty;
            if (selectedDevice != null && selectedDevice.Connected)
            {
                bool connected = channel != null && channel.Connected;
                uint baudRate = channel.BaudRate;
                J2534ConnectFlags flags = channel.ConnectFlags;
                if ((!connected) && flags == 0)
                {
                    flags = channel.DefaultConnectFlags;
                }
                int comboIndex = channel.DefaultBaudRateIndex;
                for (int i = 0; i < channel.BaudRates.Length; i++)
                {
                    ComboBoxBaudRate.Items.Add(channel.BaudRates[i].ToString());
                    if (baudRate == channel.BaudRates[i])
                    {
                        comboIndex = i;
                    }
                }
                if (ComboBoxBaudRate.Items.Count > 0)
                {
                    if (connected && baudRate != 0)
                    {
                        ComboBoxBaudRate.Text = baudRate.ToString();
                    }
                    else
                    {
                        if (comboIndex >= channel.BaudRates.Length)
                        {
                            comboIndex = channel.BaudRates.Length - 1;
                        }
                        ComboBoxBaudRate.Text = channel.BaudRates[comboIndex].ToString();
                    }
                }
                ComboBoxBaudRate.Enabled = (ComboBoxBaudRate.Items.Count > 1) && (!connected);
                if (channel.IsAnalog)
                {
                    TextBoxConnectFlags.Text = string.Empty;
                    TextBoxConnectFlags.Enabled = false;
                    LabelConnectFlags.Enabled = false;
                }
                else
                {
                    TextBoxConnectFlags.Text = J2534Code.FlagsToText(flags);
                    TextBoxConnectFlags.Enabled = !connected;
                    LabelConnectFlags.Enabled = TextBoxConnectFlags.Enabled;
                }

            }
        }

        private void UpdateComboBoxConnector(J2534Channel channel)
        {
            ComboBoxConnector.Items.Clear();
            if (selectedDevice == null || channel == null)
            {
                ComboBoxConnector.Enabled = false;
            }
            else
            {

                if (channel.J1962Pins.Length > 0)
                {
                    ComboBoxConnector.Items.Add("J1962");
                }
                if (channel.J1939Pins.Length > 0)
                {
                    ComboBoxConnector.Items.Add("J1939");
                }
                if (channel.J1708Pins.Length > 0)
                {
                    ComboBoxConnector.Items.Add("J1708");
                }
                if (ComboBoxConnector.Items.Count > 0)
                {
                    switch ((J2534ConfigParameterId)channel.Connector)
                    {
                        case J1962_PINS:
                            ComboBoxConnector.Text = "J1962";

                            break;
                        case J1939_PINS:
                            ComboBoxConnector.Text = "J1939";

                            break;
                        case J1708_PINS:
                            ComboBoxConnector.Text = "J1708";

                            break;
                        default:
                            ComboBoxConnector.SelectedIndex = 0;

                            break;
                    }
                }
                ComboBoxConnector.Enabled = channel.IsPinSwitched && selectedDevice.Connected && (!channel.Connected) && channel.SelectedPins == 0;

            }
            LabelConn.Enabled = ComboBoxConnector.Enabled;
        }

        private void UpdateComboBoxPins(J2534Channel channel)
        {
            if (channel == null)
            {
                return;
            }
            uint[] pins = {};
            int setting = ComboBoxPins.SelectedIndex;
            ComboBoxPins.Items.Clear();

            switch (ComboBoxConnector.Text)
            {
                case "J1962":
                    pins = channel.J1962Pins;

                    break;
                case "J1939":
                    pins = channel.J1939Pins;

                    break;
                case "J1708":
                    pins = channel.J1708Pins;

                    break;
            }

            for (int i = 0; i < pins.Length; i++)
            {
                ComboBoxPins.Items.Add(PinsToText(pins[i]));
            }

            if (ComboBoxPins.Items.Count > 0)
            {
                if (channel.Connected)
                {
                    ComboBoxPins.SelectedIndex = FindPin(pins, channel.SelectedPins);
                }
                else if (setting >= 0 && setting < ComboBoxPins.Items.Count)
                {
                    ComboBoxPins.SelectedIndex = setting;
                }
                else
                {
                    ComboBoxPins.SelectedIndex = 0;
                }
            }
            ComboBoxPins.Enabled = ComboBoxConnector.Enabled;
            LabelPins.Enabled = ComboBoxPins.Enabled;
        }

        private static string PinsToText(uint pins)
        {
            PinMap pin = new PinMap {Value = pins};
            if (pin.LoPin == 0)
            {
                return string.Concat(pin.HiPin, " Only");
            }
            else
            {
                return string.Concat(pin.HiPin, ", ",  pin.LoPin);
            }
        }

        private static int FindPin(uint[] pinSet, uint pins)
        {
            int result = 0;
            for (int i = 0; i < pinSet.Length; i++)
            {
                if (pins == pinSet[i])
                {
                    result = i;
                    break;
                }
            }
            return result;
        }

        private void UpdateButtonSend()
        {
            if (ButtonSend.Focused)
            {
                return;
            }
            ButtonSend.Enabled = false;
            bool channelIsCan = false;
            if (selectedDevice != null)
            {
                J2534Channel channel = null;
                if (selectedDevice.ListViewMessageOut.Focused)
                {
                    if (selectedDevice.ListViewMessageOut.SelectedItems.Count > 0)
                    {
                        channel = selectedDevice.Channel(selectedDevice.ListViewMessageOut.SelectedItems[0].Text);
                        if (J2534Code.IsSendable(selectedDevice, channel))
                        {
                            ButtonSend.Tag = selectedDevice.ListViewMessageOut;
                            ButtonSend.Enabled = true;
                        }
                    }
                    ButtonSend.Text = string.Concat("Send", Environment.NewLine, "Selected");
                }
                else if (!ButtonSend.Enabled)
                {
                    if (ComboBoxMessageChannel.SelectedIndex >= 0 && TextBoxOutFlags.Text.Length > 0 && TextBoxMessageOut.Text.Length > 0)
                    {
                        channel = ComboBoxMessageChannel.SelectedItem as J2534Channel;
                        if (J2534Code.IsSendable(selectedDevice, channel))
                        {
                            ButtonSend.Tag = TextBoxMessageOut;
                            ButtonSend.Enabled = true;
                        }
                    }
                    ButtonSend.Text = string.Concat("Send", Environment.NewLine, "ScratchPad");
                }
                channelIsCan = channel != null && channel.IsCAN;
            }
            CheckBoxPadMessage.Visible = ButtonSend.Enabled && channelIsCan;
        }

        public void GetMessages(J2534Device device)
        {
            if (device?.API?.PassThruReadMsgs == null)
            {
                return;
            }
            if (device.Connected)
            {
                for (int index = 0; index <= device.MaxChannel; index++)
                {
                    if (device.Channel(index).Connected)
                    {
                        int count = 100;
                        int size = Marshal.SizeOf(typeof(PASSTHRU_MSG));
                        IntPtr pointer = Marshal.AllocHGlobal(size * count);
                        J2534Result result = device.API.PassThruReadMsgs(device.Channel(index).ChannelId, pointer.ToInt32(), ref count, 0);
                        if (result == J2534Result.STATUS_NOERROR || result == J2534Result.ERR_TIMEOUT || result == J2534Result.ERR_BUFFER_OVERFLOW)
                        {
                            if (count > 100)
                            {
                                count = 100;
                            }
                            for (int m = 0; m < count; m++)
                            {
                                PASSTHRU_MSG message = (PASSTHRU_MSG)Marshal.PtrToStructure(pointer + size * m, typeof(PASSTHRU_MSG));
                                if (J2534Code.IsAnalog(message.ProtocolId))
                                {
                                    J2534Code.ProcessAnalogMessage(device, device.Channel(index), message);
                                    ShowAnalogInputs(device);
                                }
                                J2534Code.DisplayMessage(device, message.ToMessageText());
                            }
                        }
                        ShowResult(device, result, "PassThruReadMsgs");
                        Marshal.FreeHGlobal(pointer);
                    }
                }
            }
            ToolStripStatusLabel1.Text = string.Concat("In: ", selectedDevice.MessageInCount, "     Out: ", selectedDevice.MessageOutCount);
        }

        public void SetFilterControls()
        {
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || channel.IsAnalog)
            {
                ClearFilterControls();
            }
            else
            {
                J2534MessageText messageText = null;
                bool mixedMode = channel.MixedMode;
                BuildFilterComboBoxes(channel.IsISO15765, mixedMode);
                for (int i = 0; i <= J2534Constants.MAX_FILTER; i++)
                {
                    messageText = channel.Filter[i].MaskMessage.ToMessageText();
                    TextBoxFilterMask[i].Text = messageText.Data;
                    messageText = channel.Filter[i].PatternMessage.ToMessageText();
                    TextBoxFilterPatt[i].Text = messageText.Data;
                    messageText = channel.Filter[i].FlowMessage.ToMessageText();
                    TextBoxFilterFlow[i].Text = messageText.Data;
                    TextBoxFilterFlags[i].Text = messageText.TxFlags;
                    ComboBoxFilterType[i].Text = J2534Code.FilterTypeToName(channel.Filter[i].FilterType);
                    LabelFilterId[i].Text = channel.Filter[i].Enabled ? "*" : "";
                    LabelFilter[i].Enabled = true;
                    TextBoxFilterMask[i].Enabled = true;
                    TextBoxFilterPatt[i].Enabled = true;
                    TextBoxFilterFlow[i].Enabled = channel.IsISO15765;
                    TextBoxFilterFlags[i].Enabled = true;
                    ComboBoxFilterType[i].Enabled = true;
                    LabelFilterId[i].Enabled = true;
                    FilterEditBuffer[i] = channel.Filter[i].Clone();
                }
                LabelFilterMask.Enabled = true;
                LabelFilterPatt.Enabled = true;
                LabelFilterFlow.Enabled = channel.IsISO15765;
                LabelFilterFlags.Enabled = true;
                LabelFilterType.Enabled = true;
                LabelFilterIds.Enabled = true;
                ButtonClearAllFilter.Enabled = true;
                ButtonCreatePassFilter.Visible = ((!channel.IsISO15765) || mixedMode) && (channel.NextFilter >= 0);
            }
        }

        private void ClearFilterControls()
        {
            settingFilters = true;
            for (int i = 0; i <= J2534Constants.MAX_FILTER; i++)
            {
                TextBoxFilterMask[i].Text = string.Empty;
                TextBoxFilterPatt[i].Text = string.Empty;
                TextBoxFilterFlow[i].Text = string.Empty;
                ComboBoxFilterType[i].SelectedIndex = 0;
                LabelFilter[i].Enabled = false;
                TextBoxFilterFlags[i].Text = string.Empty;
                TextBoxFilterMask[i].Enabled = false;
                TextBoxFilterPatt[i].Enabled = false;
                TextBoxFilterFlow[i].Enabled = false;
                TextBoxFilterFlags[i].Enabled = false;
                ComboBoxFilterType[i].Enabled = false;
                LabelFilterId[i].Text = string.Empty;
                LabelFilterId[i].Enabled = false;
            }
            LabelFilterMask.Enabled = false;
            LabelFilterPatt.Enabled = false;
            LabelFilterFlow.Enabled = false;
            LabelFilterFlags.Enabled = false;
            LabelFilterType.Enabled = false;
            LabelFilterIds.Enabled = false;
            ButtonClearAllFilter.Enabled = false;
            ButtonApplyFilter.Enabled = false;
            ButtonCancelFilter.Enabled = false;
            ButtonCreatePassFilter.Visible = false;
            settingFilters = false;
        }

        private void SetFilterEditBuffer(int index)
        {
            J2534MessageText maskTxt = new J2534MessageText
            {
                Data = TextBoxFilterMask[index].Text,
                ProtocolName = ComboBoxAvailableChannel.Text,
                TxFlags = TextBoxFilterFlags[index].Text
            };
            FilterEditBuffer[index].MaskMessage = maskTxt.ToMessage();

            J2534MessageText pattTxt = new J2534MessageText
            {
                Data = TextBoxFilterPatt[index].Text,
                ProtocolName = ComboBoxAvailableChannel.Text,
                TxFlags = TextBoxFilterFlags[index].Text
            };
            FilterEditBuffer[index].PatternMessage = pattTxt.ToMessage();

            J2534MessageText flowTxt = new J2534MessageText
            {
                Data = TextBoxFilterFlow[index].Text,
                ProtocolName = ComboBoxAvailableChannel.Text,
                TxFlags = TextBoxFilterFlags[index].Text
            };
            FilterEditBuffer[index].FlowMessage = flowTxt.ToMessage();

            FilterEditBuffer[index].FilterType = J2534Code.NameToFilterType(ComboBoxFilterType[index].Text);

        }

        private bool FilterChangesPresent()
        {
            return FilterChangesPresent(-1);
        }
        private bool FilterChangesPresent(int index)
        {
            bool result = false;
            int loFilter = 0;
            int hiFilter = 0;
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || channel.IsAnalog)
            {
                return result;
            }
            if (index < 0)
            {
                loFilter = 0;
                hiFilter = J2534Constants.MAX_FILTER;
            }
            else
            {
                loFilter = index;
                hiFilter = index;
            }
            MessageCompareResults compare = 0;
            for (int i = loFilter; i <= hiFilter; i++)
            {
                if (channel.Filter[i].FilterType != FilterEditBuffer[i].FilterType)
                {
                    result = true;
                }
                compare = J2534Code.CompareMessages(channel.Filter[i].MaskMessage, FilterEditBuffer[i].MaskMessage);
                if ((compare & ~MessageCompareResults.ProtocolMismatch) != MessageCompareResults.None)
                {
                    result = true;
                }
                compare = J2534Code.CompareMessages(channel.Filter[i].PatternMessage, FilterEditBuffer[i].PatternMessage);
                if ((compare & ~MessageCompareResults.ProtocolMismatch) != MessageCompareResults.None)
                {
                    result = true;
                }
                compare = J2534Code.CompareMessages(channel.Filter[i].FlowMessage, FilterEditBuffer[i].FlowMessage);
                if ((compare & ~MessageCompareResults.ProtocolMismatch) != MessageCompareResults.None)
                {
                    result = true;
                }
            }
            return result;
        }

        public void UpdateFilterEdit()
        {
            ButtonApplyFilter.Enabled = FilterChangesPresent();
            ButtonCancelFilter.Enabled = ButtonApplyFilter.Enabled;
        }

        private void BuildFilterComboBoxes(bool isISO15765, bool mixedMode)
        {
            for (int i = 0; i <= J2534Constants.MAX_FILTER; i++)
            {
                ComboBoxFilterType[i].Items.Clear();
                ComboBoxFilterType[i].Items.Add("None");
                if (!isISO15765 || mixedMode)
                {
                    ComboBoxFilterType[i].Items.Add("Pass");
                    ComboBoxFilterType[i].Items.Add("Block");
                }
                if (isISO15765)
                {
                    ComboBoxFilterType[i].Items.Add("Flow");
                }
                ComboBoxFilterType[i].SelectedIndex = 0;
            }
        }

        public void SetPeriodicMessageControls(J2534Device device)
        {
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || channel.IsAnalog)
            {
                ClearPeriodicMessageControls();
            }
            else
            {
                J2534Channel channelCAN = null;
                bool mixedMode = channel.MixedMode && channel.IsISO15765;
                LabelPeriodicMessageChannel.Visible = mixedMode;
                if (mixedMode)
                {
                    channelCAN = J2534Code.ISO_ChannelToCAN_Channel(device, channel);
                }
                for (int i = 0; i <= J2534Constants.MAX_PM; i++)
                {
                    J2534MessageText messageText = channel.PeriodicMessage[i].Message.ToMessageText();
                    ComboBoxPeriodicMessageChannel[i].Items.Clear();
                    if (mixedMode && channelCAN != null)
                    {
                        ComboBoxPeriodicMessageChannel[i].Items.Add(channelCAN);
                    }
                    ComboBoxPeriodicMessageChannel[i].Items.Add(channel);
                    ComboBoxPeriodicMessageChannel[i].Text = messageText.ProtocolName;
                    if (mixedMode)
                    {
                        TextBoxPeriodicMessage[i].Width = 265;
                        ComboBoxPeriodicMessageChannel[i].Visible = true;
                    }
                    else
                    {
                        TextBoxPeriodicMessage[i].Width = 377;
                        ComboBoxPeriodicMessageChannel[i].Visible = false;
                    }
                    TextBoxPeriodicMessage[i].Text = messageText.Data;
                    TextBoxPeriodicMessageFlags[i].Text = messageText.TxFlags;
                    if (channel.PeriodicMessage[i].Interval < 5 || channel.PeriodicMessage[i].Interval > 65535)
                    {
                        channel.PeriodicMessage[i].Interval = 100;
                    }
                    TextBoxPeriodicMessageInterval[i].Text = channel.PeriodicMessage[i].Interval.ToString();
                    LabelPeriodicMessageId[i].Text = channel.PeriodicMessage[i].MessageId.ToString();
                    CheckBoxPeriodicMessageEnable[i].Checked = channel.PeriodicMessage[i].Enabled;
                    LabelPeriodicMessage[i].Enabled = true;
                    TextBoxPeriodicMessage[i].Enabled = true;
                    TextBoxPeriodicMessageFlags[i].Enabled = true;
                    TextBoxPeriodicMessageInterval[i].Enabled = true;
                    LabelPeriodicMessageId[i].Enabled = true;
                    CheckBoxPeriodicMessageEnable[i].Enabled = true;
                    PeriodicMessageEditBuffer[i] = channel.PeriodicMessage[i].Clone();
                    PeriodicMessageCheckBoxHit[i] = false;
                }
                LabelPMMessage.Enabled = true;
                LabelPeriodicMessageFlags.Enabled = true;
                LabelPeriodicMessageInterval.Enabled = true;
                LabelPeriodicMessageIds.Enabled = true;
                LabelPeriodicMessageDelete.Enabled = true;
                ButtonClearAllPeriodicMessages.Enabled = true;
                CheckBoxPadMessage.Checked = true;
            }
        }

        private void ClearPeriodicMessageControls()
        {
            settingPeriodicMessages = true;
            for (int i = 0; i <= J2534Constants.MAX_PM; i++)
            {
                ComboBoxPeriodicMessageChannel[i].Items.Clear();
                TextBoxPeriodicMessage[i].Text = string.Empty;
                CheckBoxPeriodicMessageEnable[i].Checked = false;
                LabelPeriodicMessage[i].Enabled = false;
                TextBoxPeriodicMessageFlags[i].Text = string.Empty;
                TextBoxPeriodicMessage[i].Enabled = false;
                TextBoxPeriodicMessageFlags[i].Enabled = false;
                TextBoxPeriodicMessageInterval[i].Text = string.Empty;
                TextBoxPeriodicMessageInterval[i].Enabled = false;
                CheckBoxPeriodicMessageEnable[i].Enabled = false;
                LabelPeriodicMessageId[i].Text = "-";
                LabelPeriodicMessageId[i].Enabled = false;
                TextBoxPeriodicMessage[i].Width = 377;
                ComboBoxPeriodicMessageChannel[i].Visible = false;
            }
            settingPeriodicMessages = false;
            LabelPMMessage.Enabled = false;
            LabelPeriodicMessageChannel.Visible = false;
            LabelPeriodicMessageFlags.Enabled = false;
            LabelPeriodicMessageInterval.Enabled = false;
            LabelPeriodicMessageIds.Enabled = false;
            LabelPeriodicMessageDelete.Enabled = false;
            ButtonClearAllPeriodicMessages.Enabled = false;
            ButtonApplyPeriodicMessages.Enabled = false;
            ButtonCancelPeriodicMessages.Enabled = false;
        }

        private void SetPeriodicMessageEditBuffer(int index)
        {
            if (index < 0)
            {
                return;
            }
            J2534MessageText messageText = new J2534MessageText
            {
                Data = TextBoxPeriodicMessage[index].Text,
                TxFlags = TextBoxPeriodicMessageFlags[index].Text
            };
            if (string.IsNullOrWhiteSpace(messageText.Data))
            {
                messageText.ProtocolName = string.Empty;
            }
            else
            {
                messageText.ProtocolName = ComboBoxPeriodicMessageChannel[index].Text;
                if (selectedDevice == null || !selectedDevice.HasChannel(messageText.ProtocolName))
                {
                    messageText.ProtocolName = ComboBoxAvailableChannel.Text;
                }
            }
            PeriodicMessageEditBuffer[index].Message = messageText.ToMessage();
            uint.TryParse(TextBoxPeriodicMessageInterval[index].Text, out uint value);
            PeriodicMessageEditBuffer[index].Interval = value;
        }

        private bool PeriodicMessageChangesPresent()
        {
            return PeriodicMessageChangesPresent(-1);
        }
        private bool PeriodicMessageChangesPresent(int index)
        {
            bool result = false;
            int loPM = 0;
            int hiPM = 0;
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || index > J2534Constants.MAX_PM || channel.IsAnalog)
            {
                return result;
            }
            if (index < 0)
            {
                loPM = 0;
                hiPM = J2534Constants.MAX_PM;
            }
            else
            {
                loPM = index;
                hiPM = index;
            }
            for (int i = loPM; i <= hiPM; i++)
            {
                if (channel.PeriodicMessage[i].Interval != PeriodicMessageEditBuffer[i].Interval)
                {
                    result = true;
                }
                if (J2534Code.CompareMessages(channel.PeriodicMessage[i].Message, PeriodicMessageEditBuffer[i].Message) != 0)
                {
                    result = true;
                }
                if (channel.PeriodicMessage[i].Enabled != CheckBoxPeriodicMessageEnable[i].Checked)
                {
                    result = true;
                }
            }
            return result;
        }

        public void UpdatePeriodicMessageEdit()
        {
            ButtonApplyPeriodicMessages.Enabled = PeriodicMessageChangesPresent();
            ButtonCancelPeriodicMessages.Enabled = ButtonApplyPeriodicMessages.Enabled;
        }

        private void UpdatePeriodicMessageCheck(int index)
        {
            if (index > J2534Constants.MAX_PM || index < 0)
            {
                return;
            }
            if (TextBoxPeriodicMessage[index].Text.Length > 0)
            {
                if (!PeriodicMessageCheckBoxHit[index])
                {
                    CheckBoxPeriodicMessageEnable[index].Checked = true;
                }
            }
            else
            {
                CheckBoxPeriodicMessageEnable[index].Checked = false;
            }
        }

        private void UpdateTabs()
        {
            bool showComboBoxAvailableProtocol = true;
            bool showComboBoxAvailableDevice = true;
            TabPage tab = TabControl1.SelectedTab;
            if (tab == TabPageConnect)
            {
                showComboBoxAvailableProtocol = false;
                UpdateConnectControls();
            }
            else if (tab == TabPageMessages)
            {
                UpdateButtonSend();
                showComboBoxAvailableProtocol = false;
                UpdateMessageControls();
            }
            else if (tab == TabPagePeriodicMessages)
            {
                SetPeriodicMessageControls(selectedDevice);
            }
            else if (tab == TabPageFilters)
            {
                SetFilterControls();
                UpdateFilterEdit();
            }
            else if (tab == TabPageConfig)
            {
                SetConfigControls();
            }
            else if (tab == TabPageInit)
            {
                SetInitControls();
            }
            else if (tab == TabPageFunctionalMessages)
            {
                SetFunctionalMessageLookupTableControls();
            }
            else if (tab == TabPageAnalog)
            {
                SetVoltControls();
                showComboBoxAvailableProtocol = false;
            }
            else if (tab == TabPageResults)
            {
                showComboBoxAvailableProtocol = false;
                showComboBoxAvailableDevice = false;
            }

            ComboBoxAvailableChannel.Parent = tab;
            foreach (Label locator in ComboAvailableChannelLocator)
            {
                if (locator.Parent == tab)
                {
                    ComboBoxAvailableChannel.Location = locator.Location;
                }
            }
            ComboBoxAvailableChannel.Visible = showComboBoxAvailableProtocol;

            foreach (Label locator in ComboAvailableBoxLocator)
            {
                if (tab == TabPageConnect)
                {
                    if (locator.Parent == GroupBoxConnect)
                    {
                        ComboBoxAvailableDevice.Location = locator.Location;
                    }
                    ComboBoxAvailableDevice.Parent = GroupBoxConnect;
                }
                else
                {
                    if (locator.Parent == tab)
                    {
                        ComboBoxAvailableDevice.Location = locator.Location;
                    }
                    ComboBoxAvailableDevice.Parent = tab;
                }

            }
            ComboBoxAvailableDevice.Visible = showComboBoxAvailableDevice;

        }

        private void SetConfigControls()
        {
            if (selectedDevice == null)
            {
                ClearAndDisableConfigControls();
                return;
            }
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel))
            {
                ClearAndDisableConfigControls();
            }
            else
            {
                GetConfigParameterValues(selectedDevice, channel);
                ClearAndEnableConfigControls();
                for (int i = 0; i < channel.ConfParameter.Length; i++)
                {
                    if (channel.ConfParameter[i].Id != 0)
                    {
                        LabelParameterName[i].Text = channel.ConfParameter[i].Id.ToString();
                        LabelParameterName[i].Visible = true;
                        switch ((J2534ConfigParameterId)channel.ConfParameter[i].Id)
                        {
                            case NODE_ADDRESS:
                                LabelParamVal[i].Text = string.Concat("0x", channel.ConfParameter[i].Value.ToString("X2"));

                                break;
                            case J1962_PINS:
                            case J1939_PINS:
                            case J1708_PINS:
                                LabelParamVal[i].Text = string.Concat("0x", channel.ConfParameter[i].Value.ToString("X4"));

                                break;
                            case ACTIVE_CHANNELS:
                                LabelParamVal[i].Text = string.Concat("0x", channel.ConfParameter[i].Value.ToString("X8"));
                                TextBoxActiveChannelConfig = TextBoxParamVal[i];
                                configChannels = channel.ConfParameter[i].Value;
                                UpdateChannelCheckBoxes();

                                break;
                            case SAMPLE_RATE:
                                LabelParamVal[i].Text = string.Concat("0x", channel.ConfParameter[i].Value.ToString("X8"));
                                TextBoxAnalogRateConfig = TextBoxParamVal[i];
                                configRate = channel.ConfParameter[i].Value;
                                SetRateControls();

                                break;
                            default:
                                LabelParamVal[i].Text = channel.ConfParameter[i].Value.ToString();

                                break;
                        }
                        LabelParamVal[i].Visible = true;
                        TextBoxParamVal[i].Text = channel.ConfParameter[i].NewText;
                        TextBoxParamVal[i].Visible = !(channel.ConfParameter[i].IsReadOnly);
                        if (i == 12)
                        {
                            LabelParameters1.Visible = true;
                            LabelValues1.Visible = true;
                            LabelMod1.Visible = true;
                        }

                    }
                }
                if (channel.IsAnalog)
                {
                    ShowAnalogConfig();
                }
                LabelParameters0.Enabled = true;
                LabelParameters1.Enabled = true;
                LabelValues0.Enabled = true;
                LabelValues1.Enabled = true;
                LabelMod0.Enabled = true;
                LabelMod1.Enabled = true;
            }
            UpdateConfigEdit();
        }

        private void ClearAndEnableConfigControls()
        {
            foreach (Label label in LabelParamVal)
            {
                label.Visible = false;
            }
            foreach (Label label in LabelParameterName)
            {
                label.Visible = false;
            }
            foreach (TextBox txtBox in TextBoxParamVal)
            {
                txtBox.Visible = false;
            }
            LabelParameters0.Enabled = true;
            LabelParameters1.Enabled = true;
            LabelParameters1.Visible = false;
            LabelValues0.Enabled = true;
            LabelValues1.Enabled = true;
            LabelValues1.Visible = false;
            LabelMod0.Enabled = true;
            LabelMod1.Enabled = true;
            LabelMod1.Visible = false;
            ButtonSetConfig.Enabled = true;
            ButtonClearConfig.Enabled = true;
            TextBoxActiveChannelConfig = null;
            TextBoxAnalogRateConfig = null;
            HideAnalogConfig();
        }

        private void ClearAndDisableConfigControls()
        {
            foreach (Label item in LabelParamVal)
            {
                item.Visible = false;
            }
            foreach (Label item in LabelParameterName)
            {
                item.Visible = false;
            }
            foreach (TextBox item in TextBoxParamVal)
            {
                item.Visible = false;
            }
            LabelParameters0.Enabled = false;
            LabelParameters1.Enabled = false;
            LabelParameters1.Visible = false;
            LabelValues0.Enabled = false;
            LabelValues1.Enabled = false;
            LabelValues1.Visible = false;
            LabelMod0.Enabled = false;
            LabelMod1.Enabled = false;
            LabelMod1.Visible = false;
            ButtonSetConfig.Enabled = false;
            ButtonClearConfig.Enabled = false;
            TextBoxActiveChannelConfig = null;
            TextBoxAnalogRateConfig = null;
            HideAnalogConfig();
        }

        private void GetConfigParameterValues(J2534Device device, J2534Channel channel)
        {
            if (device == null || channel == null)
            {
                return;
            }
            for (int i = 0; i < channel.ConfParameter.Length; i++)
            {
                if (channel.ConfParameter[i].Id != 0)
                {
                    J2534Result result = J2534Code.GetConfigParameter(device, channel, channel.ConfParameter[i].Id);
                    ShowResult(device, result, string.Concat("PassThruIoctl - GET_CONFIG ", channel.ConfParameter[i].Id.ToString()));
                }
            }
            return;
        }

        public ConfigChanges ConfigChangesPresent()
        {
            return ConfigChangesPresent(-1);
        }
        public ConfigChanges ConfigChangesPresent(int index)
        {
            ConfigChanges result = ConfigChanges.None;
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || index >= channel.ConfParameter.Length)
            {
                return result;
            }
            if (index >= 0)
            {
                if (TextBoxParamVal[index].Text.Length != 0)
                {
                    result |= ConfigChanges.TextExists;
                    if (channel.ConfParameter[index].Value != GetConfigValue(channel, channel.ConfParameter[index].Id))
                    {
                        result |= ConfigChanges.ValueMismatch;
                    }
                }
                if (!string.IsNullOrWhiteSpace(channel.ConfParameter[index].NewText))
                {
                    result |= ConfigChanges.TextExists;
                }
            }
            else
            {
                for (int i = 0; i < channel.ConfParameter.Length; i++)
                {
                    if (LabelParamVal[i] != null && LabelParamVal[i].Visible)
                    {
                        if (TextBoxParamVal[i].Text.Length != 0)
                        {
                            result |= ConfigChanges.TextExists;
                            if (channel.ConfParameter[i].Value != GetConfigValue(channel, channel.ConfParameter[i].Id))
                            {
                                result |= ConfigChanges.ValueMismatch;
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(channel.ConfParameter[i].NewText))
                        {
                            result |= ConfigChanges.TextExists;
                        }
                    }
                }
            }
            return result;
        }

        private void SetEditConfig(int index)
        {
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || index < 0 || index >= channel.ConfParameter.Length)
            {
                return;
            }
            channel.ConfParameter[index].NewText = TextBoxParamVal[index].Text;
        }

        private void UpdateConfigEdit()
        {
            ConfigChanges changes = ConfigChangesPresent();
            ButtonSetConfig.Enabled = changes.HasFlag(ConfigChanges.ValueMismatch);
            ButtonClearConfig.Enabled = changes.HasFlag(ConfigChanges.TextExists);
        }

        private void ShowAnalogConfig()
        {
            GroupBoxAnalogConfig.Visible = true;
            GroupBoxAnalogConfig.Location = new Point(350, 25);
            UpdateChannelCheckBoxes();
        }

        private void HideAnalogConfig()
        {
            GroupBoxAnalogConfig.Visible = false;
        }

        private void SetChannelText()
        {
            if (TextBoxActiveChannelConfig == null)
            {
                return;
            }
            uint mask = 0;
            for (int i = 0; i <= 31; i++)
            {
                mask = 1U << i;
                if (CheckBoxCH[i].Checked)
                {
                    configChannels |= mask;
                }
                else
                {
                    configChannels &= ~mask;
                }
            }
            settingTextBoxActiveChannelConfig = true;
            TextBoxActiveChannelConfig.Text = string.Concat("0x", configChannels.ToString("X8"));
            settingTextBoxActiveChannelConfig = false;
        }

        private void SetRateText()
        {
            uint.TryParse(TextBoxAnalogRate.Text, out configRate);
            uint mask = 0x8000_0000U;
            if (ComboBoxAnalogRateMode.SelectedIndex == 1)
            {
                configRate |= mask;
            }
            else
            {
                configRate &= ~mask;
            }
            TextBoxAnalogRateConfig.Text = string.Concat("0x", configRate.ToString("X8"));
        }

        private void UpdateChannelCheckBoxes()
        {
            uint mask = 0;
            int maxSelected = PublicCode.MSB(configChannels);
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel))
            {
                return;
            }

            int maxActiveChannel = PublicCode.MSB(channel.AnalogSubsystem.ActiveChannels) + 1;
            for (int i = 0; i <= J2534Constants.MAX_ANALOG_CH; i++)
            {
                mask = 1U << i;
                CheckBoxCH[i].Checked = (configChannels & mask) != 0;
                CheckBoxCH[i].Enabled = i < Math.Max(maxActiveChannel, channel.AnalogSubsystem.MaxChannel);
                CheckBoxCH[i].Visible = i < 16 || i < Math.Max(maxActiveChannel, channel.AnalogSubsystem.MaxChannel) || i <= maxSelected;
            }
            LabelMaxSample.Text = string.Concat("Max sample rate: ", channel.AnalogSubsystem.MaxSampleRate);

        }

        private void SetRateControls()
        {
            uint mask = 0x8000_0000U;
            ComboBoxAnalogRateMode.SelectedIndex = ((configRate & mask) != 0) ? 1 : 0;
            settingTextBoxAnalogRate = true;
            TextBoxAnalogRate.Text = (configRate & ~mask).ToString();
            settingTextBoxAnalogRate = false;
        }

        private void UpdateRateControls()
        {
            uint rateValue = TextBoxAnalogRateConfig.Text.ToUint32();
            uint mask = 0x8000_0000U;
            ComboBoxAnalogRateMode.SelectedIndex = ((rateValue & mask) != 0) ? 1 : 0;
            settingTextBoxAnalogRate = true;
            TextBoxAnalogRate.Text = (rateValue & ~mask).ToString();
            settingTextBoxAnalogRate = false;
        }

        public void ShowResult(J2534Device device, J2534Result errorCode, string message)
        {
            byte[] errorBytes = new byte[80];
            string display = null;
            string errorDesc = null;
            if (errorCode == J2534Result.ERR_BUFFER_EMPTY)
            {
                return;
            }
            if (errorCode != J2534Result.STATUS_NOERROR)
            {
                if (device?.API?.PassThruGetLastError != null)
                {
                    device.API.PassThruGetLastError(errorBytes);
                }
                errorDesc = Encoding.ASCII.GetString(errorBytes).TrimEnd('\0');
            }
            else
            {
                errorDesc = "OK";
            }
            if (!string.IsNullOrWhiteSpace(message))
            {
                display = string.Concat(message, (errorDesc.Length > 0) ? string.Concat(" - ", errorDesc) : "");
            }
            else
            {
                display = errorDesc;
            }
            if (ToolStripStatusLabel2.Text != display)
            {
                ListViewItem item = ListViewResults.Items.Add(device.Name);
                item.SubItems.Add(display);
                ToolStripStatusLabel2.Text = display;
            }
            if (ListViewResults.Items.Count > 5000)
            {
                ListViewResults.Items.RemoveAt(0);
            }
        }

        private uint GetConfigValue(J2534Channel channel, J2534ConfigParameterId parameterId)
        {
            uint result = 0;
            if (channel == null)
            {
                return result;
            }
            int index = channel.ParameterIndex(parameterId);
            if (index < 0)
            {
                return result;
            }
            bool isHex = false;
            string text = PublicCode.StripSpaces(TextBoxParamVal[index].Text);
            if (text.StartsWith("&h", PublicDeclarations.IgnoreCase) || text.StartsWith("0x", PublicDeclarations.IgnoreCase))
            {
                text = text.Substring(2);
                isHex = true;
            }
            else if (PublicCode.MustBeHex(text) || 
                     parameterId == NODE_ADDRESS ||
                     parameterId == J1962_PINS ||
                     parameterId == J1939_PINS ||
                     parameterId == J1708_PINS)
            {
                isHex = true;
            }
            if (isHex)
            {
                uint.TryParse(text, NumberStyles.HexNumber, null, out result);
            }
            else
            {
                uint.TryParse(text, out result);
            }
            return result;
        }

        private bool OpenJ2534Device(J2534Device device)
        {
            bool result = false;
            if (device?.API?.PassThruOpen == null || device?.API?.PassThruReadVersion == null)
            {
                ShowResult(device, (J2534Result )(-1), "DLL Failed to Load");
                RemoveBox(device);
                return result;
            }

            if (device.Connected)
            {
                return result;
            }

            uint deviceId = 0;
            string tempName = "J2534-2:";
            if (ComboBoxDevice.SelectedIndex < 0)
            {
                tempName += ComboBoxDevice.Text;
            }
            IntPtr namePointer = Marshal.StringToHGlobalAnsi(tempName);
            J2534Result passThruOpenResult = device.API.PassThruOpen(namePointer.ToInt32(), ref deviceId);
            Marshal.FreeHGlobal(namePointer);
            if (passThruOpenResult != J2534Result.STATUS_NOERROR)
            {
                ShowResult(device, passThruOpenResult, "PassThruOpen()");
                RemoveBox(device);
                return result;
            }
            device.SetDeviceId(deviceId);

            if (ComboBoxDevice.SelectedIndex < 0)
            {
                device.Name = ComboBoxDevice.Text;
                ComboBoxDevice.Items.Add(ComboBoxDevice.Text);
            }

            byte[] fwVersionBytes = new byte[80];
            byte[] dllVersionBytes = new byte[80];
            byte[] apiVersionBytes = new byte[80];
            J2534Result passThruReadVersionResult = device.API.PassThruReadVersion(device.DeviceId, fwVersionBytes, dllVersionBytes, apiVersionBytes);
            if (passThruReadVersionResult == J2534Result.STATUS_NOERROR)
            {
                device.BuildChannels();
                device.Version = J2534Code.GetVersionNumericPart(Encoding.ASCII.GetString(fwVersionBytes).TrimEnd('\0'));
                device.Description = string.Concat("Device Name: ", device.Name, Environment.NewLine,
                                                   "Serial Number: ", device.SerialNo, Environment.NewLine,
                                                   "Device Firmware Version: ", device.Version, Environment.NewLine,
                                                   "J2534 DLL Version: ", Encoding.ASCII.GetString(dllVersionBytes).TrimEnd('\0'), Environment.NewLine,
                                                   "SAE J2534 API Version: ", Encoding.ASCII.GetString(apiVersionBytes).TrimEnd('\0'));
                device.Connected = true;
                device.Online = true;
                device.TimerMessageReceive.Tick += TimerMessageReceive_Tick;
                result = true;
            }
            else
            {
                device.Description = "Unable to connect";
                device.Connected = false;
                device.Online = false;
                device.ChannelSet = new J2534Channel[0] ;
                result = false;
            }
            return result;
        }

        private bool CloseJ2534Device(J2534Device device)
        {
            bool result = false;
            if (device?.API?.PassThruClose == null)
            {
                return result;
            }
            J2534Result passThruCloseResult = device.API.PassThruClose(device.DeviceId);
            ShowResult(device, passThruCloseResult, string.Concat("PassThruClose(", device.DeviceId, ")"));
            if (passThruCloseResult == J2534Result.STATUS_NOERROR || passThruCloseResult == J2534Result.ERR_DEVICE_NOT_CONNECTED)
            {
                device.Connected = false;
                result = true;
            }
            else
            {
                result = false;
            }
            device.TimerMessageReceive.Tick -= TimerMessageReceive_Tick;
            HandleOutgoingDevice(device);
            RemoveBox(device);
            return result;
        }

        private J2534Result ConnectChannel(J2534Device device, J2534ConnectOptions options)
        {
            J2534Result result = (J2534Result )(-1);
            if (device?.API?.PassThruConnect == null || device?.API?.PassThruIoctl == null || options?.Channel == null)
            {
                return result;
            }

            uint channelId = 0;
            SCONFIG config = new SCONFIG();
            SCONFIG_LIST configList = new SCONFIG_LIST();
            if (!options.Channel.Connected)
            {
                result = device.API.PassThruConnect(device.DeviceId, options.Channel.ProtocolId, options.ConnectFlags, options.BaudRate, ref channelId);
                ShowResult(device, result, "PassThruConnect");
                if (result == J2534Result.STATUS_NOERROR)
                {
                    options.Channel.ChannelId = channelId;
                    options.Channel.MixedMode = false;
                    options.Channel.Connected = true;
                    options.Channel.BaudRate = options.BaudRate;
                    options.Channel.ConnectFlags = options.ConnectFlags;
                    if (options.Channel.IsPinSwitched)
                    {
                        config.Parameter = options.Connector;
                        config.Value = options.Pins;
                        configList.NumOfParams = 1;
                        IntPtr configPointer = Marshal.AllocHGlobal(Marshal.SizeOf(config));
                        Marshal.StructureToPtr(config, configPointer, false);
                        configList.ConfigPtr = configPointer.ToInt32();
                        IntPtr configListPointer = Marshal.AllocHGlobal(Marshal.SizeOf(configList));
                        Marshal.StructureToPtr(configList, configListPointer, false);

                        result = device.API.PassThruIoctl(channelId, SET_CONFIG, configListPointer.ToInt32(), 0);
                        if (result == J2534Result.STATUS_NOERROR)
                        {
                            options.Channel.Connector = config.Parameter;
                            options.Channel.SelectedPins = config.Value;
                        }
                        Marshal.FreeHGlobal(configListPointer);
                        Marshal.FreeHGlobal(configPointer);
                    }
                    if (result == J2534Result.STATUS_NOERROR)
                    {
                        //Sets Loopback by default
                        config.Parameter = (uint)LOOPBACK;
                        config.Value = 1;
                        configList.NumOfParams = 1;
                        IntPtr configPointer = Marshal.AllocHGlobal(Marshal.SizeOf(config));
                        Marshal.StructureToPtr(config, configPointer, false);
                        configList.ConfigPtr = configPointer.ToInt32();
                        IntPtr configListPointer = Marshal.AllocHGlobal(Marshal.SizeOf(configList));
                        Marshal.StructureToPtr(configList, configListPointer, false);
                        result = device.API.PassThruIoctl(channelId, SET_CONFIG, configListPointer.ToInt32(), 0);
                        Marshal.FreeHGlobal(configListPointer);
                        Marshal.FreeHGlobal(configPointer);
                    }
                    if (options.MixedMode)
                    {
                        config.Parameter = (uint)CAN_MIXED_FORMAT;
                        config.Value = (uint)J2534MixedMode.CAN_MIXED_FORMAT_ON;
                        configList.NumOfParams = 1;
                        IntPtr configPointer = Marshal.AllocHGlobal(Marshal.SizeOf(config));
                        Marshal.StructureToPtr(config, configPointer, false);
                        configList.ConfigPtr = configPointer.ToInt32();
                        IntPtr configListPointer = Marshal.AllocHGlobal(Marshal.SizeOf(configList));
                        Marshal.StructureToPtr(configList, configListPointer, false);
                        result = device.API.PassThruIoctl(channelId, SET_CONFIG, configListPointer.ToInt32(), 0);
                        if (result != J2534Result.STATUS_NOERROR)
                        {
                            ShowResult(device, result, "PasThruIoctl CAN_MIXED_FORMAT");
                        }
                        else
                        {
                            options.Channel.MixedMode = true;
                        }
                        Marshal.FreeHGlobal(configListPointer);
                        Marshal.FreeHGlobal(configPointer);
                    }
                    if (options.Channel.IsCAN || options.Channel.IsISO15765)
                    {
                        if ((options.ConnectFlags & J2534ConnectFlags.CAN_29BIT_ID) != 0)
                        {
                            options.Channel.DefaultTxFlags |= J2534TxFlags.CAN_29BIT_ID;
                        }
                        else
                        {
                            options.Channel.DefaultTxFlags &= ~J2534TxFlags.CAN_29BIT_ID;
                        }
                    }
                    GetConfigParameterValues(device, options.Channel);
                }
            }
            else
            {
                ToolStripStatusLabel2.Text = string.Concat("Protocol ID (", options.Channel.ProtocolId, " - ", options.Channel.Name, ") is already open");
                result = J2534Result.ERR_CHANNEL_IN_USE;
            }
            UpdateConnectControls();
            UpdateComboBoxAvailableChannel();
            ComboBoxConnectChannel.Focus();
            return result;
        }

        public J2534Result DisconnectChannel(J2534Device device, J2534Channel channel)
        {
            J2534Result result = (J2534Result)(-1);
            if (device?.API?.PassThruDisconnect == null || channel == null)
            {
                return result;
            }
            if (channel.Connected)
            {
                result = selectedDevice.API.PassThruDisconnect(channel.ChannelId);
                channel.ChannelId = 0;
                channel.Connected = false;
                ShowResult(selectedDevice, result, "PassThruDisconnect");
                if (result == J2534Result.STATUS_NOERROR)
                {
                    if (channel.Filter != null)
                    {
                        for (int i = 0; i < channel.Filter.Length; i++)
                        {
                            channel.Filter[i].FilterType = 0;
                            channel.Filter[i].MaskMessage.Clear();
                            channel.Filter[i].PatternMessage.Clear();
                            channel.Filter[i].FlowMessage.Clear();
                            channel.Filter[i].MessageId = 0;
                            channel.Filter[i].Enabled = false;
                        }
                    }
                    if (channel.PeriodicMessage != null)
                    {
                        for (int i = 0; i < channel.PeriodicMessage.Length; i++)
                        {
                            channel.PeriodicMessage[i].Message.Clear();
                            channel.PeriodicMessage[i].Interval = 100;
                            channel.PeriodicMessage[i].MessageId = 0;
                            channel.PeriodicMessage[i].Enabled = false;
                        }
                    }
                    if (channel.FunctionalMessage != null)
                    {
                        for (int i = 0; i < channel.FunctionalMessage.Length; i++)
                        {
                            channel.FunctionalMessage[i].Value = 0;
                            channel.FunctionalMessage[i].NewText = string.Empty;
                        }
                    }
                    channel.Connector = 0;
                    channel.SelectedPins = 0;
                }
            }
            else
            {
                ToolStripStatusLabel2.Text = string.Concat("Protocol ID (", channel.ProtocolId, " - ", channel.Name, ") is not open");
            }

            UpdateConnectControls();
            UpdateComboBoxAvailableChannel();
            return result;
        }

        private void SetInitControls()
        {
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || !(channel.IsISO9141 || channel.IsISO14230))
            {
                TextBox5BInitECU.Text = string.Empty;
                TextBox5BInitECU.Enabled = false;
                Label5BKeyWord0.Text = string.Empty;
                Label5BKeyWord0.Enabled = false;
                Label5BKeyWord1.Text = string.Empty;
                Label5BKeyWord1.Enabled = false;
                ButtonExecute5BInit.Enabled = false;
                Label5BECU.Enabled = false;
                LabelKWlabel0.Enabled = false;
                LabelKWlabel1.Enabled = false;
                GroupBox5BInit.Enabled = false;

                TextBoxFIMessage.Text = string.Empty;
                TextBoxFIMessage.Enabled = false;
                TextBoxFIFlags.Text = string.Empty;
                TextBoxFIFlags.Enabled = false;
                LabelFIResponse.Text = string.Empty;
                LabelFIResponse.Enabled = false;
                LabelFIRxStatus.Text = string.Empty;
                LabelFIRxStatus.Enabled = false;
                ButtonExecuteFastInit.Enabled = false;
                LabelFIMsgData.Enabled = false;
                LabelFIRXTitle.Enabled = false;
                LabelFIMsgResp.Enabled = false;
                LabelFITXFlags.Enabled = false;
                GroupBoxFastInit.Enabled = false;
            }
            else
            {
                TextBox5BInitECU.Text = "0x33";
                TextBox5BInitECU.Enabled = true;
                Label5BKeyWord0.Text = string.Empty;
                Label5BKeyWord0.Enabled = true;
                Label5BKeyWord1.Text = string.Empty;
                Label5BKeyWord1.Enabled = true;
                ButtonExecute5BInit.Enabled = true;
                Label5BECU.Enabled = true;
                LabelKWlabel0.Enabled = true;
                LabelKWlabel1.Enabled = true;
                GroupBox5BInit.Enabled = true;

                TextBoxFIMessage.Text = "c1 33 f1 81";
                TextBoxFIMessage.Enabled = true;
                TextBoxFIFlags.Text = "0x00000000";
                TextBoxFIFlags.Enabled = true;
                LabelFIResponse.Text = string.Empty;
                LabelFIResponse.Enabled = true;
                LabelFIRxStatus.Text = string.Empty;
                LabelFIRxStatus.Enabled = false;
                ButtonExecuteFastInit.Enabled = true;
                LabelFIMsgData.Enabled = true;
                LabelFIRXTitle.Enabled = true;
                LabelFIMsgResp.Enabled = true;
                LabelFITXFlags.Enabled = true;
                GroupBoxFastInit.Enabled = true;
            }

        }

        private void SetMessageControls()
        {
            bool connected = ComboBoxMessageChannel.Items.Count > 0;
            ButtonReceive.Enabled = connected;
            ButtonClearRx.Enabled = connected;
            ButtonClearTx.Enabled = connected;
            TextBoxOutFlags.Enabled = connected;
            TextBoxMessageOut.Enabled = connected;
            TextBoxReadRate.Enabled = connected;
            TextBoxTimeOut.Enabled = connected;
            LabelScratchPad.Enabled = TextBoxMessageOut.Enabled;
            LabelMsgFlags.Enabled = TextBoxOutFlags.Enabled;
            LabelTO.Enabled = TextBoxTimeOut.Enabled;
            LabelReadRate.Enabled = TextBoxReadRate.Enabled;
        }

        private void SetFunctionalMessageLookupTableControls()
        {
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || !channel.IsJ1850PWM)
            {
                for (int i = 0; i <= J2534Constants.MAX_FUNC_MSG; i++)
                {
                    LabelFuncId[i].Enabled = false;
                    LabelFunctionalMessage[i].Text = string.Empty;
                    LabelFunctionalMessage[i].Enabled = false;
                    TextBoxFunctionalMessage[i].Text = string.Empty;
                    TextBoxFunctionalMessage[i].Enabled = false;
                    CheckBoxFunctionalMessageDelete[i].Checked = false;
                    CheckBoxFunctionalMessageDelete[i].Visible = false;
                    LabelFunctionalMessageValues.Enabled = false;
                    LabelFunctionalMessageModify.Enabled = false;
                    ButtonClearAllFunctionalMessages.Enabled = false;
                    ButtonCancelFunctionalMessages.Enabled = false;
                    ButtonApplyFunctionalMessages.Enabled = false;
                }
            }
            else
            {
                for (int i = 0; i <= J2534Constants.MAX_FUNC_MSG; i++)
                {
                    LabelFuncId[i].Enabled = true;
                    if (channel.FunctionalMessage[i].Value == 0)
                    {
                        LabelFunctionalMessage[i].Text = string.Empty;
                        CheckBoxFunctionalMessageDelete[i].Visible = false;
                    }
                    else
                    {
                        LabelFunctionalMessage[i].Text = string.Concat("0x", channel.FunctionalMessage[i].Value.ToString("X2"));
                        CheckBoxFunctionalMessageDelete[i].Visible = true;
                    }
                    LabelFunctionalMessage[i].Enabled = true;
                    TextBoxFunctionalMessage[i].Text = channel.FunctionalMessage[i].NewText;
                    TextBoxFunctionalMessage[i].Enabled = true;
                    CheckBoxFunctionalMessageDelete[i].Checked = false;
                    LabelFunctionalMessageValues.Enabled = true;
                    LabelFunctionalMessageModify.Enabled = true;
                    ButtonClearAllFunctionalMessages.Enabled = true;
                    ButtonCancelFunctionalMessages.Enabled = true;
                    ButtonApplyFunctionalMessages.Enabled = true;
                }
            }
            UpdateFunctionalMessageLookupTableEdit();
        }

        private void DeleteFromFunctionalMessageLookupTable(byte funcAddress)
        {
            SBYTE_ARRAY inputMsg = new SBYTE_ARRAY
            {
                NumOfBytes = 1
            };
            byte[] byteAddress = {funcAddress};
            J2534Channel channel = ComboBoxAvailableChannel.SelectedItem as J2534Channel;
            if (selectedDevice?.API?.PassThruIoctl == null || channel == null)
            {
                return;
            }
            if (string.IsNullOrWhiteSpace(ComboBoxAvailableChannel.Text))
            {
                return;
            }
            if (!channel.IsJ1850PWM)
            {
                SetFunctionalMessageLookupTableControls();
                return;
            }
            IntPtr funcAddressPointer = Marshal.AllocHGlobal(Marshal.SizeOf(byteAddress[0]));
            Marshal.Copy(byteAddress, 0, funcAddressPointer, 1);
            inputMsg.BytePtr = funcAddressPointer.ToInt32();
            IntPtr inputMsgPointer = Marshal.AllocHGlobal(Marshal.SizeOf(inputMsg));
            Marshal.StructureToPtr(inputMsg, inputMsgPointer, true);
            J2534Result result = selectedDevice.API.PassThruIoctl(channel.ChannelId, DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE, inputMsgPointer.ToInt32(), 0);
            Marshal.FreeHGlobal(funcAddressPointer);
            Marshal.FreeHGlobal(inputMsgPointer);
            ShowResult(selectedDevice, result, "PassThruIoctl - DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE");
        }

        private bool FunctionalMessageLookupTableChangesPresent()
        {
            return FunctionalMessageLookupTableChangesPresent(-1);
        }
        private bool FunctionalMessageLookupTableChangesPresent(int index)
        {
            bool result = false;
            int loFM = 0;
            int hiFM = 0;
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel) || !channel.IsJ1850PWM)
            {
                return result;
            }
            if (index < 0)
            {
                loFM = 0;
                hiFM = J2534Constants.MAX_FUNC_MSG;
            }
            else
            {
                loFM = index;
                hiFM = index;
            }
            for (int i = loFM; i <= hiFM; i++)
            {
                if (LabelFunctionalMessage[i].Text.ToByte() != TextBoxFunctionalMessage[i].Text.ToByte())
                {
                    result = true;
                }
                byte b = TextBoxFunctionalMessage[i].Text.ToByte();
                if (b != channel.FunctionalMessage[i].NewText.ToByte())
                {
                    result = true;
                }
                if (b != channel.FunctionalMessage[i].Value)
                {
                    result = true;
                }
                if (CheckBoxFunctionalMessageDelete[i].Checked)
                {
                    result = true;
                }
            }
            return result;
        }

        private void UpdateFunctionalMessageLookupTableEdit()
        {
            ButtonApplyFunctionalMessages.Enabled = FunctionalMessageLookupTableChangesPresent();
            ButtonCancelFunctionalMessages.Enabled = ButtonApplyFunctionalMessages.Enabled;
        }

        public J2534Result SetProgrammingVoltage(J2534Device device)
        {
            J2534Result result = (J2534Result)(-1);
            if (device?.API?.PassThruSetProgrammingVoltage == null)
            {
                return result;
            }
            uint voltSetting = 0;
            PinMode mode = 0;
            if (optSet.Checked)
            {
                if (device.Analog.PinSetting < 5000)
                {
                    device.Analog.PinSetting = 5000;
                }
                if (device.Analog.PinSetting > 20000)
                {
                    device.Analog.PinSetting = 20000;
                }
                TextBoxVoltSetting.Text = device.Analog.PinSetting.ToString();
                voltSetting = device.Analog.PinSetting;
                mode = PinMode.Volt;
            }
            else if (optVoltOff.Checked)
            {
                voltSetting = (uint)J2534VoltageValue.VOLTAGE_OFF;
                mode = PinMode.Open;
            }
            else if (optShortToGround.Checked)
            {
                voltSetting = (uint)J2534VoltageValue.SHORT_TO_GROUND;
                mode = PinMode.Shorted;
            }
            for (int i = 0; i < RadioButtonPin.Length; i++)
            {
                if (RadioButtonPin[i] != null)
                {
                    if (RadioButtonPin[i].Checked)
                    {
                        result = device.API.PassThruSetProgrammingVoltage(device.DeviceId, (uint)i, voltSetting);
                        if (result == J2534Result.STATUS_NOERROR)
                        {
                            RadioButtonPin[i].Tag = mode;
                            if (mode == PinMode.Volt)
                            {
                                LabelPin[i].Text = "Volt";
                            }
                            if (mode == PinMode.Shorted)
                            {
                                LabelPin[i].Text = "Gnd";
                            }
                            if (mode == PinMode.Open)
                            {
                                LabelPin[i].Text = string.Empty;
                            }
                        }
                        ShowResult(device, result, "PassThruSetProgrammingVoltage");
                    }
                    device.Analog.Pin[i].Mode = (PinMode)RadioButtonPin[i].Tag;
                    device.Analog.Pin[i].Selected = RadioButtonPin[i].Checked;
                }
            }
            return result;
        }

        public void SetVoltControls()
        {
            string selectedAnalog = ComboBoxAnalogChannel.Text;
            ComboBoxAnalogChannel.Items.Clear();
            if (selectedDevice != null && selectedDevice.Connected)
            {
                foreach (J2534Channel channel in selectedDevice.ChannelSet)
                {
                    if (channel.IsAnalog && channel.Connected)
                    {
                        ComboBoxAnalogChannel.Items.Add(channel);
                    }
                }
                if (ComboBoxAnalogChannel.Items.Count > 0)
                {
                    if (selectedAnalog.Length > 0)
                    {
                        ComboBoxAnalogChannel.Text = selectedAnalog;
                    }
                    else
                    {
                        ComboBoxAnalogChannel.SelectedIndex = 0;
                    }
                }
                ComboBoxAnalogChannel.Enabled = ComboBoxAnalogChannel.Items.Count > 1;

                int selectedPin = -1;
                for (int i = 0; i < RadioButtonPin.Length; i++)
                {
                    if (RadioButtonPin[i] != null)
                    {
                        RadioButtonPin[i].Enabled = true;
                        if (!(RadioButtonPin[i].Tag is PinMode))
                        {
                            RadioButtonPin[i].Tag = PinMode.Open;
                        }
                        if (RadioButtonPin[i].Checked)
                        {
                            selectedPin = i;
                        }
                        if ((PinMode)RadioButtonPin[i].Tag == PinMode.Volt)
                        {
                            LabelPin[i].Text = "Volt";
                        }
                        else if ((PinMode)RadioButtonPin[i].Tag == PinMode.Shorted)
                        {
                            LabelPin[i].Text = "Gnd";
                        }
                        else
                        {
                            LabelPin[i].Text = string.Empty;
                        }

                    }
                }
                if (selectedPin < 0)
                {
                    RadioButtonPin[0].Checked = true;
                    selectedPin = 0;
                }
                GroupBoxPin.Enabled = true;
                optVoltOff.Enabled = true;
                optShortToGround.Enabled = selectedDevice.Analog.Pin[selectedPin].CanBeShorted;
                optSet.Enabled = selectedDevice.Analog.Pin[selectedPin].CanBeSet;
                optVoltOff.Checked = !(optShortToGround.Checked || optSet.Checked);

                TextBoxVoltSetting.Text = selectedDevice.Analog.PinSetting.ToString();
                TextBoxVoltSetting.Enabled = true;
                LabelMV.Enabled = true;
                LabelVoltRead.Enabled = true;
                if (selectedDevice.Analog.PinReading < 1000)
                {
                    LabelVoltRead.Text = string.Concat(selectedDevice.Analog.PinReading.ToString(), " mVDC");
                }
                else
                {
                    LabelVoltRead.Text = string.Concat((selectedDevice.Analog.PinReading / 1000).ToString(), " VDC");
                }
                ButtonSetVoltage.Enabled = true;
                ButtonReadVolt.Enabled = true;
                GroupBoxVolt.Enabled = true;
                GroupBoxProgVoltage.Enabled = true;

                LabelBattRead.Enabled = true;
                if (selectedDevice.Analog.BatteryReading < 1000)
                {
                    LabelBattRead.Text = string.Concat(selectedDevice.Analog.BatteryReading.ToString(), " mVDC");
                }
                else
                {
                    LabelBattRead.Text = string.Concat((selectedDevice.Analog.BatteryReading / 1000).ToString(), " VDC");
                }
                ButtonReadBatt.Enabled = true;
                GroupBoxBattVoltage.Enabled = true;

            }
            else //box is not OK
            {
                for (int i = 0; i < RadioButtonPin.Length; i++)
                {
                    if (RadioButtonPin[i] != null)
                    {
                        RadioButtonPin[i].Enabled = false;
                        RadioButtonPin[i].Checked = false;
                        LabelPin[i].Text = string.Empty;
                        RadioButtonPin[i].Tag = PinMode.Open;
                    }
                }
                GroupBoxPin.Enabled = false;
                optVoltOff.Enabled = false;
                optVoltOff.Checked = false;
                optShortToGround.Enabled = false;
                optShortToGround.Checked = false;
                optSet.Enabled = false;
                optSet.Checked = false;
                TextBoxVoltSetting.Text = string.Empty;
                TextBoxVoltSetting.Enabled = false;
                LabelMV.Enabled = false;
                LabelVoltRead.Enabled = false;
                LabelVoltRead.Text = string.Empty;
                ButtonSetVoltage.Enabled = false;
                ButtonReadVolt.Enabled = false;
                GroupBoxVolt.Enabled = false;
                GroupBoxProgVoltage.Enabled = false;
                LabelBattRead.Enabled = false;
                LabelBattRead.Text = string.Empty;
                ButtonReadBatt.Enabled = false;
                GroupBoxBattVoltage.Enabled = false;
                ComboBoxAnalogChannel.Enabled = false;
            }
            ShowAnalogInputs(selectedDevice);
            LabelAnalogChannel.Enabled = ComboBoxAnalogChannel.Enabled;
        }

        public void UpdatePinControls(J2534Device device)
        {
            if (device == null)
            {
                return;
            }
            int index = 0;
            if (!GroupBoxProgVoltage.Enabled)
            {
                return;
            }
            for (int i = 0; i < RadioButtonPin.Length; i++)
            {
                if (RadioButtonPin[i] != null && RadioButtonPin[i].Checked)
                {
                    index = i;
                    break;
                }
            }
            optShortToGround.Enabled = selectedDevice.Analog.Pin[index].CanBeShorted;
            optSet.Enabled = selectedDevice.Analog.Pin[index].CanBeSet;
            if (optSet.Checked && !optSet.Enabled)
            {
                optVoltOff.Checked = true;
            }
            if (optShortToGround.Checked && !optShortToGround.Enabled)
            {
                optVoltOff.Checked = true;
            }
        }

        private void ShowAnalogInputs(J2534Device device)
        {
            if (device != selectedDevice)
            {
                return;
            }
            J2534Channel channel = ComboBoxAnalogChannel.SelectedItem as J2534Channel;
            if (device != null && channel != null && device.Connected && channel.Connected && channel.AnalogSubsystem.ActiveChannels != 0)
            {

                int maxActive = PublicCode.MSB(channel.AnalogSubsystem.ActiveChannels);
                for (int i = 0; i <= J2534Constants.MAX_ANALOG_CH; i++)
                {
                    if ((channel.AnalogSubsystem.ActiveChannels & (1 << i).ToUint32()) != 0)
                    {
                        LabelAnalogCH[i].Enabled = true;
                        LabelAnalogRead[i].Enabled = true;
                        if (Math.Abs(channel.AnalogSubsystem.AnalogIn[i]) < 1000)
                        {
                            LabelAnalogRead[i].Text = string.Concat(channel.AnalogSubsystem.AnalogIn[i], " mVDC");
                        }
                        else
                        {
                            LabelAnalogRead[i].Text = string.Concat(channel.AnalogSubsystem.AnalogIn[i] / 1000.0, " VDC");
                        }
                        ProgressBarAnalog[i].Minimum = channel.AnalogSubsystem.RangeLow;
                        ProgressBarAnalog[i].Maximum = channel.AnalogSubsystem.RangeHigh + 1;
                        if (channel.AnalogSubsystem.AnalogIn[i] < ProgressBarAnalog[i].Minimum)
                        {
                            ProgressBarAnalog[i].Value = ProgressBarAnalog[i].Minimum;
                        }
                        else if (channel.AnalogSubsystem.AnalogIn[i] >= channel.AnalogSubsystem.RangeHigh)
                        {
                            ProgressBarAnalog[i].Value = ProgressBarAnalog[i].Maximum;
                            ProgressBarAnalog[i].Value = channel.AnalogSubsystem.RangeHigh;
                        }
                        else
                        {
                            ProgressBarAnalog[i].Value = channel.AnalogSubsystem.AnalogIn[i] + 1;
                            ProgressBarAnalog[i].Value = channel.AnalogSubsystem.AnalogIn[i];
                        }
                    }
                    else
                    {
                        LabelAnalogCH[i].Enabled = false;
                        LabelAnalogRead[i].Enabled = false;
                        LabelAnalogRead[i].Text = "0 mVDC";
                        ProgressBarAnalog[i].Value = ProgressBarAnalog[i].Minimum;
                    }

                    bool channelVisible = i < 10 || i <= Math.Max(maxActive, channel.AnalogSubsystem.MaxChannel);
                    LabelAnalogCH[i].Visible = channelVisible;
                    LabelAnalogRead[i].Visible = channelVisible;
                    ProgressBarAnalog[i].Visible = channelVisible;

                }
                LabelAnalogReading.Enabled = true;
                LabelAnalogLow0.Enabled = true;
                LabelAnalogLow1.Enabled = true;
                LabelAnalogHigh0.Enabled = true;
                LabelAnalogHigh1.Enabled = true;
                LabelAnalogLow0.Text = string.Concat(channel.AnalogSubsystem.RangeLow / 1000.0, " VDC");
                LabelAnalogLow1.Text = LabelAnalogLow0.Text;
                LabelAnalogHigh0.Text = string.Concat(channel.AnalogSubsystem.RangeHigh / 1000.0, " VDC");
                LabelAnalogHigh1.Text = LabelAnalogHigh0.Text;
                GroupBoxAnalog.Enabled = true;

            }
            else
            {
                for (int i = 0; i <= J2534Constants.MAX_ANALOG_CH; i++)
                {
                    LabelAnalogCH[i].Enabled = false;
                    LabelAnalogRead[i].Enabled = false;
                    LabelAnalogRead[i].Text = "0 mVDC";
                    ProgressBarAnalog[i].Value = ProgressBarAnalog[i].Minimum;
                    if (i > 9)
                    {
                        LabelAnalogCH[i].Visible = false;
                        LabelAnalogRead[i].Visible = false;
                        ProgressBarAnalog[i].Visible = false;
                    }
                }
                LabelAnalogReading.Enabled = false;
                LabelAnalogLow0.Enabled = false;
                LabelAnalogLow1.Enabled = false;
                LabelAnalogHigh0.Enabled = false;
                LabelAnalogHigh1.Enabled = false;
                LabelAnalogLow0.Text = string.Empty;
                LabelAnalogLow1.Text = LabelAnalogLow0.Text;
                LabelAnalogHigh0.Text = string.Empty;
                LabelAnalogHigh1.Text = LabelAnalogHigh0.Text;
                GroupBoxAnalog.Enabled = false;
            }
        }

        private static void AddMessageToOutgoingListView(ListView thisListView, J2534MessageText messageText)
        {
            ListViewItem item = thisListView.Items.Add(messageText.ProtocolName);
            item.SubItems.Add(messageText.TxFlags);
            item.SubItems.Add(messageText.Data);
            item.SubItems.Add(messageText.Comment);
        }

        private J2534MessageText GetListViewOutMessageText()
        {
            J2534MessageText result = new J2534MessageText();
            if (selectedDevice != null && selectedDevice.ListViewMessageOut.SelectedItems.Count > 0)
            {
                result.ProtocolName = selectedDevice.ListViewMessageOut.SelectedItems[0].Text;
                result.TxFlags = selectedDevice.ListViewMessageOut.SelectedItems[0].SubItems[1].Text;
                result.Data = selectedDevice.ListViewMessageOut.SelectedItems[0].SubItems[2].Text;
                result.Comment = selectedDevice.ListViewMessageOut.SelectedItems[0].SubItems[3].Text;
            }
            return result;
        }

        private uint GetDefaultFlags(J2534Device device, TextBox thisTextBox)
        {
            if (device == null)
            {
                return 0;
            }
            uint flags = 0;
            J2534Channel channel = null;
            if (thisTextBox == TextBoxConnectFlags)
            {
                channel = ComboBoxConnectChannel.SelectedItem as J2534Channel;
                if (channel != null)
                {
                    flags = (uint)channel.DefaultConnectFlags;
                }
            }
            else if (thisTextBox == TextBoxOutFlags)
            {
                channel = ComboBoxMessageChannel.SelectedItem as J2534Channel;
                if (channel != null)
                {
                    flags = (uint)channel.DefaultTxFlags;
                }
            }
            else
            {
                channel = ComboBoxAvailableChannel.SelectedItem as J2534Channel;
                if (channel == null)
                {
                    return 0;
                }
                if (thisTextBox.Name.StartsWith("_TextBoxPeriodicMessageFlags", PublicDeclarations.IgnoreCase))
                {
                    if (channel.IsISO15765 && channel.MixedMode)
                    {
                        int index = Array.IndexOf(TextBoxPeriodicMessageFlags, thisTextBox);
                        if (ComboBoxPeriodicMessageChannel[index].SelectedItem is J2534Channel testChannel)
                        {
                            channel = testChannel;
                        }
                    }
                }
                else if (thisTextBox.Name.StartsWith("_TextBoxFilterFlags", PublicDeclarations.IgnoreCase))
                {
                    if (channel.IsISO15765 && channel.MixedMode)
                    {
                        int index = Array.IndexOf(TextBoxFilterFlags, thisTextBox);
                        if (ComboBoxFilterType[index].Text.Equals("Pass", PublicDeclarations.IgnoreCase) || ComboBoxFilterType[index].Text.Equals("Block", PublicDeclarations.IgnoreCase))
                        {
                            channel = J2534Code.ISO_ChannelToCAN_Channel(device, channel);
                        }
                    }
                }
                if (channel != null)
                {
                    flags = (uint)channel.DefaultTxFlags;
                }
            }
            return flags;
        }

        private void InitComboBoxAPI()
        {
            ComboBoxAPI.Items.Clear();
            int count = J2534Code.GetRegistryPassThruSupportInfo();
            if (count == 0)
            {
                ComboBoxAPI.Items.Add("No information found in the registry");
                ComboBoxAPI.SelectedIndex = 0;
            }
            else
            {
                ComboBoxAPI.Items.AddRange(J2534_APIs.ToArray());
                ComboBoxAPI.SelectedIndex = 0;
            }
            ComboBoxAPI.Enabled = ComboBoxAPI.Items.Count > 1;
        }

        private void UpdateAPILabels()
        {
            if (J2534_APIs.Count < 1)
            {
                TextBoxDllPath.Text = string.Empty;
                LabelVendor.Text = "Vendor: none";
                LabelDevice.Text = "Device: none";
                LabelDllName.Text = "J2534 dll: none";
                ButtonLoadDLL.Enabled = false;
            }
            else
            {
                if (ComboBoxAPI.SelectedItem is J2534_API api)
                {
                    TextBoxDllPath.Text = api.LibraryName;
                    LabelVendor.Text = string.Concat("Vendor: ", api.VendorName);
                    LabelDevice.Text = string.Concat("Device: ", api.DeviceName);
                    LabelDllName.Text = string.Concat("J2534 dll: ", api.LibraryName, api.DllIsLoaded ? " - Loaded" : "");
                    ButtonLoadDLL.Enabled = !api.DllIsLoaded;
                }
                else
                {
                    ButtonLoadDLL.Enabled = false;
                }
            }
        }

        private void UpdateComboBoxDevice()
        {
            string initialText = ComboBoxDevice.Text;
            ComboBoxDevice.Items.Clear();
            ComboBoxDevice.Text = "";
            int nextBox = GetAvailableDeviceIndex();
            if (ComboBoxAPI.SelectedItem is J2534_API api)
            {
                if (api.DllIsLoaded)
                {
                    foreach (J2534Device Box in J2534Devices.Values)
                    {
                        bool okToAdd = Box.API != null && Box.API == api;
                        foreach (string deviceItem in ComboBoxDevice.Items)
                        {
                            if (Box.Name.Equals(deviceItem, PublicDeclarations.IgnoreCase))
                            {
                                okToAdd = false;
                            }
                        }
                        if (okToAdd)
                        {
                            ComboBoxDevice.Items.Add(Box.Name);
                        }
                    }
                    string baseItem = string.Concat(api.DeviceName, " ", nextBox);
                    ComboBoxDevice.Items.Add(baseItem);
                }
            }
            int j = 0;
            for (int i = 0; i < ComboBoxDevice.Items.Count; i++)
            {
                if (!string.IsNullOrWhiteSpace(initialText) && string.Equals(initialText, ComboBoxDevice.Items[i].ToString(), PublicDeclarations.IgnoreCase))
                {
                    j = i;
                }
            }
            if (ComboBoxDevice.Items.Count > 0)
            {
                ComboBoxDevice.SelectedIndex = j;
            }
            ComboBoxDevice.Enabled = ComboBoxDevice.Items.Count > 0;
            LabelComboDevice.Enabled = ComboBoxDevice.Enabled;
        }

        private void UpdateDeviceLabels()
        {
            J2534Device device = J2534Code.NameToDevice(ComboBoxDevice.Text);
            if (device != null)
            {
                string infoString = string.Concat("Name: ", device.Name, Environment.NewLine,
                                                   "Version: ", device.Version, Environment.NewLine);
                if (device.Connected)
                {
                    infoString = string.Concat(infoString, "Status: Open", Environment.NewLine);
                }
                else
                {
                    infoString = string.Concat(infoString, "Status: Available", Environment.NewLine);
                }
                if (device.Connected)
                {
                    infoString = string.Concat(infoString, "Device ID: ", device.DeviceId);
                }
                else
                {
                    infoString = string.Concat(infoString, "Device ID: none");
                }
                LabelDeviceInfo.Text = infoString;

                ButtonOpenBox.Enabled = device.API != null && device.API.DllIsLoaded && !device.Connected;
                ButtonCloseBox.Enabled = device.API != null && device.API.DllIsLoaded && ComboBoxAPI.SelectedItem != null && (NCS_J2534_2_Tool.J2534_API)ComboBoxAPI.SelectedItem == device.API && device.Connected;
            }
            else
            {
                J2534_API api = ComboBoxAPI.SelectedItem as J2534_API;
                LabelDeviceInfo.Text = string.Empty;
                ButtonOpenBox.Enabled = api != null && api.DllIsLoaded && !string.IsNullOrWhiteSpace(ComboBoxDevice.Text);
                ButtonCloseBox.Enabled = false;
            }
        }

        private void UpdateDeviceInfo(J2534Device device)
        {
            if (device != null && J2534Devices.Values.Contains(device))
            {
                LabelJ2534Info.Text = device.Description;
                if (device.Connected)
                {
                    LabelProtSupport.Text = string.Concat("Supported Protocols: ", device.SupportedProtocols);
                }
                else
                {
                    LabelProtSupport.Text = string.Empty;
                }
            }
            else
            {
                LabelJ2534Info.Text = string.Empty;
                LabelProtSupport.Text = string.Empty;
            }
        }

        private void UpdateComboBoxAvailableDevice()
        {
            string initialText = ComboBoxAvailableDevice.Text;
            ComboBoxAvailableDevice.Items.Clear();
            foreach (J2534Device device in J2534Devices.Values)
            {
                if (device.Connected)
                {
                    ComboBoxAvailableDevice.Items.Add(device);
                }
            }
            int itemCount = ComboBoxAvailableDevice.Items.Count - 1;
            if (itemCount >= 0)
            {
                int index = 0;
                if (!string.IsNullOrEmpty(initialText))
                {
                    for (int i = 0; i <= itemCount; i++)
                    {
                        if (string.Equals(ComboBoxAvailableDevice.Items[i].ToString(), initialText, PublicDeclarations.IgnoreCase))
                        {
                            index = i;
                            break;
                        }
                    }
                }
                ComboBoxAvailableDevice.SelectedIndex = index;
            }
            else
            {
                UpdateDeviceLabels();
            }
            ComboBoxAvailableDevice.Enabled = ComboBoxAvailableDevice.Items.Count > 1;
            for (int i = 0; i < LabelDeviceCombo.Length; i++)
            {
                LabelDeviceCombo[i].Enabled = ComboBoxAvailableDevice.Enabled;
            }
            UpdateTabs();
            if (TabControl1.SelectedTab != TabPageConnect)
            {
                ComboBoxAvailableDevice.Focus();
            }
        }

        private static void MsgInCopyLine(ListView thisListView)
        {
            if (thisListView.Items.Count < 1)
            {
                return;
            }
            StringBuilder sb = new StringBuilder();
            foreach (ListViewItem item in thisListView.SelectedItems)
            {
                sb.Append(item.Text);
                sb.Append(",");
                sb.Append(item.SubItems[1].Text);
                sb.Append(",");
                sb.Append(item.SubItems[2].Text);
                sb.Append(",");
                sb.AppendLine(item.SubItems[3].Text);
            }
            Clipboard.SetText(sb.ToString());
        }

        private static void ResultsCopyLine(ListView thisListView)
        {
            if (thisListView.Items.Count < 1)
            {
                return;
            }
            StringBuilder sb = new StringBuilder();
            foreach (ListViewItem item in thisListView.SelectedItems)
            {
                sb.Append(item.Text);
                sb.Append(",");
                sb.AppendLine(item.SubItems[1].Text);
            }
            Clipboard.SetText(sb.ToString());
        }

        private void MakeFilterFromIncomingMessage(ListView thisListView)
        {
            J2534Device device = J2534Code.GetListViewParentDevice(thisListView);
            if (device != null && thisListView.SelectedItems.Count > 0)
            {
                J2534MessageText messageText = new J2534MessageText {ProtocolName = thisListView.SelectedItems[0].SubItems[1].Text};
                J2534Channel channel = device.Channel(messageText.ProtocolName);
                if (channel == null)
                {
                    return;
                }
                J2534Channel channelISO = null;
                if (channel.IsCAN && !channel.Connected)
                {
                    channelISO = J2534Code.CAN_ChannelToISO_Channel(device, channel);
                }
                if (channelISO != null)
                {
                    channel = channelISO;
                }
                int nextFilter = channel.NextFilter;
                if (nextFilter < 0)
                {
                    return;
                }
                messageText.Data = thisListView.SelectedItems[0].SubItems[3].Text;
                channel.Filter[nextFilter].PatternMessage = messageText.ToMessage();
                channel.Filter[nextFilter].MaskMessage = J2534Code.GetMessageMask(channel.Filter[nextFilter].PatternMessage);
                channel.Filter[nextFilter].FlowMessage = PASSTHRU_MSG.CreateInstance();
                channel.Filter[nextFilter].FlowMessage.TxFlags = channel.DefaultTxFlags;
                channel.Filter[nextFilter].FilterType = J2534FilterType.PASS_FILTER;
                SetFilterEditBuffer(nextFilter);
            }
        }

        private void SaveListViewItemListToFile(ListViewItem[] itemList)
        {
            if (itemList.Length < 1)
            {
                return;
            }
            string fileFolder = Properties.Settings.Default.DataFilePath;
            if (string.IsNullOrWhiteSpace(fileFolder) || !System.IO.Directory.Exists(fileFolder))
            {
                fileFolder = GetDataPath();
            }
            DialogFileSave.InitialDirectory = fileFolder;
            DialogFileSave.FileName = string.Empty;
            DialogFileSave.DefaultExt = ".in";
            DialogFileSave.Filter = "Message In Files (*.in)|*.in";
            DialogFileSave.Title = "Save Messages As:";
            DialogFileSave.RestoreDirectory = true;
            DialogFileSave.CheckPathExists = true;
            DialogFileSave.OverwritePrompt = true;
            if (DialogFileSave.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string fileName = DialogFileSave.FileName;
            StringBuilder sb = new StringBuilder();
            foreach (ListViewItem listItem in itemList)
            {
                sb.Append(listItem.Text);
                sb.Append(",");
                sb.Append(listItem.SubItems[1].Text);
                sb.Append(",");
                sb.Append(listItem.SubItems[2].Text);
                sb.Append(",");
                sb.AppendLine(listItem.SubItems[3].Text);
            }
            if (sb.Length == 0)
            {
                return;
            }
            using (StreamWriter sw = new StreamWriter(fileName, false, Encoding.ASCII))
            {
                sw.Write(sb.ToString());
            }
            Properties.Settings.Default.DataFilePath = System.IO.Path.GetDirectoryName(fileName);
        }

        private void ModifySelectedOutgoingMessage(ListView thisListView)
        {
            if (thisListView == null)
            {
                return;
            }
            if (thisListView.SelectedItems.Count <= 0)
            {
                return;
            }
            J2534MessageText messageText = GetListViewOutMessageText();
            ListViewItem selectedItem = thisListView.SelectedItems[0];
            DialogResult result = FormMessageEdit.DefaultInstance.ShowDialog(messageText, selectedDevice);
            if (result == DialogResult.OK && thisListView.SelectedItems[0] == selectedItem)
            {
                if (thisListView.SelectedItems.Count > 0)
                {
                    selectedDevice.ListViewMessageOut.SelectedItems[0].Text = messageText.ProtocolName;
                    selectedDevice.ListViewMessageOut.SelectedItems[0].SubItems[1].Text = messageText.TxFlags;
                    selectedDevice.ListViewMessageOut.SelectedItems[0].SubItems[2].Text = messageText.Data;
                    selectedDevice.ListViewMessageOut.SelectedItems[0].SubItems[3].Text = messageText.Comment;
                }
            }
        }

        private void LoadOutgoingMessageListFromFile(ListView thisListView)
        {
            string fileFolder = Properties.Settings.Default.DataFilePath;
            if (string.IsNullOrWhiteSpace(fileFolder) || !System.IO.Directory.Exists(fileFolder))
            {
                fileFolder = GetDataPath();
            }
            DialogFileOpen.InitialDirectory = fileFolder;
            DialogFileOpen.FileName = string.Empty;
            DialogFileOpen.DefaultExt = ".out";
            DialogFileOpen.Filter = "Message Files (*.out;*.msg)|*.out;*.msg";
            DialogFileOpen.Title = "Load Message File:";
            DialogFileOpen.RestoreDirectory = true;
            DialogFileOpen.CheckPathExists = false;
            DialogFileOpen.Multiselect = true;
            if (DialogFileOpen.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            foreach (string fileName in DialogFileOpen.FileNames)
            {
                using (Microsoft.VisualBasic.FileIO.TextFieldParser sr = new Microsoft.VisualBasic.FileIO.TextFieldParser(fileName))
                {
                    sr.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                    sr.SetDelimiters(",");
                    while (!sr.EndOfData)
                    {
                        try
                        {
                            string[] data = sr.ReadFields();
                            if (data.Length > 0)
                            {
                                int c = data.Length;
                                ListViewItem listItem = thisListView.Items.Add(data[0]);
                                if (c > 1)
                                {
                                    listItem.SubItems.Add(data[1]);
                                }
                                if (c > 2)
                                {
                                    listItem.SubItems.Add(data[2]);
                                }
                                if (c > 3)
                                {
                                    listItem.SubItems.Add(data[3].Replace(string.Concat('\"'.ToString(), '\"'.ToString()), '\"'.ToString()));
                                }
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
            Properties.Settings.Default.DataFilePath = System.IO.Path.GetDirectoryName(DialogFileOpen.FileName);
        }

        private void SaveOutgoingMessageListToFile(ListView thisListView)
        {
            if (thisListView == null)
            {
                return;
            }
            if (thisListView.Items.Count < 1)
            {
                return;
            }
            string fileFolder = Properties.Settings.Default.DataFilePath;
            if (string.IsNullOrWhiteSpace(fileFolder) || !System.IO.Directory.Exists(fileFolder))
            {
                fileFolder = GetDataPath();
            }
            DialogFileSave.InitialDirectory = fileFolder;
            DialogFileSave.FileName = string.Empty;
            DialogFileSave.DefaultExt = ".out";
            DialogFileSave.Filter = "Message In Files (*.out)|*.out";
            DialogFileSave.Title = "Save Messages As:";
            DialogFileSave.RestoreDirectory = true;
            DialogFileSave.CheckPathExists = true;
            DialogFileSave.OverwritePrompt = true;
            if (DialogFileSave.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string fileName = DialogFileSave.FileName;
            StringBuilder sb = new StringBuilder();
            foreach (ListViewItem item in thisListView.Items)
            {
                sb.Append(item.Text);
                sb.Append(",");
                sb.Append(item.SubItems[1].Text);
                sb.Append(",");
                sb.Append(item.SubItems[2].Text);
                sb.Append(",");
                sb.AppendLine(PrepareCSV(item.SubItems[3].Text));
            }
            using (StreamWriter sw = new StreamWriter(DialogFileSave.FileName, false, Encoding.ASCII))
            {
                sw.Write(sb.ToString());
            }
            Properties.Settings.Default.DataFilePath = System.IO.Path.GetDirectoryName(fileName);
        }

        private static string PrepareCSV(string value)
        {
            char c = '\0';
            bool mustQuote = false;
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }
            for (int i = 0; i < value.Length; i++)
            {
                c = value[i];
                if (c == '\"' || c == '\n' || c == ',')
                {
                    mustQuote = true;
                }
            }
            string result = value.Replace('\"'.ToString(), string.Concat('\"', '\"'));
            if (mustQuote)
            {
                result = string.Concat('\"', result, '\"');
            }
            return result;
        }

        private void CopyOutgoingMessageToScratchPad(ListView thisListView)
        {
            if (thisListView.SelectedItems.Count > 0)
            {
                ComboBoxMessageChannel.Text = thisListView.SelectedItems[0].Text;
                TextBoxOutFlags.Text = thisListView.SelectedItems[0].SubItems[1].Text;
                TextBoxMessageOut.Text = thisListView.SelectedItems[0].SubItems[2].Text;
            }
        }

        private J2534MessageText GetScratchPadMessageText()
        {
            J2534MessageText result = new J2534MessageText();
            string comment = FormInputBox.DefaultInstance.ShowDialog("Please enter a comment for the message:", "Add to Outgoing Message Set", null, this.Left + this.Width / 2, this.Top + this.Height / 2 - 60).Response;
            result.ProtocolName = ComboBoxMessageChannel.Text;
            result.Data = TextBoxMessageOut.Text;
            result.TxFlags = TextBoxOutFlags.Text;
            result.Comment = comment;
            return result;
        }

        private J2534MessageText GetNewMessageTextFromDialog()
        {
            J2534MessageText result = new J2534MessageText
            {
                Comment = "Add a comment for this message",
                ProtocolName = ComboBoxMessageChannel.Text,
                TxFlags = J2534Code.FlagsToText(0U)
            };
            if (string.IsNullOrWhiteSpace(result.ProtocolName))
            {
                result.ProtocolName = "CAN";
            }
            if (!(FormMessageEdit.DefaultInstance.ShowDialog(result, selectedDevice) == DialogResult.OK))
            {
                result.ProtocolName = string.Empty;
            }
            return result;
        }

        private void MakePeriodicFromOutgoingMessage(ListView thisListView)
        {
            if (thisListView.SelectedItems.Count > 0)
            {
                J2534Device device = J2534Code.GetListViewParentDevice(thisListView);
                if (device == null)
                {
                    return;
                }
                J2534MessageText messageText = new J2534MessageText {ProtocolName = thisListView.SelectedItems[0].Text};
                J2534Channel channel = device.Channel(messageText.ProtocolName);
                if (channel == null)
                {
                    return;
                }
                J2534Channel channelISO = null;
                if (channel.IsCAN && !channel.Connected)
                {
                    channelISO = J2534Code.CAN_ChannelToISO_Channel(device, channel);
                }
                if (channelISO != null)
                {
                    channel = channelISO;
                }
                int nextPM = channel?.NextPeriodicMessage ?? -1;
                if (nextPM < 0)
                {
                    return;
                }
                messageText.Data = thisListView.SelectedItems[0].SubItems[2].Text;
                messageText.TxFlags = thisListView.SelectedItems[0].SubItems[1].Text;
                channel.PeriodicMessage[nextPM].Message = messageText.ToMessage();
                SetPeriodicMessageEditBuffer(nextPM);
            }
        }

        public void LoadFiltersFromFile()
        {
            if (!TextBoxFilterMask[0].Enabled)
            {
                return;
            }
            J2534FilterText[] filterTxt = {};
            string fileFolder = Properties.Settings.Default.DataFilePath;
            if (string.IsNullOrWhiteSpace(fileFolder) || !System.IO.Directory.Exists(fileFolder))
            {
                fileFolder = GetDataPath();
            }
            DialogFileOpen.InitialDirectory = fileFolder;
            DialogFileOpen.FileName = string.Empty;
            DialogFileOpen.DefaultExt = ".filter";
            DialogFileOpen.Filter = "Filter Files (*.filter)|*.filter";
            DialogFileOpen.Title = "Load Filters:";
            DialogFileOpen.ShowReadOnly = false;
            DialogFileOpen.RestoreDirectory = true;
            DialogFileOpen.CheckFileExists = true;
            DialogFileOpen.CheckPathExists = false;
            DialogFileOpen.Multiselect = false;
            if (DialogFileOpen.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string fileName = DialogFileOpen.FileName;
            XmlDocument filterDocument = new XmlDocument();
            if (!File.Exists(fileName))
            {
                return;
            }
            try
            {
                filterDocument.Load(fileName);
            }
            catch
            {
                MessageBox.Show(this, "File may not have been a valid filter file", "Load Filter File", MessageBoxButtons.OK);
                return;
            }

            try
            {
                XmlNodeList nodeList = filterDocument.SelectSingleNode("Filters").ChildNodes;
                foreach (XmlNode node in nodeList)
                {
                    if (node.Name.StartsWith("Filter", PublicDeclarations.IgnoreCase))
                    {
                        J2534FilterText localFilter = new J2534FilterText();
                        foreach (XmlNode item in node.ChildNodes)
                        {
                            if (item.Name.Equals("Type", PublicDeclarations.IgnoreCase))
                            {
                                if (!string.IsNullOrWhiteSpace(item.InnerText))
                                {
                                    localFilter.FilterType = item.InnerText;
                                }
                            }
                            else if (item.Name.Equals("MaskMsg", PublicDeclarations.IgnoreCase))
                            {
                                foreach (XmlNode subItem in item.ChildNodes)
                                {
                                    if (subItem.Name.Equals("Protocol", PublicDeclarations.IgnoreCase))
                                    {
                                        if (!string.IsNullOrWhiteSpace(subItem.InnerText))
                                        {
                                            localFilter.MaskMessageText.ProtocolName = subItem.InnerText;
                                        }
                                    }
                                    else if (subItem.Name.Equals("Data", PublicDeclarations.IgnoreCase))
                                    {
                                        if (!string.IsNullOrWhiteSpace(subItem.InnerText))
                                        {
                                            localFilter.MaskMessageText.Data = subItem.InnerText;
                                        }
                                    }
                                    else if (subItem.Name.Equals("TxFlags", PublicDeclarations.IgnoreCase))
                                    {
                                        if (!string.IsNullOrWhiteSpace(subItem.InnerText))
                                        {
                                            localFilter.MaskMessageText.TxFlags = subItem.InnerText;
                                        }
                                    }
                                }
                            }
                            else if (item.Name.Equals("PattMsg", PublicDeclarations.IgnoreCase))
                            {
                                foreach (XmlNode subItem in item.ChildNodes)
                                {
                                    if (subItem.Name.Equals("Protocol", PublicDeclarations.IgnoreCase))
                                    {
                                        if (!string.IsNullOrWhiteSpace(subItem.InnerText))
                                        {
                                            localFilter.PatternMessageText.ProtocolName = subItem.InnerText;
                                        }
                                    }
                                    else if (subItem.Name.Equals("Data", PublicDeclarations.IgnoreCase))
                                    {
                                        if (!string.IsNullOrWhiteSpace(subItem.InnerText))
                                        {
                                            localFilter.PatternMessageText.Data = subItem.InnerText;
                                        }
                                    }
                                    else if (subItem.Name.Equals("TxFlags", PublicDeclarations.IgnoreCase))
                                    {
                                        if (!string.IsNullOrWhiteSpace(subItem.InnerText))
                                        {
                                            localFilter.PatternMessageText.TxFlags = subItem.InnerText;
                                        }
                                    }
                                }
                            }
                            else if (item.Name.Equals("FlowMsg", PublicDeclarations.IgnoreCase))
                            {
                                foreach (XmlNode subItem in item.ChildNodes)
                                {
                                    if (subItem.Name.Equals("Protocol", PublicDeclarations.IgnoreCase))
                                    {
                                        if (!string.IsNullOrWhiteSpace(subItem.InnerText))
                                        {
                                            localFilter.FlowMessageText.ProtocolName = subItem.InnerText;
                                        }
                                    }
                                    else if (subItem.Name.Equals("Data", PublicDeclarations.IgnoreCase))
                                    {
                                        if (!string.IsNullOrWhiteSpace(subItem.InnerText))
                                        {
                                            localFilter.FlowMessageText.Data = subItem.InnerText;
                                        }
                                    }
                                    else if (subItem.Name.Equals("TxFlags", PublicDeclarations.IgnoreCase))
                                    {
                                        if (!string.IsNullOrWhiteSpace(subItem.InnerText))
                                        {
                                            localFilter.FlowMessageText.TxFlags = subItem.InnerText;
                                        }
                                    }
                                }
                            }
                            else if (item.Name.Equals("Type", PublicDeclarations.IgnoreCase))
                            {
                                if (!string.IsNullOrWhiteSpace(item.InnerText))
                                {
                                    localFilter.FilterType = item.InnerText;
                                }
                            }
                            else if (item.Name.Equals("Enabled", PublicDeclarations.IgnoreCase))
                            {
                                bool.TryParse(item.InnerText, out bool temp);
                                localFilter.Enabled = temp;
                            }

                        }
                        int nextFilter = filterTxt.Length;
                        Array.Resize(ref filterTxt, nextFilter + 1);
                        filterTxt[nextFilter] = localFilter;
                    }
                }

            }
            catch
            {
            }
            int c = filterTxt.Length - 1;
            if (c > J2534Constants.MAX_FILTER)
            {
                c = J2534Constants.MAX_FILTER;
            }
            settingFilters = true;
            for (int i = 0; i <= c; i++)
            {
                TextBoxFilterMask[i].Text = filterTxt[i].MaskMessageText.Data;
                TextBoxFilterPatt[i].Text = filterTxt[i].PatternMessageText.Data;
                TextBoxFilterFlow[i].Text = filterTxt[i].FlowMessageText.Data;
                TextBoxFilterFlags[i].Text = filterTxt[i].FlowMessageText.TxFlags;
                ComboBoxFilterType[i].Text = filterTxt[i].FilterType;
                SetFilterEditBuffer(i);
            }
            settingFilters = false;
            UpdateFilterEdit();
            Properties.Settings.Default.DataFilePath = System.IO.Path.GetDirectoryName(fileName);
        }

        private void SaveFiltersToFile()
        {
            string protName = ComboBoxAvailableChannel.Text;
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel))
            {
                return;
            }
            string fileFolder = Properties.Settings.Default.DataFilePath;
            if (string.IsNullOrWhiteSpace(fileFolder) || !System.IO.Directory.Exists(fileFolder))
            {
                fileFolder = GetDataPath();
            }
            DialogFileSave.InitialDirectory = fileFolder;
            DialogFileSave.FileName = string.Empty;
            DialogFileSave.DefaultExt = ".filter";
            DialogFileSave.Filter = "Filter Files (*.filter)|*.filter";
            DialogFileSave.Title = "Save Filters As:";
            DialogFileSave.RestoreDirectory = true;
            DialogFileSave.CheckPathExists = true;
            DialogFileSave.OverwritePrompt = true;
            if (DialogFileSave.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string fileName = DialogFileSave.FileName;
            XmlDocument document = new XmlDocument();
            document.AppendChild(document.CreateXmlDeclaration("1.0", "utf-8", string.Empty));

            string filterType = string.Empty;
            XmlNode filtersNode = document.CreateElement("Filters");
            document.AppendChild(filtersNode);
            XmlNode filterNode = null;
            XmlNode itemNode = null;
            XmlNode subItemNode = null;
            XmlNode protocolNode = document.CreateElement("Protocol");
            protocolNode.InnerText = protName;
            filtersNode.AppendChild(protocolNode);
            for (int filterIndex = 0; filterIndex <= J2534Constants.MAX_FILTER; filterIndex++)
            {
                if (!channel.Filter[filterIndex].IsEmpty())
                {
                    J2534MessageText mtxtMask = channel.Filter[filterIndex].MaskMessage.ToMessageText();
                    J2534MessageText mtxtPatt = channel.Filter[filterIndex].PatternMessage.ToMessageText();
                    J2534MessageText mtxtFlow = channel.Filter[filterIndex].FlowMessage.ToMessageText();
                    filterNode = document.CreateElement(string.Concat("Filter-", filterIndex.ToString("00")));
                    filterType = J2534Code.FilterTypeToName(channel.Filter[filterIndex].FilterType);
                    itemNode = document.CreateElement("Type");
                    itemNode.InnerText = filterType;
                    filterNode.AppendChild(itemNode);
                    if (!string.IsNullOrWhiteSpace(mtxtMask.Data))
                    {
                        itemNode = document.CreateElement("MaskMsg");
                        subItemNode = document.CreateElement("Protocol");
                        subItemNode.InnerText = mtxtMask.ProtocolName;
                        itemNode.AppendChild(subItemNode);
                        subItemNode = document.CreateElement("Data");
                        subItemNode.InnerText = mtxtMask.Data;
                        itemNode.AppendChild(subItemNode);
                        subItemNode = document.CreateElement("TxFlags");
                        subItemNode.InnerText = mtxtMask.TxFlags;
                        itemNode.AppendChild(subItemNode);
                        filterNode.AppendChild(itemNode);
                    }
                    if (!string.IsNullOrWhiteSpace(mtxtPatt.Data))
                    {
                        itemNode = document.CreateElement("PattMsg");
                        subItemNode = document.CreateElement("Protocol");
                        subItemNode.InnerText = mtxtPatt.ProtocolName;
                        itemNode.AppendChild(subItemNode);
                        subItemNode = document.CreateElement("Data");
                        subItemNode.InnerText = mtxtPatt.Data;
                        itemNode.AppendChild(subItemNode);
                        subItemNode = document.CreateElement("TxFlags");
                        subItemNode.InnerText = mtxtPatt.TxFlags;
                        itemNode.AppendChild(subItemNode);
                        filterNode.AppendChild(itemNode);
                    }
                    if (!string.IsNullOrWhiteSpace(mtxtFlow.Data))
                    {
                        itemNode = document.CreateElement("FlowMsg");
                        subItemNode = document.CreateElement("Protocol");
                        subItemNode.InnerText = mtxtFlow.ProtocolName;
                        itemNode.AppendChild(subItemNode);
                        subItemNode = document.CreateElement("Data");
                        subItemNode.InnerText = mtxtFlow.Data;
                        itemNode.AppendChild(subItemNode);
                        subItemNode = document.CreateElement("TxFlags");
                        subItemNode.InnerText = mtxtFlow.TxFlags;
                        itemNode.AppendChild(subItemNode);
                        filterNode.AppendChild(itemNode);
                    }
                    itemNode = document.CreateElement("ID");
                    itemNode.InnerText = channel.Filter[filterIndex].MessageId.ToString();
                    filterNode.AppendChild(itemNode);
                    itemNode = document.CreateElement("Enabled");
                    itemNode.InnerText = channel.Filter[filterIndex].Enabled.ToString();
                    filterNode.AppendChild(itemNode);
                    filtersNode.AppendChild(filterNode);
                } //filter is not empty
            }

            XmlWriterSettings xws = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "    ",
                CloseOutput = true,
                NamespaceHandling = NamespaceHandling.OmitDuplicates
            };
            try
            {
                using (XmlWriter xm = XmlWriter.Create(fileName, xws))
                {
                    document.Save(xm);
                }
            }
            catch (Exception ex)
            {
            }
            Properties.Settings.Default.DataFilePath = System.IO.Path.GetDirectoryName(fileName);
        }

        public void LoadPeriodicMessagesFromFile()
        {
            if (!(TextBoxPeriodicMessage[0].Enabled))
            {
                return;
            }
            J2534PeriodicMessageText[] PMText = {};
            string fileFolder = Properties.Settings.Default.DataFilePath;
            if (string.IsNullOrWhiteSpace(fileFolder) || !System.IO.Directory.Exists(fileFolder))
            {
                fileFolder = GetDataPath();
            }
            DialogFileOpen.InitialDirectory = fileFolder;
            DialogFileOpen.FileName = string.Empty;
            DialogFileOpen.DefaultExt = ".periodic";
            DialogFileOpen.Filter = "Periodic Message Files (*.periodic)|*.periodic";
            DialogFileOpen.Title = "Load Periodic Messages:";
            DialogFileOpen.ShowReadOnly = false;
            DialogFileOpen.RestoreDirectory = true;
            DialogFileOpen.CheckFileExists = true;
            DialogFileOpen.CheckPathExists = false;
            DialogFileOpen.Multiselect = false;
            if (DialogFileOpen.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string fileName = DialogFileOpen.FileName;
            XmlDocument pmDocument = new XmlDocument();
            if (!File.Exists(fileName))
            {
                return;
            }
            try
            {
                pmDocument.Load(fileName);
            }
            catch
            {
                MessageBox.Show(this, "File may not have been a valid filter file", "Load Filter File", MessageBoxButtons.OK);
                return;
            }

            try
            {
                XmlNodeList nodeList = pmDocument.SelectSingleNode("PeriodicMessages").ChildNodes;
                foreach (XmlNode node in nodeList)
                {
                    if (node.Name.StartsWith("PeriodicMessage", PublicDeclarations.IgnoreCase))
                    {
                        J2534PeriodicMessageText localPMText = new J2534PeriodicMessageText();
                        foreach (XmlNode item in node.ChildNodes)
                        {
                            if (item.Name.Equals("Protocol", PublicDeclarations.IgnoreCase))
                            {
                                if (!string.IsNullOrWhiteSpace(item.InnerText))
                                {
                                    localPMText.MessageText.ProtocolName = item.InnerText;
                                }
                            }
                            else if (item.Name.Equals("ID", PublicDeclarations.IgnoreCase))
                            {
                                uint.TryParse(item.InnerText, out uint temp);
                                localPMText.MessageId = temp;
                            }
                            else if (item.Name.Equals("Message", PublicDeclarations.IgnoreCase))
                            {
                                if (!string.IsNullOrWhiteSpace(item.InnerText))
                                {
                                    localPMText.MessageText.Data = item.InnerText;
                                }
                            }
                            else if (item.Name.Equals("TxFlags", PublicDeclarations.IgnoreCase))
                            {
                                if (!string.IsNullOrWhiteSpace(item.InnerText))
                                {
                                    localPMText.MessageText.TxFlags = item.InnerText;
                                }
                            }
                            else if (item.Name.Equals("Enabled", PublicDeclarations.IgnoreCase))
                            {
                                bool.TryParse(item.InnerText, out bool temp);
                                localPMText.Enabled = temp;
                            }
                            else if (item.Name.Equals("Interval", PublicDeclarations.IgnoreCase))
                            {
                                uint.TryParse(item.InnerText, out uint temp);
                                localPMText.Interval = temp;
                            }
                        }
                        int nextPM = PMText.Length;
                        Array.Resize(ref PMText, nextPM + 1);
                        PMText[nextPM] = localPMText;
                    }
                }
            }
            catch
            {
            }
            int n = PMText.Length - 1;
            if (n > J2534Constants.MAX_PM)
            {
                n = J2534Constants.MAX_PM;
            }
            for (int i = 0; i <= n; i++)
            {
                settingPeriodicMessages = true;
                TextBoxPeriodicMessage[i].Text = PMText[i].MessageText.Data;
                if (ComboBoxPeriodicMessageChannel[i].Visible)
                {
                    ComboBoxPeriodicMessageChannel[i].Text = PMText[i].MessageText.ProtocolName;
                }
                TextBoxPeriodicMessageFlags[i].Text = PMText[i].MessageText.TxFlags;
                TextBoxPeriodicMessageInterval[i].Text = PMText[i].Interval.ToString();
                settingPeriodicMessages = false;
                SetPeriodicMessageEditBuffer(i);
                UpdatePeriodicMessageCheck(i);
            }
            UpdatePeriodicMessageEdit();
            Properties.Settings.Default.DataFilePath = System.IO.Path.GetDirectoryName(fileName);
        }

        private void SavePeriodicMessagesToFile()
        {
            string protName = ComboBoxAvailableChannel.Text;
            if (!(ComboBoxAvailableChannel.SelectedItem is J2534Channel channel))
            {
                return;
            }
            string fileFolder = Properties.Settings.Default.DataFilePath;
            if (string.IsNullOrWhiteSpace(fileFolder) || !System.IO.Directory.Exists(fileFolder))
            {
                fileFolder = GetDataPath();
            }
            DialogFileSave.InitialDirectory = fileFolder;
            DialogFileSave.FileName = string.Empty;
            DialogFileSave.DefaultExt = ".periodic";
            DialogFileSave.Filter = "Periodic Message Files (*.periodic)|*.periodic";
            DialogFileSave.Title = "Save Periodic Messages As:";
            DialogFileSave.RestoreDirectory = true;
            DialogFileSave.CheckPathExists = true;
            DialogFileSave.OverwritePrompt = true;
            if (DialogFileSave.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string fileName = DialogFileSave.FileName;
            XmlDocument document = new XmlDocument();
            document.AppendChild(document.CreateXmlDeclaration("1.0", "utf-8", string.Empty));

            XmlNode pmsNode = document.CreateElement("PeriodicMessages");
            document.AppendChild(pmsNode);
            XmlNode pmNode = null;
            XmlNode itemNode = null;
            XmlNode protocolNode = document.CreateElement("Protocol");
            protocolNode.InnerText = protName;
            pmsNode.AppendChild(protocolNode);
            for (int pmIndex = 0; pmIndex <= J2534Constants.MAX_PM; pmIndex++)
            {
                if (!channel.PeriodicMessage[pmIndex].IsEmpty())
                {
                    J2534MessageText messageText = channel.PeriodicMessage[pmIndex].Message.ToMessageText();
                    pmNode = document.CreateElement(string.Concat("PeriodicMessage-", pmIndex.ToString("00")));
                    itemNode = document.CreateElement("Protocol");
                    itemNode.InnerText = protName;
                    pmNode.AppendChild(itemNode);
                    itemNode = document.CreateElement("ID");
                    itemNode.InnerText = channel.PeriodicMessage[pmIndex].MessageId.ToString();
                    pmNode.AppendChild(itemNode);
                    if (!string.IsNullOrWhiteSpace(messageText.Data))
                    {
                        itemNode = document.CreateElement("Message");
                        itemNode.InnerText = messageText.Data;
                        pmNode.AppendChild(itemNode);
                    }
                    itemNode = document.CreateElement("TxFlags");
                    itemNode.InnerText = messageText.TxFlags;
                    pmNode.AppendChild(itemNode);
                    itemNode = document.CreateElement("Interval");
                    itemNode.InnerText = channel.PeriodicMessage[pmIndex].Interval.ToString();
                    pmNode.AppendChild(itemNode);
                    itemNode = document.CreateElement("Enabled");
                    itemNode.InnerText = channel.PeriodicMessage[pmIndex].Enabled.ToString();
                    pmNode.AppendChild(itemNode);
                    pmsNode.AppendChild(pmNode);
                } //pm is not empty
            }

            XmlWriterSettings xws = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "    ",
                CloseOutput = true,
                NamespaceHandling = NamespaceHandling.OmitDuplicates
            };
            try
            {
                using (XmlWriter xm = XmlWriter.Create(fileName, xws))
                {
                    document.Save(xm);
                }
            }
            catch (Exception ex)
            {
            }
            Properties.Settings.Default.DataFilePath = System.IO.Path.GetDirectoryName(fileName);
        }

        private static string GetDataPath()
        {
            string appPath = new System.IO.FileInfo(Application.ExecutablePath).DirectoryName;
            string tryPath = string.Concat(appPath , "\\Data");
            if (System.IO.Directory.Exists(tryPath))
            {
                return tryPath;
            }
            return appPath;
        }

        private void SetControlArrays()
        {
            InitializeComboAvailableChannelLocator();
            InitializeComboAvailableBoxLocator();
            InitializeTextBoxParamVal();
            for (int i = 0; i < TextBoxParamVal.Length; i++)
            {
                TextBoxParamVal[i].TextChanged += TextBoxParamVal_TextChanged;
                TextBoxParamVal[i].KeyPress += TextBoxParamVal_KeyPress;
                TextBoxParamVal[i].Leave += TextBoxParamVal_Leave;
            }
            InitializeTextBoxPeriodicMessage();
            for (int i = 0; i < TextBoxPeriodicMessage.Length; i++)
            {
                TextBoxPeriodicMessage[i].TextChanged += TextBoxPeriodicMessage_TextChanged;
                TextBoxPeriodicMessage[i].KeyPress += TextBoxPeriodicMessage_KeyPress;
                TextBoxPeriodicMessage[i].Leave += TextBoxPeriodicMessage_Leave;
            }
            InitializeTextBoxPeriodicMessageInterval();
            for (int i = 0; i < TextBoxPeriodicMessageInterval.Length; i++)
            {
                TextBoxPeriodicMessageInterval[i].TextChanged += TextBoxPeriodicMessageInterval_TextChanged;
                TextBoxPeriodicMessageInterval[i].KeyPress += TextBoxPeriodicMessageInterval_KeyPress;
                TextBoxPeriodicMessageInterval[i].Leave += TextBoxPeriodicMessageInterval_Leave;
            }
            InitializeTextBoxPeriodicMessageFlags();
            for (int i = 0; i < TextBoxPeriodicMessageFlags.Length; i++)
            {
                TextBoxPeriodicMessageFlags[i].TextChanged += TextBoxPeriodicMessageFlags_TextChanged;
                TextBoxPeriodicMessageFlags[i].KeyPress += TextBoxPeriodicMessageFlags_KeyPress;
                TextBoxPeriodicMessageFlags[i].Leave += TextBoxPeriodicMessageFlags_Leave;
            }
            InitializeTextBoxFunctionalMessage();
            for (int i = 0; i < TextBoxFunctionalMessage.Length; i++)
            {
                TextBoxFunctionalMessage[i].TextChanged += TextBoxFunctionalMessage_TextChanged;
                TextBoxFunctionalMessage[i].KeyPress += TextBoxFunctionalMessage_KeyPress;
                TextBoxFunctionalMessage[i].Leave += TextBoxFunctionalMessage_Leave;
            }
            InitializeTextBoxFilterPatt();
            for (int i = 0; i < TextBoxFilterPatt.Length; i++)
            {
                TextBoxFilterPatt[i].TextChanged += TextBoxFilterPatt_TextChanged;
                TextBoxFilterPatt[i].KeyPress += TextBoxFilterPatt_KeyPress;
                TextBoxFilterPatt[i].Leave += TextBoxFilterPatt_Leave;
            }
            InitializeTextBoxFilterMask();
            for (int i = 0; i < TextBoxFilterMask.Length; i++)
            {
                TextBoxFilterMask[i].TextChanged += TextBoxFilterMask_TextChanged;
                TextBoxFilterMask[i].KeyPress += TextBoxFilterMask_KeyPress;
                TextBoxFilterMask[i].Leave += TextBoxFilterMask_Leave;
            }
            InitializeTextBoxFilterFlow();
            for (int i = 0; i < TextBoxFilterFlow.Length; i++)
            {
                TextBoxFilterFlow[i].TextChanged += TextBoxFilterFlow_TextChanged;
                TextBoxFilterFlow[i].KeyPress += TextBoxFilterFlow_KeyPress;
                TextBoxFilterFlow[i].Leave += TextBoxFilterFlow_Leave;
            }
            InitializeTextBoxFilterFlags();
            for (int i = 0; i < TextBoxFilterFlags.Length; i++)
            {
                TextBoxFilterFlags[i].TextChanged += TextBoxFilterFlags_TextChanged;
                TextBoxFilterFlags[i].KeyPress += TextBoxFilterFlags_KeyPress;
                TextBoxFilterFlags[i].Leave += TextBoxFilterFlags_Leave;
            }
            InitializeProgressBarAnalog();
            InitializeRadioButtonPin();
            for (int i = 0; i < RadioButtonPin.Length; i++)
            {
                if (RadioButtonPin[i] != null)
                {
                    RadioButtonPin[i].CheckedChanged += RadioButtonPin_CheckedChanged;
                }
            }
            InitializeLabelChannel();
            InitializeLabelPin();
            InitializeLabelParameterName();
            InitializeLabelParamVal();
            InitializeLabelPeriodicMessageId();
            InitializeLabelPeriodicMessage();
            InitializeLabelFunctionalMessage();
            InitializeLabelFuncId();
            InitializeLabelFilterId();
            InitializeLabelFilter();
            InitializeLabelDeviceCombo();
            InitializeLabelAnalogRead();
            InitializeLabelAnalogCH();
            InitializeCheckBoxPeriodicMessageEnable();
            for (int i = 0; i < CheckBoxPeriodicMessageEnable.Length; i++)
            {
                CheckBoxPeriodicMessageEnable[i].Click += CheckBoxPeriodicMessageEnable_Click;
            }
            InitializeCheckBoxFunctionalMessageDelete();
            for (int i = 0; i < CheckBoxFunctionalMessageDelete.Length; i++)
            {
                CheckBoxFunctionalMessageDelete[i].Click += CheckBoxFunctionalMessageDelete_Click;
            }
            InitializeComboBoxPeriodicMessageChannel();
            for (int i = 0; i < ComboBoxPeriodicMessageChannel.Length; i++)
            {
                ComboBoxPeriodicMessageChannel[i].SelectionChangeCommitted += ComboBoxPeriodicMessageChannel_SelectionChangeCommitted;
                ComboBoxPeriodicMessageChannel[i].Leave += ComboBoxPeriodicMessageChannel_Leave;
            }
            InitializeComboBoxFilterType();
            for (int i = 0; i < ComboBoxFilterType.Length; i++)
            {
                ComboBoxFilterType[i].SelectionChangeCommitted += ComboBoxFilterType_SelectionChangeCommitted;
            }
            InitializeCheckBoxCH();
            for (int i = 0; i < CheckBoxCH.Length; i++)
            {
                CheckBoxCH[i].Click += CheckBoxCH_Click;
            }
        }

    }
}