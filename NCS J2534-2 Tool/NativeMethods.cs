﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace NCS_J2534_2_Tool
{
    public sealed class NativeMethods
    {
        [DllImport("kernel32.dll", CharSet=CharSet.Unicode)]
        internal extern static IntPtr LoadLibrary(string libName);

        [DllImport("kernel32.dll")]
        internal extern static int FreeLibrary(IntPtr hModule);

        [DllImport("kernel32.dll", CharSet=CharSet.Ansi, BestFitMapping=false, ThrowOnUnmappableChar=true)]
        internal extern static IntPtr GetProcAddress(IntPtr hModule, string lpProcName);

        [DllImport("User32.dll", CharSet=CharSet.Unicode, SetLastError=true)]
        internal extern static Int32 SendMessage(IntPtr hWnd, UInt32 msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet=CharSet.Unicode, SetLastError=true)]
        internal extern static IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("user32.dll", CharSet=CharSet.Auto, BestFitMapping=false, ThrowOnUnmappableChar=true)]
        internal extern static Int32 GetClassName(IntPtr hWnd, System.Text.StringBuilder lpClassName, Int32 nMaxCount);

        private NativeMethods()
        {
        }
    }

}