﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    public partial class FormConnectFlags
    {
        public FormConnectFlags()
        {
            InitializeComponent();
        }

        private uint localFlags;

        protected override void OnLoad(EventArgs e)
        {
            SetControlArrays();
            SetCheckBoxCaptions();
            TextBoxFlags.Text = J2534Code.FlagsToText(localFlags);
            SetCheckBoxes();
            LabelDesc.Text = string.Empty;
            base.OnLoad(e);
        }

        private void SetCheckBoxCaptions()
        {
            for (int i = 0; i <= 7; i++)
            {
                CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Reserved for SAE");
            }
            CheckBoxFlag[8].Text = "Bit 8 - CAN_29BIT_ID";
            CheckBoxFlag[8].Tag = string.Concat("0 = standard", Environment.NewLine,"1 = extended");
            CheckBoxFlag[9].Text = "Bit 9 - ISO9141_NO_CHECKSUM";
            CheckBoxFlag[9].Tag = "0 = generate checksum";
            CheckBoxFlag[10].Text = "Bit 10 - Reserved for SAE";
            CheckBoxFlag[11].Text = "Bit 11 - CAN_ID_BOTH";
            CheckBoxFlag[11].Tag = string.Concat("0 = determined by CAN_29BIT_ID", Environment.NewLine, "1 = Both standard and 29 bit");
            CheckBoxFlag[12].Text = "Bit 12 - ISO9141_K_LINE_ONLY";
            CheckBoxFlag[12].Tag = string.Concat("0 = use L and K-lines", Environment.NewLine, "1 = use K-line only");
            for (int i = 13; i <= 23; i++)
            {
                CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Reserved for SAE");
            }
            for (int i = 24; i <= 31; i++)
            {
                CheckBoxFlag[i].Text = string.Concat("Bit ", i, " - Tool mfg specific");
            }
        }

        private void CheckFlag_Click(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            SetTextBox();
        }

        private void CheckFlag_Enter(object sender, EventArgs e) //Event Handler added in SetControlArrays
        {
            int index = Array.IndexOf(this.CheckBoxFlag, sender);
            LabelDesc.Text = string.Concat(CheckBoxFlag[index].Text, Environment.NewLine, Convert.ToString(CheckBoxFlag[index].Tag));
        }

        private void SetCheckBoxes()
        {
            uint mask = 0;
            for (int i = 0; i <= 31; i++)
            {
                mask = 1U << i;
                CheckBoxFlag[i].Checked = (localFlags & mask) != 0;
            }
        }

        private void SetTextBox()
        {
            uint mask = 0;
            for (int i = 0; i <= 31; i++)
            {
                mask = 1U << i;
                if (CheckBoxFlag[i].Checked)
                {
                    localFlags |= mask;
                }
                else
                {
                    localFlags &= ~mask;
                }
            }
            TextBoxFlags.Text = J2534Code.FlagsToText(localFlags);
        }

        private void TextBoxFlags_TextChanged(object sender, EventArgs e)
        {
            if (!IsHandleCreated)
            {
                return;
            }
            localFlags = J2534Code.TextToFlags(TextBoxFlags.Text);
            SetCheckBoxes();
        }

        private void TextBoxFlags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void SetControlArrays()
        {
            InitializeCheckBoxFlag();
            for (int i = 0; i < CheckBoxFlag.Length; i++)
            {
                CheckBoxFlag[i].Click += CheckFlag_Click;
                CheckBoxFlag[i].Enter += CheckFlag_Enter;
            }
        }

        public DialogResult ShowDialog(ref uint flags)
        {
            DialogResult result = 0;
            localFlags = flags;
            result = this.ShowDialog();
            if (result == DialogResult.OK)
            {
                flags = localFlags;
            }
            return result;
        }

    }
}