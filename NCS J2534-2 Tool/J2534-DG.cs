﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    public enum LINProtocolId: uint
    {
        LIN = 0x10101
    }

    public enum LINConfigParameterId: uint
    {
        LIN_VERSION = 0x10001
    }

    public enum LINIoCtlID: uint
    {
        CAN_SET_BTR = 0x10100,
        GET_TIMESTAMP = 0x10101,
        GET_SERIAL_NUMBER = 0x10103,
        CAN_SET_ERROR_REPORTING = 0x10105,
        CAN_SET_INTERNAL_TERMINATION = 0x10107,
        DEVICE_RESET = 0x10200,

        LIN_SENDWAKEUP = 0, //unknown?
        LIN_ADD_SCHED = 0,
        LIN_GET_SCHED = 0,
        LIN_GET_SCHED_SIZE = 0,
        LIN_DEL_SCHED = 0,
        LIN_ACT_SCHED = 0,
        LIN_DEACT_SCHED = 0,
        LIN_GET_ACT_SCHED = 0,
        LIN_GET_NUM_SCHEDS = 0,
        LIN_GET_SCHED_NAMES = 0,
        LIN_SET_FLAGS = 0
    }
}