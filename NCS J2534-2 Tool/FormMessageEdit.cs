﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace NCS_J2534_2_Tool
{
    public partial class FormMessageEdit
    {
        public FormMessageEdit()
        {
            InitializeComponent();
        }

        private J2534MessageText localMessageText;
        private J2534MessageText referenceMessageText;
        private J2534Device localDevice;
        private bool settingComboProt;
        private bool updatingMsgEdit;

        protected override void OnLoad(EventArgs e)
        {
            settingComboProt = true;
            if (localDevice != null)
            {
                ComboBoxProtocol.Items.AddRange(localDevice.ChannelSet);
            }
            settingComboProt = false;
            UpdateMsgEdit();
            base.OnLoad(e);
        }

        private void ComboBoxProtocol_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (settingComboProt)
            {
                return;
            }
            SetEditMsg();
            UpdateMsgEdit();
        }

        private void TextBoxComment_TextChanged(object sender, EventArgs e)
        {
            if (!IsHandleCreated)
            {
                return;
            }
            if (!updatingMsgEdit)
            {
                localMessageText.Comment = TextBoxComment.Text;
                UpdateButtons();
            }
        }

        private void TextBoxComment_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == KeyChars.Cr)
            {
                localMessageText.Comment = TextBoxComment.Text;
                UpdateButtons();
            }
        }

        private void TextBoxComment_Leave(object sender, EventArgs e)
        {
            localMessageText.Comment = TextBoxComment.Text;
            UpdateButtons();
        }

        private void TextBoxData_TextChanged(object sender, EventArgs e)
        {
            if (!IsHandleCreated)
            {
                return;
            }
            if (!updatingMsgEdit)
            {
                SetEditMsg();
                UpdateMsgEdit();
            }
        }

        private void TextBoxData_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case KeyChars.Cr:
                    SetEditMsg();
                    UpdateMsgEdit();
                    break;
                case KeyChars.Back:
                case KeyChars.Copy:
                case KeyChars.Paste:
                case KeyChars.Cut:
                case KeyChars.Undo:

                break;
                default:
                    if (!e.KeyChar.IsHex())
                    {
                        e.Handled = true;
                    }
                    break;
            }
        }

        private void TextBoxData_Leave(object sender, EventArgs e)
        {
            SetEditMsg();
            UpdateMsgEdit();
        }

        private void TextBoxFlags_TextChanged(object sender, EventArgs e)
        {
            if (!IsHandleCreated)
            {
                return;
            }
            if (!IsHandleCreated)
            {
                return;
            }
            if (!updatingMsgEdit)
            {
                SetEditMsg();
                UpdateMsgEdit();
            }
        }

        private void TextBoxFlags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(sender is TextBox))
            {
                return;
            }
            if (e.KeyChar == KeyChars.Cr)
            {
                SetEditMsg();
                UpdateMsgEdit();
                e.Handled = true;
            }
            else
            {
                J2534Code.FlagsKeyPress(sender, e);
            }
        }

        private void SetEditMsg()
        {
            localMessageText.Data = TextBoxData.Text;
            localMessageText.ProtocolName = ComboBoxProtocol.Text;
            localMessageText.TxFlags = TextBoxFlags.Text;
        }

        private void UpdateMsgEdit()
        {
            updatingMsgEdit = true;
            if (!TextBoxData.Focused)
            {
                TextBoxData.Text = localMessageText.Data;
                TextBoxData.SelectionStart = TextBoxData.Text.Length;
            }
            if (!TextBoxFlags.Focused)
            {
                TextBoxFlags.Text = localMessageText.TxFlags;
                TextBoxFlags.SelectionStart = TextBoxFlags.Text.Length;
            }
            if (localMessageText.ProtocolName.Length > 0)
            {
                ComboBoxProtocol.Text = localMessageText.ProtocolName;
            }
            if (!TextBoxComment.Focused)
            {
                TextBoxComment.Text = localMessageText.Comment;
                TextBoxComment.SelectionStart = TextBoxComment.Text.Length;
            }
            updatingMsgEdit = false;
            UpdateButtons();
        }

        private void UpdateButtons()
        {
            bool changesExist = false;
            if (referenceMessageText.ProtocolName != localMessageText.ProtocolName)
            {
                changesExist = true;
            }
            if (referenceMessageText.TxFlags != localMessageText.TxFlags)
            {
                changesExist = true;
            }
            if (referenceMessageText.Data != localMessageText.Data)
            {
                changesExist = true;
            }
            if (referenceMessageText.Comment != localMessageText.Comment)
            {
                changesExist = true;
            }
            ButtonOK.Enabled = changesExist;
            ButtonCancel.Enabled = true;
        }

        private void TextFlags_Leave(object sender, EventArgs e)
        {
            SetEditMsg();
            UpdateMsgEdit();
        }

        private void MenuFlagClear_Click(object sender, EventArgs e)
        {
            TextBoxFlags.Text = "0x00000000";
        }

        private void MenuFlagDefault_Click(object sender, EventArgs e)
        {
            if (!(ComboBoxProtocol.SelectedItem is J2534Channel channel))
            {
                return;
            }
            TextBoxFlags.Text = J2534Code.FlagsToText(channel.DefaultTxFlags);
        }

        private void MenuFlagEdit_Click(object sender, EventArgs e)
        {
            uint flags = J2534Code.TextToFlags(TextBoxFlags.Text);
            if (!(ComboBoxProtocol.SelectedItem is J2534Channel channel))
            {
                return;
            }
            if (FormTxFlags.DefaultInstance.ShowDialog(ref flags, channel) == DialogResult.OK)
            {
                TextBoxFlags.Text = J2534Code.FlagsToText(flags);
            }
        }

        private void MenuFlagsCopy_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Copy();
            }
        }

        private void MenuFlagsCut_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Cut();
            }
        }

        private void MenuFlagsPaste_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Paste();
            }
        }

        private void MenuFlagsSelectAll_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).SelectAll();
            }
        }

        private void MenuFlagsUndo_Click(object sender, EventArgs e)
        {
            if (!(((ToolStripMenuItem)sender).Owner is ContextMenuStrip thisMenuStrip))
            {
                return;
            }
            if (thisMenuStrip.SourceControl is TextBox)
            {
                ((TextBox)thisMenuStrip.SourceControl).Undo();
            }
        }

        private void ContextMenuStripFlags_Opening(object sender, CancelEventArgs e)
        {
            if (ContextMenuStripFlags.SourceControl is TextBox)
            {
                if (!(ComboBoxProtocol.SelectedItem is J2534Channel channel))
                {
                    return;
                }
                TextBox thisTextBox = (TextBox)ContextMenuStripFlags.SourceControl;
                if (thisTextBox.Enabled)
                {
                    thisTextBox.Focus();
                    int selectedLength = thisTextBox.SelectionLength;
                    int textLength = thisTextBox.Text.Length;
                    MenuFlagsPaste.Enabled = Clipboard.ContainsData(DataFormats.Text);
                    MenuFlagsCut.Enabled = selectedLength > 0;
                    MenuFlagsCopy.Enabled = selectedLength > 0;
                    MenuFlagsUndo.Enabled = thisTextBox.CanUndo;
                    MenuFlagsSelectAll.Enabled = (selectedLength < textLength);
                    MenuFlagsSpace3.Available = thisTextBox == TextBoxFlags;
                    MenuFlagsDefault.Available = thisTextBox == TextBoxFlags && channel.DefaultTxFlags != 0;
                    MenuFlagsClear.Available = thisTextBox == TextBoxFlags;
                    MenuFlagsEdit.Available = thisTextBox == TextBoxFlags;
                }
            }
        }

        public DialogResult ShowDialog(J2534MessageText messageText, J2534Device device)
        {
            DialogResult result = 0;
            if (device == null || messageText == null)
            {
                return result;
            }
            localMessageText = messageText;
            referenceMessageText = messageText.Clone();
            localDevice = device;
            result = this.ShowDialog();
            return result;
        }

    }
}