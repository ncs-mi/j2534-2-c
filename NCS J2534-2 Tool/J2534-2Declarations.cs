﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace NCS_J2534_2_Tool
{
    public enum J2534ProtocolId : uint
    {
        J1850VPW = 1,
        J1850PWM = 2,
        ISO9141 = 3,
        ISO14230 = 4,
        CAN = 5,
        ISO15765 = 6,
        SCI_A_ENGINE = 7,
        SCI_A_TRANS = 8,
        SCI_B_ENGINE = 9,
        SCI_B_TRANS = 10,

        //J2534-2 Pin Switched Protocol IDs
        J1850VPW_PS = 0x8000,
        J1850PWM_PS = 0x8001,
        ISO9141_PS = 0x8002,
        ISO14230_PS = 0x8003,
        CAN_PS = 0x8004,
        ISO15765_PS = 0x8005,
        J2610_PS = 0x8006,
        SW_ISO15765_PS = 0x8007,
        SW_CAN_PS = 0x8008,
        GM_UART_PS = 0x8009,
        UART_ECHO_BYTE_PS = 0x800A,
        HONDA_DIAGH_PS = 0x800B,
        J1939_PS = 0x800C,
        J1708_PS = 0x800D,
        TP2_0_PS = 0x800E,
        FT_CAN_PS = 0x800F,
        FT_ISO15765_PS = 0x8010,

        CAN_CH1 = 0x9000,
        CAN_CH128 = 0x907F,
        J1850VPW_CH1 = 0x9080,
        J1850VPW_CH128 = 0x90FF,
        J1850PWM_CH1 = 0x9100,
        J1850PWM_CH128 = 0x917F,
        ISO9141_CH1 = 0x9180,
        ISO9141_CH128 = 0x91FF,
        ISO14230_CH1 = 0x9200,
        ISO14230_CH128 = 0x927F,
        ISO15765_CH1 = 0x9280,
        ISO15765_CH128 = 0x92FF,
        SW_CAN_CAN_CH1 = 0x9300,
        SW_CAN_CAN_CH128 = 0x937F,
        SW_CAN_ISO15765_CH1 = 0x9380,
        SW_CAN_ISO15765_CH128 = 0x93FF,
        J2610_CH1 = 0x9400,
        J2610_CH128 = 0x947F,
        FT_CAN_CH1 = 0x9480,
        FT_CAN_CH128 = 0x94FF,
        FT_ISO15765_CH1 = 0x9500,
        FT_ISO15765_CH128 = 0x957F,
        GM_UART_CH1 = 0x9580,
        GM_UART_CH128 = 0x95FF,
        ECHO_BYTE_CH1 = 0x9600,
        ECHO_BYTE_CH128 = 0x967F,
        HONDA_DIAGH_CH1 = 0x9680,
        HONDA_DIAGH_CH128 = 0x96FF,
        J1939_CH1 = 0x9700,
        J1939_CH128 = 0x977F,
        J1708_CH1 = 0x9780,
        J1708_CH128 = 0x97FF,
        TP2_0_CH1 = 0x9800,
        TP2_0_CH128 = 0x987F,

        ANALOG_IN_1 = 0xC000,
        ANALOG_IN_32 = 0xC01F,
        LIN = LINProtocolId.LIN
    }

    public enum J2534IoCtlId : uint
    {
        //IOCTL IDs
        GET_CONFIG = 0x1,
        SET_CONFIG = 0x2,
        READ_VBATT = 0x3,
        FIVE_BAUD_INIT = 0x4,
        FAST_INIT = 0x5,
        CLEAR_TX_BUFFER = 0x7,
        CLEAR_RX_BUFFER = 0x8,
        CLEAR_PERIODIC_MSGS = 0x9,
        CLEAR_MSG_FILTERS = 0xA,
        CLEAR_FUNCT_MSG_LOOKUP_TABLE = 0xB,
        ADD_TO_FUNCT_MSG_LOOKUP_TABLE = 0xC,
        DELETE_FROM_FUNCT_MSG_LOOKUP_TABLE = 0xD,
        READ_PROG_VOLTAGE = 0xE,

        SW_CAN_HS = 0x8000,
        SW_CAN_NS = 0x8001,
        SET_POLL_RESPONSE = 0x8002,
        BECOME_MASTER = 0x8003,
        START_REPEAT_MESSAGE = 0x8004,
        QUERY_REPEAT_MESSAGE = 0x8005,
        STOP_REPEAT_MESSAGE = 0x8006,
        GET_DEVICE_CONFIG = 0x8007,
        SET_DEVICE_CONFIG = 0x8008,
        PROTECT_J1939_ADDR = 0x8009,

        REQUEST_CONNECTION = 0x800A,
        TEARDOWN_CONNECTION = 0x800B,

        GET_DEVICE_INFO = 0x800C,
        GET_PROTOCOL_INFO = 0x800D
    }
    public enum J2534ConfigParameterId : uint
    {

        DATA_RATE = 0x1,
        LOOPBACK = 0x3,
        NODE_ADDRESS = 0x4,
        NETWORK_LINE = 0x5,
        P1_MIN = 0x6,
        P1_MAX = 0x7,
        P2_MIN = 0x8,
        P2_MAX = 0x9,
        P3_MIN = 0xA,
        P3_MAX = 0xB,
        P4_MIN = 0xC,
        P4_MAX = 0xD,
        W1 = 0xE,
        W2 = 0xF,
        W3 = 0x10,
        W4 = 0x11,
        W5 = 0x12,
        TIDLE = 0x13,
        TINIL = 0x14,
        TWUP = 0x15,
        PARITY = 0x16,
        BIT_SAMPLE_POINT = 0x17,
        SYNC_JUMP_WIDTH = 0x18,
        W0 = 0x19,
        T1_MAX = 0x1A,
        T2_MAX = 0x1B,

        T4_MAX = 0x1C,
        T5_MAX = 0x1D,
        ISO15765_BS = 0x1E,
        ISO15765_STMIN = 0x1F,
        //----------------------------
        DATA_BITS = 0x20,
        FIVE_BAUD_MOD = 0x21,
        BS_TX = 0x22,
        STMIN_TX = 0x23,
        T3_MAX = 0x24,
        ISO15765_WFT_MAX = 0x25,

        CAN_MIXED_FORMAT = 0x8000,
        SW_CAN_HS_DATA_RATE = 0x8010,
        SW_CAN_SPEEDCHANGE_ENABLE = 0x8011,
        SW_CAN_RES_SWITCH = 0x8012,
        ACTIVE_CHANNELS = 0x8020,
        SAMPLE_RATE = 0x8021,
        SAMPLES_PER_READING = 0x8022,
        READINGS_PER_MSG = 0x8023,
        AVERAGING_METHOD = 0x8024,
        SAMPLE_RESOLUTION = 0x8025,
        INPUT_RANGE_LOW = 0x8026,
        INPUT_RANGE_HIGH = 0x8027,

        UEB_T0_MIN = 0x8028,
        UEB_T1_MAX = 0x8029,
        UEB_T2_MAX = 0x802A,
        UEB_T3_MAX = 0x802B,
        UEB_T4_MIN = 0x802C,
        UEB_T5_MAX = 0x802D,
        UEB_T6_MAX = 0x802E,
        UEB_T7_MIN = 0x802F,
        UEB_T7_MAX = 0x8030,
        UEB_T9_MIN = 0x8031,

        J1962_PINS = 0x8001, //OBD-II
        J1939_PINS = 0x803D, //Newer Heavy Truck
        J1708_PINS = 0x803E, //Older Heavy Truck

        J1939_T1 = 0x803F,
        J1939_T2 = 0x8040,
        J1939_T3 = 0x8041,
        J1939_T4 = 0x8042,
        J1939_BRDCST_MIN_DELAY = 0x8043,

        TP2_0_T_BR_INT = 0x8044,
        TP2_0_T_E = 0x8045,
        TP2_0_MNTC = 0x8046,
        TP2_0_T_CTA = 0x8047,
        TP2_0_MNCT = 0x8048,
        TP2_0_MNTB = 0x8049,
        TP2_0_MNT = 0x804A,
        TP2_0_T_WAIT = 0x804B,
        TP2_0_T1 = 0x804C,
        TP2_0_T3 = 0x804D,
        TP2_0_IDENTIFER = 0x804E,
        TP2_0_RXIDPASSIVE = 0x804F,

        NON_VOLATILE_STORE_1 = 0xC001,
        NON_VOLATILE_STORE_2 = 0xC002,
        NON_VOLATILE_STORE_3 = 0xC003,
        NON_VOLATILE_STORE_4 = 0xC004,
        NON_VOLATILE_STORE_5 = 0xC005,
        NON_VOLATILE_STORE_6 = 0xC006,
        NON_VOLATILE_STORE_7 = 0xC007,
        NON_VOLATILE_STORE_8 = 0xC008,
        NON_VOLATILE_STORE_9 = 0xC009,
        NON_VOLATILE_STORE_10 = 0xC00A,

        LIN_VERSION = LINConfigParameterId.LIN_VERSION
    }

    public enum J2534GetDeviceInfoParameter : uint
    {
        SERIAL_NUMBER = 0x1,
        J1850PWM_SUPPORTED = 0x2,
        J1850VPW_SUPPORTED = 0x3,
        ISO9141_SUPPORTED = 0x4,
        ISO14230_SUPPORTED = 0x5,
        CAN_SUPPORTED = 0x6,
        ISO15765_SUPPORTED = 0x7,
        SCI_A_ENGINE_SUPPORTED = 0x8,
        SCI_A_TRANS_SUPPORTED = 0x9,
        SCI_B_ENGINE_SUPPORTED = 0xA,
        SCI_B_TRANS_SUPPORTED = 0xB,
        SW_ISO15765_SUPPORTED = 0xC,
        SW_CAN_SUPPORTED = 0xD,
        GM_UART_SUPPORTED = 0xE,
        UART_ECHO_BYTE_SUPPORTED = 0xF,
        HONDA_DIAGH_SUPPORTED = 0x10,
        J1939_SUPPORTED = 0x11,
        J1708_SUPPORTED = 0x12,
        TP2_0_SUPPORTED = 0x13,
        J2610_SUPPORTED = 0x14,
        ANALOG_IN_SUPPORTED = 0x15,
        MAX_NON_VOLATILE_STORAGE = 0x16,
        SHORT_TO_GND_J1962 = 0x17,
        PGM_VOLTAGE_J1962 = 0x18,
        J1850PWM_PS_J1962 = 0x19,
        J1850VPW_PS_J1962 = 0x1A,
        ISO9141_PS_K_LINE_J1962 = 0x1B,
        ISO14230_PS_K_LINE_J1962 = 0x1C,
        ISO9141_PS_L_LINE_J1962 = 0x1D,
        ISO14230_PS_L_LINE_J1962 = 0x1E,
        CAN_PS_J1962 = 0x1F,
        ISO15765_PS_J1962 = 0x20,
        SW_CAN_PS_J1962 = 0x21,
        SW_ISO15765_PS_J1962 = 0x22,
        GM_UART_PS_J1962 = 0x23,
        UART_ECHO_BYTE_PS_J1962 = 0x24,
        HONDA_DIAGH_PS_J1962 = 0x25,
        J1939_PS_J1962 = 0x26,
        J1708_PS_J1962 = 0x27,
        TP2_0_PS_J1962 = 0x28,
        J2610_PS_J1962 = 0x29,
        J1939_PS_J1939 = 0x2A,
        J1708_PS_J1939 = 0x2B,
        ISO9141_PS_K_LINE_J1939 = 0x2C,
        ISO9141_PS_L_LINE_J1939 = 0x2D,
        ISO14230_PS_K_LINE_J1939 = 0x2E,
        ISO14230_PS_L_LINE_J1939 = 0x2F,
        J1708_PS_J1708 = 0x30,
        FT_CAN_SUPPORTED = 0x31,
        FT_ISO15765_SUPPORTED = 0x32,
        FT_CAN_PS_J1962 = 0x33,
        FT_ISO15765_PS_J1962 = 0x34,
        J1850PWM_SIMULTANEOUS = 0x35,
        J1850VPW_SIMULTANEOUS = 0x36,
        ISO9141_SIMULTANEOUS = 0x37,
        ISO14230_SIMULTANEOUS = 0x38,
        CAN_SIMULTANEOUS = 0x39,
        ISO15765_SIMULTANEOUS = 0x3A,
        SCI_A_ENGINE_SIMULTANEOUS = 0x3B,
        SCI_A_TRANS_SIMULTANEOUS = 0x3C,
        SCI_B_ENGINE_SIMULTANEOUS = 0x3D,
        SCI_B_TRANS_SIMULTANEOUS = 0x3E,
        SW_ISO15765_SIMULTANEOUS = 0x3F,
        SW_CAN_SIMULTANEOUS = 0x40,
        GM_UART_SIMULTANEOUS = 0x41,
        UART_ECHO_BYTE_SIMULTANEOUS = 0x42,
        HONDA_DIAGH_SIMULTANEOUS = 0x43,
        J1939_SIMULTANEOUS = 0x44,
        J1708_SIMULTANEOUS = 0x45,
        TP2_0_SIMULTANEOUS = 0x46,
        J2610_SIMULTANEOUS = 0x47,
        ANALOG_IN_SIMULTANEOUS = 0x48,
        PART_NUMBER = 0x49,
        FT_CAN_SIMULTANEOUS = 0x4A,
        FT_ISO15765_SIMULTANEOUS = 0x4B,
    }

    public enum J2534GetProtocolInfoParameter : uint
    {
        MAX_RX_BUFFER_SIZE = 0x1,
        MAX_PASS_FILTER = 0x2,
        MAX_BLOCK_FILTER = 0x3,
        MAX_FILTER_MSG_LENGTH = 0x4,
        MAX_PERIODIC_MSGS = 0x5,
        MAX_PERIODIC_MSG_LENGTH = 0x6,
        DESIRED_DATA_RATE = 0x7,
        MAX_REPEAT_MESSAGING = 0x8,
        MAX_REPEAT_MESSAGING_LENGTH = 0x9,
        NETWORK_LINE_SUPPORTED = 0xA,
        MAX_FUNCT_MSG_LOOKUP = 0xB,
        PARITY_SUPPORTED = 0xC,
        DATA_BITS_SUPPORTED = 0xD,
        FIVE_BAUD_MOD_SUPPORTED = 0xE,
        L_LINE_SUPPORTED = 0xF,
        CAN_11_29_IDS_SUPPORTED = 0x10,
        CAN_MIXED_FORMAT_SUPPORTED = 0x11,
        MAX_FLOW_CONTROL_FILTER = 0x12,
        MAX_ISO15765_WFT_MAX = 0x13,
        MAX_AD_ACTIVE_CHANNELS = 0x14,
        MAX_AD_SAMPLE_RATE = 0x15,
        MAX_AD_SAMPLES_PER_READING = 0x16,
        AD_SAMPLE_RESOLUTION = 0x17,
        AD_INPUT_RANGE_LOW = 0x18,
        AD_INPUT_RANGE_HIGH = 0x19,
        RESOURCE_GROUP = 0x1A,
        TIMESTAMP_RESOLUTION = 0x1B,
    }

    public enum J2534Result : int
    {
        STATUS_NOERROR = 0x0,
        ERR_NOT_SUPPORTED = 0x1,
        ERR_INVALID_CHANNEL_ID = 0x2,
        ERR_INVALID_PROTOCOL_ID = 0x3,
        ERR_NULL_PARAMETER = 0x4,
        ERR_INVALID_IOCTL_VALUE = 0x5,
        ERR_INVALID_FLAGS = 0x6,
        ERR_FAILED = 0x7,
        ERR_DEVICE_NOT_CONNECTED = 0x8,
        ERR_TIMEOUT = 0x9,
        ERR_INVALID_MSG = 0xA,
        ERR_INVALID_TIME_INTERVAL = 0xB,
        ERR_EXCEEDED_LIMIT = 0xC,
        ERR_INVALID_MSG_ID = 0xD,
        ERR_DEVICE_IN_USE = 0xE,
        ERR_INVALID_IOCTL_ID = 0xF,
        ERR_BUFFER_EMPTY = 0x10,
        ERR_BUFFER_FULL = 0x11,
        ERR_BUFFER_OVERFLOW = 0x12,
        ERR_PIN_INVALID = 0x13,
        ERR_CHANNEL_IN_USE = 0x14,
        ERR_MSG_PROTOCOL_ID = 0x15,
        ERR_INVALID_FILTER_ID = 0x16,
        ERR_NO_FLOW_CONTROL = 0x17,
        ERR_NOT_UNIQUE = 0x18,
        ERR_INVALID_BAUDRATE = 0x19,
        ERR_INVALID_DEVICE_ID = 0x1A,

        ERR_INVALID_IOCTL_PARAM_ID = 0x1E,
        ERR_VOLTAGE_IN_USE = 0x1F,
        ERR_PIN_IN_USE = 0x20,

        ERR_ADDRESS_NOT_CLAIMED = 0x10000,
        ERR_NO_CONNECTION_ESTABLISHED = 0x10001,
        ERR_RESOURCE_IN_USE = 0x10002
    }

    public enum J2534VoltageValue : uint
    {
        SHORT_TO_GROUND = 0xFFFFFFFEU,
        VOLTAGE_OFF = 0xFFFFFFFFU
    }

    public enum J2534Parity : uint
    {
        NO_PARITY = 0,
        ODD_PARITY = 1,
        EVEN_PARITY = 2,
    }

    public enum J2534MixedMode : uint
    {
        CAN_MIXED_FORMAT_OFF = 0,
        CAN_MIXED_FORMAT_ON = 1,
        CAN_MIXED_FORMAT_ALL_FRAMES = 2 // Not supported
    }

    public enum J2534DiscoveryResult : uint
    {
        //J2534-2 Discovery
        NOT_SUPPORTED = 0,
        SUPPORTED = 1
    }

    public enum J2534AnalogAveraging : uint
    {
        SIMPLE_AVERAGE = 0x0, // Simple arithmetic mean
        MAX_LIMIT_AVERAGE = 0x1, // Choose the biggest value
        MIN_LIMIT_AVERAGE = 0x2, // Choose the lowest value
        MEDIAN_AVERAGE = 0x3 // Choose the arithmetic median
    }

    [Flags]
    public enum J2534ConnectFlags : uint
    {
        CAN_29BIT_ID = 0x100,
        ISO9141_NO_CHECKSUM = 0x200,
        NO_CHECKSUM = 0x200,
        CHECKSUM_DISABLED = 0x200,
        CAN_ID_BOTH = 0x800,
        ISO9141_K_LINE_ONLY = 0x1000
    }

    [Flags]
    public enum J2534RxStatusFlags : uint
    {
        TX_MSG_TYPE = 0x1,
        START_OF_MESSAGE = 0x2,
        RX_BREAK = 0x4,
        TX_INDICATION = 0x8,
        ISO15765_PADDING_ERROR = 0x10,
        ISO15765_ADDR_TYPE = 0x80,
        CAN_29BIT_ID = 0x100,
        SW_CAN_HV_RX = 0x10000,
        SW_CAN_HS_RX = 0x20000,
        SW_CAN_NS_RX = 0x40000,
        OVERFLOW = 0x10000,
        CONNECTION_ESTABLISHED = 0x10000,
        CONNECTION_LOST = 0x20000
    }

    [Flags]
    public enum J2534TxFlags : uint
    {
        ISO15765_FRAME_PAD = 0x40,
        ISO15765_ADDR_TYPE = 0x80,
        CAN_29BIT_ID = 0x100,
        WAIT_P3_MIN_ONLY = 0x200,
        SW_CAN_HV_TX = 0x400,
        SCI_MODE = 0x400000,
        SCI_TX_VOLTAGE = 0x800000
    }

    public enum J2534FilterType : uint
    { 
        NO_FILTER = 0,
        PASS_FILTER = 1,
        BLOCK_FILTER = 2,
        FLOW_CONTROL_FILTER = 3
    }

    [StructLayout(LayoutKind.Sequential, Pack=1)]
    public struct PASSTHRU_MSG
    {
        public J2534ProtocolId ProtocolId;
        public J2534RxStatusFlags RxStatus;
        public J2534TxFlags TxFlags;
        public UInt32 Timestamp;
        public UInt32 DataSize;
        public UInt32 ExtraDataIndex;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst=4128)]
        public byte[] Data;

        public static PASSTHRU_MSG CreateInstance()
        {
            PASSTHRU_MSG result = new PASSTHRU_MSG
            {
                Data = new byte[4128]
            };
            Array.Clear(result.Data, 0, 4128);
            return result;
        }

        public PASSTHRU_MSG Clone()
        {
            PASSTHRU_MSG result = this;
            if (Data == null || Data.Length != 4128)
            {
                Data = new byte[4128];
            }
            Array.Copy(this.Data, result.Data, 4128);
            return result;
        }

        public void Clear()
        {
            ProtocolId = 0;
            RxStatus = 0;
            TxFlags = 0;
            Timestamp = 0;
            DataSize = 0;
            ExtraDataIndex = 0;
            if (Data == null || Data.Length != 4128)
            {
                Data = new byte[4128];
            }
            Array.Clear(Data, 0, 4128);
        }

    } 

    [StructLayout(LayoutKind.Sequential)]
    public struct SCONFIG
    {
        public UInt32 Parameter;
        public UInt32 Value;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SCONFIG_LIST
    {
        public UInt32 NumOfParams;
        public Int32 ConfigPtr;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SBYTE_ARRAY
    {
        public UInt32 NumOfBytes;
        public Int32 BytePtr;
    }

    //This is a plain J2534-2 SPARAM. see J2534Misc.SPARAM for a better version
    //[StructLayout(LayoutKind.Sequential)]
    //public struct SPARAM2
    //{
    //    public UInt32 Parameter;
    //    public UInt32 Value;
    //    public UInt32 Supported;
    //}

    [StructLayout(LayoutKind.Sequential)]
    public struct SPARAM_LIST
    {
        public UInt32 NumOfParams;
        public Int32 ParamPtr;
    }






}