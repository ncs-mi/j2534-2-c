﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace NCS_J2534_2_Tool
{
    public partial class FormInputBox
    {
        public FormInputBox()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            TextBoxInput.Select();
            TextBoxInput.SelectAll();
            base.OnLoad(e);
        }

        private void ContextMenuStripTextBox_Opening(object sender, CancelEventArgs e)
        {
            int selectedLength = TextBoxInput.SelectionLength;
            int textLength = TextBoxInput.TextLength;
            MenuTextBoxPaste.Enabled = Clipboard.ContainsText();
            MenuTextBoxCut.Enabled = selectedLength > 0;
            MenuTextBoxCopy.Enabled = MenuTextBoxCut.Enabled;
            MenuTextBoxDelete.Enabled = MenuTextBoxCut.Enabled;
            MenuTextBoxUndo.Enabled = TextBoxInput.CanUndo;
            MenuTextBoxSelectAll.Enabled = selectedLength < textLength;
        }

        private void MenuTextBoxCopy_Click(object sender, EventArgs e)
        {
            TextBoxInput.Copy();
        }

        private void MenuTextBoxCut_Click(object sender, EventArgs e)
        {
            TextBoxInput.Cut();
        }

        private void MenuTextBoxDelete_Click(object sender, EventArgs e)
        {
            TextBoxInput.Delete();
        }

        private void MenuTextBoxPaste_Click(object sender, EventArgs e)
        {
            TextBoxInput.Paste();
        }

        private void MenuTextBoxSelectAll_Click(object sender, EventArgs e)
        {
            TextBoxInput.SelectAll();
        }

        private void MenuTextBoxUndo_Click(object sender, EventArgs e)
        {
            TextBoxInput.Undo();
        }

        public InputBoxResponse ShowDialog(string prompt, string title, string defaultResponse, int positionX, int positionY)
        {
            InputBoxResponse result = new InputBoxResponse();
            if (string.IsNullOrEmpty(prompt))
            {
                LabelPrompt.Text = "Enter a value:";
            }
            else
            {
                LabelPrompt.Text = prompt;
            }
            if (string.IsNullOrEmpty(title))
            {
                this.Text = My.MyApplication.Application.Info.ProductName;
            }
            else
            {
                this.Text = title;
            }
            TextBoxInput.Text = defaultResponse;
            TextBoxInput.SelectAll();
            if (positionX < 0 && positionY < 0)
            {
                this.StartPosition = FormStartPosition.CenterParent;
            }
            else
            {
                this.StartPosition = FormStartPosition.Manual;
                if (positionX >= 0)
                {
                    this.Left = positionX;
                }
                if (positionY >= 0)
                {
                    this.Top = positionY;
                }
            }
            result.Result = this.ShowDialog();
            if (result.Result == System.Windows.Forms.DialogResult.OK)
            {
                result.Response = TextBoxInput.Text;
            }
            else
            {
                result.Response = defaultResponse;
            }
            return result;
        }
    }

}