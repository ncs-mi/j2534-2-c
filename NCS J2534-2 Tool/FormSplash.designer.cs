﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class FormSplash : System.Windows.Forms.Form
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSplash));
            this.ButtonOK = new System.Windows.Forms.Button();
            this.TextBoxLicense = new System.Windows.Forms.TextBox();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.LabelCopyright = new System.Windows.Forms.Label();
            this.LabelCompany = new System.Windows.Forms.Label();
            this.LabelVersion = new System.Windows.Forms.Label();
            this.LabelProductName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            //
            //ButtonOK
            //
            this.ButtonOK.Location = new System.Drawing.Point(351, 5);
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.Size = new System.Drawing.Size(33, 19);
            this.ButtonOK.TabIndex = 0;
            this.ButtonOK.Text = "OK";
            this.ButtonOK.UseVisualStyleBackColor = true;
            this.ButtonOK.Visible = false;
            //
            //TextBoxLicense
            //
            this.TextBoxLicense.BackColor = System.Drawing.SystemColors.Window;
            this.TextBoxLicense.Location = new System.Drawing.Point(13, 93);
            this.TextBoxLicense.Multiline = true;
            this.TextBoxLicense.Name = "TextBoxLicense";
            this.TextBoxLicense.ReadOnly = true;
            this.TextBoxLicense.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxLicense.Size = new System.Drawing.Size(371, 116);
            this.TextBoxLicense.TabIndex = 1;
            this.TextBoxLicense.Text = resources.GetString("TextBoxLicense.Text");
            //
            //Timer1
            //
            this.Timer1.Enabled = true;
            this.Timer1.Interval = 10000;
            //
            //LabelCopyright
            //
            this.LabelCopyright.Location = new System.Drawing.Point(10, 72);
            this.LabelCopyright.Name = "LabelCopyright";
            this.LabelCopyright.Size = new System.Drawing.Size(257, 18);
            this.LabelCopyright.TabIndex = 4;
            this.LabelCopyright.Text = "Copyright";
            //
            //LabelCompany
            //
            this.LabelCompany.Location = new System.Drawing.Point(10, 8);
            this.LabelCompany.Name = "LabelCompany";
            this.LabelCompany.Size = new System.Drawing.Size(161, 17);
            this.LabelCompany.TabIndex = 2;
            this.LabelCompany.Text = "Company";
            //
            //LabelVersion
            //
            this.LabelVersion.AutoSize = true;
            this.LabelVersion.Location = new System.Drawing.Point(10, 212);
            this.LabelVersion.Name = "LabelVersion";
            this.LabelVersion.Size = new System.Drawing.Size(42, 13);
            this.LabelVersion.TabIndex = 5;
            this.LabelVersion.Text = "Version";
            this.LabelVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelProductName
            //
            this.LabelProductName.AutoSize = true;
            this.LabelProductName.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.LabelProductName.Location = new System.Drawing.Point(9, 25);
            this.LabelProductName.Name = "LabelProductName";
            this.LabelProductName.Size = new System.Drawing.Size(90, 24);
            this.LabelProductName.TabIndex = 3;
            this.LabelProductName.Text = "Product";
            //
            //FormSplash
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(396, 230);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonOK);
            this.Controls.Add(this.TextBoxLicense);
            this.Controls.Add(this.LabelCopyright);
            this.Controls.Add(this.LabelCompany);
            this.Controls.Add(this.LabelVersion);
            this.Controls.Add(this.LabelProductName);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(14, 91);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSplash";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

            base.Click += new System.EventHandler(FormSplash_Click);
            LabelCompany.Click += new System.EventHandler(FormSplash_Click);
            LabelCopyright.Click += new System.EventHandler(FormSplash_Click);
            LabelProductName.Click += new System.EventHandler(FormSplash_Click);
            LabelVersion.Click += new System.EventHandler(FormSplash_Click);
            ButtonOK.Click += new System.EventHandler(FormSplash_Click);
            base.KeyPress += new System.Windows.Forms.KeyPressEventHandler(FormSplash_KeyPress);
            Timer1.Tick += new System.EventHandler(Timer1_Tick);
        }

        internal System.Windows.Forms.Button ButtonOK;
        internal System.Windows.Forms.Timer Timer1;
        private System.Windows.Forms.Label LabelCopyright;
        private System.Windows.Forms.Label LabelCompany;
        private System.Windows.Forms.Label LabelVersion;
        private System.Windows.Forms.Label LabelProductName;
        private TextBox TextBoxLicense;

        private static FormSplash _DefaultInstance;
        public static FormSplash DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new FormSplash();

                return _DefaultInstance;
            }
        }
    }
}