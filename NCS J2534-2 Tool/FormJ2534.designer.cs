﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901


using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class FormJ2534 : System.Windows.Forms.Form
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormJ2534));
            this.ComboAvailableBoxLocator0 = new System.Windows.Forms.Label();
            this.ComboAvailableChannelLocator0 = new System.Windows.Forms.Label();
            this.ComboAvailableBoxLocator1 = new System.Windows.Forms.Label();
            this.ComboAvailableChannelLocator1 = new System.Windows.Forms.Label();
            this.ComboAvailableBoxLocator2 = new System.Windows.Forms.Label();
            this.ComboAvailableChannelLocator2 = new System.Windows.Forms.Label();
            this.ComboAvailableBoxLocator3 = new System.Windows.Forms.Label();
            this.ComboAvailableChannelLocator3 = new System.Windows.Forms.Label();
            this.ComboAvailableBoxLocator4 = new System.Windows.Forms.Label();
            this.ComboAvailableChannelLocator4 = new System.Windows.Forms.Label();
            this.ComboAvailableBoxLocator5 = new System.Windows.Forms.Label();
            this.ComboAvailableChannelLocator5 = new System.Windows.Forms.Label();
            this.ComboAvailableBoxLocator6 = new System.Windows.Forms.Label();
            this.ComboAvailableChannelLocator6 = new System.Windows.Forms.Label();
            this.ComboAvailableBoxLocator7 = new System.Windows.Forms.Label();
            this.ComboAvailableChannelLocator7 = new System.Windows.Forms.Label();
            this.MenuStripMain = new System.Windows.Forms.MenuStrip();
            this.MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFileMsgOutLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFileFilterLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFilePeriodicMessageLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFileSpace1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuFileMsgInSaveSelected = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFileMsgInSaveAll = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFileMsgOutSave = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFileFilterSave = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFilePeriodicMessageSave = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFileSpace2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditSpace1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuEditCut = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditCopyLine = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditMsgInCopyData = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditSpace2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuEditMsgInMakeFilter = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditFlagsEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditRxStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditFlagsClear = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditFlagsSetDefault = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditMsgOutEditMessage = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditMsgOutAddMessage = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditMsgOutCopyToScratchPad = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditScratchAddToOutgoingMessageSet = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditMsgOutMakePeriodicMessage = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuEditClear = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuHelpContents = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgInCopyLine = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgInCopyData = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgInMakeFilter = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgInRxStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgInSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgInSpace1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuMsgInSaveSelected = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgInSaveAll = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgInClear = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgOutDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgOutEditMessage = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgOutAddMessage = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgOutCopyToScratchPad = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgOutMakePeriodicMessage = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgOutSpace1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuMsgOutSave = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgOutLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMsgOutClear = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsSpace1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuFlagsCut = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsSpace2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuFlagsSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsSpace3 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuFlagsEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsClear = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFlagsSetDefault = new System.Windows.Forms.ToolStripMenuItem();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPageConnect = new System.Windows.Forms.TabPage();
            this.GroupBoxDevices = new System.Windows.Forms.GroupBox();
            this.ComboBoxDevice = new System.Windows.Forms.ComboBox();
            this.ButtonDiscover = new System.Windows.Forms.Button();
            this.ButtonCloseBox = new System.Windows.Forms.Button();
            this.ButtonOpenBox = new System.Windows.Forms.Button();
            this.LabelDeviceInfo = new System.Windows.Forms.Label();
            this.LabelComboDevice = new System.Windows.Forms.Label();
            this.GroupBoxAPIs = new System.Windows.Forms.GroupBox();
            this.ComboBoxAPI = new System.Windows.Forms.ComboBox();
            this.ButtonLoadDLL = new System.Windows.Forms.Button();
            this.TextBoxDllPath = new System.Windows.Forms.TextBox();
            this.ContextMenuStripTextBox = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuTextBoxUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuTextBoxCut = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuTextBoxSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuTextBoxSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.LabelAPI = new System.Windows.Forms.Label();
            this.LabelVendor = new System.Windows.Forms.Label();
            this.LabelDevice = new System.Windows.Forms.Label();
            this.LabelDllPath = new System.Windows.Forms.Label();
            this.LabelDllName = new System.Windows.Forms.Label();
            this.GroupBoxConnect = new System.Windows.Forms.GroupBox();
            this.ComboBoxPins = new System.Windows.Forms.ComboBox();
            this.ComboBoxConnector = new System.Windows.Forms.ComboBox();
            this.CheckBoxMixedMode = new System.Windows.Forms.CheckBox();
            this.TextBoxConnectFlags = new System.Windows.Forms.TextBox();
            this.ContextMenuStripFlags = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuFlagsDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.ComboBoxBaudRate = new System.Windows.Forms.ComboBox();
            this.ButtonConnect = new System.Windows.Forms.Button();
            this.ButtonDisconnect = new System.Windows.Forms.Button();
            this.ComboBoxConnectChannel = new System.Windows.Forms.ComboBox();
            this.LabelPins = new System.Windows.Forms.Label();
            this.LabelConn = new System.Windows.Forms.Label();
            this.LabelConnectFlags = new System.Windows.Forms.Label();
            this.LabelDeviceCombo0 = new System.Windows.Forms.Label();
            this.LabelBaud = new System.Windows.Forms.Label();
            this.LabelConnectChannel = new System.Windows.Forms.Label();
            this.GroupBoxJ2534Info = new System.Windows.Forms.GroupBox();
            this.LabelProtSupport = new System.Windows.Forms.Label();
            this.LabelJ2534Info = new System.Windows.Forms.Label();
            this.TabPageMessages = new System.Windows.Forms.TabPage();
            this.ButtonClaimJ1939Address = new System.Windows.Forms.Button();
            this.LabelMsgFlags = new System.Windows.Forms.Label();
            this.LabelChannel1 = new System.Windows.Forms.Label();
            this.LabelDeviceCombo1 = new System.Windows.Forms.Label();
            this.LvOutLocator = new System.Windows.Forms.Label();
            this.LvInLocator = new System.Windows.Forms.Label();
            this.ComboBoxMessageChannel = new System.Windows.Forms.ComboBox();
            this.CheckBoxPadMessage = new System.Windows.Forms.CheckBox();
            this.TextBoxTimeOut = new System.Windows.Forms.TextBox();
            this.TextBoxOutFlags = new System.Windows.Forms.TextBox();
            this.ButtonClearRx = new System.Windows.Forms.Button();
            this.ButtonClearTx = new System.Windows.Forms.Button();
            this.TextBoxMessageOut = new System.Windows.Forms.TextBox();
            this.ContextMenuStripScratchPad = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuScratchPadUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuScratchPadSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuScratchPadCut = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuScratchPadCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuScratchPadPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuScratchPadDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuScratchPadSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuScratchPadSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuScratchPadSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuScratchPadAddToOutgoingMessageSet = new System.Windows.Forms.ToolStripMenuItem();
            this.ButtonSend = new System.Windows.Forms.Button();
            this.ButtonReceive = new System.Windows.Forms.Button();
            this.TextBoxReadRate = new System.Windows.Forms.TextBox();
            this.ButtonClearList = new System.Windows.Forms.Button();
            this.LabelScratchPad = new System.Windows.Forms.Label();
            this.LabelTO = new System.Windows.Forms.Label();
            this.LabelReadRate = new System.Windows.Forms.Label();
            this.TabPagePeriodicMessages = new System.Windows.Forms.TabPage();
            this.ComboBoxPeriodicMessageChannel0 = new System.Windows.Forms.ComboBox();
            this.ComboBoxPeriodicMessageChannel1 = new System.Windows.Forms.ComboBox();
            this.ComboBoxPeriodicMessageChannel2 = new System.Windows.Forms.ComboBox();
            this.ComboBoxPeriodicMessageChannel3 = new System.Windows.Forms.ComboBox();
            this.ComboBoxPeriodicMessageChannel4 = new System.Windows.Forms.ComboBox();
            this.ComboBoxPeriodicMessageChannel5 = new System.Windows.Forms.ComboBox();
            this.ComboBoxPeriodicMessageChannel6 = new System.Windows.Forms.ComboBox();
            this.ComboBoxPeriodicMessageChannel7 = new System.Windows.Forms.ComboBox();
            this.ComboBoxPeriodicMessageChannel8 = new System.Windows.Forms.ComboBox();
            this.ComboBoxPeriodicMessageChannel9 = new System.Windows.Forms.ComboBox();
            this.ButtonClearAllPeriodicMessages = new System.Windows.Forms.Button();
            this.ButtonCancelPeriodicMessages = new System.Windows.Forms.Button();
            this.ButtonApplyPeriodicMessages = new System.Windows.Forms.Button();
            this.TextBoxPeriodicMessageFlags0 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageFlags1 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageFlags2 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageFlags3 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageFlags4 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageFlags5 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageFlags6 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageFlags7 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageFlags8 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageFlags9 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessage0 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessage1 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessage2 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessage3 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessage4 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessage5 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessage6 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessage7 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessage8 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessage9 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageInterval0 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageInterval1 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageInterval2 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageInterval3 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageInterval4 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageInterval5 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageInterval6 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageInterval7 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageInterval8 = new System.Windows.Forms.TextBox();
            this.TextBoxPeriodicMessageInterval9 = new System.Windows.Forms.TextBox();
            this.CheckBoxPeriodicMessageEnable0 = new System.Windows.Forms.CheckBox();
            this.CheckBoxPeriodicMessageEnable1 = new System.Windows.Forms.CheckBox();
            this.CheckBoxPeriodicMessageEnable2 = new System.Windows.Forms.CheckBox();
            this.CheckBoxPeriodicMessageEnable3 = new System.Windows.Forms.CheckBox();
            this.CheckBoxPeriodicMessageEnable4 = new System.Windows.Forms.CheckBox();
            this.CheckBoxPeriodicMessageEnable5 = new System.Windows.Forms.CheckBox();
            this.CheckBoxPeriodicMessageEnable6 = new System.Windows.Forms.CheckBox();
            this.CheckBoxPeriodicMessageEnable7 = new System.Windows.Forms.CheckBox();
            this.CheckBoxPeriodicMessageEnable8 = new System.Windows.Forms.CheckBox();
            this.CheckBoxPeriodicMessageEnable9 = new System.Windows.Forms.CheckBox();
            this.LabelPeriodicMessageChannel = new System.Windows.Forms.Label();
            this.LabelChannel2 = new System.Windows.Forms.Label();
            this.LabelDeviceCombo2 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageIds = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageId0 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageId1 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageId2 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageId3 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageId4 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageId5 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageId6 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageId7 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageId8 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageId9 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageFlags = new System.Windows.Forms.Label();
            this.LabelPMMessage = new System.Windows.Forms.Label();
            this.LabelPeriodicMessage0 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessage1 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessage2 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessage3 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessage4 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessage5 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessage6 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessage7 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessage8 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessage9 = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageInterval = new System.Windows.Forms.Label();
            this.LabelPeriodicMessageDelete = new System.Windows.Forms.Label();
            this.TabPageFilters = new System.Windows.Forms.TabPage();
            this.ButtonCreatePassFilter = new System.Windows.Forms.Button();
            this.ButtonClearAllFilter = new System.Windows.Forms.Button();
            this.ButtonCancelFilter = new System.Windows.Forms.Button();
            this.TextBoxFilterFlow0 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlow1 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlow2 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlow3 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlow4 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlow5 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlow6 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlow7 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlow8 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlow9 = new System.Windows.Forms.TextBox();
            this.ButtonApplyFilter = new System.Windows.Forms.Button();
            this.TextBoxFilterFlags0 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlags1 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlags2 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlags3 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlags4 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlags5 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlags6 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlags7 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlags8 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterFlags9 = new System.Windows.Forms.TextBox();
            this.ComboBoxFilterType0 = new System.Windows.Forms.ComboBox();
            this.ComboBoxFilterType1 = new System.Windows.Forms.ComboBox();
            this.ComboBoxFilterType2 = new System.Windows.Forms.ComboBox();
            this.ComboBoxFilterType3 = new System.Windows.Forms.ComboBox();
            this.ComboBoxFilterType4 = new System.Windows.Forms.ComboBox();
            this.ComboBoxFilterType5 = new System.Windows.Forms.ComboBox();
            this.ComboBoxFilterType6 = new System.Windows.Forms.ComboBox();
            this.ComboBoxFilterType7 = new System.Windows.Forms.ComboBox();
            this.ComboBoxFilterType8 = new System.Windows.Forms.ComboBox();
            this.ComboBoxFilterType9 = new System.Windows.Forms.ComboBox();
            this.TextBoxFilterPatt0 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterPatt1 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterPatt2 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterPatt3 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterPatt4 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterPatt5 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterPatt6 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterPatt7 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterPatt8 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterPatt9 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterMask0 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterMask1 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterMask2 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterMask3 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterMask4 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterMask5 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterMask6 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterMask7 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterMask8 = new System.Windows.Forms.TextBox();
            this.TextBoxFilterMask9 = new System.Windows.Forms.TextBox();
            this.LabelChannel3 = new System.Windows.Forms.Label();
            this.LabelDeviceCombo3 = new System.Windows.Forms.Label();
            this.LabelFilterIds = new System.Windows.Forms.Label();
            this.LabelFilterId0 = new System.Windows.Forms.Label();
            this.LabelFilterId1 = new System.Windows.Forms.Label();
            this.LabelFilterId2 = new System.Windows.Forms.Label();
            this.LabelFilterId3 = new System.Windows.Forms.Label();
            this.LabelFilterId4 = new System.Windows.Forms.Label();
            this.LabelFilterId5 = new System.Windows.Forms.Label();
            this.LabelFilterId6 = new System.Windows.Forms.Label();
            this.LabelFilterId7 = new System.Windows.Forms.Label();
            this.LabelFilterId8 = new System.Windows.Forms.Label();
            this.LabelFilterId9 = new System.Windows.Forms.Label();
            this.LabelFilterType = new System.Windows.Forms.Label();
            this.LabelFilterFlags = new System.Windows.Forms.Label();
            this.LabelFilterFlow = new System.Windows.Forms.Label();
            this.LabelFilterPatt = new System.Windows.Forms.Label();
            this.LabelFilterMask = new System.Windows.Forms.Label();
            this.LabelFilter0 = new System.Windows.Forms.Label();
            this.LabelFilter1 = new System.Windows.Forms.Label();
            this.LabelFilter2 = new System.Windows.Forms.Label();
            this.LabelFilter3 = new System.Windows.Forms.Label();
            this.LabelFilter4 = new System.Windows.Forms.Label();
            this.LabelFilter5 = new System.Windows.Forms.Label();
            this.LabelFilter6 = new System.Windows.Forms.Label();
            this.LabelFilter7 = new System.Windows.Forms.Label();
            this.LabelFilter8 = new System.Windows.Forms.Label();
            this.LabelFilter9 = new System.Windows.Forms.Label();
            this.TabPageConfig = new System.Windows.Forms.TabPage();
            this.GroupBoxAnalogConfig = new System.Windows.Forms.GroupBox();
            this.LabelMaxSample = new System.Windows.Forms.Label();
            this.LabelAnalogRate = new System.Windows.Forms.Label();
            this.TextBoxAnalogRate = new System.Windows.Forms.TextBox();
            this.PanelAudioChannel = new System.Windows.Forms.Panel();
            this.CheckBoxCH0 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH1 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH2 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH3 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH4 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH5 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH6 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH7 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH8 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH9 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH10 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH11 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH12 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH13 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH14 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH15 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH16 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH17 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH18 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH19 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH20 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH21 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH22 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH23 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH24 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH25 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH26 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH27 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH28 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH29 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH30 = new System.Windows.Forms.CheckBox();
            this.CheckBoxCH31 = new System.Windows.Forms.CheckBox();
            this.ComboBoxAnalogRateMode = new System.Windows.Forms.ComboBox();
            this.ComboBoxAvailableDevice = new System.Windows.Forms.ComboBox();
            this.ComboBoxAvailableChannel = new System.Windows.Forms.ComboBox();
            this.ButtonSetConfig = new System.Windows.Forms.Button();
            this.ButtonClearConfig = new System.Windows.Forms.Button();
            this.LabelParamVal0 = new System.Windows.Forms.Label();
            this.LabelParamVal1 = new System.Windows.Forms.Label();
            this.LabelParamVal2 = new System.Windows.Forms.Label();
            this.LabelParamVal3 = new System.Windows.Forms.Label();
            this.LabelParamVal4 = new System.Windows.Forms.Label();
            this.LabelParamVal5 = new System.Windows.Forms.Label();
            this.LabelParamVal6 = new System.Windows.Forms.Label();
            this.LabelParamVal7 = new System.Windows.Forms.Label();
            this.LabelParamVal8 = new System.Windows.Forms.Label();
            this.LabelParamVal9 = new System.Windows.Forms.Label();
            this.LabelParamVal10 = new System.Windows.Forms.Label();
            this.LabelParamVal11 = new System.Windows.Forms.Label();
            this.LabelParamVal12 = new System.Windows.Forms.Label();
            this.LabelParamVal13 = new System.Windows.Forms.Label();
            this.LabelParamVal14 = new System.Windows.Forms.Label();
            this.LabelParamVal15 = new System.Windows.Forms.Label();
            this.LabelParamVal16 = new System.Windows.Forms.Label();
            this.LabelParamVal17 = new System.Windows.Forms.Label();
            this.LabelParamVal18 = new System.Windows.Forms.Label();
            this.LabelParamVal19 = new System.Windows.Forms.Label();
            this.LabelParamVal20 = new System.Windows.Forms.Label();
            this.LabelParamVal21 = new System.Windows.Forms.Label();
            this.LabelParameterName0 = new System.Windows.Forms.Label();
            this.LabelParameterName1 = new System.Windows.Forms.Label();
            this.LabelParameterName2 = new System.Windows.Forms.Label();
            this.LabelParameterName3 = new System.Windows.Forms.Label();
            this.LabelParameterName4 = new System.Windows.Forms.Label();
            this.LabelParameterName5 = new System.Windows.Forms.Label();
            this.LabelParameterName6 = new System.Windows.Forms.Label();
            this.LabelParameterName7 = new System.Windows.Forms.Label();
            this.LabelParameterName8 = new System.Windows.Forms.Label();
            this.LabelParameterName9 = new System.Windows.Forms.Label();
            this.LabelParameterName10 = new System.Windows.Forms.Label();
            this.LabelParameterName11 = new System.Windows.Forms.Label();
            this.LabelParameterName12 = new System.Windows.Forms.Label();
            this.LabelParameterName13 = new System.Windows.Forms.Label();
            this.LabelParameterName14 = new System.Windows.Forms.Label();
            this.LabelParameterName15 = new System.Windows.Forms.Label();
            this.LabelParameterName16 = new System.Windows.Forms.Label();
            this.LabelParameterName17 = new System.Windows.Forms.Label();
            this.LabelParameterName18 = new System.Windows.Forms.Label();
            this.LabelParameterName19 = new System.Windows.Forms.Label();
            this.LabelParameterName20 = new System.Windows.Forms.Label();
            this.LabelParameterName21 = new System.Windows.Forms.Label();
            this.TextBoxParamVal0 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal1 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal2 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal3 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal4 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal5 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal6 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal7 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal8 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal9 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal10 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal11 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal12 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal13 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal14 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal15 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal16 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal17 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal18 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal19 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal20 = new System.Windows.Forms.TextBox();
            this.TextBoxParamVal21 = new System.Windows.Forms.TextBox();
            this.LabelChannel4 = new System.Windows.Forms.Label();
            this.LabelDeviceCombo4 = new System.Windows.Forms.Label();
            this.LabelParameters0 = new System.Windows.Forms.Label();
            this.LabelValues0 = new System.Windows.Forms.Label();
            this.LabelMod0 = new System.Windows.Forms.Label();
            this.LabelParameters1 = new System.Windows.Forms.Label();
            this.LabelValues1 = new System.Windows.Forms.Label();
            this.LabelMod1 = new System.Windows.Forms.Label();
            this.TabPageInit = new System.Windows.Forms.TabPage();
            this.GroupBoxFastInit = new System.Windows.Forms.GroupBox();
            this.ButtonExecuteFastInit = new System.Windows.Forms.Button();
            this.TextBoxFIMessage = new System.Windows.Forms.TextBox();
            this.TextBoxFIFlags = new System.Windows.Forms.TextBox();
            this.LabelFIRXTitle = new System.Windows.Forms.Label();
            this.LabelFITXFlags = new System.Windows.Forms.Label();
            this.LabelFIRxStatus = new System.Windows.Forms.Label();
            this.LabelFIResponse = new System.Windows.Forms.Label();
            this.LabelFIMsgResp = new System.Windows.Forms.Label();
            this.LabelFIMsgData = new System.Windows.Forms.Label();
            this.GroupBox5BInit = new System.Windows.Forms.GroupBox();
            this.TextBox5BInitECU = new System.Windows.Forms.TextBox();
            this.ButtonExecute5BInit = new System.Windows.Forms.Button();
            this.LabelKWlabel1 = new System.Windows.Forms.Label();
            this.LabelKWlabel0 = new System.Windows.Forms.Label();
            this.Label5BECU = new System.Windows.Forms.Label();
            this.Label5BKeyWord0 = new System.Windows.Forms.Label();
            this.Label5BKeyWord1 = new System.Windows.Forms.Label();
            this.LabelChannel5 = new System.Windows.Forms.Label();
            this.LabelDeviceCombo5 = new System.Windows.Forms.Label();
            this.TabPageFunctionalMessages = new System.Windows.Forms.TabPage();
            this.ButtonClearAllFunctionalMessages = new System.Windows.Forms.Button();
            this.ButtonApplyFunctionalMessages = new System.Windows.Forms.Button();
            this.ButtonCancelFunctionalMessages = new System.Windows.Forms.Button();
            this.CheckBoxFunctionalMessageDelete0 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete1 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete2 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete3 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete4 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete5 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete6 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete7 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete8 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete9 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete10 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete11 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete12 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete13 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete14 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete15 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete16 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete17 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete18 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete19 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete20 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete21 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete22 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete23 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete24 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete25 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete26 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete27 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete28 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete29 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete30 = new System.Windows.Forms.CheckBox();
            this.CheckBoxFunctionalMessageDelete31 = new System.Windows.Forms.CheckBox();
            this.TextBoxFunctionalMessage0 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage1 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage2 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage3 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage4 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage5 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage6 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage7 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage8 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage9 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage10 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage11 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage12 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage13 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage14 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage15 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage16 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage17 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage18 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage19 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage20 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage21 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage22 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage23 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage24 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage25 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage26 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage27 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage28 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage29 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage30 = new System.Windows.Forms.TextBox();
            this.TextBoxFunctionalMessage31 = new System.Windows.Forms.TextBox();
            this.LabelChannel6 = new System.Windows.Forms.Label();
            this.LabelDeviceCombo6 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessageModify = new System.Windows.Forms.Label();
            this.LabelFunctionalMessageValues = new System.Windows.Forms.Label();
            this.LabelFuncId31 = new System.Windows.Forms.Label();
            this.LabelFuncId30 = new System.Windows.Forms.Label();
            this.LabelFuncId29 = new System.Windows.Forms.Label();
            this.LabelFuncId28 = new System.Windows.Forms.Label();
            this.LabelFuncId27 = new System.Windows.Forms.Label();
            this.LabelFuncId26 = new System.Windows.Forms.Label();
            this.LabelFuncId25 = new System.Windows.Forms.Label();
            this.LabelFuncId24 = new System.Windows.Forms.Label();
            this.LabelFuncId23 = new System.Windows.Forms.Label();
            this.LabelFuncId22 = new System.Windows.Forms.Label();
            this.LabelFuncId21 = new System.Windows.Forms.Label();
            this.LabelFuncId20 = new System.Windows.Forms.Label();
            this.LabelFuncId19 = new System.Windows.Forms.Label();
            this.LabelFuncId18 = new System.Windows.Forms.Label();
            this.LabelFuncId17 = new System.Windows.Forms.Label();
            this.LabelFuncId16 = new System.Windows.Forms.Label();
            this.LabelFuncId15 = new System.Windows.Forms.Label();
            this.LabelFuncId14 = new System.Windows.Forms.Label();
            this.LabelFuncId13 = new System.Windows.Forms.Label();
            this.LabelFuncId12 = new System.Windows.Forms.Label();
            this.LabelFuncId11 = new System.Windows.Forms.Label();
            this.LabelFuncId10 = new System.Windows.Forms.Label();
            this.LabelFuncId9 = new System.Windows.Forms.Label();
            this.LabelFuncId8 = new System.Windows.Forms.Label();
            this.LabelFuncId7 = new System.Windows.Forms.Label();
            this.LabelFuncId6 = new System.Windows.Forms.Label();
            this.LabelFuncId5 = new System.Windows.Forms.Label();
            this.LabelFuncId4 = new System.Windows.Forms.Label();
            this.LabelFuncId3 = new System.Windows.Forms.Label();
            this.LabelFuncId2 = new System.Windows.Forms.Label();
            this.LabelFuncId1 = new System.Windows.Forms.Label();
            this.LabelFuncId0 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage0 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage1 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage2 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage3 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage4 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage5 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage6 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage7 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage8 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage9 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage10 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage11 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage12 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage13 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage14 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage15 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage16 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage17 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage18 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage19 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage20 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage21 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage22 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage23 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage24 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage25 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage26 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage27 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage28 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage29 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage30 = new System.Windows.Forms.Label();
            this.LabelFunctionalMessage31 = new System.Windows.Forms.Label();
            this.TabPageAnalog = new System.Windows.Forms.TabPage();
            this.ComboBoxAnalogChannel = new System.Windows.Forms.ComboBox();
            this.GroupBoxAnalog = new System.Windows.Forms.GroupBox();
            this.PanelAnalog = new System.Windows.Forms.Panel();
            this.LabelAnalogCH0 = new System.Windows.Forms.Label();
            this.LabelAnalogCH1 = new System.Windows.Forms.Label();
            this.LabelAnalogCH2 = new System.Windows.Forms.Label();
            this.LabelAnalogCH3 = new System.Windows.Forms.Label();
            this.LabelAnalogCH4 = new System.Windows.Forms.Label();
            this.LabelAnalogCH5 = new System.Windows.Forms.Label();
            this.LabelAnalogCH6 = new System.Windows.Forms.Label();
            this.LabelAnalogCH7 = new System.Windows.Forms.Label();
            this.LabelAnalogCH8 = new System.Windows.Forms.Label();
            this.LabelAnalogCH9 = new System.Windows.Forms.Label();
            this.LabelAnalogCH10 = new System.Windows.Forms.Label();
            this.LabelAnalogCH11 = new System.Windows.Forms.Label();
            this.LabelAnalogCH12 = new System.Windows.Forms.Label();
            this.LabelAnalogCH13 = new System.Windows.Forms.Label();
            this.LabelAnalogCH14 = new System.Windows.Forms.Label();
            this.LabelAnalogCH15 = new System.Windows.Forms.Label();
            this.LabelAnalogCH16 = new System.Windows.Forms.Label();
            this.LabelAnalogCH17 = new System.Windows.Forms.Label();
            this.LabelAnalogCH18 = new System.Windows.Forms.Label();
            this.LabelAnalogCH19 = new System.Windows.Forms.Label();
            this.LabelAnalogCH20 = new System.Windows.Forms.Label();
            this.LabelAnalogCH21 = new System.Windows.Forms.Label();
            this.LabelAnalogCH22 = new System.Windows.Forms.Label();
            this.LabelAnalogCH23 = new System.Windows.Forms.Label();
            this.LabelAnalogCH24 = new System.Windows.Forms.Label();
            this.LabelAnalogCH25 = new System.Windows.Forms.Label();
            this.LabelAnalogCH26 = new System.Windows.Forms.Label();
            this.LabelAnalogCH27 = new System.Windows.Forms.Label();
            this.LabelAnalogCH28 = new System.Windows.Forms.Label();
            this.LabelAnalogCH29 = new System.Windows.Forms.Label();
            this.LabelAnalogCH30 = new System.Windows.Forms.Label();
            this.LabelAnalogCH31 = new System.Windows.Forms.Label();
            this.LabelAnalogRead0 = new System.Windows.Forms.Label();
            this.LabelAnalogRead1 = new System.Windows.Forms.Label();
            this.LabelAnalogRead2 = new System.Windows.Forms.Label();
            this.LabelAnalogRead3 = new System.Windows.Forms.Label();
            this.LabelAnalogRead4 = new System.Windows.Forms.Label();
            this.LabelAnalogRead5 = new System.Windows.Forms.Label();
            this.LabelAnalogRead6 = new System.Windows.Forms.Label();
            this.LabelAnalogRead7 = new System.Windows.Forms.Label();
            this.LabelAnalogRead8 = new System.Windows.Forms.Label();
            this.LabelAnalogRead9 = new System.Windows.Forms.Label();
            this.LabelAnalogRead10 = new System.Windows.Forms.Label();
            this.LabelAnalogRead11 = new System.Windows.Forms.Label();
            this.LabelAnalogRead12 = new System.Windows.Forms.Label();
            this.LabelAnalogRead13 = new System.Windows.Forms.Label();
            this.LabelAnalogRead14 = new System.Windows.Forms.Label();
            this.LabelAnalogRead15 = new System.Windows.Forms.Label();
            this.LabelAnalogRead16 = new System.Windows.Forms.Label();
            this.LabelAnalogRead17 = new System.Windows.Forms.Label();
            this.LabelAnalogRead18 = new System.Windows.Forms.Label();
            this.LabelAnalogRead19 = new System.Windows.Forms.Label();
            this.LabelAnalogRead20 = new System.Windows.Forms.Label();
            this.LabelAnalogRead21 = new System.Windows.Forms.Label();
            this.LabelAnalogRead22 = new System.Windows.Forms.Label();
            this.LabelAnalogRead23 = new System.Windows.Forms.Label();
            this.LabelAnalogRead24 = new System.Windows.Forms.Label();
            this.LabelAnalogRead25 = new System.Windows.Forms.Label();
            this.LabelAnalogRead26 = new System.Windows.Forms.Label();
            this.LabelAnalogRead27 = new System.Windows.Forms.Label();
            this.LabelAnalogRead28 = new System.Windows.Forms.Label();
            this.LabelAnalogRead29 = new System.Windows.Forms.Label();
            this.LabelAnalogRead30 = new System.Windows.Forms.Label();
            this.LabelAnalogRead31 = new System.Windows.Forms.Label();
            this.ProgressBarAnalog0 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog1 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog2 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog3 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog4 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog5 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog6 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog7 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog8 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog9 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog10 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog11 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog12 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog13 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog14 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog15 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog16 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog17 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog18 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog19 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog20 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog21 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog22 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog23 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog24 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog25 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog26 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog27 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog28 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog29 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog30 = new System.Windows.Forms.ProgressBar();
            this.ProgressBarAnalog31 = new System.Windows.Forms.ProgressBar();
            this.LabelAnalogLow1 = new System.Windows.Forms.Label();
            this.LabelAnalogHigh1 = new System.Windows.Forms.Label();
            this.LabelAnalogHigh0 = new System.Windows.Forms.Label();
            this.LabelAnalogLow0 = new System.Windows.Forms.Label();
            this.LabelAnalogReading = new System.Windows.Forms.Label();
            this.GroupBoxBattVoltage = new System.Windows.Forms.GroupBox();
            this.ButtonReadBatt = new System.Windows.Forms.Button();
            this.LabelBattRead = new System.Windows.Forms.Label();
            this.GroupBoxProgVoltage = new System.Windows.Forms.GroupBox();
            this.GroupBoxVolt = new System.Windows.Forms.GroupBox();
            this.LabelVoltWarning = new System.Windows.Forms.Label();
            this.ButtonSetVoltage = new System.Windows.Forms.Button();
            this.ButtonReadVolt = new System.Windows.Forms.Button();
            this.TextBoxVoltSetting = new System.Windows.Forms.TextBox();
            this.optSet = new System.Windows.Forms.RadioButton();
            this.optShortToGround = new System.Windows.Forms.RadioButton();
            this.optVoltOff = new System.Windows.Forms.RadioButton();
            this.LabelVoltRead = new System.Windows.Forms.Label();
            this.LabelMV = new System.Windows.Forms.Label();
            this.GroupBoxPin = new System.Windows.Forms.GroupBox();
            this.RadioButtonPin15 = new System.Windows.Forms.RadioButton();
            this.RadioButtonPin14 = new System.Windows.Forms.RadioButton();
            this.RadioButtonPin13 = new System.Windows.Forms.RadioButton();
            this.RadioButtonPin12 = new System.Windows.Forms.RadioButton();
            this.RadioButtonPin11 = new System.Windows.Forms.RadioButton();
            this.RadioButtonPin9 = new System.Windows.Forms.RadioButton();
            this.RadioButtonPin6 = new System.Windows.Forms.RadioButton();
            this.RadioButtonPin0 = new System.Windows.Forms.RadioButton();
            this.LabelPin15 = new System.Windows.Forms.Label();
            this.LabelPin14 = new System.Windows.Forms.Label();
            this.LabelPin13 = new System.Windows.Forms.Label();
            this.LabelPin12 = new System.Windows.Forms.Label();
            this.LabelPin11 = new System.Windows.Forms.Label();
            this.LabelPin9 = new System.Windows.Forms.Label();
            this.LabelPin6 = new System.Windows.Forms.Label();
            this.LabelPin0 = new System.Windows.Forms.Label();
            this.LabelAnalogChannel = new System.Windows.Forms.Label();
            this.LabelDeviceCombo7 = new System.Windows.Forms.Label();
            this.TabPageResults = new System.Windows.Forms.TabPage();
            this.ListViewResults = new System.Windows.Forms.ListView();
            this.ListViewResultsColumnHeader1 = (System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader());
            this.ListViewResultsColumnHeader2 = (System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader());
            this.ContextMenuStripResults = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuResultsCopyLine = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuResultsDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuResultsSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuResultsSpace1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuResultsClear = new System.Windows.Forms.ToolStripMenuItem();
            this.DialogFileOpen = new System.Windows.Forms.OpenFileDialog();
            this.DialogFileSave = new System.Windows.Forms.SaveFileDialog();
            this.ContextMenuStripMessageIn = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ContextMenuStripMessageOut = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolTipMessageIn = new System.Windows.Forms.ToolTip(this.components);
            this.MenuStripMain.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.TabControl1.SuspendLayout();
            this.TabPageConnect.SuspendLayout();
            this.GroupBoxDevices.SuspendLayout();
            this.GroupBoxAPIs.SuspendLayout();
            this.ContextMenuStripTextBox.SuspendLayout();
            this.GroupBoxConnect.SuspendLayout();
            this.ContextMenuStripFlags.SuspendLayout();
            this.GroupBoxJ2534Info.SuspendLayout();
            this.TabPageMessages.SuspendLayout();
            this.ContextMenuStripScratchPad.SuspendLayout();
            this.TabPagePeriodicMessages.SuspendLayout();
            this.TabPageFilters.SuspendLayout();
            this.TabPageConfig.SuspendLayout();
            this.GroupBoxAnalogConfig.SuspendLayout();
            this.PanelAudioChannel.SuspendLayout();
            this.TabPageInit.SuspendLayout();
            this.GroupBoxFastInit.SuspendLayout();
            this.GroupBox5BInit.SuspendLayout();
            this.TabPageFunctionalMessages.SuspendLayout();
            this.TabPageAnalog.SuspendLayout();
            this.GroupBoxAnalog.SuspendLayout();
            this.PanelAnalog.SuspendLayout();
            this.GroupBoxBattVoltage.SuspendLayout();
            this.GroupBoxProgVoltage.SuspendLayout();
            this.GroupBoxVolt.SuspendLayout();
            this.GroupBoxPin.SuspendLayout();
            this.TabPageResults.SuspendLayout();
            this.ContextMenuStripResults.SuspendLayout();
            this.ContextMenuStripMessageIn.SuspendLayout();
            this.ContextMenuStripMessageOut.SuspendLayout();
            this.SuspendLayout();
            //
            //ComboAvailableBoxLocator0
            //
            this.ComboAvailableBoxLocator0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableBoxLocator0.Enabled = false;
            this.ComboAvailableBoxLocator0.Location = new System.Drawing.Point(16, 169);
            this.ComboAvailableBoxLocator0.Name = "ComboAvailableBoxLocator0";
            this.ComboAvailableBoxLocator0.Size = new System.Drawing.Size(120, 21);
            this.ComboAvailableBoxLocator0.TabIndex = 13;
            this.ComboAvailableBoxLocator0.Visible = false;
            //
            //ComboAvailableChannelLocator0
            //
            this.ComboAvailableChannelLocator0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableChannelLocator0.Enabled = false;
            this.ComboAvailableChannelLocator0.Location = new System.Drawing.Point(136, 169);
            this.ComboAvailableChannelLocator0.Name = "ComboAvailableChannelLocator0";
            this.ComboAvailableChannelLocator0.Size = new System.Drawing.Size(105, 21);
            this.ComboAvailableChannelLocator0.TabIndex = 14;
            this.ComboAvailableChannelLocator0.Visible = false;
            //
            //ComboAvailableBoxLocator1
            //
            this.ComboAvailableBoxLocator1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableBoxLocator1.Enabled = false;
            this.ComboAvailableBoxLocator1.Location = new System.Drawing.Point(8, 237);
            this.ComboAvailableBoxLocator1.Name = "ComboAvailableBoxLocator1";
            this.ComboAvailableBoxLocator1.Size = new System.Drawing.Size(120, 21);
            this.ComboAvailableBoxLocator1.TabIndex = 20;
            this.ComboAvailableBoxLocator1.Visible = false;
            //
            //ComboAvailableChannelLocator1
            //
            this.ComboAvailableChannelLocator1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableChannelLocator1.Enabled = false;
            this.ComboAvailableChannelLocator1.Location = new System.Drawing.Point(134, 237);
            this.ComboAvailableChannelLocator1.Name = "ComboAvailableChannelLocator1";
            this.ComboAvailableChannelLocator1.Size = new System.Drawing.Size(105, 21);
            this.ComboAvailableChannelLocator1.TabIndex = 2;
            this.ComboAvailableChannelLocator1.Visible = false;
            //
            //ComboAvailableBoxLocator2
            //
            this.ComboAvailableBoxLocator2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableBoxLocator2.Enabled = false;
            this.ComboAvailableBoxLocator2.Location = new System.Drawing.Point(398, 353);
            this.ComboAvailableBoxLocator2.Name = "ComboAvailableBoxLocator2";
            this.ComboAvailableBoxLocator2.Size = new System.Drawing.Size(120, 21);
            this.ComboAvailableBoxLocator2.TabIndex = 73;
            this.ComboAvailableBoxLocator2.Visible = false;
            //
            //ComboAvailableChannelLocator2
            //
            this.ComboAvailableChannelLocator2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableChannelLocator2.Enabled = false;
            this.ComboAvailableChannelLocator2.Location = new System.Drawing.Point(527, 353);
            this.ComboAvailableChannelLocator2.Name = "ComboAvailableChannelLocator2";
            this.ComboAvailableChannelLocator2.Size = new System.Drawing.Size(161, 21);
            this.ComboAvailableChannelLocator2.TabIndex = 74;
            this.ComboAvailableChannelLocator2.Visible = false;
            //
            //ComboAvailableBoxLocator3
            //
            this.ComboAvailableBoxLocator3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableBoxLocator3.Enabled = false;
            this.ComboAvailableBoxLocator3.Location = new System.Drawing.Point(398, 353);
            this.ComboAvailableBoxLocator3.Name = "ComboAvailableBoxLocator3";
            this.ComboAvailableBoxLocator3.Size = new System.Drawing.Size(120, 21);
            this.ComboAvailableBoxLocator3.TabIndex = 54;
            this.ComboAvailableBoxLocator3.Visible = false;
            //
            //ComboAvailableChannelLocator3
            //
            this.ComboAvailableChannelLocator3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableChannelLocator3.Enabled = false;
            this.ComboAvailableChannelLocator3.Location = new System.Drawing.Point(527, 353);
            this.ComboAvailableChannelLocator3.Name = "ComboAvailableChannelLocator3";
            this.ComboAvailableChannelLocator3.Size = new System.Drawing.Size(161, 21);
            this.ComboAvailableChannelLocator3.TabIndex = 55;
            this.ComboAvailableChannelLocator3.Visible = false;
            //
            //ComboAvailableBoxLocator4
            //
            this.ComboAvailableBoxLocator4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableBoxLocator4.Enabled = false;
            this.ComboAvailableBoxLocator4.Location = new System.Drawing.Point(398, 353);
            this.ComboAvailableBoxLocator4.Name = "ComboAvailableBoxLocator4";
            this.ComboAvailableBoxLocator4.Size = new System.Drawing.Size(120, 21);
            this.ComboAvailableBoxLocator4.TabIndex = 514;
            this.ComboAvailableBoxLocator4.Visible = false;
            //
            //ComboAvailableChannelLocator4
            //
            this.ComboAvailableChannelLocator4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableChannelLocator4.Enabled = false;
            this.ComboAvailableChannelLocator4.Location = new System.Drawing.Point(527, 353);
            this.ComboAvailableChannelLocator4.Name = "ComboAvailableChannelLocator4";
            this.ComboAvailableChannelLocator4.Size = new System.Drawing.Size(161, 21);
            this.ComboAvailableChannelLocator4.TabIndex = 515;
            this.ComboAvailableChannelLocator4.Visible = false;
            //
            //ComboAvailableBoxLocator5
            //
            this.ComboAvailableBoxLocator5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableBoxLocator5.Enabled = false;
            this.ComboAvailableBoxLocator5.Location = new System.Drawing.Point(398, 353);
            this.ComboAvailableBoxLocator5.Name = "ComboAvailableBoxLocator5";
            this.ComboAvailableBoxLocator5.Size = new System.Drawing.Size(120, 21);
            this.ComboAvailableBoxLocator5.TabIndex = 2;
            this.ComboAvailableBoxLocator5.Visible = false;
            //
            //ComboAvailableChannelLocator5
            //
            this.ComboAvailableChannelLocator5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableChannelLocator5.Enabled = false;
            this.ComboAvailableChannelLocator5.Location = new System.Drawing.Point(527, 353);
            this.ComboAvailableChannelLocator5.Name = "ComboAvailableChannelLocator5";
            this.ComboAvailableChannelLocator5.Size = new System.Drawing.Size(161, 21);
            this.ComboAvailableChannelLocator5.TabIndex = 3;
            this.ComboAvailableChannelLocator5.Visible = false;
            //
            //ComboAvailableBoxLocator6
            //
            this.ComboAvailableBoxLocator6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableBoxLocator6.Enabled = false;
            this.ComboAvailableBoxLocator6.Location = new System.Drawing.Point(398, 353);
            this.ComboAvailableBoxLocator6.Name = "ComboAvailableBoxLocator6";
            this.ComboAvailableBoxLocator6.Size = new System.Drawing.Size(120, 21);
            this.ComboAvailableBoxLocator6.TabIndex = 37;
            this.ComboAvailableBoxLocator6.Visible = false;
            //
            //ComboAvailableChannelLocator6
            //
            this.ComboAvailableChannelLocator6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableChannelLocator6.Enabled = false;
            this.ComboAvailableChannelLocator6.Location = new System.Drawing.Point(527, 353);
            this.ComboAvailableChannelLocator6.Name = "ComboAvailableChannelLocator6";
            this.ComboAvailableChannelLocator6.Size = new System.Drawing.Size(161, 21);
            this.ComboAvailableChannelLocator6.TabIndex = 38;
            this.ComboAvailableChannelLocator6.Visible = false;
            //
            //ComboAvailableBoxLocator7
            //
            this.ComboAvailableBoxLocator7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableBoxLocator7.Enabled = false;
            this.ComboAvailableBoxLocator7.Location = new System.Drawing.Point(398, 353);
            this.ComboAvailableBoxLocator7.Name = "ComboAvailableBoxLocator7";
            this.ComboAvailableBoxLocator7.Size = new System.Drawing.Size(120, 21);
            this.ComboAvailableBoxLocator7.TabIndex = 5;
            this.ComboAvailableBoxLocator7.Visible = false;
            //
            //ComboAvailableChannelLocator7
            //
            this.ComboAvailableChannelLocator7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ComboAvailableChannelLocator7.Enabled = false;
            this.ComboAvailableChannelLocator7.Location = new System.Drawing.Point(527, 353);
            this.ComboAvailableChannelLocator7.Name = "ComboAvailableChannelLocator7";
            this.ComboAvailableChannelLocator7.Size = new System.Drawing.Size(105, 21);
            this.ComboAvailableChannelLocator7.TabIndex = 438;
            this.ComboAvailableChannelLocator7.Visible = false;
            //
            //MenuStripMain
            //
            this.MenuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuFile, this.MenuEdit, this.MenuHelp});
            this.MenuStripMain.Location = new System.Drawing.Point(0, 0);
            this.MenuStripMain.Name = "MenuStripMain";
            this.MenuStripMain.Size = new System.Drawing.Size(720, 24);
            this.MenuStripMain.TabIndex = 3;
            //
            //MenuFile
            //
            this.MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuFileMsgOutLoad, this.MenuFileFilterLoad, this.MenuFilePeriodicMessageLoad, this.MenuFileSpace1, this.MenuFileMsgInSaveSelected, this.MenuFileMsgInSaveAll, this.MenuFileMsgOutSave, this.MenuFileFilterSave, this.MenuFilePeriodicMessageSave, this.MenuFileSpace2, this.MenuFileExit});
            this.MenuFile.Name = "MenuFile";
            this.MenuFile.Size = new System.Drawing.Size(37, 20);
            this.MenuFile.Text = "&File";
            //
            //MenuFileMsgOutLoad
            //
            this.MenuFileMsgOutLoad.Name = "MenuFileMsgOutLoad";
            this.MenuFileMsgOutLoad.Size = new System.Drawing.Size(257, 22);
            this.MenuFileMsgOutLoad.Text = "Load Outgoing Message Set...";
            //
            //MenuFileFilterLoad
            //
            this.MenuFileFilterLoad.Name = "MenuFileFilterLoad";
            this.MenuFileFilterLoad.Size = new System.Drawing.Size(257, 22);
            this.MenuFileFilterLoad.Text = "Load Filters...";
            //
            //MenuFilePeriodicMessageLoad
            //
            this.MenuFilePeriodicMessageLoad.Name = "MenuFilePeriodicMessageLoad";
            this.MenuFilePeriodicMessageLoad.Size = new System.Drawing.Size(257, 22);
            this.MenuFilePeriodicMessageLoad.Text = "Load Periodic Messages...";
            //
            //MenuFileSpace1
            //
            this.MenuFileSpace1.Name = "MenuFileSpace1";
            this.MenuFileSpace1.Size = new System.Drawing.Size(254, 6);
            //
            //MenuFileMsgInSaveSelected
            //
            this.MenuFileMsgInSaveSelected.Name = "MenuFileMsgInSaveSelected";
            this.MenuFileMsgInSaveSelected.Size = new System.Drawing.Size(257, 22);
            this.MenuFileMsgInSaveSelected.Text = "Save Selected Incoming Message...";
            //
            //MenuFileMsgInSaveAll
            //
            this.MenuFileMsgInSaveAll.Name = "MenuFileMsgInSaveAll";
            this.MenuFileMsgInSaveAll.Size = new System.Drawing.Size(257, 22);
            this.MenuFileMsgInSaveAll.Text = "Save All Incoming Messages...";
            //
            //MenuFileMsgOutSave
            //
            this.MenuFileMsgOutSave.Name = "MenuFileMsgOutSave";
            this.MenuFileMsgOutSave.Size = new System.Drawing.Size(257, 22);
            this.MenuFileMsgOutSave.Text = "Save Outgoing Message Set...";
            //
            //MenuFileFilterSave
            //
            this.MenuFileFilterSave.Name = "MenuFileFilterSave";
            this.MenuFileFilterSave.Size = new System.Drawing.Size(257, 22);
            this.MenuFileFilterSave.Text = "Save Filters...";
            //
            //MenuFilePeriodicMessageSave
            //
            this.MenuFilePeriodicMessageSave.Name = "MenuFilePeriodicMessageSave";
            this.MenuFilePeriodicMessageSave.Size = new System.Drawing.Size(257, 22);
            this.MenuFilePeriodicMessageSave.Text = "Save Periodic Messages...";
            //
            //MenuFileSpace2
            //
            this.MenuFileSpace2.Name = "MenuFileSpace2";
            this.MenuFileSpace2.Size = new System.Drawing.Size(254, 6);
            //
            //MenuFileExit
            //
            this.MenuFileExit.Name = "MenuFileExit";
            this.MenuFileExit.Size = new System.Drawing.Size(257, 22);
            this.MenuFileExit.Text = "E&xit";
            //
            //MenuEdit
            //
            this.MenuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuEditUndo, this.MenuEditSpace1, this.MenuEditCut, this.MenuEditCopy, this.MenuEditCopyLine, this.MenuEditMsgInCopyData, this.MenuEditPaste, this.MenuEditDelete, this.MenuEditSpace2, this.MenuEditMsgInMakeFilter, this.MenuEditFlagsEdit, this.MenuEditRxStatus, this.MenuEditFlagsClear, this.MenuEditFlagsSetDefault, this.MenuEditMsgOutEditMessage, this.MenuEditMsgOutAddMessage, this.MenuEditMsgOutCopyToScratchPad, this.MenuEditScratchAddToOutgoingMessageSet, this.MenuEditMsgOutMakePeriodicMessage, this.MenuEditSelectAll, this.MenuEditClear});
            this.MenuEdit.Name = "MenuEdit";
            this.MenuEdit.Size = new System.Drawing.Size(39, 20);
            this.MenuEdit.Text = "&Edit";
            //
            //MenuEditUndo
            //
            this.MenuEditUndo.Name = "MenuEditUndo";
            this.MenuEditUndo.ShortcutKeys = (System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z);
            this.MenuEditUndo.Size = new System.Drawing.Size(227, 22);
            this.MenuEditUndo.Text = "&Undo";
            //
            //MenuEditSpace1
            //
            this.MenuEditSpace1.Name = "MenuEditSpace1";
            this.MenuEditSpace1.Size = new System.Drawing.Size(224, 6);
            //
            //MenuEditCut
            //
            this.MenuEditCut.Name = "MenuEditCut";
            this.MenuEditCut.ShortcutKeys = (System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X);
            this.MenuEditCut.Size = new System.Drawing.Size(227, 22);
            this.MenuEditCut.Text = "Cu&t";
            //
            //MenuEditCopy
            //
            this.MenuEditCopy.Name = "MenuEditCopy";
            this.MenuEditCopy.ShortcutKeys = (System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C);
            this.MenuEditCopy.Size = new System.Drawing.Size(227, 22);
            this.MenuEditCopy.Text = "&Copy";
            //
            //MenuEditCopyLine
            //
            this.MenuEditCopyLine.Name = "MenuEditCopyLine";
            this.MenuEditCopyLine.Size = new System.Drawing.Size(227, 22);
            this.MenuEditCopyLine.Text = "Copy Line";
            //
            //MenuEditMsgInCopyData
            //
            this.MenuEditMsgInCopyData.Name = "MenuEditMsgInCopyData";
            this.MenuEditMsgInCopyData.Size = new System.Drawing.Size(227, 22);
            this.MenuEditMsgInCopyData.Text = "Copy Data";
            //
            //MenuEditPaste
            //
            this.MenuEditPaste.Name = "MenuEditPaste";
            this.MenuEditPaste.ShortcutKeys = (System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V);
            this.MenuEditPaste.Size = new System.Drawing.Size(227, 22);
            this.MenuEditPaste.Text = "&Paste";
            //
            //MenuEditDelete
            //
            this.MenuEditDelete.Name = "MenuEditDelete";
            this.MenuEditDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.MenuEditDelete.Size = new System.Drawing.Size(227, 22);
            this.MenuEditDelete.Text = "&Delete";
            //
            //MenuEditSpace2
            //
            this.MenuEditSpace2.Name = "MenuEditSpace2";
            this.MenuEditSpace2.Size = new System.Drawing.Size(224, 6);
            //
            //MenuEditMsgInMakeFilter
            //
            this.MenuEditMsgInMakeFilter.Name = "MenuEditMsgInMakeFilter";
            this.MenuEditMsgInMakeFilter.Size = new System.Drawing.Size(227, 22);
            this.MenuEditMsgInMakeFilter.Text = "Make Filter";
            //
            //MenuEditFlagsEdit
            //
            this.MenuEditFlagsEdit.Name = "MenuEditFlagsEdit";
            this.MenuEditFlagsEdit.Size = new System.Drawing.Size(227, 22);
            this.MenuEditFlagsEdit.Text = "Edit Flags";
            //
            //MenuEditRxStatus
            //
            this.MenuEditRxStatus.Name = "MenuEditRxStatus";
            this.MenuEditRxStatus.Size = new System.Drawing.Size(227, 22);
            this.MenuEditRxStatus.Text = "Rx Status";
            //
            //MenuEditFlagsClear
            //
            this.MenuEditFlagsClear.Name = "MenuEditFlagsClear";
            this.MenuEditFlagsClear.Size = new System.Drawing.Size(227, 22);
            this.MenuEditFlagsClear.Text = "Clear Flags";
            //
            //MenuEditFlagsSetDefault
            //
            this.MenuEditFlagsSetDefault.Name = "MenuEditFlagsSetDefault";
            this.MenuEditFlagsSetDefault.Size = new System.Drawing.Size(227, 22);
            this.MenuEditFlagsSetDefault.Text = "Set Default Flags";
            //
            //MenuEditMsgOutEditMessage
            //
            this.MenuEditMsgOutEditMessage.Name = "MenuEditMsgOutEditMessage";
            this.MenuEditMsgOutEditMessage.Size = new System.Drawing.Size(227, 22);
            this.MenuEditMsgOutEditMessage.Text = "Edit Message";
            //
            //MenuEditMsgOutAddMessage
            //
            this.MenuEditMsgOutAddMessage.Name = "MenuEditMsgOutAddMessage";
            this.MenuEditMsgOutAddMessage.Size = new System.Drawing.Size(227, 22);
            this.MenuEditMsgOutAddMessage.Text = "Add Message";
            //
            //MenuEditMsgOutCopyToScratchPad
            //
            this.MenuEditMsgOutCopyToScratchPad.Name = "MenuEditMsgOutCopyToScratchPad";
            this.MenuEditMsgOutCopyToScratchPad.Size = new System.Drawing.Size(227, 22);
            this.MenuEditMsgOutCopyToScratchPad.Text = "Copy to Scratch Pad";
            //
            //MenuEditScratchAddToOutgoingMessageSet
            //
            this.MenuEditScratchAddToOutgoingMessageSet.Name = "MenuEditScratchAddToOutgoingMessageSet";
            this.MenuEditScratchAddToOutgoingMessageSet.Size = new System.Drawing.Size(227, 22);
            this.MenuEditScratchAddToOutgoingMessageSet.Text = "Add to Outgoing Mesage Set";
            //
            //MenuEditMsgOutMakePeriodicMessage
            //
            this.MenuEditMsgOutMakePeriodicMessage.Name = "MenuEditMsgOutMakePeriodicMessage";
            this.MenuEditMsgOutMakePeriodicMessage.Size = new System.Drawing.Size(227, 22);
            this.MenuEditMsgOutMakePeriodicMessage.Text = "Make Periodic Message";
            //
            //MenuEditSelectAll
            //
            this.MenuEditSelectAll.Name = "MenuEditSelectAll";
            this.MenuEditSelectAll.ShortcutKeys = (System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A);
            this.MenuEditSelectAll.Size = new System.Drawing.Size(227, 22);
            this.MenuEditSelectAll.Text = "Select &All";
            //
            //MenuEditClear
            //
            this.MenuEditClear.Name = "MenuEditClear";
            this.MenuEditClear.Size = new System.Drawing.Size(227, 22);
            this.MenuEditClear.Text = "Clear";
            //
            //MenuHelp
            //
            this.MenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuHelpContents, this.MenuHelpAbout});
            this.MenuHelp.Name = "MenuHelp";
            this.MenuHelp.Size = new System.Drawing.Size(44, 20);
            this.MenuHelp.Text = "Help";
            //
            //MenuHelpContents
            //
            this.MenuHelpContents.Enabled = false;
            this.MenuHelpContents.Name = "MenuHelpContents";
            this.MenuHelpContents.Size = new System.Drawing.Size(131, 22);
            this.MenuHelpContents.Text = "Contents...";
            //
            //MenuHelpAbout
            //
            this.MenuHelpAbout.Name = "MenuHelpAbout";
            this.MenuHelpAbout.Size = new System.Drawing.Size(131, 22);
            this.MenuHelpAbout.Text = "About...";
            //
            //MenuMsgInCopyLine
            //
            this.MenuMsgInCopyLine.Enabled = false;
            this.MenuMsgInCopyLine.Name = "MenuMsgInCopyLine";
            this.MenuMsgInCopyLine.Size = new System.Drawing.Size(145, 22);
            this.MenuMsgInCopyLine.Text = "Copy Line";
            //
            //MenuMsgInCopyData
            //
            this.MenuMsgInCopyData.Enabled = false;
            this.MenuMsgInCopyData.Name = "MenuMsgInCopyData";
            this.MenuMsgInCopyData.Size = new System.Drawing.Size(145, 22);
            this.MenuMsgInCopyData.Text = "Copy Data";
            //
            //MenuMsgInMakeFilter
            //
            this.MenuMsgInMakeFilter.Enabled = false;
            this.MenuMsgInMakeFilter.Name = "MenuMsgInMakeFilter";
            this.MenuMsgInMakeFilter.Size = new System.Drawing.Size(145, 22);
            this.MenuMsgInMakeFilter.Text = "Make Filter";
            //
            //MenuMsgInRxStatus
            //
            this.MenuMsgInRxStatus.Name = "MenuMsgInRxStatus";
            this.MenuMsgInRxStatus.Size = new System.Drawing.Size(145, 22);
            this.MenuMsgInRxStatus.Text = "Rx Status";
            //
            //MenuMsgInSelectAll
            //
            this.MenuMsgInSelectAll.Enabled = false;
            this.MenuMsgInSelectAll.Name = "MenuMsgInSelectAll";
            this.MenuMsgInSelectAll.Size = new System.Drawing.Size(145, 22);
            this.MenuMsgInSelectAll.Text = "Select All";
            //
            //MenuMsgInSpace1
            //
            this.MenuMsgInSpace1.Name = "MenuMsgInSpace1";
            this.MenuMsgInSpace1.Size = new System.Drawing.Size(142, 6);
            //
            //MenuMsgInSaveSelected
            //
            this.MenuMsgInSaveSelected.Enabled = false;
            this.MenuMsgInSaveSelected.Name = "MenuMsgInSaveSelected";
            this.MenuMsgInSaveSelected.Size = new System.Drawing.Size(145, 22);
            this.MenuMsgInSaveSelected.Text = "Save Selected";
            //
            //MenuMsgInSaveAll
            //
            this.MenuMsgInSaveAll.Enabled = false;
            this.MenuMsgInSaveAll.Name = "MenuMsgInSaveAll";
            this.MenuMsgInSaveAll.Size = new System.Drawing.Size(145, 22);
            this.MenuMsgInSaveAll.Text = "Save All";
            //
            //MenuMsgInClear
            //
            this.MenuMsgInClear.Enabled = false;
            this.MenuMsgInClear.Name = "MenuMsgInClear";
            this.MenuMsgInClear.Size = new System.Drawing.Size(145, 22);
            this.MenuMsgInClear.Text = "Clear";
            //
            //MenuMsgOutDelete
            //
            this.MenuMsgOutDelete.Enabled = false;
            this.MenuMsgOutDelete.Name = "MenuMsgOutDelete";
            this.MenuMsgOutDelete.Size = new System.Drawing.Size(198, 22);
            this.MenuMsgOutDelete.Text = "Delete";
            //
            //MenuMsgOutEditMessage
            //
            this.MenuMsgOutEditMessage.Enabled = false;
            this.MenuMsgOutEditMessage.Name = "MenuMsgOutEditMessage";
            this.MenuMsgOutEditMessage.Size = new System.Drawing.Size(198, 22);
            this.MenuMsgOutEditMessage.Text = "Edit Message";
            //
            //MenuMsgOutAddMessage
            //
            this.MenuMsgOutAddMessage.Enabled = false;
            this.MenuMsgOutAddMessage.Name = "MenuMsgOutAddMessage";
            this.MenuMsgOutAddMessage.Size = new System.Drawing.Size(198, 22);
            this.MenuMsgOutAddMessage.Text = "Add Message";
            //
            //MenuMsgOutCopyToScratchPad
            //
            this.MenuMsgOutCopyToScratchPad.Name = "MenuMsgOutCopyToScratchPad";
            this.MenuMsgOutCopyToScratchPad.Size = new System.Drawing.Size(198, 22);
            this.MenuMsgOutCopyToScratchPad.Text = "Copy to Scratch Pad";
            //
            //MenuMsgOutMakePeriodicMessage
            //
            this.MenuMsgOutMakePeriodicMessage.Name = "MenuMsgOutMakePeriodicMessage";
            this.MenuMsgOutMakePeriodicMessage.Size = new System.Drawing.Size(198, 22);
            this.MenuMsgOutMakePeriodicMessage.Text = "Make Periodic Message";
            //
            //MenuMsgOutSpace1
            //
            this.MenuMsgOutSpace1.Name = "MenuMsgOutSpace1";
            this.MenuMsgOutSpace1.Size = new System.Drawing.Size(195, 6);
            //
            //MenuMsgOutSave
            //
            this.MenuMsgOutSave.Enabled = false;
            this.MenuMsgOutSave.Name = "MenuMsgOutSave";
            this.MenuMsgOutSave.Size = new System.Drawing.Size(198, 22);
            this.MenuMsgOutSave.Text = "Save";
            //
            //MenuMsgOutLoad
            //
            this.MenuMsgOutLoad.Enabled = false;
            this.MenuMsgOutLoad.Name = "MenuMsgOutLoad";
            this.MenuMsgOutLoad.Size = new System.Drawing.Size(198, 22);
            this.MenuMsgOutLoad.Text = "Load";
            //
            //MenuMsgOutClear
            //
            this.MenuMsgOutClear.Enabled = false;
            this.MenuMsgOutClear.Name = "MenuMsgOutClear";
            this.MenuMsgOutClear.Size = new System.Drawing.Size(198, 22);
            this.MenuMsgOutClear.Text = "Clear";
            //
            //MenuFlagsUndo
            //
            this.MenuFlagsUndo.Enabled = false;
            this.MenuFlagsUndo.Name = "MenuFlagsUndo";
            this.MenuFlagsUndo.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsUndo.Text = "Undo";
            //
            //MenuFlagsSpace1
            //
            this.MenuFlagsSpace1.Name = "MenuFlagsSpace1";
            this.MenuFlagsSpace1.Size = new System.Drawing.Size(158, 6);
            //
            //MenuFlagsCut
            //
            this.MenuFlagsCut.Enabled = false;
            this.MenuFlagsCut.Name = "MenuFlagsCut";
            this.MenuFlagsCut.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsCut.Text = "Cut";
            //
            //MenuFlagsCopy
            //
            this.MenuFlagsCopy.Enabled = false;
            this.MenuFlagsCopy.Name = "MenuFlagsCopy";
            this.MenuFlagsCopy.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsCopy.Text = "Copy";
            //
            //MenuFlagsPaste
            //
            this.MenuFlagsPaste.Enabled = false;
            this.MenuFlagsPaste.Name = "MenuFlagsPaste";
            this.MenuFlagsPaste.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsPaste.Text = "Paste";
            //
            //MenuFlagsSpace2
            //
            this.MenuFlagsSpace2.Name = "MenuFlagsSpace2";
            this.MenuFlagsSpace2.Size = new System.Drawing.Size(158, 6);
            //
            //MenuFlagsSelectAll
            //
            this.MenuFlagsSelectAll.Name = "MenuFlagsSelectAll";
            this.MenuFlagsSelectAll.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsSelectAll.Text = "Select All";
            //
            //MenuFlagsSpace3
            //
            this.MenuFlagsSpace3.Name = "MenuFlagsSpace3";
            this.MenuFlagsSpace3.Size = new System.Drawing.Size(158, 6);
            //
            //MenuFlagsEdit
            //
            this.MenuFlagsEdit.Name = "MenuFlagsEdit";
            this.MenuFlagsEdit.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsEdit.Text = "Edit Flags";
            //
            //MenuFlagsClear
            //
            this.MenuFlagsClear.Name = "MenuFlagsClear";
            this.MenuFlagsClear.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsClear.Text = "Clear Flags";
            //
            //MenuFlagsSetDefault
            //
            this.MenuFlagsSetDefault.Name = "MenuFlagsSetDefault";
            this.MenuFlagsSetDefault.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsSetDefault.Text = "Set Default Flags";
            //
            //StatusStrip1
            //
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.ToolStripStatusLabel1, this.ToolStripStatusLabel2});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 441);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.ShowItemToolTips = true;
            this.StatusStrip1.Size = new System.Drawing.Size(720, 25);
            this.StatusStrip1.SizingGrip = false;
            this.StatusStrip1.TabIndex = 1;
            //
            //ToolStripStatusLabel1
            //
            this.ToolStripStatusLabel1.AutoSize = false;
            this.ToolStripStatusLabel1.BorderSides = (System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom);
            this.ToolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.ToolStripStatusLabel1.Margin = new System.Windows.Forms.Padding(0);
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(200, 25);
            this.ToolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            //
            //ToolStripStatusLabel2
            //
            this.ToolStripStatusLabel2.AutoSize = false;
            this.ToolStripStatusLabel2.BorderSides = (System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom);
            this.ToolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.ToolStripStatusLabel2.Margin = new System.Windows.Forms.Padding(0);
            this.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2";
            this.ToolStripStatusLabel2.Size = new System.Drawing.Size(500, 25);
            this.ToolStripStatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            //
            //TabControl1
            //
            this.TabControl1.Controls.Add(this.TabPageConnect);
            this.TabControl1.Controls.Add(this.TabPageMessages);
            this.TabControl1.Controls.Add(this.TabPagePeriodicMessages);
            this.TabControl1.Controls.Add(this.TabPageFilters);
            this.TabControl1.Controls.Add(this.TabPageConfig);
            this.TabControl1.Controls.Add(this.TabPageInit);
            this.TabControl1.Controls.Add(this.TabPageFunctionalMessages);
            this.TabControl1.Controls.Add(this.TabPageAnalog);
            this.TabControl1.Controls.Add(this.TabPageResults);
            this.TabControl1.ItemSize = new System.Drawing.Size(47, 18);
            this.TabControl1.Location = new System.Drawing.Point(0, 24);
            this.TabControl1.Multiline = true;
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(722, 421);
            this.TabControl1.TabIndex = 0;
            //
            //TabPageConnect
            //
            this.TabPageConnect.Controls.Add(this.GroupBoxDevices);
            this.TabPageConnect.Controls.Add(this.GroupBoxAPIs);
            this.TabPageConnect.Controls.Add(this.GroupBoxConnect);
            this.TabPageConnect.Controls.Add(this.GroupBoxJ2534Info);
            this.TabPageConnect.Location = new System.Drawing.Point(4, 22);
            this.TabPageConnect.Margin = new System.Windows.Forms.Padding(0);
            this.TabPageConnect.Name = "TabPageConnect";
            this.TabPageConnect.Size = new System.Drawing.Size(714, 395);
            this.TabPageConnect.TabIndex = 0;
            this.TabPageConnect.Text = "Connect";
            //
            //GroupBoxDevices
            //
            this.GroupBoxDevices.Controls.Add(this.ComboBoxDevice);
            this.GroupBoxDevices.Controls.Add(this.ButtonDiscover);
            this.GroupBoxDevices.Controls.Add(this.ButtonCloseBox);
            this.GroupBoxDevices.Controls.Add(this.ButtonOpenBox);
            this.GroupBoxDevices.Controls.Add(this.LabelDeviceInfo);
            this.GroupBoxDevices.Controls.Add(this.LabelComboDevice);
            this.GroupBoxDevices.Location = new System.Drawing.Point(428, 4);
            this.GroupBoxDevices.Name = "GroupBoxDevices";
            this.GroupBoxDevices.Size = new System.Drawing.Size(281, 177);
            this.GroupBoxDevices.TabIndex = 1;
            this.GroupBoxDevices.TabStop = false;
            this.GroupBoxDevices.Text = "J2534 Devices:";
            //
            //ComboBoxDevice
            //
            this.ComboBoxDevice.Enabled = false;
            this.ComboBoxDevice.Location = new System.Drawing.Point(16, 32);
            this.ComboBoxDevice.Name = "ComboBoxDevice";
            this.ComboBoxDevice.Size = new System.Drawing.Size(209, 21);
            this.ComboBoxDevice.TabIndex = 3;
            //
            //ButtonDiscover
            //
            this.ButtonDiscover.Enabled = false;
            this.ButtonDiscover.Location = new System.Drawing.Point(16, 64);
            this.ButtonDiscover.Name = "ButtonDiscover";
            this.ButtonDiscover.Size = new System.Drawing.Size(73, 25);
            this.ButtonDiscover.TabIndex = 0;
            this.ButtonDiscover.Text = "Discover";
            this.ButtonDiscover.UseVisualStyleBackColor = true;
            this.ButtonDiscover.Visible = false;
            //
            //ButtonCloseBox
            //
            this.ButtonCloseBox.Location = new System.Drawing.Point(168, 64);
            this.ButtonCloseBox.Name = "ButtonCloseBox";
            this.ButtonCloseBox.Size = new System.Drawing.Size(57, 25);
            this.ButtonCloseBox.TabIndex = 2;
            this.ButtonCloseBox.Text = "Close";
            this.ButtonCloseBox.UseVisualStyleBackColor = true;
            //
            //ButtonOpenBox
            //
            this.ButtonOpenBox.Location = new System.Drawing.Point(104, 64);
            this.ButtonOpenBox.Name = "ButtonOpenBox";
            this.ButtonOpenBox.Size = new System.Drawing.Size(57, 25);
            this.ButtonOpenBox.TabIndex = 1;
            this.ButtonOpenBox.Text = "Open";
            this.ButtonOpenBox.UseVisualStyleBackColor = true;
            //
            //LabelDeviceInfo
            //
            this.LabelDeviceInfo.Location = new System.Drawing.Point(16, 104);
            this.LabelDeviceInfo.Name = "LabelDeviceInfo";
            this.LabelDeviceInfo.Size = new System.Drawing.Size(225, 65);
            this.LabelDeviceInfo.TabIndex = 5;
            this.LabelDeviceInfo.Text = "LabelDeviceInfo";
            //
            //LabelComboDevice
            //
            this.LabelComboDevice.AutoSize = true;
            this.LabelComboDevice.Location = new System.Drawing.Point(16, 16);
            this.LabelComboDevice.Name = "LabelComboDevice";
            this.LabelComboDevice.Size = new System.Drawing.Size(44, 13);
            this.LabelComboDevice.TabIndex = 4;
            this.LabelComboDevice.Text = "Device:";
            //
            //GroupBoxAPIs
            //
            this.GroupBoxAPIs.Controls.Add(this.ComboBoxAPI);
            this.GroupBoxAPIs.Controls.Add(this.ButtonLoadDLL);
            this.GroupBoxAPIs.Controls.Add(this.TextBoxDllPath);
            this.GroupBoxAPIs.Controls.Add(this.LabelAPI);
            this.GroupBoxAPIs.Controls.Add(this.LabelVendor);
            this.GroupBoxAPIs.Controls.Add(this.LabelDevice);
            this.GroupBoxAPIs.Controls.Add(this.LabelDllPath);
            this.GroupBoxAPIs.Controls.Add(this.LabelDllName);
            this.GroupBoxAPIs.Location = new System.Drawing.Point(4, 4);
            this.GroupBoxAPIs.Name = "GroupBoxAPIs";
            this.GroupBoxAPIs.Size = new System.Drawing.Size(409, 177);
            this.GroupBoxAPIs.TabIndex = 0;
            this.GroupBoxAPIs.TabStop = false;
            this.GroupBoxAPIs.Text = "J2534 APIs:";
            //
            //ComboBoxAPI
            //
            this.ComboBoxAPI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxAPI.Location = new System.Drawing.Point(10, 32);
            this.ComboBoxAPI.Name = "ComboBoxAPI";
            this.ComboBoxAPI.Size = new System.Drawing.Size(321, 21);
            this.ComboBoxAPI.TabIndex = 0;
            //
            //ButtonLoadDLL
            //
            this.ButtonLoadDLL.Location = new System.Drawing.Point(336, 69);
            this.ButtonLoadDLL.Name = "ButtonLoadDLL";
            this.ButtonLoadDLL.Size = new System.Drawing.Size(65, 25);
            this.ButtonLoadDLL.TabIndex = 2;
            this.ButtonLoadDLL.Text = "Load DLL";
            this.ButtonLoadDLL.UseVisualStyleBackColor = true;
            //
            //TextBoxDllPath
            //
            this.TextBoxDllPath.AcceptsReturn = true;
            this.TextBoxDllPath.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxDllPath.Enabled = false;
            this.TextBoxDllPath.Location = new System.Drawing.Point(10, 72);
            this.TextBoxDllPath.MaxLength = 0;
            this.TextBoxDllPath.Name = "TextBoxDllPath";
            this.TextBoxDllPath.Size = new System.Drawing.Size(321, 20);
            this.TextBoxDllPath.TabIndex = 1;
            //
            //ContextMenuStripTextBox
            //
            this.ContextMenuStripTextBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuTextBoxUndo, this.MenuTextBoxSeparator1, this.MenuTextBoxCut, this.MenuTextBoxCopy, this.MenuTextBoxPaste, this.MenuTextBoxDelete, this.MenuTextBoxSeparator2, this.MenuTextBoxSelectAll});
            this.ContextMenuStripTextBox.Name = "ContextMenuStripTextBox";
            this.ContextMenuStripTextBox.Size = new System.Drawing.Size(123, 148);
            //
            //MenuTextBoxUndo
            //
            this.MenuTextBoxUndo.Name = "MenuTextBoxUndo";
            this.MenuTextBoxUndo.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxUndo.Text = "Undo";
            //
            //MenuTextBoxSeparator1
            //
            this.MenuTextBoxSeparator1.Name = "MenuTextBoxSeparator1";
            this.MenuTextBoxSeparator1.Size = new System.Drawing.Size(119, 6);
            //
            //MenuTextBoxCut
            //
            this.MenuTextBoxCut.Name = "MenuTextBoxCut";
            this.MenuTextBoxCut.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxCut.Text = "Cut";
            //
            //MenuTextBoxCopy
            //
            this.MenuTextBoxCopy.Name = "MenuTextBoxCopy";
            this.MenuTextBoxCopy.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxCopy.Text = "Copy";
            //
            //MenuTextBoxPaste
            //
            this.MenuTextBoxPaste.Name = "MenuTextBoxPaste";
            this.MenuTextBoxPaste.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxPaste.Text = "Paste";
            //
            //MenuTextBoxDelete
            //
            this.MenuTextBoxDelete.Name = "MenuTextBoxDelete";
            this.MenuTextBoxDelete.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxDelete.Text = "Delete";
            //
            //MenuTextBoxSeparator2
            //
            this.MenuTextBoxSeparator2.Name = "MenuTextBoxSeparator2";
            this.MenuTextBoxSeparator2.Size = new System.Drawing.Size(119, 6);
            //
            //MenuTextBoxSelectAll
            //
            this.MenuTextBoxSelectAll.Name = "MenuTextBoxSelectAll";
            this.MenuTextBoxSelectAll.Size = new System.Drawing.Size(122, 22);
            this.MenuTextBoxSelectAll.Text = "Select All";
            //
            //LabelAPI
            //
            this.LabelAPI.AutoSize = true;
            this.LabelAPI.Location = new System.Drawing.Point(10, 16);
            this.LabelAPI.Name = "LabelAPI";
            this.LabelAPI.Size = new System.Drawing.Size(34, 13);
            this.LabelAPI.TabIndex = 3;
            this.LabelAPI.Text = "Type:";
            //
            //LabelVendor
            //
            this.LabelVendor.Location = new System.Drawing.Point(10, 96);
            this.LabelVendor.Name = "LabelVendor";
            this.LabelVendor.Size = new System.Drawing.Size(393, 17);
            this.LabelVendor.TabIndex = 5;
            this.LabelVendor.Text = "LabelVendor";
            //
            //LabelDevice
            //
            this.LabelDevice.Location = new System.Drawing.Point(10, 112);
            this.LabelDevice.Name = "LabelDevice";
            this.LabelDevice.Size = new System.Drawing.Size(393, 17);
            this.LabelDevice.TabIndex = 6;
            this.LabelDevice.Text = "LabelDevice";
            //
            //LabelDllPath
            //
            this.LabelDllPath.AutoSize = true;
            this.LabelDllPath.Location = new System.Drawing.Point(10, 56);
            this.LabelDllPath.Name = "LabelDllPath";
            this.LabelDllPath.Size = new System.Drawing.Size(67, 13);
            this.LabelDllPath.TabIndex = 4;
            this.LabelDllPath.Text = "Path to DLL:";
            //
            //LabelDllName
            //
            this.LabelDllName.Location = new System.Drawing.Point(10, 128);
            this.LabelDllName.Name = "LabelDllName";
            this.LabelDllName.Size = new System.Drawing.Size(393, 17);
            this.LabelDllName.TabIndex = 7;
            this.LabelDllName.Text = "LabelDllName";
            //
            //GroupBoxConnect
            //
            this.GroupBoxConnect.Controls.Add(this.ComboBoxPins);
            this.GroupBoxConnect.Controls.Add(this.ComboBoxConnector);
            this.GroupBoxConnect.Controls.Add(this.CheckBoxMixedMode);
            this.GroupBoxConnect.Controls.Add(this.TextBoxConnectFlags);
            this.GroupBoxConnect.Controls.Add(this.ComboBoxBaudRate);
            this.GroupBoxConnect.Controls.Add(this.ButtonConnect);
            this.GroupBoxConnect.Controls.Add(this.ButtonDisconnect);
            this.GroupBoxConnect.Controls.Add(this.ComboBoxConnectChannel);
            this.GroupBoxConnect.Controls.Add(this.LabelPins);
            this.GroupBoxConnect.Controls.Add(this.LabelConn);
            this.GroupBoxConnect.Controls.Add(this.LabelConnectFlags);
            this.GroupBoxConnect.Controls.Add(this.LabelDeviceCombo0);
            this.GroupBoxConnect.Controls.Add(this.LabelBaud);
            this.GroupBoxConnect.Controls.Add(this.LabelConnectChannel);
            this.GroupBoxConnect.Controls.Add(this.ComboAvailableBoxLocator0);
            this.GroupBoxConnect.Controls.Add(this.ComboAvailableChannelLocator0);
            this.GroupBoxConnect.Location = new System.Drawing.Point(428, 188);
            this.GroupBoxConnect.Name = "GroupBoxConnect";
            this.GroupBoxConnect.Size = new System.Drawing.Size(281, 201);
            this.GroupBoxConnect.TabIndex = 3;
            this.GroupBoxConnect.TabStop = false;
            this.GroupBoxConnect.Text = "Connect:";
            //
            //ComboBoxPins
            //
            this.ComboBoxPins.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPins.Location = new System.Drawing.Point(96, 88);
            this.ComboBoxPins.Name = "ComboBoxPins";
            this.ComboBoxPins.Size = new System.Drawing.Size(81, 21);
            this.ComboBoxPins.TabIndex = 4;
            //
            //ComboBoxConnector
            //
            this.ComboBoxConnector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxConnector.Location = new System.Drawing.Point(16, 88);
            this.ComboBoxConnector.Name = "ComboBoxConnector";
            this.ComboBoxConnector.Size = new System.Drawing.Size(73, 21);
            this.ComboBoxConnector.TabIndex = 3;
            //
            //CheckBoxMixedMode
            //
            this.CheckBoxMixedMode.Location = new System.Drawing.Point(24, 56);
            this.CheckBoxMixedMode.Name = "CheckBoxMixedMode";
            this.CheckBoxMixedMode.Size = new System.Drawing.Size(97, 17);
            this.CheckBoxMixedMode.TabIndex = 8;
            this.CheckBoxMixedMode.Text = "Mixed Mode";
            this.CheckBoxMixedMode.UseVisualStyleBackColor = true;
            //
            //TextBoxConnectFlags
            //
            this.TextBoxConnectFlags.AcceptsReturn = true;
            this.TextBoxConnectFlags.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxConnectFlags.Location = new System.Drawing.Point(184, 128);
            this.TextBoxConnectFlags.MaxLength = 0;
            this.TextBoxConnectFlags.Name = "TextBoxConnectFlags";
            this.TextBoxConnectFlags.Size = new System.Drawing.Size(81, 20);
            this.TextBoxConnectFlags.TabIndex = 6;
            //
            //ContextMenuStripFlags
            //
            this.ContextMenuStripFlags.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuFlagsUndo, this.MenuFlagsSpace1, this.MenuFlagsCut, this.MenuFlagsCopy, this.MenuFlagsPaste, this.MenuFlagsDelete, this.MenuFlagsSpace2, this.MenuFlagsSelectAll, this.MenuFlagsSpace3, this.MenuFlagsEdit, this.MenuFlagsClear, this.MenuFlagsSetDefault});
            this.ContextMenuStripFlags.Name = "ContextMenuStripFlags";
            this.ContextMenuStripFlags.Size = new System.Drawing.Size(162, 220);
            //
            //MenuFlagsDelete
            //
            this.MenuFlagsDelete.Name = "MenuFlagsDelete";
            this.MenuFlagsDelete.Size = new System.Drawing.Size(161, 22);
            this.MenuFlagsDelete.Text = "Delete";
            //
            //ComboBoxBaudRate
            //
            this.ComboBoxBaudRate.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.ComboBoxBaudRate.Location = new System.Drawing.Point(16, 128);
            this.ComboBoxBaudRate.Name = "ComboBoxBaudRate";
            this.ComboBoxBaudRate.Size = new System.Drawing.Size(161, 21);
            this.ComboBoxBaudRate.TabIndex = 5;
            //
            //ButtonConnect
            //
            this.ButtonConnect.Location = new System.Drawing.Point(184, 32);
            this.ButtonConnect.Name = "ButtonConnect";
            this.ButtonConnect.Size = new System.Drawing.Size(81, 25);
            this.ButtonConnect.TabIndex = 1;
            this.ButtonConnect.Text = "Connect";
            this.ButtonConnect.UseVisualStyleBackColor = true;
            //
            //ButtonDisconnect
            //
            this.ButtonDisconnect.Location = new System.Drawing.Point(184, 64);
            this.ButtonDisconnect.Name = "ButtonDisconnect";
            this.ButtonDisconnect.Size = new System.Drawing.Size(81, 25);
            this.ButtonDisconnect.TabIndex = 2;
            this.ButtonDisconnect.Text = "Disconnect";
            this.ButtonDisconnect.UseVisualStyleBackColor = true;
            //
            //ComboBoxConnectChannel
            //
            this.ComboBoxConnectChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxConnectChannel.Location = new System.Drawing.Point(16, 32);
            this.ComboBoxConnectChannel.Name = "ComboBoxConnectChannel";
            this.ComboBoxConnectChannel.Size = new System.Drawing.Size(161, 21);
            this.ComboBoxConnectChannel.TabIndex = 0;
            //
            //LabelPins
            //
            this.LabelPins.AutoSize = true;
            this.LabelPins.Location = new System.Drawing.Point(96, 72);
            this.LabelPins.Name = "LabelPins";
            this.LabelPins.Size = new System.Drawing.Size(30, 13);
            this.LabelPins.TabIndex = 10;
            this.LabelPins.Text = "Pins:";
            //
            //LabelConn
            //
            this.LabelConn.AutoSize = true;
            this.LabelConn.Location = new System.Drawing.Point(16, 72);
            this.LabelConn.Name = "LabelConn";
            this.LabelConn.Size = new System.Drawing.Size(59, 13);
            this.LabelConn.TabIndex = 9;
            this.LabelConn.Text = "Connector:";
            //
            //LabelConnectFlags
            //
            this.LabelConnectFlags.AutoSize = true;
            this.LabelConnectFlags.Location = new System.Drawing.Point(184, 112);
            this.LabelConnectFlags.Name = "LabelConnectFlags";
            this.LabelConnectFlags.Size = new System.Drawing.Size(78, 13);
            this.LabelConnectFlags.TabIndex = 15;
            this.LabelConnectFlags.Text = "Connect Flags:";
            //
            //LabelDeviceCombo0
            //
            this.LabelDeviceCombo0.Location = new System.Drawing.Point(16, 152);
            this.LabelDeviceCombo0.Name = "LabelDeviceCombo0";
            this.LabelDeviceCombo0.Size = new System.Drawing.Size(89, 17);
            this.LabelDeviceCombo0.TabIndex = 12;
            this.LabelDeviceCombo0.Text = "Device:";
            //
            //LabelBaud
            //
            this.LabelBaud.AutoSize = true;
            this.LabelBaud.Location = new System.Drawing.Point(16, 112);
            this.LabelBaud.Name = "LabelBaud";
            this.LabelBaud.Size = new System.Drawing.Size(61, 13);
            this.LabelBaud.TabIndex = 11;
            this.LabelBaud.Text = "Baud Rate:";
            //
            //LabelConnectChannel
            //
            this.LabelConnectChannel.Location = new System.Drawing.Point(16, 16);
            this.LabelConnectChannel.Name = "LabelConnectChannel";
            this.LabelConnectChannel.Size = new System.Drawing.Size(49, 17);
            this.LabelConnectChannel.TabIndex = 7;
            this.LabelConnectChannel.Text = "Channel:";
            //
            //GroupBoxJ2534Info
            //
            this.GroupBoxJ2534Info.Controls.Add(this.LabelProtSupport);
            this.GroupBoxJ2534Info.Controls.Add(this.LabelJ2534Info);
            this.GroupBoxJ2534Info.Location = new System.Drawing.Point(4, 188);
            this.GroupBoxJ2534Info.Name = "GroupBoxJ2534Info";
            this.GroupBoxJ2534Info.Size = new System.Drawing.Size(409, 201);
            this.GroupBoxJ2534Info.TabIndex = 2;
            this.GroupBoxJ2534Info.TabStop = false;
            this.GroupBoxJ2534Info.Text = "J2534 Device Information:";
            //
            //LabelProtSupport
            //
            this.LabelProtSupport.Location = new System.Drawing.Point(10, 101);
            this.LabelProtSupport.Name = "LabelProtSupport";
            this.LabelProtSupport.Size = new System.Drawing.Size(385, 97);
            this.LabelProtSupport.TabIndex = 1;
            this.LabelProtSupport.Text = "LabelProtSupport";
            //
            //LabelJ2534Info
            //
            this.LabelJ2534Info.AutoSize = true;
            this.LabelJ2534Info.Location = new System.Drawing.Point(10, 16);
            this.LabelJ2534Info.Name = "LabelJ2534Info";
            this.LabelJ2534Info.Size = new System.Drawing.Size(80, 13);
            this.LabelJ2534Info.TabIndex = 0;
            this.LabelJ2534Info.Text = "LabelJ2534Info";
            //
            //TabPageMessages
            //
            this.TabPageMessages.Controls.Add(this.ButtonClaimJ1939Address);
            this.TabPageMessages.Controls.Add(this.LabelMsgFlags);
            this.TabPageMessages.Controls.Add(this.LabelChannel1);
            this.TabPageMessages.Controls.Add(this.LabelDeviceCombo1);
            this.TabPageMessages.Controls.Add(this.LvOutLocator);
            this.TabPageMessages.Controls.Add(this.LvInLocator);
            this.TabPageMessages.Controls.Add(this.ComboBoxMessageChannel);
            this.TabPageMessages.Controls.Add(this.CheckBoxPadMessage);
            this.TabPageMessages.Controls.Add(this.TextBoxTimeOut);
            this.TabPageMessages.Controls.Add(this.TextBoxOutFlags);
            this.TabPageMessages.Controls.Add(this.ButtonClearRx);
            this.TabPageMessages.Controls.Add(this.ButtonClearTx);
            this.TabPageMessages.Controls.Add(this.TextBoxMessageOut);
            this.TabPageMessages.Controls.Add(this.ButtonSend);
            this.TabPageMessages.Controls.Add(this.ButtonReceive);
            this.TabPageMessages.Controls.Add(this.TextBoxReadRate);
            this.TabPageMessages.Controls.Add(this.ButtonClearList);
            this.TabPageMessages.Controls.Add(this.LabelScratchPad);
            this.TabPageMessages.Controls.Add(this.LabelTO);
            this.TabPageMessages.Controls.Add(this.LabelReadRate);
            this.TabPageMessages.Controls.Add(this.ComboAvailableBoxLocator1);
            this.TabPageMessages.Controls.Add(this.ComboAvailableChannelLocator1);
            this.TabPageMessages.Location = new System.Drawing.Point(4, 22);
            this.TabPageMessages.Name = "TabPageMessages";
            this.TabPageMessages.Size = new System.Drawing.Size(714, 395);
            this.TabPageMessages.TabIndex = 1;
            this.TabPageMessages.Text = "Messages";
            //
            //ButtonClaimJ1939Address
            //
            this.ButtonClaimJ1939Address.Enabled = false;
            this.ButtonClaimJ1939Address.Location = new System.Drawing.Point(633, 351);
            this.ButtonClaimJ1939Address.Name = "ButtonClaimJ1939Address";
            this.ButtonClaimJ1939Address.Size = new System.Drawing.Size(72, 39);
            this.ButtonClaimJ1939Address.TabIndex = 5;
            this.ButtonClaimJ1939Address.Text = "Claim J1939" + "\r" + "\n" + "Address...";
            this.ButtonClaimJ1939Address.UseVisualStyleBackColor = true;
            this.ButtonClaimJ1939Address.Visible = false;
            //
            //LabelMsgFlags
            //
            this.LabelMsgFlags.AutoSize = true;
            this.LabelMsgFlags.Location = new System.Drawing.Point(242, 224);
            this.LabelMsgFlags.Name = "LabelMsgFlags";
            this.LabelMsgFlags.Size = new System.Drawing.Size(35, 13);
            this.LabelMsgFlags.TabIndex = 18;
            this.LabelMsgFlags.Text = "Flags:";
            //
            //LabelChannel1
            //
            this.LabelChannel1.AutoSize = true;
            this.LabelChannel1.Location = new System.Drawing.Point(134, 224);
            this.LabelChannel1.Name = "LabelChannel1";
            this.LabelChannel1.Size = new System.Drawing.Size(49, 13);
            this.LabelChannel1.TabIndex = 17;
            this.LabelChannel1.Text = "Channel:";
            //
            //LabelDeviceCombo1
            //
            this.LabelDeviceCombo1.AutoSize = true;
            this.LabelDeviceCombo1.Location = new System.Drawing.Point(5, 224);
            this.LabelDeviceCombo1.Name = "LabelDeviceCombo1";
            this.LabelDeviceCombo1.Size = new System.Drawing.Size(44, 13);
            this.LabelDeviceCombo1.TabIndex = 16;
            this.LabelDeviceCombo1.Text = "Device:";
            //
            //LvOutLocator
            //
            this.LvOutLocator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LvOutLocator.Enabled = false;
            this.LvOutLocator.Location = new System.Drawing.Point(8, 261);
            this.LvOutLocator.Name = "LvOutLocator";
            this.LvOutLocator.Size = new System.Drawing.Size(617, 129);
            this.LvOutLocator.TabIndex = 21;
            //
            //LvInLocator
            //
            this.LvInLocator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LvInLocator.Enabled = false;
            this.LvInLocator.Location = new System.Drawing.Point(8, 13);
            this.LvInLocator.Name = "LvInLocator";
            this.LvInLocator.Size = new System.Drawing.Size(617, 201);
            this.LvInLocator.TabIndex = 15;
            //
            //ComboBoxMessageChannel
            //
            this.ComboBoxMessageChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxMessageChannel.Location = new System.Drawing.Point(134, 237);
            this.ComboBoxMessageChannel.Name = "ComboBoxMessageChannel";
            this.ComboBoxMessageChannel.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxMessageChannel.TabIndex = 1;
            //
            //CheckBoxPadMessage
            //
            this.CheckBoxPadMessage.Location = new System.Drawing.Point(633, 277);
            this.CheckBoxPadMessage.Name = "CheckBoxPadMessage";
            this.CheckBoxPadMessage.Size = new System.Drawing.Size(49, 17);
            this.CheckBoxPadMessage.TabIndex = 13;
            this.CheckBoxPadMessage.Text = "Pad";
            this.CheckBoxPadMessage.UseVisualStyleBackColor = true;
            //
            //TextBoxTimeOut
            //
            this.TextBoxTimeOut.AcceptsReturn = true;
            this.TextBoxTimeOut.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxTimeOut.Location = new System.Drawing.Point(632, 325);
            this.TextBoxTimeOut.MaxLength = 0;
            this.TextBoxTimeOut.Name = "TextBoxTimeOut";
            this.TextBoxTimeOut.Size = new System.Drawing.Size(73, 20);
            this.TextBoxTimeOut.TabIndex = 6;
            //
            //TextBoxOutFlags
            //
            this.TextBoxOutFlags.AcceptsReturn = true;
            this.TextBoxOutFlags.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxOutFlags.Location = new System.Drawing.Point(245, 237);
            this.TextBoxOutFlags.MaxLength = 0;
            this.TextBoxOutFlags.Name = "TextBoxOutFlags";
            this.TextBoxOutFlags.Size = new System.Drawing.Size(81, 20);
            this.TextBoxOutFlags.TabIndex = 3;
            //
            //ButtonClearRx
            //
            this.ButtonClearRx.Location = new System.Drawing.Point(632, 13);
            this.ButtonClearRx.Name = "ButtonClearRx";
            this.ButtonClearRx.Size = new System.Drawing.Size(72, 34);
            this.ButtonClearRx.TabIndex = 7;
            this.ButtonClearRx.Text = "Clear" + "\r" + "\n" + "Rx Buffer";
            this.ButtonClearRx.UseVisualStyleBackColor = true;
            //
            //ButtonClearTx
            //
            this.ButtonClearTx.Location = new System.Drawing.Point(632, 53);
            this.ButtonClearTx.Name = "ButtonClearTx";
            this.ButtonClearTx.Size = new System.Drawing.Size(72, 34);
            this.ButtonClearTx.TabIndex = 8;
            this.ButtonClearTx.Text = "Clear" + "\r" + "\n" + "Tx Buffer";
            this.ButtonClearTx.UseVisualStyleBackColor = true;
            //
            //TextBoxMessageOut
            //
            this.TextBoxMessageOut.AcceptsReturn = true;
            this.TextBoxMessageOut.ContextMenuStrip = this.ContextMenuStripScratchPad;
            this.TextBoxMessageOut.Location = new System.Drawing.Point(332, 237);
            this.TextBoxMessageOut.MaxLength = 0;
            this.TextBoxMessageOut.Name = "TextBoxMessageOut";
            this.TextBoxMessageOut.Size = new System.Drawing.Size(293, 20);
            this.TextBoxMessageOut.TabIndex = 4;
            //
            //ContextMenuStripScratchPad
            //
            this.ContextMenuStripScratchPad.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuScratchPadUndo, this.MenuScratchPadSeparator1, this.MenuScratchPadCut, this.MenuScratchPadCopy, this.MenuScratchPadPaste, this.MenuScratchPadDelete, this.MenuScratchPadSeparator2, this.MenuScratchPadSelectAll, this.MenuScratchPadSeparator3, this.MenuScratchPadAddToOutgoingMessageSet});
            this.ContextMenuStripScratchPad.Name = "ContextMenuStripScratchPad";
            this.ContextMenuStripScratchPad.Size = new System.Drawing.Size(224, 176);
            //
            //MenuScratchPadUndo
            //
            this.MenuScratchPadUndo.Name = "MenuScratchPadUndo";
            this.MenuScratchPadUndo.Size = new System.Drawing.Size(223, 22);
            this.MenuScratchPadUndo.Text = "Undo";
            //
            //MenuScratchPadSeparator1
            //
            this.MenuScratchPadSeparator1.Name = "MenuScratchPadSeparator1";
            this.MenuScratchPadSeparator1.Size = new System.Drawing.Size(220, 6);
            //
            //MenuScratchPadCut
            //
            this.MenuScratchPadCut.Name = "MenuScratchPadCut";
            this.MenuScratchPadCut.Size = new System.Drawing.Size(223, 22);
            this.MenuScratchPadCut.Text = "Cut";
            //
            //MenuScratchPadCopy
            //
            this.MenuScratchPadCopy.Name = "MenuScratchPadCopy";
            this.MenuScratchPadCopy.Size = new System.Drawing.Size(223, 22);
            this.MenuScratchPadCopy.Text = "Copy";
            //
            //MenuScratchPadPaste
            //
            this.MenuScratchPadPaste.Name = "MenuScratchPadPaste";
            this.MenuScratchPadPaste.Size = new System.Drawing.Size(223, 22);
            this.MenuScratchPadPaste.Text = "Paste";
            //
            //MenuScratchPadDelete
            //
            this.MenuScratchPadDelete.Name = "MenuScratchPadDelete";
            this.MenuScratchPadDelete.Size = new System.Drawing.Size(223, 22);
            this.MenuScratchPadDelete.Text = "Delete";
            //
            //MenuScratchPadSeparator2
            //
            this.MenuScratchPadSeparator2.Name = "MenuScratchPadSeparator2";
            this.MenuScratchPadSeparator2.Size = new System.Drawing.Size(220, 6);
            //
            //MenuScratchPadSelectAll
            //
            this.MenuScratchPadSelectAll.Name = "MenuScratchPadSelectAll";
            this.MenuScratchPadSelectAll.Size = new System.Drawing.Size(223, 22);
            this.MenuScratchPadSelectAll.Text = "Select All";
            //
            //MenuScratchPadSeparator3
            //
            this.MenuScratchPadSeparator3.Name = "MenuScratchPadSeparator3";
            this.MenuScratchPadSeparator3.Size = new System.Drawing.Size(220, 6);
            //
            //MenuScratchPadAddToOutgoingMessageSet
            //
            this.MenuScratchPadAddToOutgoingMessageSet.Name = "MenuScratchPadAddToOutgoingMessageSet";
            this.MenuScratchPadAddToOutgoingMessageSet.Size = new System.Drawing.Size(223, 22);
            this.MenuScratchPadAddToOutgoingMessageSet.Text = "Add to Ougoing Mesage Set";
            //
            //ButtonSend
            //
            this.ButtonSend.Location = new System.Drawing.Point(632, 232);
            this.ButtonSend.Name = "ButtonSend";
            this.ButtonSend.Size = new System.Drawing.Size(72, 39);
            this.ButtonSend.TabIndex = 0;
            this.ButtonSend.Text = "Send";
            this.ButtonSend.UseVisualStyleBackColor = true;
            //
            //ButtonReceive
            //
            this.ButtonReceive.Location = new System.Drawing.Point(632, 125);
            this.ButtonReceive.Name = "ButtonReceive";
            this.ButtonReceive.Size = new System.Drawing.Size(72, 25);
            this.ButtonReceive.TabIndex = 10;
            this.ButtonReceive.Text = "Start";
            this.ButtonReceive.UseVisualStyleBackColor = true;
            //
            //TextBoxReadRate
            //
            this.TextBoxReadRate.AcceptsReturn = true;
            this.TextBoxReadRate.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxReadRate.Location = new System.Drawing.Point(632, 173);
            this.TextBoxReadRate.MaxLength = 0;
            this.TextBoxReadRate.Name = "TextBoxReadRate";
            this.TextBoxReadRate.Size = new System.Drawing.Size(73, 20);
            this.TextBoxReadRate.TabIndex = 11;
            //
            //ButtonClearList
            //
            this.ButtonClearList.Location = new System.Drawing.Point(632, 93);
            this.ButtonClearList.Name = "ButtonClearList";
            this.ButtonClearList.Size = new System.Drawing.Size(72, 25);
            this.ButtonClearList.TabIndex = 9;
            this.ButtonClearList.Text = "Clear";
            this.ButtonClearList.UseVisualStyleBackColor = true;
            //
            //LabelScratchPad
            //
            this.LabelScratchPad.AutoSize = true;
            this.LabelScratchPad.Location = new System.Drawing.Point(332, 224);
            this.LabelScratchPad.Name = "LabelScratchPad";
            this.LabelScratchPad.Size = new System.Drawing.Size(69, 13);
            this.LabelScratchPad.TabIndex = 19;
            this.LabelScratchPad.Text = "Scratch Pad:";
            //
            //LabelTO
            //
            this.LabelTO.Location = new System.Drawing.Point(631, 309);
            this.LabelTO.Name = "LabelTO";
            this.LabelTO.Size = new System.Drawing.Size(72, 13);
            this.LabelTO.TabIndex = 14;
            this.LabelTO.Text = "Timeout:";
            //
            //LabelReadRate
            //
            this.LabelReadRate.Location = new System.Drawing.Point(632, 157);
            this.LabelReadRate.Name = "LabelReadRate";
            this.LabelReadRate.Size = new System.Drawing.Size(41, 17);
            this.LabelReadRate.TabIndex = 12;
            this.LabelReadRate.Text = "Rate:";
            //
            //TabPagePeriodicMessages
            //
            this.TabPagePeriodicMessages.Controls.Add(this.ComboBoxPeriodicMessageChannel0);
            this.TabPagePeriodicMessages.Controls.Add(this.ComboBoxPeriodicMessageChannel9);
            this.TabPagePeriodicMessages.Controls.Add(this.ComboBoxPeriodicMessageChannel8);
            this.TabPagePeriodicMessages.Controls.Add(this.ComboBoxPeriodicMessageChannel7);
            this.TabPagePeriodicMessages.Controls.Add(this.ComboBoxPeriodicMessageChannel6);
            this.TabPagePeriodicMessages.Controls.Add(this.ComboBoxPeriodicMessageChannel5);
            this.TabPagePeriodicMessages.Controls.Add(this.ComboBoxPeriodicMessageChannel4);
            this.TabPagePeriodicMessages.Controls.Add(this.ComboBoxPeriodicMessageChannel3);
            this.TabPagePeriodicMessages.Controls.Add(this.ComboBoxPeriodicMessageChannel2);
            this.TabPagePeriodicMessages.Controls.Add(this.ComboBoxPeriodicMessageChannel1);
            this.TabPagePeriodicMessages.Controls.Add(this.ButtonClearAllPeriodicMessages);
            this.TabPagePeriodicMessages.Controls.Add(this.ButtonCancelPeriodicMessages);
            this.TabPagePeriodicMessages.Controls.Add(this.ButtonApplyPeriodicMessages);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageFlags0);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageFlags1);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageFlags2);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageFlags3);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageFlags4);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageFlags5);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageFlags6);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageFlags7);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageFlags8);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageFlags9);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessage0);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessage1);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessage2);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessage3);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessage4);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessage5);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessage6);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessage7);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessage8);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessage9);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageInterval0);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageInterval1);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageInterval2);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageInterval3);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageInterval4);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageInterval5);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageInterval6);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageInterval7);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageInterval8);
            this.TabPagePeriodicMessages.Controls.Add(this.TextBoxPeriodicMessageInterval9);
            this.TabPagePeriodicMessages.Controls.Add(this.CheckBoxPeriodicMessageEnable0);
            this.TabPagePeriodicMessages.Controls.Add(this.CheckBoxPeriodicMessageEnable1);
            this.TabPagePeriodicMessages.Controls.Add(this.CheckBoxPeriodicMessageEnable2);
            this.TabPagePeriodicMessages.Controls.Add(this.CheckBoxPeriodicMessageEnable3);
            this.TabPagePeriodicMessages.Controls.Add(this.CheckBoxPeriodicMessageEnable4);
            this.TabPagePeriodicMessages.Controls.Add(this.CheckBoxPeriodicMessageEnable5);
            this.TabPagePeriodicMessages.Controls.Add(this.CheckBoxPeriodicMessageEnable6);
            this.TabPagePeriodicMessages.Controls.Add(this.CheckBoxPeriodicMessageEnable7);
            this.TabPagePeriodicMessages.Controls.Add(this.CheckBoxPeriodicMessageEnable8);
            this.TabPagePeriodicMessages.Controls.Add(this.CheckBoxPeriodicMessageEnable9);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageChannel);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelChannel2);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelDeviceCombo2);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageIds);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageId0);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageId1);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageId2);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageId3);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageId4);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageId5);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageId6);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageId7);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageId8);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageId9);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageFlags);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPMMessage);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessage0);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessage1);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessage2);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessage3);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessage4);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessage5);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessage6);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessage7);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessage8);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessage9);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageInterval);
            this.TabPagePeriodicMessages.Controls.Add(this.LabelPeriodicMessageDelete);
            this.TabPagePeriodicMessages.Controls.Add(this.ComboAvailableBoxLocator2);
            this.TabPagePeriodicMessages.Controls.Add(this.ComboAvailableChannelLocator2);
            this.TabPagePeriodicMessages.Location = new System.Drawing.Point(4, 22);
            this.TabPagePeriodicMessages.Name = "TabPagePeriodicMessages";
            this.TabPagePeriodicMessages.Size = new System.Drawing.Size(714, 395);
            this.TabPagePeriodicMessages.TabIndex = 2;
            this.TabPagePeriodicMessages.Text = "Periodic Messages";
            //
            //ComboBoxPeriodicMessageChannel0
            //
            this.ComboBoxPeriodicMessageChannel0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPeriodicMessageChannel0.Location = new System.Drawing.Point(297, 23);
            this.ComboBoxPeriodicMessageChannel0.Name = "ComboBoxPeriodicMessageChannel0";
            this.ComboBoxPeriodicMessageChannel0.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxPeriodicMessageChannel0.TabIndex = 1;
            //
            //ComboBoxPeriodicMessageChannel1
            //
            this.ComboBoxPeriodicMessageChannel1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPeriodicMessageChannel1.Location = new System.Drawing.Point(297, 48);
            this.ComboBoxPeriodicMessageChannel1.Name = "ComboBoxPeriodicMessageChannel1";
            this.ComboBoxPeriodicMessageChannel1.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxPeriodicMessageChannel1.TabIndex = 6;
            //
            //ComboBoxPeriodicMessageChannel2
            //
            this.ComboBoxPeriodicMessageChannel2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPeriodicMessageChannel2.Location = new System.Drawing.Point(297, 72);
            this.ComboBoxPeriodicMessageChannel2.Name = "ComboBoxPeriodicMessageChannel2";
            this.ComboBoxPeriodicMessageChannel2.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxPeriodicMessageChannel2.TabIndex = 11;
            //
            //ComboBoxPeriodicMessageChannel3
            //
            this.ComboBoxPeriodicMessageChannel3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPeriodicMessageChannel3.Location = new System.Drawing.Point(297, 96);
            this.ComboBoxPeriodicMessageChannel3.Name = "ComboBoxPeriodicMessageChannel3";
            this.ComboBoxPeriodicMessageChannel3.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxPeriodicMessageChannel3.TabIndex = 16;
            //
            //ComboBoxPeriodicMessageChannel4
            //
            this.ComboBoxPeriodicMessageChannel4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPeriodicMessageChannel4.Location = new System.Drawing.Point(297, 120);
            this.ComboBoxPeriodicMessageChannel4.Name = "ComboBoxPeriodicMessageChannel4";
            this.ComboBoxPeriodicMessageChannel4.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxPeriodicMessageChannel4.TabIndex = 21;
            //
            //ComboBoxPeriodicMessageChannel5
            //
            this.ComboBoxPeriodicMessageChannel5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPeriodicMessageChannel5.Location = new System.Drawing.Point(297, 144);
            this.ComboBoxPeriodicMessageChannel5.Name = "ComboBoxPeriodicMessageChannel5";
            this.ComboBoxPeriodicMessageChannel5.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxPeriodicMessageChannel5.TabIndex = 26;
            //
            //ComboBoxPeriodicMessageChannel6
            //
            this.ComboBoxPeriodicMessageChannel6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPeriodicMessageChannel6.Location = new System.Drawing.Point(297, 168);
            this.ComboBoxPeriodicMessageChannel6.Name = "ComboBoxPeriodicMessageChannel6";
            this.ComboBoxPeriodicMessageChannel6.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxPeriodicMessageChannel6.TabIndex = 31;
            //
            //ComboBoxPeriodicMessageChannel7
            //
            this.ComboBoxPeriodicMessageChannel7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPeriodicMessageChannel7.Location = new System.Drawing.Point(297, 192);
            this.ComboBoxPeriodicMessageChannel7.Name = "ComboBoxPeriodicMessageChannel7";
            this.ComboBoxPeriodicMessageChannel7.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxPeriodicMessageChannel7.TabIndex = 36;
            //
            //ComboBoxPeriodicMessageChannel8
            //
            this.ComboBoxPeriodicMessageChannel8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPeriodicMessageChannel8.Location = new System.Drawing.Point(297, 216);
            this.ComboBoxPeriodicMessageChannel8.Name = "ComboBoxPeriodicMessageChannel8";
            this.ComboBoxPeriodicMessageChannel8.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxPeriodicMessageChannel8.TabIndex = 41;
            //
            //ComboBoxPeriodicMessageChannel9
            //
            this.ComboBoxPeriodicMessageChannel9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxPeriodicMessageChannel9.Location = new System.Drawing.Point(297, 240);
            this.ComboBoxPeriodicMessageChannel9.Name = "ComboBoxPeriodicMessageChannel9";
            this.ComboBoxPeriodicMessageChannel9.Size = new System.Drawing.Size(105, 21);
            this.ComboBoxPeriodicMessageChannel9.TabIndex = 46;
            //
            //ButtonClearAllPeriodicMessages
            //
            this.ButtonClearAllPeriodicMessages.Location = new System.Drawing.Point(480, 288);
            this.ButtonClearAllPeriodicMessages.Name = "ButtonClearAllPeriodicMessages";
            this.ButtonClearAllPeriodicMessages.Size = new System.Drawing.Size(73, 25);
            this.ButtonClearAllPeriodicMessages.TabIndex = 50;
            this.ButtonClearAllPeriodicMessages.Text = "Clear All";
            this.ButtonClearAllPeriodicMessages.UseVisualStyleBackColor = true;
            //
            //ButtonCancelPeriodicMessages
            //
            this.ButtonCancelPeriodicMessages.Location = new System.Drawing.Point(560, 288);
            this.ButtonCancelPeriodicMessages.Name = "ButtonCancelPeriodicMessages";
            this.ButtonCancelPeriodicMessages.Size = new System.Drawing.Size(49, 25);
            this.ButtonCancelPeriodicMessages.TabIndex = 51;
            this.ButtonCancelPeriodicMessages.Text = "Cancel";
            this.ButtonCancelPeriodicMessages.UseVisualStyleBackColor = true;
            //
            //ButtonApplyPeriodicMessage
            //
            this.ButtonApplyPeriodicMessages.Location = new System.Drawing.Point(616, 288);
            this.ButtonApplyPeriodicMessages.Name = "ButtonApplyPeriodicMessages";
            this.ButtonApplyPeriodicMessages.Size = new System.Drawing.Size(49, 25);
            this.ButtonApplyPeriodicMessages.TabIndex = 52;
            this.ButtonApplyPeriodicMessages.Text = "Apply";
            this.ButtonApplyPeriodicMessages.UseVisualStyleBackColor = true;
            //
            //TextBoxPeriodicMessageFlags0
            //
            this.TextBoxPeriodicMessageFlags0.AcceptsReturn = true;
            this.TextBoxPeriodicMessageFlags0.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxPeriodicMessageFlags0.Location = new System.Drawing.Point(417, 24);
            this.TextBoxPeriodicMessageFlags0.MaxLength = 0;
            this.TextBoxPeriodicMessageFlags0.Name = "TextBoxPeriodicMessageFlags0";
            this.TextBoxPeriodicMessageFlags0.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageFlags0.TabIndex = 2;
            //
            //TextBoxPeriodicMessageFlags1
            //
            this.TextBoxPeriodicMessageFlags1.AcceptsReturn = true;
            this.TextBoxPeriodicMessageFlags1.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxPeriodicMessageFlags1.Location = new System.Drawing.Point(417, 48);
            this.TextBoxPeriodicMessageFlags1.MaxLength = 0;
            this.TextBoxPeriodicMessageFlags1.Name = "TextBoxPeriodicMessageFlags1";
            this.TextBoxPeriodicMessageFlags1.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageFlags1.TabIndex = 7;
            //
            //TextBoxPeriodicMessageFlags2
            //
            this.TextBoxPeriodicMessageFlags2.AcceptsReturn = true;
            this.TextBoxPeriodicMessageFlags2.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxPeriodicMessageFlags2.Location = new System.Drawing.Point(417, 72);
            this.TextBoxPeriodicMessageFlags2.MaxLength = 0;
            this.TextBoxPeriodicMessageFlags2.Name = "TextBoxPeriodicMessageFlags2";
            this.TextBoxPeriodicMessageFlags2.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageFlags2.TabIndex = 12;
            //
            //TextBoxPeriodicMessageFlags3
            //
            this.TextBoxPeriodicMessageFlags3.AcceptsReturn = true;
            this.TextBoxPeriodicMessageFlags3.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxPeriodicMessageFlags3.Location = new System.Drawing.Point(417, 96);
            this.TextBoxPeriodicMessageFlags3.MaxLength = 0;
            this.TextBoxPeriodicMessageFlags3.Name = "TextBoxPeriodicMessageFlags3";
            this.TextBoxPeriodicMessageFlags3.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageFlags3.TabIndex = 17;
            //
            //TextBoxPeriodicMessageFlags4
            //
            this.TextBoxPeriodicMessageFlags4.AcceptsReturn = true;
            this.TextBoxPeriodicMessageFlags4.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxPeriodicMessageFlags4.Location = new System.Drawing.Point(417, 120);
            this.TextBoxPeriodicMessageFlags4.MaxLength = 0;
            this.TextBoxPeriodicMessageFlags4.Name = "TextBoxPeriodicMessageFlags4";
            this.TextBoxPeriodicMessageFlags4.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageFlags4.TabIndex = 22;
            //
            //TextBoxPeriodicMessageFlags5
            //
            this.TextBoxPeriodicMessageFlags5.AcceptsReturn = true;
            this.TextBoxPeriodicMessageFlags5.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxPeriodicMessageFlags5.Location = new System.Drawing.Point(417, 144);
            this.TextBoxPeriodicMessageFlags5.MaxLength = 0;
            this.TextBoxPeriodicMessageFlags5.Name = "TextBoxPeriodicMessageFlags5";
            this.TextBoxPeriodicMessageFlags5.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageFlags5.TabIndex = 27;
            //
            //TextBoxPeriodicMessageFlags6
            //
            this.TextBoxPeriodicMessageFlags6.AcceptsReturn = true;
            this.TextBoxPeriodicMessageFlags6.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxPeriodicMessageFlags6.Location = new System.Drawing.Point(417, 168);
            this.TextBoxPeriodicMessageFlags6.MaxLength = 0;
            this.TextBoxPeriodicMessageFlags6.Name = "TextBoxPeriodicMessageFlags6";
            this.TextBoxPeriodicMessageFlags6.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageFlags6.TabIndex = 32;
            //
            //TextBoxPeriodicMessageFlags7
            //
            this.TextBoxPeriodicMessageFlags7.AcceptsReturn = true;
            this.TextBoxPeriodicMessageFlags7.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxPeriodicMessageFlags7.Location = new System.Drawing.Point(417, 192);
            this.TextBoxPeriodicMessageFlags7.MaxLength = 0;
            this.TextBoxPeriodicMessageFlags7.Name = "TextBoxPeriodicMessageFlags7";
            this.TextBoxPeriodicMessageFlags7.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageFlags7.TabIndex = 37;
            //
            //TextBoxPeriodicMessageFlags8
            //
            this.TextBoxPeriodicMessageFlags8.AcceptsReturn = true;
            this.TextBoxPeriodicMessageFlags8.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxPeriodicMessageFlags8.Location = new System.Drawing.Point(417, 216);
            this.TextBoxPeriodicMessageFlags8.MaxLength = 0;
            this.TextBoxPeriodicMessageFlags8.Name = "TextBoxPeriodicMessageFlags8";
            this.TextBoxPeriodicMessageFlags8.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageFlags8.TabIndex = 42;
            //
            //TextBoxPeriodicMessageFlags9
            //
            this.TextBoxPeriodicMessageFlags9.AcceptsReturn = true;
            this.TextBoxPeriodicMessageFlags9.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxPeriodicMessageFlags9.Location = new System.Drawing.Point(417, 240);
            this.TextBoxPeriodicMessageFlags9.MaxLength = 0;
            this.TextBoxPeriodicMessageFlags9.Name = "TextBoxPeriodicMessageFlags9";
            this.TextBoxPeriodicMessageFlags9.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageFlags9.TabIndex = 47;
            //
            //TextBoxPeriodicMessage0
            //
            this.TextBoxPeriodicMessage0.AcceptsReturn = true;
            this.TextBoxPeriodicMessage0.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessage0.Location = new System.Drawing.Point(25, 24);
            this.TextBoxPeriodicMessage0.MaxLength = 0;
            this.TextBoxPeriodicMessage0.Name = "TextBoxPeriodicMessage0";
            this.TextBoxPeriodicMessage0.Size = new System.Drawing.Size(265, 20);
            this.TextBoxPeriodicMessage0.TabIndex = 0;
            //
            //TextBoxPeriodicMessage1
            //
            this.TextBoxPeriodicMessage1.AcceptsReturn = true;
            this.TextBoxPeriodicMessage1.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessage1.Location = new System.Drawing.Point(25, 48);
            this.TextBoxPeriodicMessage1.MaxLength = 0;
            this.TextBoxPeriodicMessage1.Name = "TextBoxPeriodicMessage1";
            this.TextBoxPeriodicMessage1.Size = new System.Drawing.Size(265, 20);
            this.TextBoxPeriodicMessage1.TabIndex = 5;
            //
            //TextBoxPeriodicMessage2
            //
            this.TextBoxPeriodicMessage2.AcceptsReturn = true;
            this.TextBoxPeriodicMessage2.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessage2.Location = new System.Drawing.Point(25, 72);
            this.TextBoxPeriodicMessage2.MaxLength = 0;
            this.TextBoxPeriodicMessage2.Name = "TextBoxPeriodicMessage2";
            this.TextBoxPeriodicMessage2.Size = new System.Drawing.Size(265, 20);
            this.TextBoxPeriodicMessage2.TabIndex = 10;
            //
            //TextBoxPeriodicMessage3
            //
            this.TextBoxPeriodicMessage3.AcceptsReturn = true;
            this.TextBoxPeriodicMessage3.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessage3.Location = new System.Drawing.Point(25, 96);
            this.TextBoxPeriodicMessage3.MaxLength = 0;
            this.TextBoxPeriodicMessage3.Name = "TextBoxPeriodicMessage3";
            this.TextBoxPeriodicMessage3.Size = new System.Drawing.Size(265, 20);
            this.TextBoxPeriodicMessage3.TabIndex = 15;
            //
            //TextBoxPeriodicMessage4
            //
            this.TextBoxPeriodicMessage4.AcceptsReturn = true;
            this.TextBoxPeriodicMessage4.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessage4.Location = new System.Drawing.Point(25, 120);
            this.TextBoxPeriodicMessage4.MaxLength = 0;
            this.TextBoxPeriodicMessage4.Name = "TextBoxPeriodicMessage4";
            this.TextBoxPeriodicMessage4.Size = new System.Drawing.Size(265, 20);
            this.TextBoxPeriodicMessage4.TabIndex = 20;
            //
            //TextBoxPeriodicMessage5
            //
            this.TextBoxPeriodicMessage5.AcceptsReturn = true;
            this.TextBoxPeriodicMessage5.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessage5.Location = new System.Drawing.Point(25, 144);
            this.TextBoxPeriodicMessage5.MaxLength = 0;
            this.TextBoxPeriodicMessage5.Name = "TextBoxPeriodicMessage5";
            this.TextBoxPeriodicMessage5.Size = new System.Drawing.Size(265, 20);
            this.TextBoxPeriodicMessage5.TabIndex = 25;
            //
            //TextBoxPeriodicMessage6
            //
            this.TextBoxPeriodicMessage6.AcceptsReturn = true;
            this.TextBoxPeriodicMessage6.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessage6.Location = new System.Drawing.Point(25, 168);
            this.TextBoxPeriodicMessage6.MaxLength = 0;
            this.TextBoxPeriodicMessage6.Name = "TextBoxPeriodicMessage6";
            this.TextBoxPeriodicMessage6.Size = new System.Drawing.Size(265, 20);
            this.TextBoxPeriodicMessage6.TabIndex = 30;
            //
            //TextBoxPeriodicMessage7
            //
            this.TextBoxPeriodicMessage7.AcceptsReturn = true;
            this.TextBoxPeriodicMessage7.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessage7.Location = new System.Drawing.Point(25, 192);
            this.TextBoxPeriodicMessage7.MaxLength = 0;
            this.TextBoxPeriodicMessage7.Name = "TextBoxPeriodicMessage7";
            this.TextBoxPeriodicMessage7.Size = new System.Drawing.Size(265, 20);
            this.TextBoxPeriodicMessage7.TabIndex = 35;
            //
            //TextBoxPeriodicMessage8
            //
            this.TextBoxPeriodicMessage8.AcceptsReturn = true;
            this.TextBoxPeriodicMessage8.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessage8.Location = new System.Drawing.Point(25, 216);
            this.TextBoxPeriodicMessage8.MaxLength = 0;
            this.TextBoxPeriodicMessage8.Name = "TextBoxPeriodicMessage8";
            this.TextBoxPeriodicMessage8.Size = new System.Drawing.Size(265, 20);
            this.TextBoxPeriodicMessage8.TabIndex = 40;
            //
            //TextBoxPeriodicMessage9
            //
            this.TextBoxPeriodicMessage9.AcceptsReturn = true;
            this.TextBoxPeriodicMessage9.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessage9.Location = new System.Drawing.Point(25, 240);
            this.TextBoxPeriodicMessage9.MaxLength = 0;
            this.TextBoxPeriodicMessage9.Name = "TextBoxPeriodicMessage9";
            this.TextBoxPeriodicMessage9.Size = new System.Drawing.Size(265, 20);
            this.TextBoxPeriodicMessage9.TabIndex = 45;
            //
            //TextBoxPeriodicMessageInterval0
            //
            this.TextBoxPeriodicMessageInterval0.AcceptsReturn = true;
            this.TextBoxPeriodicMessageInterval0.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessageInterval0.Location = new System.Drawing.Point(513, 24);
            this.TextBoxPeriodicMessageInterval0.MaxLength = 0;
            this.TextBoxPeriodicMessageInterval0.Name = "TextBoxPeriodicMessageInterval0";
            this.TextBoxPeriodicMessageInterval0.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageInterval0.TabIndex = 3;
            //
            //TextBoxPeriodicMessageInterval1
            //
            this.TextBoxPeriodicMessageInterval1.AcceptsReturn = true;
            this.TextBoxPeriodicMessageInterval1.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessageInterval1.Location = new System.Drawing.Point(513, 48);
            this.TextBoxPeriodicMessageInterval1.MaxLength = 0;
            this.TextBoxPeriodicMessageInterval1.Name = "TextBoxPeriodicMessageInterval1";
            this.TextBoxPeriodicMessageInterval1.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageInterval1.TabIndex = 8;
            //
            //TextBoxPeriodicMessageInterval2
            //
            this.TextBoxPeriodicMessageInterval2.AcceptsReturn = true;
            this.TextBoxPeriodicMessageInterval2.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessageInterval2.Location = new System.Drawing.Point(513, 72);
            this.TextBoxPeriodicMessageInterval2.MaxLength = 0;
            this.TextBoxPeriodicMessageInterval2.Name = "TextBoxPeriodicMessageInterval2";
            this.TextBoxPeriodicMessageInterval2.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageInterval2.TabIndex = 13;
            //
            //TextBoxPeriodicMessageInterval3
            //
            this.TextBoxPeriodicMessageInterval3.AcceptsReturn = true;
            this.TextBoxPeriodicMessageInterval3.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessageInterval3.Location = new System.Drawing.Point(513, 96);
            this.TextBoxPeriodicMessageInterval3.MaxLength = 0;
            this.TextBoxPeriodicMessageInterval3.Name = "TextBoxPeriodicMessageInterval3";
            this.TextBoxPeriodicMessageInterval3.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageInterval3.TabIndex = 18;
            //
            //TextBoxPeriodicMessageInterval4
            //
            this.TextBoxPeriodicMessageInterval4.AcceptsReturn = true;
            this.TextBoxPeriodicMessageInterval4.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessageInterval4.Location = new System.Drawing.Point(513, 120);
            this.TextBoxPeriodicMessageInterval4.MaxLength = 0;
            this.TextBoxPeriodicMessageInterval4.Name = "TextBoxPeriodicMessageInterval4";
            this.TextBoxPeriodicMessageInterval4.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageInterval4.TabIndex = 23;
            //
            //TextBoxPeriodicMessageInterval5
            //
            this.TextBoxPeriodicMessageInterval5.AcceptsReturn = true;
            this.TextBoxPeriodicMessageInterval5.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessageInterval5.Location = new System.Drawing.Point(513, 144);
            this.TextBoxPeriodicMessageInterval5.MaxLength = 0;
            this.TextBoxPeriodicMessageInterval5.Name = "TextBoxPeriodicMessageInterval5";
            this.TextBoxPeriodicMessageInterval5.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageInterval5.TabIndex = 28;
            //
            //TextBoxPeriodicMessageInterval6
            //
            this.TextBoxPeriodicMessageInterval6.AcceptsReturn = true;
            this.TextBoxPeriodicMessageInterval6.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessageInterval6.Location = new System.Drawing.Point(513, 168);
            this.TextBoxPeriodicMessageInterval6.MaxLength = 0;
            this.TextBoxPeriodicMessageInterval6.Name = "TextBoxPeriodicMessageInterval6";
            this.TextBoxPeriodicMessageInterval6.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageInterval6.TabIndex = 33;
            //
            //TextBoxPeriodicMessageInterval7
            //
            this.TextBoxPeriodicMessageInterval7.AcceptsReturn = true;
            this.TextBoxPeriodicMessageInterval7.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessageInterval7.Location = new System.Drawing.Point(513, 192);
            this.TextBoxPeriodicMessageInterval7.MaxLength = 0;
            this.TextBoxPeriodicMessageInterval7.Name = "TextBoxPeriodicMessageInterval7";
            this.TextBoxPeriodicMessageInterval7.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageInterval7.TabIndex = 38;
            //
            //TextBoxPeriodicMessageInterval8
            //
            this.TextBoxPeriodicMessageInterval8.AcceptsReturn = true;
            this.TextBoxPeriodicMessageInterval8.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessageInterval8.Location = new System.Drawing.Point(513, 216);
            this.TextBoxPeriodicMessageInterval8.MaxLength = 0;
            this.TextBoxPeriodicMessageInterval8.Name = "TextBoxPeriodicMessageInterval8";
            this.TextBoxPeriodicMessageInterval8.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageInterval8.TabIndex = 43;
            //
            //TextBoxPeriodicMessageInterval9
            //
            this.TextBoxPeriodicMessageInterval9.AcceptsReturn = true;
            this.TextBoxPeriodicMessageInterval9.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxPeriodicMessageInterval9.Location = new System.Drawing.Point(513, 240);
            this.TextBoxPeriodicMessageInterval9.MaxLength = 0;
            this.TextBoxPeriodicMessageInterval9.Name = "TextBoxPeriodicMessageInterval9";
            this.TextBoxPeriodicMessageInterval9.Size = new System.Drawing.Size(81, 20);
            this.TextBoxPeriodicMessageInterval9.TabIndex = 48;
            //
            //CheckBoxPeriodicMessageEnable0
            //
            this.CheckBoxPeriodicMessageEnable0.Location = new System.Drawing.Point(625, 24);
            this.CheckBoxPeriodicMessageEnable0.Name = "CheckBoxPeriodicMessageEnable0";
            this.CheckBoxPeriodicMessageEnable0.Size = new System.Drawing.Size(65, 17);
            this.CheckBoxPeriodicMessageEnable0.TabIndex = 63;
            this.CheckBoxPeriodicMessageEnable0.Text = "Enabled";
            this.CheckBoxPeriodicMessageEnable0.UseVisualStyleBackColor = true;
            //
            //CheckBoxPeriodicMessageEnable1
            //
            this.CheckBoxPeriodicMessageEnable1.Location = new System.Drawing.Point(625, 48);
            this.CheckBoxPeriodicMessageEnable1.Name = "CheckBoxPeriodicMessageEnable1";
            this.CheckBoxPeriodicMessageEnable1.Size = new System.Drawing.Size(65, 17);
            this.CheckBoxPeriodicMessageEnable1.TabIndex = 64;
            this.CheckBoxPeriodicMessageEnable1.Text = "Enabled";
            this.CheckBoxPeriodicMessageEnable1.UseVisualStyleBackColor = true;
            //
            //CheckBoxPeriodicMessageEnable2
            //
            this.CheckBoxPeriodicMessageEnable2.Location = new System.Drawing.Point(625, 72);
            this.CheckBoxPeriodicMessageEnable2.Name = "CheckBoxPeriodicMessageEnable2";
            this.CheckBoxPeriodicMessageEnable2.Size = new System.Drawing.Size(65, 17);
            this.CheckBoxPeriodicMessageEnable2.TabIndex = 65;
            this.CheckBoxPeriodicMessageEnable2.Text = "Enabled";
            this.CheckBoxPeriodicMessageEnable2.UseVisualStyleBackColor = true;
            //
            //CheckBoxPeriodicMessageEnable3
            //
            this.CheckBoxPeriodicMessageEnable3.Location = new System.Drawing.Point(625, 96);
            this.CheckBoxPeriodicMessageEnable3.Name = "CheckBoxPeriodicMessageEnable3";
            this.CheckBoxPeriodicMessageEnable3.Size = new System.Drawing.Size(65, 17);
            this.CheckBoxPeriodicMessageEnable3.TabIndex = 66;
            this.CheckBoxPeriodicMessageEnable3.Text = "Enabled";
            this.CheckBoxPeriodicMessageEnable3.UseVisualStyleBackColor = true;
            //
            //CheckBoxPeriodicMessageEnable4
            //
            this.CheckBoxPeriodicMessageEnable4.Location = new System.Drawing.Point(625, 120);
            this.CheckBoxPeriodicMessageEnable4.Name = "CheckBoxPeriodicMessageEnable4";
            this.CheckBoxPeriodicMessageEnable4.Size = new System.Drawing.Size(65, 17);
            this.CheckBoxPeriodicMessageEnable4.TabIndex = 67;
            this.CheckBoxPeriodicMessageEnable4.Text = "Enabled";
            this.CheckBoxPeriodicMessageEnable4.UseVisualStyleBackColor = true;
            //
            //CheckBoxPeriodicMessageEnable5
            //
            this.CheckBoxPeriodicMessageEnable5.Location = new System.Drawing.Point(625, 144);
            this.CheckBoxPeriodicMessageEnable5.Name = "CheckBoxPeriodicMessageEnable5";
            this.CheckBoxPeriodicMessageEnable5.Size = new System.Drawing.Size(65, 17);
            this.CheckBoxPeriodicMessageEnable5.TabIndex = 68;
            this.CheckBoxPeriodicMessageEnable5.Text = "Enabled";
            this.CheckBoxPeriodicMessageEnable5.UseVisualStyleBackColor = true;
            //
            //CheckBoxPeriodicMessageEnable6
            //
            this.CheckBoxPeriodicMessageEnable6.Location = new System.Drawing.Point(625, 168);
            this.CheckBoxPeriodicMessageEnable6.Name = "CheckBoxPeriodicMessageEnable6";
            this.CheckBoxPeriodicMessageEnable6.Size = new System.Drawing.Size(65, 17);
            this.CheckBoxPeriodicMessageEnable6.TabIndex = 69;
            this.CheckBoxPeriodicMessageEnable6.Text = "Enabled";
            this.CheckBoxPeriodicMessageEnable6.UseVisualStyleBackColor = true;
            //
            //CheckBoxPeriodicMessageEnable7
            //
            this.CheckBoxPeriodicMessageEnable7.Location = new System.Drawing.Point(625, 192);
            this.CheckBoxPeriodicMessageEnable7.Name = "CheckBoxPeriodicMessageEnable7";
            this.CheckBoxPeriodicMessageEnable7.Size = new System.Drawing.Size(65, 17);
            this.CheckBoxPeriodicMessageEnable7.TabIndex = 70;
            this.CheckBoxPeriodicMessageEnable7.Text = "Enabled";
            this.CheckBoxPeriodicMessageEnable7.UseVisualStyleBackColor = true;
            //
            //CheckBoxPeriodicMessageEnable8
            //
            this.CheckBoxPeriodicMessageEnable8.Location = new System.Drawing.Point(625, 216);
            this.CheckBoxPeriodicMessageEnable8.Name = "CheckBoxPeriodicMessageEnable8";
            this.CheckBoxPeriodicMessageEnable8.Size = new System.Drawing.Size(65, 17);
            this.CheckBoxPeriodicMessageEnable8.TabIndex = 71;
            this.CheckBoxPeriodicMessageEnable8.Text = "Enabled";
            this.CheckBoxPeriodicMessageEnable8.UseVisualStyleBackColor = true;
            //
            //CheckBoxPeriodicMessageEnable9
            //
            this.CheckBoxPeriodicMessageEnable9.Location = new System.Drawing.Point(625, 240);
            this.CheckBoxPeriodicMessageEnable9.Name = "CheckBoxPeriodicMessageEnable9";
            this.CheckBoxPeriodicMessageEnable9.Size = new System.Drawing.Size(65, 17);
            this.CheckBoxPeriodicMessageEnable9.TabIndex = 72;
            this.CheckBoxPeriodicMessageEnable9.Text = "Enabled";
            this.CheckBoxPeriodicMessageEnable9.UseVisualStyleBackColor = true;
            //
            //LabelPeriodicMessageChannel
            //
            this.LabelPeriodicMessageChannel.Location = new System.Drawing.Point(294, 8);
            this.LabelPeriodicMessageChannel.Name = "LabelPeriodicMessageChannel";
            this.LabelPeriodicMessageChannel.Size = new System.Drawing.Size(103, 13);
            this.LabelPeriodicMessageChannel.TabIndex = 78;
            this.LabelPeriodicMessageChannel.Text = "Channel:";
            //
            //LabelChannel2
            //
            this.LabelChannel2.Location = new System.Drawing.Point(524, 336);
            this.LabelChannel2.Name = "LabelChannel2";
            this.LabelChannel2.Size = new System.Drawing.Size(49, 17);
            this.LabelChannel2.TabIndex = 76;
            this.LabelChannel2.Text = "Channel:";
            //
            //LabelDeviceCombo2
            //
            this.LabelDeviceCombo2.Location = new System.Drawing.Point(395, 336);
            this.LabelDeviceCombo2.Name = "LabelDeviceCombo2";
            this.LabelDeviceCombo2.Size = new System.Drawing.Size(89, 17);
            this.LabelDeviceCombo2.TabIndex = 75;
            this.LabelDeviceCombo2.Text = "Device:";
            //
            //LabelPeriodicMessageIds
            //
            this.LabelPeriodicMessageIds.AutoSize = true;
            this.LabelPeriodicMessageIds.Location = new System.Drawing.Point(601, 8);
            this.LabelPeriodicMessageIds.Name = "LabelPeriodicMessageIds";
            this.LabelPeriodicMessageIds.Size = new System.Drawing.Size(21, 13);
            this.LabelPeriodicMessageIds.TabIndex = 81;
            this.LabelPeriodicMessageIds.Text = "ID:";
            this.LabelPeriodicMessageIds.Visible = false;
            //
            //LabelPeriodicMessageId0
            //
            this.LabelPeriodicMessageId0.Location = new System.Drawing.Point(601, 24);
            this.LabelPeriodicMessageId0.Name = "LabelPeriodicMessageId0";
            this.LabelPeriodicMessageId0.Size = new System.Drawing.Size(17, 17);
            this.LabelPeriodicMessageId0.TabIndex = 4;
            this.LabelPeriodicMessageId0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelPeriodicMessageId0.Visible = false;
            //
            //LabelPeriodicMessageId1
            //
            this.LabelPeriodicMessageId1.Location = new System.Drawing.Point(601, 48);
            this.LabelPeriodicMessageId1.Name = "LabelPeriodicMessageId1";
            this.LabelPeriodicMessageId1.Size = new System.Drawing.Size(17, 17);
            this.LabelPeriodicMessageId1.TabIndex = 9;
            this.LabelPeriodicMessageId1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelPeriodicMessageId1.Visible = false;
            //
            //LabelPeriodicMessageId2
            //
            this.LabelPeriodicMessageId2.Location = new System.Drawing.Point(601, 72);
            this.LabelPeriodicMessageId2.Name = "LabelPeriodicMessageId2";
            this.LabelPeriodicMessageId2.Size = new System.Drawing.Size(17, 17);
            this.LabelPeriodicMessageId2.TabIndex = 14;
            this.LabelPeriodicMessageId2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelPeriodicMessageId2.Visible = false;
            //
            //LabelPeriodicMessageId3
            //
            this.LabelPeriodicMessageId3.Location = new System.Drawing.Point(601, 96);
            this.LabelPeriodicMessageId3.Name = "LabelPeriodicMessageId3";
            this.LabelPeriodicMessageId3.Size = new System.Drawing.Size(17, 17);
            this.LabelPeriodicMessageId3.TabIndex = 19;
            this.LabelPeriodicMessageId3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelPeriodicMessageId3.Visible = false;
            //
            //LabelPeriodicMessageId4
            //
            this.LabelPeriodicMessageId4.Location = new System.Drawing.Point(601, 120);
            this.LabelPeriodicMessageId4.Name = "LabelPeriodicMessageId4";
            this.LabelPeriodicMessageId4.Size = new System.Drawing.Size(17, 17);
            this.LabelPeriodicMessageId4.TabIndex = 24;
            this.LabelPeriodicMessageId4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelPeriodicMessageId4.Visible = false;
            //
            //LabelPeriodicMessageId5
            //
            this.LabelPeriodicMessageId5.Location = new System.Drawing.Point(601, 144);
            this.LabelPeriodicMessageId5.Name = "LabelPeriodicMessageId5";
            this.LabelPeriodicMessageId5.Size = new System.Drawing.Size(17, 17);
            this.LabelPeriodicMessageId5.TabIndex = 29;
            this.LabelPeriodicMessageId5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelPeriodicMessageId5.Visible = false;
            //
            //LabelPeriodicMessageId6
            //
            this.LabelPeriodicMessageId6.Location = new System.Drawing.Point(601, 168);
            this.LabelPeriodicMessageId6.Name = "LabelPeriodicMessageId6";
            this.LabelPeriodicMessageId6.Size = new System.Drawing.Size(17, 17);
            this.LabelPeriodicMessageId6.TabIndex = 34;
            this.LabelPeriodicMessageId6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelPeriodicMessageId6.Visible = false;
            //
            //LabelPeriodicMessageId7
            //
            this.LabelPeriodicMessageId7.Location = new System.Drawing.Point(601, 192);
            this.LabelPeriodicMessageId7.Name = "LabelPeriodicMessageId7";
            this.LabelPeriodicMessageId7.Size = new System.Drawing.Size(17, 17);
            this.LabelPeriodicMessageId7.TabIndex = 39;
            this.LabelPeriodicMessageId7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelPeriodicMessageId7.Visible = false;
            //
            //LabelPeriodicMessageId8
            //
            this.LabelPeriodicMessageId8.Location = new System.Drawing.Point(601, 216);
            this.LabelPeriodicMessageId8.Name = "LabelPeriodicMessageId8";
            this.LabelPeriodicMessageId8.Size = new System.Drawing.Size(17, 17);
            this.LabelPeriodicMessageId8.TabIndex = 44;
            this.LabelPeriodicMessageId8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelPeriodicMessageId8.Visible = false;
            //
            //LabelPeriodicMessageId9
            //
            this.LabelPeriodicMessageId9.Location = new System.Drawing.Point(601, 240);
            this.LabelPeriodicMessageId9.Name = "LabelPeriodicMessageId9";
            this.LabelPeriodicMessageId9.Size = new System.Drawing.Size(17, 17);
            this.LabelPeriodicMessageId9.TabIndex = 49;
            this.LabelPeriodicMessageId9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelPeriodicMessageId9.Visible = false;
            //
            //LabelPeriodicMessageFlags
            //
            this.LabelPeriodicMessageFlags.Location = new System.Drawing.Point(417, 8);
            this.LabelPeriodicMessageFlags.Name = "LabelPeriodicMessageFlags";
            this.LabelPeriodicMessageFlags.Size = new System.Drawing.Size(81, 17);
            this.LabelPeriodicMessageFlags.TabIndex = 79;
            this.LabelPeriodicMessageFlags.Text = "Tx Flags:";
            //
            //LabelPMMessage
            //
            this.LabelPMMessage.Location = new System.Drawing.Point(25, 8);
            this.LabelPMMessage.Name = "LabelPMMessage";
            this.LabelPMMessage.Size = new System.Drawing.Size(122, 17);
            this.LabelPMMessage.TabIndex = 77;
            this.LabelPMMessage.Text = "Message:";
            //
            //LabelPeriodicMessage0
            //
            this.LabelPeriodicMessage0.Location = new System.Drawing.Point(9, 24);
            this.LabelPeriodicMessage0.Name = "LabelPeriodicMessage0";
            this.LabelPeriodicMessage0.Size = new System.Drawing.Size(9, 17);
            this.LabelPeriodicMessage0.TabIndex = 53;
            this.LabelPeriodicMessage0.Text = "0:";
            //
            //LabelPeriodicMessage1
            //
            this.LabelPeriodicMessage1.Location = new System.Drawing.Point(9, 48);
            this.LabelPeriodicMessage1.Name = "LabelPeriodicMessage1";
            this.LabelPeriodicMessage1.Size = new System.Drawing.Size(9, 17);
            this.LabelPeriodicMessage1.TabIndex = 54;
            this.LabelPeriodicMessage1.Text = "1:";
            //
            //LabelPeriodicMessage2
            //
            this.LabelPeriodicMessage2.Location = new System.Drawing.Point(9, 72);
            this.LabelPeriodicMessage2.Name = "LabelPeriodicMessage2";
            this.LabelPeriodicMessage2.Size = new System.Drawing.Size(9, 17);
            this.LabelPeriodicMessage2.TabIndex = 55;
            this.LabelPeriodicMessage2.Text = "2:";
            //
            //LabelPeriodicMessage3
            //
            this.LabelPeriodicMessage3.Location = new System.Drawing.Point(9, 96);
            this.LabelPeriodicMessage3.Name = "LabelPeriodicMessage3";
            this.LabelPeriodicMessage3.Size = new System.Drawing.Size(9, 17);
            this.LabelPeriodicMessage3.TabIndex = 56;
            this.LabelPeriodicMessage3.Text = "3:";
            //
            //LabelPeriodicMessage4
            //
            this.LabelPeriodicMessage4.Location = new System.Drawing.Point(9, 120);
            this.LabelPeriodicMessage4.Name = "LabelPeriodicMessage4";
            this.LabelPeriodicMessage4.Size = new System.Drawing.Size(9, 17);
            this.LabelPeriodicMessage4.TabIndex = 57;
            this.LabelPeriodicMessage4.Text = "4:";
            //
            //LabelPeriodicMessage5
            //
            this.LabelPeriodicMessage5.Location = new System.Drawing.Point(9, 144);
            this.LabelPeriodicMessage5.Name = "LabelPeriodicMessage5";
            this.LabelPeriodicMessage5.Size = new System.Drawing.Size(9, 17);
            this.LabelPeriodicMessage5.TabIndex = 58;
            this.LabelPeriodicMessage5.Text = "5:";
            //
            //LabelPeriodicMessage6
            //
            this.LabelPeriodicMessage6.Location = new System.Drawing.Point(9, 168);
            this.LabelPeriodicMessage6.Name = "LabelPeriodicMessage6";
            this.LabelPeriodicMessage6.Size = new System.Drawing.Size(9, 17);
            this.LabelPeriodicMessage6.TabIndex = 59;
            this.LabelPeriodicMessage6.Text = "6:";
            //
            //LabelPeriodicMessage7
            //
            this.LabelPeriodicMessage7.Location = new System.Drawing.Point(9, 192);
            this.LabelPeriodicMessage7.Name = "LabelPeriodicMessage7";
            this.LabelPeriodicMessage7.Size = new System.Drawing.Size(9, 17);
            this.LabelPeriodicMessage7.TabIndex = 60;
            this.LabelPeriodicMessage7.Text = "7:";
            //
            //LabelPeriodicMessage8
            //
            this.LabelPeriodicMessage8.Location = new System.Drawing.Point(9, 216);
            this.LabelPeriodicMessage8.Name = "LabelPeriodicMessage8";
            this.LabelPeriodicMessage8.Size = new System.Drawing.Size(9, 17);
            this.LabelPeriodicMessage8.TabIndex = 61;
            this.LabelPeriodicMessage8.Text = "8:";
            //
            //LabelPeriodicMessage9
            //
            this.LabelPeriodicMessage9.Location = new System.Drawing.Point(9, 240);
            this.LabelPeriodicMessage9.Name = "LabelPeriodicMessage9";
            this.LabelPeriodicMessage9.Size = new System.Drawing.Size(9, 17);
            this.LabelPeriodicMessage9.TabIndex = 62;
            this.LabelPeriodicMessage9.Text = "9:";
            //
            //LabelPeriodicMessageInterval
            //
            this.LabelPeriodicMessageInterval.Location = new System.Drawing.Point(513, 8);
            this.LabelPeriodicMessageInterval.Name = "LabelPeriodicMessageInterval";
            this.LabelPeriodicMessageInterval.Size = new System.Drawing.Size(70, 17);
            this.LabelPeriodicMessageInterval.TabIndex = 80;
            this.LabelPeriodicMessageInterval.Text = "Interval:";
            //
            //LabelPeriodicMessageDelete
            //
            this.LabelPeriodicMessageDelete.Location = new System.Drawing.Point(625, 8);
            this.LabelPeriodicMessageDelete.Name = "LabelPeriodicMessageDelete";
            this.LabelPeriodicMessageDelete.Size = new System.Drawing.Size(65, 17);
            this.LabelPeriodicMessageDelete.TabIndex = 82;
            this.LabelPeriodicMessageDelete.Text = "Enable:";
            //
            //TabPageFilters
            //
            this.TabPageFilters.Controls.Add(this.ButtonCreatePassFilter);
            this.TabPageFilters.Controls.Add(this.ButtonClearAllFilter);
            this.TabPageFilters.Controls.Add(this.ButtonCancelFilter);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlow0);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlow1);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlow2);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlow3);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlow4);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlow5);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlow6);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlow7);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlow8);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlow9);
            this.TabPageFilters.Controls.Add(this.ButtonApplyFilter);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlags0);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlags1);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlags2);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlags3);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlags4);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlags5);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlags6);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlags7);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlags8);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterFlags9);
            this.TabPageFilters.Controls.Add(this.ComboBoxFilterType0);
            this.TabPageFilters.Controls.Add(this.ComboBoxFilterType1);
            this.TabPageFilters.Controls.Add(this.ComboBoxFilterType2);
            this.TabPageFilters.Controls.Add(this.ComboBoxFilterType3);
            this.TabPageFilters.Controls.Add(this.ComboBoxFilterType4);
            this.TabPageFilters.Controls.Add(this.ComboBoxFilterType5);
            this.TabPageFilters.Controls.Add(this.ComboBoxFilterType6);
            this.TabPageFilters.Controls.Add(this.ComboBoxFilterType7);
            this.TabPageFilters.Controls.Add(this.ComboBoxFilterType8);
            this.TabPageFilters.Controls.Add(this.ComboBoxFilterType9);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterPatt0);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterPatt1);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterPatt2);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterPatt3);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterPatt4);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterPatt5);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterPatt6);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterPatt7);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterPatt8);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterPatt9);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterMask0);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterMask1);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterMask2);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterMask3);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterMask4);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterMask5);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterMask6);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterMask7);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterMask8);
            this.TabPageFilters.Controls.Add(this.TextBoxFilterMask9);
            this.TabPageFilters.Controls.Add(this.LabelChannel3);
            this.TabPageFilters.Controls.Add(this.LabelDeviceCombo3);
            this.TabPageFilters.Controls.Add(this.LabelFilterIds);
            this.TabPageFilters.Controls.Add(this.LabelFilterId0);
            this.TabPageFilters.Controls.Add(this.LabelFilterId1);
            this.TabPageFilters.Controls.Add(this.LabelFilterId2);
            this.TabPageFilters.Controls.Add(this.LabelFilterId3);
            this.TabPageFilters.Controls.Add(this.LabelFilterId4);
            this.TabPageFilters.Controls.Add(this.LabelFilterId5);
            this.TabPageFilters.Controls.Add(this.LabelFilterId6);
            this.TabPageFilters.Controls.Add(this.LabelFilterId7);
            this.TabPageFilters.Controls.Add(this.LabelFilterId8);
            this.TabPageFilters.Controls.Add(this.LabelFilterId9);
            this.TabPageFilters.Controls.Add(this.LabelFilterType);
            this.TabPageFilters.Controls.Add(this.LabelFilterFlags);
            this.TabPageFilters.Controls.Add(this.LabelFilterFlow);
            this.TabPageFilters.Controls.Add(this.LabelFilterPatt);
            this.TabPageFilters.Controls.Add(this.LabelFilterMask);
            this.TabPageFilters.Controls.Add(this.LabelFilter0);
            this.TabPageFilters.Controls.Add(this.LabelFilter1);
            this.TabPageFilters.Controls.Add(this.LabelFilter2);
            this.TabPageFilters.Controls.Add(this.LabelFilter3);
            this.TabPageFilters.Controls.Add(this.LabelFilter4);
            this.TabPageFilters.Controls.Add(this.LabelFilter5);
            this.TabPageFilters.Controls.Add(this.LabelFilter6);
            this.TabPageFilters.Controls.Add(this.LabelFilter7);
            this.TabPageFilters.Controls.Add(this.LabelFilter8);
            this.TabPageFilters.Controls.Add(this.LabelFilter9);
            this.TabPageFilters.Controls.Add(this.ComboAvailableBoxLocator3);
            this.TabPageFilters.Controls.Add(this.ComboAvailableChannelLocator3);
            this.TabPageFilters.Location = new System.Drawing.Point(4, 22);
            this.TabPageFilters.Name = "TabPageFilters";
            this.TabPageFilters.Size = new System.Drawing.Size(714, 395);
            this.TabPageFilters.TabIndex = 3;
            this.TabPageFilters.Text = "Filters";
            //
            //ButtonCreatePassFilter
            //
            this.ButtonCreatePassFilter.Location = new System.Drawing.Point(344, 288);
            this.ButtonCreatePassFilter.Name = "ButtonCreatePassFilter";
            this.ButtonCreatePassFilter.Size = new System.Drawing.Size(129, 25);
            this.ButtonCreatePassFilter.TabIndex = 50;
            this.ButtonCreatePassFilter.Text = "Create \"Pass All\" Filter";
            this.ButtonCreatePassFilter.UseVisualStyleBackColor = true;
            //
            //ButtonClearAllFilter
            //
            this.ButtonClearAllFilter.Location = new System.Drawing.Point(480, 288);
            this.ButtonClearAllFilter.Name = "ButtonClearAllFilter";
            this.ButtonClearAllFilter.Size = new System.Drawing.Size(73, 25);
            this.ButtonClearAllFilter.TabIndex = 51;
            this.ButtonClearAllFilter.Text = "Clear All";
            this.ButtonClearAllFilter.UseVisualStyleBackColor = true;
            //
            //ButtonCancelFilter
            //
            this.ButtonCancelFilter.Location = new System.Drawing.Point(560, 288);
            this.ButtonCancelFilter.Name = "ButtonCancelFilter";
            this.ButtonCancelFilter.Size = new System.Drawing.Size(49, 25);
            this.ButtonCancelFilter.TabIndex = 52;
            this.ButtonCancelFilter.Text = "Cancel";
            this.ButtonCancelFilter.UseVisualStyleBackColor = true;
            //
            //TextBoxFilterFlow0
            //
            this.TextBoxFilterFlow0.AcceptsReturn = true;
            this.TextBoxFilterFlow0.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterFlow0.Location = new System.Drawing.Point(359, 23);
            this.TextBoxFilterFlow0.MaxLength = 0;
            this.TextBoxFilterFlow0.Name = "TextBoxFilterFlow0";
            this.TextBoxFilterFlow0.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterFlow0.TabIndex = 2;
            //
            //TextBoxFilterFlow1
            //
            this.TextBoxFilterFlow1.AcceptsReturn = true;
            this.TextBoxFilterFlow1.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterFlow1.Location = new System.Drawing.Point(359, 47);
            this.TextBoxFilterFlow1.MaxLength = 0;
            this.TextBoxFilterFlow1.Name = "TextBoxFilterFlow1";
            this.TextBoxFilterFlow1.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterFlow1.TabIndex = 7;
            //
            //TextBoxFilterFlow2
            //
            this.TextBoxFilterFlow2.AcceptsReturn = true;
            this.TextBoxFilterFlow2.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterFlow2.Location = new System.Drawing.Point(359, 71);
            this.TextBoxFilterFlow2.MaxLength = 0;
            this.TextBoxFilterFlow2.Name = "TextBoxFilterFlow2";
            this.TextBoxFilterFlow2.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterFlow2.TabIndex = 12;
            //
            //TextBoxFilterFlow3
            //
            this.TextBoxFilterFlow3.AcceptsReturn = true;
            this.TextBoxFilterFlow3.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterFlow3.Location = new System.Drawing.Point(359, 95);
            this.TextBoxFilterFlow3.MaxLength = 0;
            this.TextBoxFilterFlow3.Name = "TextBoxFilterFlow3";
            this.TextBoxFilterFlow3.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterFlow3.TabIndex = 17;
            //
            //TextBoxFilterFlow4
            //
            this.TextBoxFilterFlow4.AcceptsReturn = true;
            this.TextBoxFilterFlow4.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterFlow4.Location = new System.Drawing.Point(359, 119);
            this.TextBoxFilterFlow4.MaxLength = 0;
            this.TextBoxFilterFlow4.Name = "TextBoxFilterFlow4";
            this.TextBoxFilterFlow4.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterFlow4.TabIndex = 22;
            //
            //TextBoxFilterFlow5
            //
            this.TextBoxFilterFlow5.AcceptsReturn = true;
            this.TextBoxFilterFlow5.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterFlow5.Location = new System.Drawing.Point(359, 143);
            this.TextBoxFilterFlow5.MaxLength = 0;
            this.TextBoxFilterFlow5.Name = "TextBoxFilterFlow5";
            this.TextBoxFilterFlow5.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterFlow5.TabIndex = 27;
            //
            //TextBoxFilterFlow6
            //
            this.TextBoxFilterFlow6.AcceptsReturn = true;
            this.TextBoxFilterFlow6.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterFlow6.Location = new System.Drawing.Point(359, 167);
            this.TextBoxFilterFlow6.MaxLength = 0;
            this.TextBoxFilterFlow6.Name = "TextBoxFilterFlow6";
            this.TextBoxFilterFlow6.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterFlow6.TabIndex = 32;
            //
            //TextBoxFilterFlow7
            //
            this.TextBoxFilterFlow7.AcceptsReturn = true;
            this.TextBoxFilterFlow7.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterFlow7.Location = new System.Drawing.Point(359, 191);
            this.TextBoxFilterFlow7.MaxLength = 0;
            this.TextBoxFilterFlow7.Name = "TextBoxFilterFlow7";
            this.TextBoxFilterFlow7.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterFlow7.TabIndex = 37;
            //
            //TextBoxFilterFlow8
            //
            this.TextBoxFilterFlow8.AcceptsReturn = true;
            this.TextBoxFilterFlow8.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterFlow8.Location = new System.Drawing.Point(359, 215);
            this.TextBoxFilterFlow8.MaxLength = 0;
            this.TextBoxFilterFlow8.Name = "TextBoxFilterFlow8";
            this.TextBoxFilterFlow8.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterFlow8.TabIndex = 42;
            //
            //TextBoxFilterFlow9
            //
            this.TextBoxFilterFlow9.AcceptsReturn = true;
            this.TextBoxFilterFlow9.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterFlow9.Location = new System.Drawing.Point(359, 239);
            this.TextBoxFilterFlow9.MaxLength = 0;
            this.TextBoxFilterFlow9.Name = "TextBoxFilterFlow9";
            this.TextBoxFilterFlow9.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterFlow9.TabIndex = 47;
            //
            //ButtonApplyFilter
            //
            this.ButtonApplyFilter.Location = new System.Drawing.Point(616, 288);
            this.ButtonApplyFilter.Name = "ButtonApplyFilter";
            this.ButtonApplyFilter.Size = new System.Drawing.Size(49, 25);
            this.ButtonApplyFilter.TabIndex = 53;
            this.ButtonApplyFilter.Text = "Apply";
            this.ButtonApplyFilter.UseVisualStyleBackColor = true;
            //
            //TextBoxFilterFlags0
            //
            this.TextBoxFilterFlags0.AcceptsReturn = true;
            this.TextBoxFilterFlags0.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFilterFlags0.Location = new System.Drawing.Point(527, 23);
            this.TextBoxFilterFlags0.MaxLength = 0;
            this.TextBoxFilterFlags0.Name = "TextBoxFilterFlags0";
            this.TextBoxFilterFlags0.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFilterFlags0.TabIndex = 3;
            //
            //TextBoxFilterFlags1
            //
            this.TextBoxFilterFlags1.AcceptsReturn = true;
            this.TextBoxFilterFlags1.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFilterFlags1.Location = new System.Drawing.Point(527, 47);
            this.TextBoxFilterFlags1.MaxLength = 0;
            this.TextBoxFilterFlags1.Name = "TextBoxFilterFlags1";
            this.TextBoxFilterFlags1.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFilterFlags1.TabIndex = 8;
            //
            //TextBoxFilterFlags2
            //
            this.TextBoxFilterFlags2.AcceptsReturn = true;
            this.TextBoxFilterFlags2.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFilterFlags2.Location = new System.Drawing.Point(527, 71);
            this.TextBoxFilterFlags2.MaxLength = 0;
            this.TextBoxFilterFlags2.Name = "TextBoxFilterFlags2";
            this.TextBoxFilterFlags2.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFilterFlags2.TabIndex = 13;
            //
            //TextBoxFilterFlags3
            //
            this.TextBoxFilterFlags3.AcceptsReturn = true;
            this.TextBoxFilterFlags3.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFilterFlags3.Location = new System.Drawing.Point(527, 95);
            this.TextBoxFilterFlags3.MaxLength = 0;
            this.TextBoxFilterFlags3.Name = "TextBoxFilterFlags3";
            this.TextBoxFilterFlags3.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFilterFlags3.TabIndex = 18;
            //
            //TextBoxFilterFlags4
            //
            this.TextBoxFilterFlags4.AcceptsReturn = true;
            this.TextBoxFilterFlags4.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFilterFlags4.Location = new System.Drawing.Point(527, 119);
            this.TextBoxFilterFlags4.MaxLength = 0;
            this.TextBoxFilterFlags4.Name = "TextBoxFilterFlags4";
            this.TextBoxFilterFlags4.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFilterFlags4.TabIndex = 23;
            //
            //TextBoxFilterFlags5
            //
            this.TextBoxFilterFlags5.AcceptsReturn = true;
            this.TextBoxFilterFlags5.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFilterFlags5.Location = new System.Drawing.Point(527, 143);
            this.TextBoxFilterFlags5.MaxLength = 0;
            this.TextBoxFilterFlags5.Name = "TextBoxFilterFlags5";
            this.TextBoxFilterFlags5.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFilterFlags5.TabIndex = 28;
            //
            //TextBoxFilterFlags6
            //
            this.TextBoxFilterFlags6.AcceptsReturn = true;
            this.TextBoxFilterFlags6.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFilterFlags6.Location = new System.Drawing.Point(527, 167);
            this.TextBoxFilterFlags6.MaxLength = 0;
            this.TextBoxFilterFlags6.Name = "TextBoxFilterFlags6";
            this.TextBoxFilterFlags6.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFilterFlags6.TabIndex = 33;
            //
            //TextBoxFilterFlags7
            //
            this.TextBoxFilterFlags7.AcceptsReturn = true;
            this.TextBoxFilterFlags7.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFilterFlags7.Location = new System.Drawing.Point(527, 191);
            this.TextBoxFilterFlags7.MaxLength = 0;
            this.TextBoxFilterFlags7.Name = "TextBoxFilterFlags7";
            this.TextBoxFilterFlags7.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFilterFlags7.TabIndex = 38;
            //
            //TextBoxFilterFlags8
            //
            this.TextBoxFilterFlags8.AcceptsReturn = true;
            this.TextBoxFilterFlags8.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFilterFlags8.Location = new System.Drawing.Point(527, 215);
            this.TextBoxFilterFlags8.MaxLength = 0;
            this.TextBoxFilterFlags8.Name = "TextBoxFilterFlags8";
            this.TextBoxFilterFlags8.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFilterFlags8.TabIndex = 43;
            //
            //TextBoxFilterFlags9
            //
            this.TextBoxFilterFlags9.AcceptsReturn = true;
            this.TextBoxFilterFlags9.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFilterFlags9.Location = new System.Drawing.Point(527, 239);
            this.TextBoxFilterFlags9.MaxLength = 0;
            this.TextBoxFilterFlags9.Name = "TextBoxFilterFlags9";
            this.TextBoxFilterFlags9.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFilterFlags9.TabIndex = 48;
            //
            //ComboBoxFilterType0
            //
            this.ComboBoxFilterType0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFilterType0.Location = new System.Drawing.Point(615, 23);
            this.ComboBoxFilterType0.Name = "ComboBoxFilterType0";
            this.ComboBoxFilterType0.Size = new System.Drawing.Size(57, 21);
            this.ComboBoxFilterType0.TabIndex = 4;
            //
            //ComboBoxFilterType1
            //
            this.ComboBoxFilterType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFilterType1.Location = new System.Drawing.Point(615, 47);
            this.ComboBoxFilterType1.Name = "ComboBoxFilterType1";
            this.ComboBoxFilterType1.Size = new System.Drawing.Size(57, 21);
            this.ComboBoxFilterType1.TabIndex = 9;
            //
            //ComboBoxFilterType2
            //
            this.ComboBoxFilterType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFilterType2.Location = new System.Drawing.Point(615, 71);
            this.ComboBoxFilterType2.Name = "ComboBoxFilterType2";
            this.ComboBoxFilterType2.Size = new System.Drawing.Size(57, 21);
            this.ComboBoxFilterType2.TabIndex = 14;
            //
            //ComboBoxFilterType3
            //
            this.ComboBoxFilterType3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFilterType3.Location = new System.Drawing.Point(615, 95);
            this.ComboBoxFilterType3.Name = "ComboBoxFilterType3";
            this.ComboBoxFilterType3.Size = new System.Drawing.Size(57, 21);
            this.ComboBoxFilterType3.TabIndex = 19;
            //
            //ComboBoxFilterType4
            //
            this.ComboBoxFilterType4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFilterType4.Location = new System.Drawing.Point(615, 119);
            this.ComboBoxFilterType4.Name = "ComboBoxFilterType4";
            this.ComboBoxFilterType4.Size = new System.Drawing.Size(57, 21);
            this.ComboBoxFilterType4.TabIndex = 24;
            //
            //ComboBoxFilterType5
            //
            this.ComboBoxFilterType5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFilterType5.Location = new System.Drawing.Point(615, 143);
            this.ComboBoxFilterType5.Name = "ComboBoxFilterType5";
            this.ComboBoxFilterType5.Size = new System.Drawing.Size(57, 21);
            this.ComboBoxFilterType5.TabIndex = 29;
            //
            //ComboBoxFilterType6
            //
            this.ComboBoxFilterType6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFilterType6.Location = new System.Drawing.Point(615, 167);
            this.ComboBoxFilterType6.Name = "ComboBoxFilterType6";
            this.ComboBoxFilterType6.Size = new System.Drawing.Size(57, 21);
            this.ComboBoxFilterType6.TabIndex = 34;
            //
            //ComboBoxFilterType7
            //
            this.ComboBoxFilterType7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFilterType7.Location = new System.Drawing.Point(615, 191);
            this.ComboBoxFilterType7.Name = "ComboBoxFilterType7";
            this.ComboBoxFilterType7.Size = new System.Drawing.Size(57, 21);
            this.ComboBoxFilterType7.TabIndex = 39;
            //
            //ComboBoxFilterType8
            //
            this.ComboBoxFilterType8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFilterType8.Location = new System.Drawing.Point(615, 215);
            this.ComboBoxFilterType8.Name = "ComboBoxFilterType8";
            this.ComboBoxFilterType8.Size = new System.Drawing.Size(57, 21);
            this.ComboBoxFilterType8.TabIndex = 44;
            //
            //ComboBoxFilterType9
            //
            this.ComboBoxFilterType9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFilterType9.Location = new System.Drawing.Point(615, 239);
            this.ComboBoxFilterType9.Name = "ComboBoxFilterType9";
            this.ComboBoxFilterType9.Size = new System.Drawing.Size(57, 21);
            this.ComboBoxFilterType9.TabIndex = 49;
            //
            //TextBoxFilterPatt0
            //
            this.TextBoxFilterPatt0.AcceptsReturn = true;
            this.TextBoxFilterPatt0.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterPatt0.Location = new System.Drawing.Point(191, 23);
            this.TextBoxFilterPatt0.MaxLength = 0;
            this.TextBoxFilterPatt0.Name = "TextBoxFilterPatt0";
            this.TextBoxFilterPatt0.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterPatt0.TabIndex = 1;
            //
            //TextBoxFilterPatt1
            //
            this.TextBoxFilterPatt1.AcceptsReturn = true;
            this.TextBoxFilterPatt1.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterPatt1.Location = new System.Drawing.Point(191, 47);
            this.TextBoxFilterPatt1.MaxLength = 0;
            this.TextBoxFilterPatt1.Name = "TextBoxFilterPatt1";
            this.TextBoxFilterPatt1.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterPatt1.TabIndex = 6;
            //
            //TextBoxFilterPatt2
            //
            this.TextBoxFilterPatt2.AcceptsReturn = true;
            this.TextBoxFilterPatt2.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterPatt2.Location = new System.Drawing.Point(191, 71);
            this.TextBoxFilterPatt2.MaxLength = 0;
            this.TextBoxFilterPatt2.Name = "TextBoxFilterPatt2";
            this.TextBoxFilterPatt2.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterPatt2.TabIndex = 11;
            //
            //TextBoxFilterPatt3
            //
            this.TextBoxFilterPatt3.AcceptsReturn = true;
            this.TextBoxFilterPatt3.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterPatt3.Location = new System.Drawing.Point(191, 95);
            this.TextBoxFilterPatt3.MaxLength = 0;
            this.TextBoxFilterPatt3.Name = "TextBoxFilterPatt3";
            this.TextBoxFilterPatt3.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterPatt3.TabIndex = 16;
            //
            //TextBoxFilterPatt4
            //
            this.TextBoxFilterPatt4.AcceptsReturn = true;
            this.TextBoxFilterPatt4.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterPatt4.Location = new System.Drawing.Point(191, 119);
            this.TextBoxFilterPatt4.MaxLength = 0;
            this.TextBoxFilterPatt4.Name = "TextBoxFilterPatt4";
            this.TextBoxFilterPatt4.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterPatt4.TabIndex = 21;
            //
            //TextBoxFilterPatt5
            //
            this.TextBoxFilterPatt5.AcceptsReturn = true;
            this.TextBoxFilterPatt5.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterPatt5.Location = new System.Drawing.Point(191, 143);
            this.TextBoxFilterPatt5.MaxLength = 0;
            this.TextBoxFilterPatt5.Name = "TextBoxFilterPatt5";
            this.TextBoxFilterPatt5.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterPatt5.TabIndex = 26;
            //
            //TextBoxFilterPatt6
            //
            this.TextBoxFilterPatt6.AcceptsReturn = true;
            this.TextBoxFilterPatt6.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterPatt6.Location = new System.Drawing.Point(191, 167);
            this.TextBoxFilterPatt6.MaxLength = 0;
            this.TextBoxFilterPatt6.Name = "TextBoxFilterPatt6";
            this.TextBoxFilterPatt6.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterPatt6.TabIndex = 31;
            //
            //TextBoxFilterPatt7
            //
            this.TextBoxFilterPatt7.AcceptsReturn = true;
            this.TextBoxFilterPatt7.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterPatt7.Location = new System.Drawing.Point(191, 191);
            this.TextBoxFilterPatt7.MaxLength = 0;
            this.TextBoxFilterPatt7.Name = "TextBoxFilterPatt7";
            this.TextBoxFilterPatt7.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterPatt7.TabIndex = 36;
            //
            //TextBoxFilterPatt8
            //
            this.TextBoxFilterPatt8.AcceptsReturn = true;
            this.TextBoxFilterPatt8.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterPatt8.Location = new System.Drawing.Point(191, 215);
            this.TextBoxFilterPatt8.MaxLength = 0;
            this.TextBoxFilterPatt8.Name = "TextBoxFilterPatt8";
            this.TextBoxFilterPatt8.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterPatt8.TabIndex = 41;
            //
            //TextBoxFilterPatt9
            //
            this.TextBoxFilterPatt9.AcceptsReturn = true;
            this.TextBoxFilterPatt9.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterPatt9.Location = new System.Drawing.Point(191, 239);
            this.TextBoxFilterPatt9.MaxLength = 0;
            this.TextBoxFilterPatt9.Name = "TextBoxFilterPatt9";
            this.TextBoxFilterPatt9.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterPatt9.TabIndex = 46;
            //
            //TextBoxFilterMask0
            //
            this.TextBoxFilterMask0.AcceptsReturn = true;
            this.TextBoxFilterMask0.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterMask0.Location = new System.Drawing.Point(23, 23);
            this.TextBoxFilterMask0.MaxLength = 0;
            this.TextBoxFilterMask0.Name = "TextBoxFilterMask0";
            this.TextBoxFilterMask0.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterMask0.TabIndex = 0;
            //
            //TextBoxFilterMask1
            //
            this.TextBoxFilterMask1.AcceptsReturn = true;
            this.TextBoxFilterMask1.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterMask1.Location = new System.Drawing.Point(23, 47);
            this.TextBoxFilterMask1.MaxLength = 0;
            this.TextBoxFilterMask1.Name = "TextBoxFilterMask1";
            this.TextBoxFilterMask1.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterMask1.TabIndex = 5;
            //
            //TextBoxFilterMask2
            //
            this.TextBoxFilterMask2.AcceptsReturn = true;
            this.TextBoxFilterMask2.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterMask2.Location = new System.Drawing.Point(23, 71);
            this.TextBoxFilterMask2.MaxLength = 0;
            this.TextBoxFilterMask2.Name = "TextBoxFilterMask2";
            this.TextBoxFilterMask2.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterMask2.TabIndex = 10;
            //
            //TextBoxFilterMask3
            //
            this.TextBoxFilterMask3.AcceptsReturn = true;
            this.TextBoxFilterMask3.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterMask3.Location = new System.Drawing.Point(23, 95);
            this.TextBoxFilterMask3.MaxLength = 0;
            this.TextBoxFilterMask3.Name = "TextBoxFilterMask3";
            this.TextBoxFilterMask3.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterMask3.TabIndex = 15;
            //
            //TextBoxFilterMask4
            //
            this.TextBoxFilterMask4.AcceptsReturn = true;
            this.TextBoxFilterMask4.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterMask4.Location = new System.Drawing.Point(23, 119);
            this.TextBoxFilterMask4.MaxLength = 0;
            this.TextBoxFilterMask4.Name = "TextBoxFilterMask4";
            this.TextBoxFilterMask4.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterMask4.TabIndex = 20;
            //
            //TextBoxFilterMask5
            //
            this.TextBoxFilterMask5.AcceptsReturn = true;
            this.TextBoxFilterMask5.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterMask5.Location = new System.Drawing.Point(23, 143);
            this.TextBoxFilterMask5.MaxLength = 0;
            this.TextBoxFilterMask5.Name = "TextBoxFilterMask5";
            this.TextBoxFilterMask5.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterMask5.TabIndex = 25;
            //
            //TextBoxFilterMask6
            //
            this.TextBoxFilterMask6.AcceptsReturn = true;
            this.TextBoxFilterMask6.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterMask6.Location = new System.Drawing.Point(23, 167);
            this.TextBoxFilterMask6.MaxLength = 0;
            this.TextBoxFilterMask6.Name = "TextBoxFilterMask6";
            this.TextBoxFilterMask6.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterMask6.TabIndex = 30;
            //
            //TextBoxFilterMask7
            //
            this.TextBoxFilterMask7.AcceptsReturn = true;
            this.TextBoxFilterMask7.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterMask7.Location = new System.Drawing.Point(23, 191);
            this.TextBoxFilterMask7.MaxLength = 0;
            this.TextBoxFilterMask7.Name = "TextBoxFilterMask7";
            this.TextBoxFilterMask7.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterMask7.TabIndex = 35;
            //
            //TextBoxFilterMask8
            //
            this.TextBoxFilterMask8.AcceptsReturn = true;
            this.TextBoxFilterMask8.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterMask8.Location = new System.Drawing.Point(23, 215);
            this.TextBoxFilterMask8.MaxLength = 0;
            this.TextBoxFilterMask8.Name = "TextBoxFilterMask8";
            this.TextBoxFilterMask8.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterMask8.TabIndex = 40;
            //
            //TextBoxFilterMask9
            //
            this.TextBoxFilterMask9.AcceptsReturn = true;
            this.TextBoxFilterMask9.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFilterMask9.Location = new System.Drawing.Point(23, 239);
            this.TextBoxFilterMask9.MaxLength = 0;
            this.TextBoxFilterMask9.Name = "TextBoxFilterMask9";
            this.TextBoxFilterMask9.Size = new System.Drawing.Size(161, 20);
            this.TextBoxFilterMask9.TabIndex = 45;
            //
            //LabelChannel3
            //
            this.LabelChannel3.Location = new System.Drawing.Point(524, 336);
            this.LabelChannel3.Name = "LabelChannel3";
            this.LabelChannel3.Size = new System.Drawing.Size(49, 17);
            this.LabelChannel3.TabIndex = 57;
            this.LabelChannel3.Text = "Channel:";
            //
            //LabelDeviceCombo3
            //
            this.LabelDeviceCombo3.Location = new System.Drawing.Point(395, 336);
            this.LabelDeviceCombo3.Name = "LabelDeviceCombo3";
            this.LabelDeviceCombo3.Size = new System.Drawing.Size(89, 17);
            this.LabelDeviceCombo3.TabIndex = 56;
            this.LabelDeviceCombo3.Text = "Device:";
            //
            //LabelFilterIds
            //
            this.LabelFilterIds.Location = new System.Drawing.Point(671, 7);
            this.LabelFilterIds.Name = "LabelFilterIds";
            this.LabelFilterIds.Size = new System.Drawing.Size(43, 17);
            this.LabelFilterIds.TabIndex = 83;
            this.LabelFilterIds.Text = "En:";
            //
            //LabelFilterId0
            //
            this.LabelFilterId0.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.LabelFilterId0.Location = new System.Drawing.Point(671, 23);
            this.LabelFilterId0.Name = "LabelFilterId0";
            this.LabelFilterId0.Size = new System.Drawing.Size(22, 17);
            this.LabelFilterId0.TabIndex = 68;
            this.LabelFilterId0.Text = "*";
            this.LabelFilterId0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFilterId1
            //
            this.LabelFilterId1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.LabelFilterId1.Location = new System.Drawing.Point(671, 47);
            this.LabelFilterId1.Name = "LabelFilterId1";
            this.LabelFilterId1.Size = new System.Drawing.Size(22, 17);
            this.LabelFilterId1.TabIndex = 69;
            this.LabelFilterId1.Text = "*";
            this.LabelFilterId1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFilterId2
            //
            this.LabelFilterId2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.LabelFilterId2.Location = new System.Drawing.Point(671, 71);
            this.LabelFilterId2.Name = "LabelFilterId2";
            this.LabelFilterId2.Size = new System.Drawing.Size(22, 17);
            this.LabelFilterId2.TabIndex = 70;
            this.LabelFilterId2.Text = "*";
            this.LabelFilterId2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFilterId3
            //
            this.LabelFilterId3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.LabelFilterId3.Location = new System.Drawing.Point(671, 95);
            this.LabelFilterId3.Name = "LabelFilterId3";
            this.LabelFilterId3.Size = new System.Drawing.Size(22, 17);
            this.LabelFilterId3.TabIndex = 71;
            this.LabelFilterId3.Text = "*";
            this.LabelFilterId3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFilterId4
            //
            this.LabelFilterId4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.LabelFilterId4.Location = new System.Drawing.Point(671, 119);
            this.LabelFilterId4.Name = "LabelFilterId4";
            this.LabelFilterId4.Size = new System.Drawing.Size(22, 17);
            this.LabelFilterId4.TabIndex = 72;
            this.LabelFilterId4.Text = "*";
            this.LabelFilterId4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFilterId5
            //
            this.LabelFilterId5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.LabelFilterId5.Location = new System.Drawing.Point(671, 143);
            this.LabelFilterId5.Name = "LabelFilterId5";
            this.LabelFilterId5.Size = new System.Drawing.Size(22, 17);
            this.LabelFilterId5.TabIndex = 73;
            this.LabelFilterId5.Text = "*";
            this.LabelFilterId5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFilterId6
            //
            this.LabelFilterId6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.LabelFilterId6.Location = new System.Drawing.Point(671, 167);
            this.LabelFilterId6.Name = "LabelFilterId6";
            this.LabelFilterId6.Size = new System.Drawing.Size(22, 17);
            this.LabelFilterId6.TabIndex = 74;
            this.LabelFilterId6.Text = "*";
            this.LabelFilterId6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFilterId7
            //
            this.LabelFilterId7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.LabelFilterId7.Location = new System.Drawing.Point(671, 191);
            this.LabelFilterId7.Name = "LabelFilterId7";
            this.LabelFilterId7.Size = new System.Drawing.Size(22, 17);
            this.LabelFilterId7.TabIndex = 75;
            this.LabelFilterId7.Text = "*";
            this.LabelFilterId7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFilterId8
            //
            this.LabelFilterId8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.LabelFilterId8.Location = new System.Drawing.Point(671, 215);
            this.LabelFilterId8.Name = "LabelFilterId8";
            this.LabelFilterId8.Size = new System.Drawing.Size(22, 17);
            this.LabelFilterId8.TabIndex = 76;
            this.LabelFilterId8.Text = "*";
            this.LabelFilterId8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFilterId9
            //
            this.LabelFilterId9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.LabelFilterId9.Location = new System.Drawing.Point(671, 239);
            this.LabelFilterId9.Name = "LabelFilterId9";
            this.LabelFilterId9.Size = new System.Drawing.Size(22, 17);
            this.LabelFilterId9.TabIndex = 77;
            this.LabelFilterId9.Text = "*";
            this.LabelFilterId9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFilterType
            //
            this.LabelFilterType.Location = new System.Drawing.Point(615, 7);
            this.LabelFilterType.Name = "LabelFilterType";
            this.LabelFilterType.Size = new System.Drawing.Size(57, 17);
            this.LabelFilterType.TabIndex = 82;
            this.LabelFilterType.Text = "Filter:";
            //
            //LabelFilterFlags
            //
            this.LabelFilterFlags.Location = new System.Drawing.Point(527, 7);
            this.LabelFilterFlags.Name = "LabelFilterFlags";
            this.LabelFilterFlags.Size = new System.Drawing.Size(63, 17);
            this.LabelFilterFlags.TabIndex = 81;
            this.LabelFilterFlags.Text = "Tx Flags:";
            //
            //LabelFilterFlow
            //
            this.LabelFilterFlow.Location = new System.Drawing.Point(359, 7);
            this.LabelFilterFlow.Name = "LabelFilterFlow";
            this.LabelFilterFlow.Size = new System.Drawing.Size(137, 17);
            this.LabelFilterFlow.TabIndex = 80;
            this.LabelFilterFlow.Text = "Flow Control:";
            //
            //LabelFilterPatt
            //
            this.LabelFilterPatt.Location = new System.Drawing.Point(191, 7);
            this.LabelFilterPatt.Name = "LabelFilterPatt";
            this.LabelFilterPatt.Size = new System.Drawing.Size(140, 17);
            this.LabelFilterPatt.TabIndex = 79;
            this.LabelFilterPatt.Text = "Pattern:";
            //
            //LabelFilterMask
            //
            this.LabelFilterMask.Location = new System.Drawing.Point(23, 7);
            this.LabelFilterMask.Name = "LabelFilterMask";
            this.LabelFilterMask.Size = new System.Drawing.Size(161, 17);
            this.LabelFilterMask.TabIndex = 78;
            this.LabelFilterMask.Text = "Mask:";
            //
            //LabelFilter0
            //
            this.LabelFilter0.Location = new System.Drawing.Point(7, 23);
            this.LabelFilter0.Name = "LabelFilter0";
            this.LabelFilter0.Size = new System.Drawing.Size(9, 17);
            this.LabelFilter0.TabIndex = 58;
            this.LabelFilter0.Text = "0:";
            //
            //LabelFilter1
            //
            this.LabelFilter1.Location = new System.Drawing.Point(7, 47);
            this.LabelFilter1.Name = "LabelFilter1";
            this.LabelFilter1.Size = new System.Drawing.Size(9, 17);
            this.LabelFilter1.TabIndex = 59;
            this.LabelFilter1.Text = "1:";
            //
            //LabelFilter2
            //
            this.LabelFilter2.Location = new System.Drawing.Point(7, 71);
            this.LabelFilter2.Name = "LabelFilter2";
            this.LabelFilter2.Size = new System.Drawing.Size(9, 17);
            this.LabelFilter2.TabIndex = 60;
            this.LabelFilter2.Text = "2:";
            //
            //LabelFilter3
            //
            this.LabelFilter3.Location = new System.Drawing.Point(7, 95);
            this.LabelFilter3.Name = "LabelFilter3";
            this.LabelFilter3.Size = new System.Drawing.Size(9, 17);
            this.LabelFilter3.TabIndex = 61;
            this.LabelFilter3.Text = "3:";
            //
            //LabelFilter4
            //
            this.LabelFilter4.Location = new System.Drawing.Point(7, 119);
            this.LabelFilter4.Name = "LabelFilter4";
            this.LabelFilter4.Size = new System.Drawing.Size(9, 17);
            this.LabelFilter4.TabIndex = 62;
            this.LabelFilter4.Text = "4:";
            //
            //LabelFilter5
            //
            this.LabelFilter5.Location = new System.Drawing.Point(7, 143);
            this.LabelFilter5.Name = "LabelFilter5";
            this.LabelFilter5.Size = new System.Drawing.Size(9, 17);
            this.LabelFilter5.TabIndex = 63;
            this.LabelFilter5.Text = "5:";
            //
            //LabelFilter6
            //
            this.LabelFilter6.Location = new System.Drawing.Point(7, 167);
            this.LabelFilter6.Name = "LabelFilter6";
            this.LabelFilter6.Size = new System.Drawing.Size(9, 17);
            this.LabelFilter6.TabIndex = 64;
            this.LabelFilter6.Text = "6:";
            //
            //LabelFilter7
            //
            this.LabelFilter7.Location = new System.Drawing.Point(7, 191);
            this.LabelFilter7.Name = "LabelFilter7";
            this.LabelFilter7.Size = new System.Drawing.Size(9, 17);
            this.LabelFilter7.TabIndex = 65;
            this.LabelFilter7.Text = "7:";
            //
            //LabelFilter8
            //
            this.LabelFilter8.Location = new System.Drawing.Point(7, 215);
            this.LabelFilter8.Name = "LabelFilter8";
            this.LabelFilter8.Size = new System.Drawing.Size(9, 17);
            this.LabelFilter8.TabIndex = 66;
            this.LabelFilter8.Text = "8:";
            //
            //LabelFilter9
            //
            this.LabelFilter9.Location = new System.Drawing.Point(7, 239);
            this.LabelFilter9.Name = "LabelFilter9";
            this.LabelFilter9.Size = new System.Drawing.Size(9, 17);
            this.LabelFilter9.TabIndex = 67;
            this.LabelFilter9.Text = "9:";
            //
            //TabPageConfig
            //
            this.TabPageConfig.Controls.Add(this.GroupBoxAnalogConfig);
            this.TabPageConfig.Controls.Add(this.ComboBoxAvailableDevice);
            this.TabPageConfig.Controls.Add(this.ComboBoxAvailableChannel);
            this.TabPageConfig.Controls.Add(this.ButtonSetConfig);
            this.TabPageConfig.Controls.Add(this.ButtonClearConfig);
            this.TabPageConfig.Controls.Add(this.LabelParamVal0);
            this.TabPageConfig.Controls.Add(this.LabelParamVal1);
            this.TabPageConfig.Controls.Add(this.LabelParamVal2);
            this.TabPageConfig.Controls.Add(this.LabelParamVal3);
            this.TabPageConfig.Controls.Add(this.LabelParamVal4);
            this.TabPageConfig.Controls.Add(this.LabelParamVal5);
            this.TabPageConfig.Controls.Add(this.LabelParamVal6);
            this.TabPageConfig.Controls.Add(this.LabelParamVal7);
            this.TabPageConfig.Controls.Add(this.LabelParamVal8);
            this.TabPageConfig.Controls.Add(this.LabelParamVal9);
            this.TabPageConfig.Controls.Add(this.LabelParamVal10);
            this.TabPageConfig.Controls.Add(this.LabelParamVal11);
            this.TabPageConfig.Controls.Add(this.LabelParamVal12);
            this.TabPageConfig.Controls.Add(this.LabelParamVal13);
            this.TabPageConfig.Controls.Add(this.LabelParamVal14);
            this.TabPageConfig.Controls.Add(this.LabelParamVal15);
            this.TabPageConfig.Controls.Add(this.LabelParamVal16);
            this.TabPageConfig.Controls.Add(this.LabelParamVal17);
            this.TabPageConfig.Controls.Add(this.LabelParamVal18);
            this.TabPageConfig.Controls.Add(this.LabelParamVal19);
            this.TabPageConfig.Controls.Add(this.LabelParamVal20);
            this.TabPageConfig.Controls.Add(this.LabelParamVal21);
            this.TabPageConfig.Controls.Add(this.LabelParameterName0);
            this.TabPageConfig.Controls.Add(this.LabelParameterName1);
            this.TabPageConfig.Controls.Add(this.LabelParameterName2);
            this.TabPageConfig.Controls.Add(this.LabelParameterName3);
            this.TabPageConfig.Controls.Add(this.LabelParameterName4);
            this.TabPageConfig.Controls.Add(this.LabelParameterName5);
            this.TabPageConfig.Controls.Add(this.LabelParameterName6);
            this.TabPageConfig.Controls.Add(this.LabelParameterName7);
            this.TabPageConfig.Controls.Add(this.LabelParameterName8);
            this.TabPageConfig.Controls.Add(this.LabelParameterName9);
            this.TabPageConfig.Controls.Add(this.LabelParameterName10);
            this.TabPageConfig.Controls.Add(this.LabelParameterName11);
            this.TabPageConfig.Controls.Add(this.LabelParameterName12);
            this.TabPageConfig.Controls.Add(this.LabelParameterName13);
            this.TabPageConfig.Controls.Add(this.LabelParameterName14);
            this.TabPageConfig.Controls.Add(this.LabelParameterName15);
            this.TabPageConfig.Controls.Add(this.LabelParameterName16);
            this.TabPageConfig.Controls.Add(this.LabelParameterName17);
            this.TabPageConfig.Controls.Add(this.LabelParameterName18);
            this.TabPageConfig.Controls.Add(this.LabelParameterName19);
            this.TabPageConfig.Controls.Add(this.LabelParameterName20);
            this.TabPageConfig.Controls.Add(this.LabelParameterName21);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal0);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal1);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal2);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal3);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal4);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal5);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal6);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal7);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal8);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal9);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal10);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal11);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal12);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal13);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal14);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal15);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal16);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal17);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal18);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal19);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal20);
            this.TabPageConfig.Controls.Add(this.TextBoxParamVal21);
            this.TabPageConfig.Controls.Add(this.LabelChannel4);
            this.TabPageConfig.Controls.Add(this.LabelDeviceCombo4);
            this.TabPageConfig.Controls.Add(this.LabelParameters0);
            this.TabPageConfig.Controls.Add(this.LabelValues0);
            this.TabPageConfig.Controls.Add(this.LabelMod0);
            this.TabPageConfig.Controls.Add(this.LabelParameters1);
            this.TabPageConfig.Controls.Add(this.LabelValues1);
            this.TabPageConfig.Controls.Add(this.LabelMod1);
            this.TabPageConfig.Controls.Add(this.ComboAvailableBoxLocator4);
            this.TabPageConfig.Controls.Add(this.ComboAvailableChannelLocator4);
            this.TabPageConfig.Location = new System.Drawing.Point(4, 22);
            this.TabPageConfig.Name = "TabPageConfig";
            this.TabPageConfig.Size = new System.Drawing.Size(714, 395);
            this.TabPageConfig.TabIndex = 4;
            this.TabPageConfig.Text = "Config";
            //
            //GroupBoxAnalogConfig
            //
            this.GroupBoxAnalogConfig.Controls.Add(this.LabelMaxSample);
            this.GroupBoxAnalogConfig.Controls.Add(this.LabelAnalogRate);
            this.GroupBoxAnalogConfig.Controls.Add(this.TextBoxAnalogRate);
            this.GroupBoxAnalogConfig.Controls.Add(this.PanelAudioChannel);
            this.GroupBoxAnalogConfig.Controls.Add(this.ComboBoxAnalogRateMode);
            this.GroupBoxAnalogConfig.Location = new System.Drawing.Point(33, 290);
            this.GroupBoxAnalogConfig.Name = "GroupBoxAnalogConfig";
            this.GroupBoxAnalogConfig.Size = new System.Drawing.Size(293, 193);
            this.GroupBoxAnalogConfig.TabIndex = 24;
            this.GroupBoxAnalogConfig.TabStop = false;
            this.GroupBoxAnalogConfig.Text = "Analog Channels and Rate:";
            this.GroupBoxAnalogConfig.Visible = false;
            //
            //LabelMaxSample
            //
            this.LabelMaxSample.AutoSize = true;
            this.LabelMaxSample.Location = new System.Drawing.Point(23, 169);
            this.LabelMaxSample.Name = "LabelMaxSample";
            this.LabelMaxSample.Size = new System.Drawing.Size(87, 13);
            this.LabelMaxSample.TabIndex = 16;
            this.LabelMaxSample.Text = "Max sample rate:";
            //
            //LabelAnalogRate
            //
            this.LabelAnalogRate.AutoSize = true;
            this.LabelAnalogRate.Location = new System.Drawing.Point(20, 123);
            this.LabelAnalogRate.Name = "LabelAnalogRate";
            this.LabelAnalogRate.Size = new System.Drawing.Size(33, 13);
            this.LabelAnalogRate.TabIndex = 15;
            this.LabelAnalogRate.Text = "Rate:";
            //
            //TextBoxAnalogRate
            //
            this.TextBoxAnalogRate.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxAnalogRate.Location = new System.Drawing.Point(23, 139);
            this.TextBoxAnalogRate.Name = "TextBoxAnalogRate";
            this.TextBoxAnalogRate.Size = new System.Drawing.Size(100, 20);
            this.TextBoxAnalogRate.TabIndex = 13;
            //
            //PanelAudioChannel
            //
            this.PanelAudioChannel.AutoScroll = true;
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH0);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH1);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH2);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH3);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH4);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH5);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH6);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH7);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH8);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH9);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH10);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH11);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH12);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH13);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH14);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH15);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH16);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH17);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH18);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH19);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH20);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH21);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH22);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH23);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH24);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH25);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH26);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH27);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH28);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH29);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH30);
            this.PanelAudioChannel.Controls.Add(this.CheckBoxCH31);
            this.PanelAudioChannel.Location = new System.Drawing.Point(6, 19);
            this.PanelAudioChannel.Name = "PanelAudioChannel";
            this.PanelAudioChannel.Size = new System.Drawing.Size(269, 95);
            this.PanelAudioChannel.TabIndex = 0;
            //
            //CheckBoxCH0
            //
            this.CheckBoxCH0.AutoSize = true;
            this.CheckBoxCH0.Location = new System.Drawing.Point(4, 4);
            this.CheckBoxCH0.Name = "CheckBoxCH0";
            this.CheckBoxCH0.Size = new System.Drawing.Size(48, 17);
            this.CheckBoxCH0.TabIndex = 0;
            this.CheckBoxCH0.Text = "Ch 1";
            this.CheckBoxCH0.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH1
            //
            this.CheckBoxCH1.AutoSize = true;
            this.CheckBoxCH1.Location = new System.Drawing.Point(63, 4);
            this.CheckBoxCH1.Name = "CheckBoxCH1";
            this.CheckBoxCH1.Size = new System.Drawing.Size(48, 17);
            this.CheckBoxCH1.TabIndex = 1;
            this.CheckBoxCH1.Text = "Ch 2";
            this.CheckBoxCH1.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH2
            //
            this.CheckBoxCH2.AutoSize = true;
            this.CheckBoxCH2.Location = new System.Drawing.Point(122, 4);
            this.CheckBoxCH2.Name = "CheckBoxCH2";
            this.CheckBoxCH2.Size = new System.Drawing.Size(48, 17);
            this.CheckBoxCH2.TabIndex = 2;
            this.CheckBoxCH2.Text = "Ch 3";
            this.CheckBoxCH2.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH3
            //
            this.CheckBoxCH3.AutoSize = true;
            this.CheckBoxCH3.Location = new System.Drawing.Point(181, 4);
            this.CheckBoxCH3.Name = "CheckBoxCH3";
            this.CheckBoxCH3.Size = new System.Drawing.Size(48, 17);
            this.CheckBoxCH3.TabIndex = 3;
            this.CheckBoxCH3.Text = "Ch 4";
            this.CheckBoxCH3.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH4
            //
            this.CheckBoxCH4.AutoSize = true;
            this.CheckBoxCH4.Location = new System.Drawing.Point(4, 27);
            this.CheckBoxCH4.Name = "CheckBoxCH4";
            this.CheckBoxCH4.Size = new System.Drawing.Size(48, 17);
            this.CheckBoxCH4.TabIndex = 4;
            this.CheckBoxCH4.Text = "Ch 5";
            this.CheckBoxCH4.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH5
            //
            this.CheckBoxCH5.AutoSize = true;
            this.CheckBoxCH5.Location = new System.Drawing.Point(63, 27);
            this.CheckBoxCH5.Name = "CheckBoxCH5";
            this.CheckBoxCH5.Size = new System.Drawing.Size(48, 17);
            this.CheckBoxCH5.TabIndex = 5;
            this.CheckBoxCH5.Text = "Ch 6";
            this.CheckBoxCH5.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH6
            //
            this.CheckBoxCH6.AutoSize = true;
            this.CheckBoxCH6.Location = new System.Drawing.Point(122, 27);
            this.CheckBoxCH6.Name = "CheckBoxCH6";
            this.CheckBoxCH6.Size = new System.Drawing.Size(48, 17);
            this.CheckBoxCH6.TabIndex = 6;
            this.CheckBoxCH6.Text = "Ch 7";
            this.CheckBoxCH6.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH7
            //
            this.CheckBoxCH7.AutoSize = true;
            this.CheckBoxCH7.Location = new System.Drawing.Point(181, 27);
            this.CheckBoxCH7.Name = "CheckBoxCH7";
            this.CheckBoxCH7.Size = new System.Drawing.Size(48, 17);
            this.CheckBoxCH7.TabIndex = 7;
            this.CheckBoxCH7.Text = "Ch 8";
            this.CheckBoxCH7.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH8
            //
            this.CheckBoxCH8.AutoSize = true;
            this.CheckBoxCH8.Location = new System.Drawing.Point(4, 50);
            this.CheckBoxCH8.Name = "CheckBoxCH8";
            this.CheckBoxCH8.Size = new System.Drawing.Size(48, 17);
            this.CheckBoxCH8.TabIndex = 8;
            this.CheckBoxCH8.Text = "Ch 9";
            this.CheckBoxCH8.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH9
            //
            this.CheckBoxCH9.AutoSize = true;
            this.CheckBoxCH9.Location = new System.Drawing.Point(63, 50);
            this.CheckBoxCH9.Name = "CheckBoxCH9";
            this.CheckBoxCH9.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH9.TabIndex = 9;
            this.CheckBoxCH9.Text = "Ch 10";
            this.CheckBoxCH9.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH10
            //
            this.CheckBoxCH10.AutoSize = true;
            this.CheckBoxCH10.Location = new System.Drawing.Point(122, 50);
            this.CheckBoxCH10.Name = "CheckBoxCH10";
            this.CheckBoxCH10.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH10.TabIndex = 10;
            this.CheckBoxCH10.Text = "Ch 11";
            this.CheckBoxCH10.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH11
            //
            this.CheckBoxCH11.AutoSize = true;
            this.CheckBoxCH11.Location = new System.Drawing.Point(181, 50);
            this.CheckBoxCH11.Name = "CheckBoxCH11";
            this.CheckBoxCH11.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH11.TabIndex = 11;
            this.CheckBoxCH11.Text = "Ch 12";
            this.CheckBoxCH11.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH12
            //
            this.CheckBoxCH12.AutoSize = true;
            this.CheckBoxCH12.Location = new System.Drawing.Point(4, 73);
            this.CheckBoxCH12.Name = "CheckBoxCH12";
            this.CheckBoxCH12.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH12.TabIndex = 12;
            this.CheckBoxCH12.Text = "Ch 13";
            this.CheckBoxCH12.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH13
            //
            this.CheckBoxCH13.AutoSize = true;
            this.CheckBoxCH13.Location = new System.Drawing.Point(63, 73);
            this.CheckBoxCH13.Name = "CheckBoxCH13";
            this.CheckBoxCH13.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH13.TabIndex = 13;
            this.CheckBoxCH13.Text = "Ch 14";
            this.CheckBoxCH13.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH14
            //
            this.CheckBoxCH14.AutoSize = true;
            this.CheckBoxCH14.Location = new System.Drawing.Point(122, 73);
            this.CheckBoxCH14.Name = "CheckBoxCH14";
            this.CheckBoxCH14.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH14.TabIndex = 14;
            this.CheckBoxCH14.Text = "Ch 15";
            this.CheckBoxCH14.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH15
            //
            this.CheckBoxCH15.AutoSize = true;
            this.CheckBoxCH15.Location = new System.Drawing.Point(181, 73);
            this.CheckBoxCH15.Name = "CheckBoxCH15";
            this.CheckBoxCH15.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH15.TabIndex = 15;
            this.CheckBoxCH15.Text = "Ch 16";
            this.CheckBoxCH15.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH16
            //
            this.CheckBoxCH16.AutoSize = true;
            this.CheckBoxCH16.Location = new System.Drawing.Point(4, 96);
            this.CheckBoxCH16.Name = "CheckBoxCH16";
            this.CheckBoxCH16.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH16.TabIndex = 16;
            this.CheckBoxCH16.Text = "Ch 17";
            this.CheckBoxCH16.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH17
            //
            this.CheckBoxCH17.AutoSize = true;
            this.CheckBoxCH17.Location = new System.Drawing.Point(63, 96);
            this.CheckBoxCH17.Name = "CheckBoxCH17";
            this.CheckBoxCH17.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH17.TabIndex = 17;
            this.CheckBoxCH17.Text = "Ch 18";
            this.CheckBoxCH17.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH18
            //
            this.CheckBoxCH18.AutoSize = true;
            this.CheckBoxCH18.Location = new System.Drawing.Point(122, 96);
            this.CheckBoxCH18.Name = "CheckBoxCH18";
            this.CheckBoxCH18.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH18.TabIndex = 18;
            this.CheckBoxCH18.Text = "Ch 19";
            this.CheckBoxCH18.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH19
            //
            this.CheckBoxCH19.AutoSize = true;
            this.CheckBoxCH19.Location = new System.Drawing.Point(181, 96);
            this.CheckBoxCH19.Name = "CheckBoxCH19";
            this.CheckBoxCH19.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH19.TabIndex = 19;
            this.CheckBoxCH19.Text = "Ch 20";
            this.CheckBoxCH19.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH20
            //
            this.CheckBoxCH20.AutoSize = true;
            this.CheckBoxCH20.Location = new System.Drawing.Point(4, 119);
            this.CheckBoxCH20.Name = "CheckBoxCH20";
            this.CheckBoxCH20.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH20.TabIndex = 20;
            this.CheckBoxCH20.Text = "Ch 21";
            this.CheckBoxCH20.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH21
            //
            this.CheckBoxCH21.AutoSize = true;
            this.CheckBoxCH21.Location = new System.Drawing.Point(63, 119);
            this.CheckBoxCH21.Name = "CheckBoxCH21";
            this.CheckBoxCH21.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH21.TabIndex = 21;
            this.CheckBoxCH21.Text = "Ch 22";
            this.CheckBoxCH21.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH22
            //
            this.CheckBoxCH22.AutoSize = true;
            this.CheckBoxCH22.Location = new System.Drawing.Point(122, 119);
            this.CheckBoxCH22.Name = "CheckBoxCH22";
            this.CheckBoxCH22.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH22.TabIndex = 22;
            this.CheckBoxCH22.Text = "Ch 23";
            this.CheckBoxCH22.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH23
            //
            this.CheckBoxCH23.AutoSize = true;
            this.CheckBoxCH23.Location = new System.Drawing.Point(181, 119);
            this.CheckBoxCH23.Name = "CheckBoxCH23";
            this.CheckBoxCH23.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH23.TabIndex = 23;
            this.CheckBoxCH23.Text = "Ch 24";
            this.CheckBoxCH23.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH24
            //
            this.CheckBoxCH24.AutoSize = true;
            this.CheckBoxCH24.Location = new System.Drawing.Point(4, 142);
            this.CheckBoxCH24.Name = "CheckBoxCH24";
            this.CheckBoxCH24.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH24.TabIndex = 24;
            this.CheckBoxCH24.Text = "Ch 25";
            this.CheckBoxCH24.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH25
            //
            this.CheckBoxCH25.AutoSize = true;
            this.CheckBoxCH25.Location = new System.Drawing.Point(63, 142);
            this.CheckBoxCH25.Name = "CheckBoxCH25";
            this.CheckBoxCH25.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH25.TabIndex = 25;
            this.CheckBoxCH25.Text = "Ch 26";
            this.CheckBoxCH25.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH26
            //
            this.CheckBoxCH26.AutoSize = true;
            this.CheckBoxCH26.Location = new System.Drawing.Point(122, 142);
            this.CheckBoxCH26.Name = "CheckBoxCH26";
            this.CheckBoxCH26.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH26.TabIndex = 26;
            this.CheckBoxCH26.Text = "Ch 27";
            this.CheckBoxCH26.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH27
            //
            this.CheckBoxCH27.AutoSize = true;
            this.CheckBoxCH27.Location = new System.Drawing.Point(181, 142);
            this.CheckBoxCH27.Name = "CheckBoxCH27";
            this.CheckBoxCH27.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH27.TabIndex = 27;
            this.CheckBoxCH27.Text = "Ch 28";
            this.CheckBoxCH27.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH28
            //
            this.CheckBoxCH28.AutoSize = true;
            this.CheckBoxCH28.Location = new System.Drawing.Point(4, 165);
            this.CheckBoxCH28.Name = "CheckBoxCH28";
            this.CheckBoxCH28.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH28.TabIndex = 28;
            this.CheckBoxCH28.Text = "Ch 29";
            this.CheckBoxCH28.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH29
            //
            this.CheckBoxCH29.AutoSize = true;
            this.CheckBoxCH29.Location = new System.Drawing.Point(63, 165);
            this.CheckBoxCH29.Name = "CheckBoxCH29";
            this.CheckBoxCH29.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH29.TabIndex = 29;
            this.CheckBoxCH29.Text = "Ch 30";
            this.CheckBoxCH29.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH30
            //
            this.CheckBoxCH30.AutoSize = true;
            this.CheckBoxCH30.Location = new System.Drawing.Point(122, 165);
            this.CheckBoxCH30.Name = "CheckBoxCH30";
            this.CheckBoxCH30.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH30.TabIndex = 30;
            this.CheckBoxCH30.Text = "Ch 31";
            this.CheckBoxCH30.UseVisualStyleBackColor = true;
            //
            //CheckBoxCH31
            //
            this.CheckBoxCH31.AutoSize = true;
            this.CheckBoxCH31.Location = new System.Drawing.Point(181, 165);
            this.CheckBoxCH31.Name = "CheckBoxCH31";
            this.CheckBoxCH31.Size = new System.Drawing.Size(54, 17);
            this.CheckBoxCH31.TabIndex = 31;
            this.CheckBoxCH31.Text = "Ch 32";
            this.CheckBoxCH31.UseVisualStyleBackColor = true;
            //
            //ComboBoxAnalogRateMode
            //
            this.ComboBoxAnalogRateMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxAnalogRateMode.FormattingEnabled = true;
            this.ComboBoxAnalogRateMode.Items.AddRange(new object[] {"Readings/Sec", "Seconds"});
            this.ComboBoxAnalogRateMode.Location = new System.Drawing.Point(129, 138);
            this.ComboBoxAnalogRateMode.Name = "ComboBoxAnalogRateMode";
            this.ComboBoxAnalogRateMode.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxAnalogRateMode.TabIndex = 14;
            //
            //ComboBoxAvailableDevice
            //
            this.ComboBoxAvailableDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxAvailableDevice.Location = new System.Drawing.Point(398, 351);
            this.ComboBoxAvailableDevice.Name = "ComboBoxAvailableDevice";
            this.ComboBoxAvailableDevice.Size = new System.Drawing.Size(120, 21);
            this.ComboBoxAvailableDevice.TabIndex = 500;
            //
            //ComboBoxAvailableChannel
            //
            this.ComboBoxAvailableChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxAvailableChannel.Location = new System.Drawing.Point(527, 351);
            this.ComboBoxAvailableChannel.Name = "ComboBoxAvailableChannel";
            this.ComboBoxAvailableChannel.Size = new System.Drawing.Size(161, 21);
            this.ComboBoxAvailableChannel.TabIndex = 501;
            //
            //ButtonSetConfig
            //
            this.ButtonSetConfig.Location = new System.Drawing.Point(631, 290);
            this.ButtonSetConfig.Name = "ButtonSetConfig";
            this.ButtonSetConfig.Size = new System.Drawing.Size(57, 25);
            this.ButtonSetConfig.TabIndex = 22;
            this.ButtonSetConfig.Text = "Set";
            this.ButtonSetConfig.UseVisualStyleBackColor = true;
            //
            //ButtonClearConfig
            //
            this.ButtonClearConfig.Location = new System.Drawing.Point(631, 322);
            this.ButtonClearConfig.Name = "ButtonClearConfig";
            this.ButtonClearConfig.Size = new System.Drawing.Size(57, 25);
            this.ButtonClearConfig.TabIndex = 23;
            this.ButtonClearConfig.Text = "Clear";
            this.ButtonClearConfig.UseVisualStyleBackColor = true;
            //
            //LabelParamVal0
            //
            this.LabelParamVal0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal0.Location = new System.Drawing.Point(168, 24);
            this.LabelParamVal0.Name = "LabelParamVal0";
            this.LabelParamVal0.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal0.TabIndex = 36;
            this.LabelParamVal0.Visible = false;
            //
            //LabelParamVal1
            //
            this.LabelParamVal1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal1.Location = new System.Drawing.Point(168, 48);
            this.LabelParamVal1.Name = "LabelParamVal1";
            this.LabelParamVal1.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal1.TabIndex = 37;
            this.LabelParamVal1.Visible = false;
            //
            //LabelParamVal2
            //
            this.LabelParamVal2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal2.Location = new System.Drawing.Point(168, 72);
            this.LabelParamVal2.Name = "LabelParamVal2";
            this.LabelParamVal2.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal2.TabIndex = 38;
            this.LabelParamVal2.Visible = false;
            //
            //LabelParamVal3
            //
            this.LabelParamVal3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal3.Location = new System.Drawing.Point(168, 96);
            this.LabelParamVal3.Name = "LabelParamVal3";
            this.LabelParamVal3.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal3.TabIndex = 39;
            this.LabelParamVal3.Visible = false;
            //
            //LabelParamVal4
            //
            this.LabelParamVal4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal4.Location = new System.Drawing.Point(168, 120);
            this.LabelParamVal4.Name = "LabelParamVal4";
            this.LabelParamVal4.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal4.TabIndex = 40;
            this.LabelParamVal4.Visible = false;
            //
            //LabelParamVal5
            //
            this.LabelParamVal5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal5.Location = new System.Drawing.Point(168, 144);
            this.LabelParamVal5.Name = "LabelParamVal5";
            this.LabelParamVal5.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal5.TabIndex = 41;
            this.LabelParamVal5.Visible = false;
            //
            //LabelParamVal6
            //
            this.LabelParamVal6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal6.Location = new System.Drawing.Point(168, 168);
            this.LabelParamVal6.Name = "LabelParamVal6";
            this.LabelParamVal6.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal6.TabIndex = 42;
            this.LabelParamVal6.Visible = false;
            //
            //LabelParamVal7
            //
            this.LabelParamVal7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal7.Location = new System.Drawing.Point(168, 192);
            this.LabelParamVal7.Name = "LabelParamVal7";
            this.LabelParamVal7.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal7.TabIndex = 43;
            this.LabelParamVal7.Visible = false;
            //
            //LabelParamVal8
            //
            this.LabelParamVal8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal8.Location = new System.Drawing.Point(168, 216);
            this.LabelParamVal8.Name = "LabelParamVal8";
            this.LabelParamVal8.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal8.TabIndex = 44;
            this.LabelParamVal8.Visible = false;
            //
            //LabelParamVal9
            //
            this.LabelParamVal9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal9.Location = new System.Drawing.Point(168, 240);
            this.LabelParamVal9.Name = "LabelParamVal9";
            this.LabelParamVal9.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal9.TabIndex = 45;
            this.LabelParamVal9.Visible = false;
            //
            //LabelParamVal10
            //
            this.LabelParamVal10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal10.Location = new System.Drawing.Point(168, 264);
            this.LabelParamVal10.Name = "LabelParamVal10";
            this.LabelParamVal10.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal10.TabIndex = 46;
            this.LabelParamVal10.Visible = false;
            //
            //LabelParamVal11
            //
            this.LabelParamVal11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal11.Location = new System.Drawing.Point(488, 24);
            this.LabelParamVal11.Name = "LabelParamVal11";
            this.LabelParamVal11.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal11.TabIndex = 58;
            this.LabelParamVal11.Visible = false;
            //
            //LabelParamVal12
            //
            this.LabelParamVal12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal12.Location = new System.Drawing.Point(488, 48);
            this.LabelParamVal12.Name = "LabelParamVal12";
            this.LabelParamVal12.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal12.TabIndex = 59;
            this.LabelParamVal12.Visible = false;
            //
            //LabelParamVal13
            //
            this.LabelParamVal13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal13.Location = new System.Drawing.Point(488, 72);
            this.LabelParamVal13.Name = "LabelParamVal13";
            this.LabelParamVal13.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal13.TabIndex = 60;
            this.LabelParamVal13.Visible = false;
            //
            //LabelParamVal14
            //
            this.LabelParamVal14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal14.Location = new System.Drawing.Point(488, 96);
            this.LabelParamVal14.Name = "LabelParamVal14";
            this.LabelParamVal14.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal14.TabIndex = 61;
            this.LabelParamVal14.Visible = false;
            //
            //LabelParamVal15
            //
            this.LabelParamVal15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal15.Location = new System.Drawing.Point(488, 120);
            this.LabelParamVal15.Name = "LabelParamVal15";
            this.LabelParamVal15.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal15.TabIndex = 62;
            this.LabelParamVal15.Visible = false;
            //
            //LabelParamVal16
            //
            this.LabelParamVal16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal16.Location = new System.Drawing.Point(488, 144);
            this.LabelParamVal16.Name = "LabelParamVal16";
            this.LabelParamVal16.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal16.TabIndex = 63;
            this.LabelParamVal16.Visible = false;
            //
            //LabelParamVal17
            //
            this.LabelParamVal17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal17.Location = new System.Drawing.Point(488, 168);
            this.LabelParamVal17.Name = "LabelParamVal17";
            this.LabelParamVal17.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal17.TabIndex = 64;
            this.LabelParamVal17.Visible = false;
            //
            //LabelParamVal18
            //
            this.LabelParamVal18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal18.Location = new System.Drawing.Point(488, 192);
            this.LabelParamVal18.Name = "LabelParamVal18";
            this.LabelParamVal18.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal18.TabIndex = 65;
            this.LabelParamVal18.Visible = false;
            //
            //LabelParamVal19
            //
            this.LabelParamVal19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal19.Location = new System.Drawing.Point(488, 216);
            this.LabelParamVal19.Name = "LabelParamVal19";
            this.LabelParamVal19.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal19.TabIndex = 66;
            this.LabelParamVal19.Visible = false;
            //
            //LabelParamVal20
            //
            this.LabelParamVal20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal20.Location = new System.Drawing.Point(488, 240);
            this.LabelParamVal20.Name = "LabelParamVal20";
            this.LabelParamVal20.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal20.TabIndex = 67;
            this.LabelParamVal20.Visible = false;
            //
            //LabelParamVal21
            //
            this.LabelParamVal21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelParamVal21.Location = new System.Drawing.Point(488, 264);
            this.LabelParamVal21.Name = "LabelParamVal21";
            this.LabelParamVal21.Size = new System.Drawing.Size(70, 19);
            this.LabelParamVal21.TabIndex = 68;
            this.LabelParamVal21.Visible = false;
            //
            //LabelParameterName0
            //
            this.LabelParameterName0.Location = new System.Drawing.Point(0, 24);
            this.LabelParameterName0.Name = "LabelParameterName0";
            this.LabelParameterName0.Size = new System.Drawing.Size(161, 17);
            this.LabelParameterName0.TabIndex = 25;
            this.LabelParameterName0.Text = "LabelParameterName(0)";
            this.LabelParameterName0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName0.Visible = false;
            //
            //LabelParameterName1
            //
            this.LabelParameterName1.Location = new System.Drawing.Point(0, 48);
            this.LabelParameterName1.Name = "LabelParameterName1";
            this.LabelParameterName1.Size = new System.Drawing.Size(161, 17);
            this.LabelParameterName1.TabIndex = 26;
            this.LabelParameterName1.Text = "LabelParameterName(1)";
            this.LabelParameterName1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName1.Visible = false;
            //
            //LabelParameterName2
            //
            this.LabelParameterName2.Location = new System.Drawing.Point(0, 72);
            this.LabelParameterName2.Name = "LabelParameterName2";
            this.LabelParameterName2.Size = new System.Drawing.Size(161, 17);
            this.LabelParameterName2.TabIndex = 27;
            this.LabelParameterName2.Text = "LabelParameterName(2)";
            this.LabelParameterName2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName2.Visible = false;
            //
            //LabelParameterName3
            //
            this.LabelParameterName3.Location = new System.Drawing.Point(0, 96);
            this.LabelParameterName3.Name = "LabelParameterName3";
            this.LabelParameterName3.Size = new System.Drawing.Size(161, 17);
            this.LabelParameterName3.TabIndex = 28;
            this.LabelParameterName3.Text = "LabelParameterName(3)";
            this.LabelParameterName3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName3.Visible = false;
            //
            //LabelParameterName4
            //
            this.LabelParameterName4.Location = new System.Drawing.Point(0, 120);
            this.LabelParameterName4.Name = "LabelParameterName4";
            this.LabelParameterName4.Size = new System.Drawing.Size(161, 17);
            this.LabelParameterName4.TabIndex = 29;
            this.LabelParameterName4.Text = "LabelParameterName(4)";
            this.LabelParameterName4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName4.Visible = false;
            //
            //LabelParameterName5
            //
            this.LabelParameterName5.Location = new System.Drawing.Point(0, 144);
            this.LabelParameterName5.Name = "LabelParameterName5";
            this.LabelParameterName5.Size = new System.Drawing.Size(161, 17);
            this.LabelParameterName5.TabIndex = 30;
            this.LabelParameterName5.Text = "LabelParameterName(5)";
            this.LabelParameterName5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName5.Visible = false;
            //
            //LabelParameterName6
            //
            this.LabelParameterName6.Location = new System.Drawing.Point(0, 168);
            this.LabelParameterName6.Name = "LabelParameterName6";
            this.LabelParameterName6.Size = new System.Drawing.Size(161, 17);
            this.LabelParameterName6.TabIndex = 31;
            this.LabelParameterName6.Text = "LabelParameterName(6)";
            this.LabelParameterName6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName6.Visible = false;
            //
            //LabelParameterName7
            //
            this.LabelParameterName7.Location = new System.Drawing.Point(0, 192);
            this.LabelParameterName7.Name = "LabelParameterName7";
            this.LabelParameterName7.Size = new System.Drawing.Size(161, 17);
            this.LabelParameterName7.TabIndex = 32;
            this.LabelParameterName7.Text = "LabelParameterName(7)";
            this.LabelParameterName7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName7.Visible = false;
            //
            //LabelParameterName8
            //
            this.LabelParameterName8.Location = new System.Drawing.Point(0, 216);
            this.LabelParameterName8.Name = "LabelParameterName8";
            this.LabelParameterName8.Size = new System.Drawing.Size(161, 17);
            this.LabelParameterName8.TabIndex = 33;
            this.LabelParameterName8.Text = "LabelParameterName(8)";
            this.LabelParameterName8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName8.Visible = false;
            //
            //LabelParameterName9
            //
            this.LabelParameterName9.Location = new System.Drawing.Point(0, 240);
            this.LabelParameterName9.Name = "LabelParameterName9";
            this.LabelParameterName9.Size = new System.Drawing.Size(161, 17);
            this.LabelParameterName9.TabIndex = 34;
            this.LabelParameterName9.Text = "LabelParameterName(9)";
            this.LabelParameterName9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName9.Visible = false;
            //
            //LabelParameterName10
            //
            this.LabelParameterName10.Location = new System.Drawing.Point(0, 264);
            this.LabelParameterName10.Name = "LabelParameterName10";
            this.LabelParameterName10.Size = new System.Drawing.Size(161, 17);
            this.LabelParameterName10.TabIndex = 35;
            this.LabelParameterName10.Text = "LabelParameterName(10)";
            this.LabelParameterName10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName10.Visible = false;
            //
            //LabelParameterName11
            //
            this.LabelParameterName11.Location = new System.Drawing.Point(326, 24);
            this.LabelParameterName11.Name = "LabelParameterName11";
            this.LabelParameterName11.Size = new System.Drawing.Size(155, 17);
            this.LabelParameterName11.TabIndex = 47;
            this.LabelParameterName11.Text = "LabelParameterName(11)";
            this.LabelParameterName11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName11.Visible = false;
            //
            //LabelParameterName12
            //
            this.LabelParameterName12.Location = new System.Drawing.Point(326, 48);
            this.LabelParameterName12.Name = "LabelParameterName12";
            this.LabelParameterName12.Size = new System.Drawing.Size(155, 17);
            this.LabelParameterName12.TabIndex = 48;
            this.LabelParameterName12.Text = "LabelParameterName(12)";
            this.LabelParameterName12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName12.Visible = false;
            //
            //LabelParameterName13
            //
            this.LabelParameterName13.Location = new System.Drawing.Point(326, 72);
            this.LabelParameterName13.Name = "LabelParameterName13";
            this.LabelParameterName13.Size = new System.Drawing.Size(155, 17);
            this.LabelParameterName13.TabIndex = 49;
            this.LabelParameterName13.Text = "LabelParameterName(13)";
            this.LabelParameterName13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName13.Visible = false;
            //
            //LabelParameterName14
            //
            this.LabelParameterName14.Location = new System.Drawing.Point(326, 96);
            this.LabelParameterName14.Name = "LabelParameterName14";
            this.LabelParameterName14.Size = new System.Drawing.Size(155, 17);
            this.LabelParameterName14.TabIndex = 50;
            this.LabelParameterName14.Text = "LabelParameterName(14)";
            this.LabelParameterName14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName14.Visible = false;
            //
            //LabelParameterName15
            //
            this.LabelParameterName15.Location = new System.Drawing.Point(326, 120);
            this.LabelParameterName15.Name = "LabelParameterName15";
            this.LabelParameterName15.Size = new System.Drawing.Size(155, 17);
            this.LabelParameterName15.TabIndex = 51;
            this.LabelParameterName15.Text = "LabelParameterName(15)";
            this.LabelParameterName15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName15.Visible = false;
            //
            //LabelParameterName16
            //
            this.LabelParameterName16.Location = new System.Drawing.Point(326, 144);
            this.LabelParameterName16.Name = "LabelParameterName16";
            this.LabelParameterName16.Size = new System.Drawing.Size(155, 17);
            this.LabelParameterName16.TabIndex = 52;
            this.LabelParameterName16.Text = "LabelParameterName(16)";
            this.LabelParameterName16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName16.Visible = false;
            //
            //LabelParameterName17
            //
            this.LabelParameterName17.Location = new System.Drawing.Point(326, 168);
            this.LabelParameterName17.Name = "LabelParameterName17";
            this.LabelParameterName17.Size = new System.Drawing.Size(155, 17);
            this.LabelParameterName17.TabIndex = 53;
            this.LabelParameterName17.Text = "LabelParameterName(17)";
            this.LabelParameterName17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName17.Visible = false;
            //
            //LabelParameterName18
            //
            this.LabelParameterName18.Location = new System.Drawing.Point(326, 192);
            this.LabelParameterName18.Name = "LabelParameterName18";
            this.LabelParameterName18.Size = new System.Drawing.Size(155, 17);
            this.LabelParameterName18.TabIndex = 54;
            this.LabelParameterName18.Text = "LabelParameterName(18)";
            this.LabelParameterName18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName18.Visible = false;
            //
            //LabelParameterName19
            //
            this.LabelParameterName19.Location = new System.Drawing.Point(326, 216);
            this.LabelParameterName19.Name = "LabelParameterName19";
            this.LabelParameterName19.Size = new System.Drawing.Size(155, 17);
            this.LabelParameterName19.TabIndex = 55;
            this.LabelParameterName19.Text = "LabelParameterName(19)";
            this.LabelParameterName19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName19.Visible = false;
            //
            //LabelParameterName20
            //
            this.LabelParameterName20.Location = new System.Drawing.Point(326, 240);
            this.LabelParameterName20.Name = "LabelParameterName20";
            this.LabelParameterName20.Size = new System.Drawing.Size(155, 17);
            this.LabelParameterName20.TabIndex = 56;
            this.LabelParameterName20.Text = "LabelParameterName(20)";
            this.LabelParameterName20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName20.Visible = false;
            //
            //LabelParameterName21
            //
            this.LabelParameterName21.Location = new System.Drawing.Point(326, 264);
            this.LabelParameterName21.Name = "LabelParameterName21";
            this.LabelParameterName21.Size = new System.Drawing.Size(155, 17);
            this.LabelParameterName21.TabIndex = 57;
            this.LabelParameterName21.Text = "LabelParameterName(21)";
            this.LabelParameterName21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.LabelParameterName21.Visible = false;
            //
            //TextBoxParamVal0
            //
            this.TextBoxParamVal0.AcceptsReturn = true;
            this.TextBoxParamVal0.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal0.Location = new System.Drawing.Point(244, 24);
            this.TextBoxParamVal0.MaxLength = 0;
            this.TextBoxParamVal0.Name = "TextBoxParamVal0";
            this.TextBoxParamVal0.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal0.TabIndex = 0;
            this.TextBoxParamVal0.Visible = false;
            //
            //TextBoxParamVal1
            //
            this.TextBoxParamVal1.AcceptsReturn = true;
            this.TextBoxParamVal1.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal1.Location = new System.Drawing.Point(244, 48);
            this.TextBoxParamVal1.MaxLength = 0;
            this.TextBoxParamVal1.Name = "TextBoxParamVal1";
            this.TextBoxParamVal1.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal1.TabIndex = 1;
            this.TextBoxParamVal1.Visible = false;
            //
            //TextBoxParamVal2
            //
            this.TextBoxParamVal2.AcceptsReturn = true;
            this.TextBoxParamVal2.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal2.Location = new System.Drawing.Point(244, 72);
            this.TextBoxParamVal2.MaxLength = 0;
            this.TextBoxParamVal2.Name = "TextBoxParamVal2";
            this.TextBoxParamVal2.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal2.TabIndex = 2;
            this.TextBoxParamVal2.Visible = false;
            //
            //TextBoxParamVal3
            //
            this.TextBoxParamVal3.AcceptsReturn = true;
            this.TextBoxParamVal3.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal3.Location = new System.Drawing.Point(244, 96);
            this.TextBoxParamVal3.MaxLength = 0;
            this.TextBoxParamVal3.Name = "TextBoxParamVal3";
            this.TextBoxParamVal3.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal3.TabIndex = 3;
            this.TextBoxParamVal3.Visible = false;
            //
            //TextBoxParamVal4
            //
            this.TextBoxParamVal4.AcceptsReturn = true;
            this.TextBoxParamVal4.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal4.Location = new System.Drawing.Point(244, 120);
            this.TextBoxParamVal4.MaxLength = 0;
            this.TextBoxParamVal4.Name = "TextBoxParamVal4";
            this.TextBoxParamVal4.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal4.TabIndex = 4;
            this.TextBoxParamVal4.Visible = false;
            //
            //TextBoxParamVal5
            //
            this.TextBoxParamVal5.AcceptsReturn = true;
            this.TextBoxParamVal5.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal5.Location = new System.Drawing.Point(244, 144);
            this.TextBoxParamVal5.MaxLength = 0;
            this.TextBoxParamVal5.Name = "TextBoxParamVal5";
            this.TextBoxParamVal5.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal5.TabIndex = 5;
            this.TextBoxParamVal5.Visible = false;
            //
            //TextBoxParamVal6
            //
            this.TextBoxParamVal6.AcceptsReturn = true;
            this.TextBoxParamVal6.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal6.Location = new System.Drawing.Point(244, 168);
            this.TextBoxParamVal6.MaxLength = 0;
            this.TextBoxParamVal6.Name = "TextBoxParamVal6";
            this.TextBoxParamVal6.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal6.TabIndex = 6;
            this.TextBoxParamVal6.Visible = false;
            //
            //TextBoxParamVal7
            //
            this.TextBoxParamVal7.AcceptsReturn = true;
            this.TextBoxParamVal7.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal7.Location = new System.Drawing.Point(244, 192);
            this.TextBoxParamVal7.MaxLength = 0;
            this.TextBoxParamVal7.Name = "TextBoxParamVal7";
            this.TextBoxParamVal7.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal7.TabIndex = 7;
            this.TextBoxParamVal7.Visible = false;
            //
            //TextBoxParamVal8
            //
            this.TextBoxParamVal8.AcceptsReturn = true;
            this.TextBoxParamVal8.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal8.Location = new System.Drawing.Point(244, 216);
            this.TextBoxParamVal8.MaxLength = 0;
            this.TextBoxParamVal8.Name = "TextBoxParamVal8";
            this.TextBoxParamVal8.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal8.TabIndex = 8;
            this.TextBoxParamVal8.Visible = false;
            //
            //TextBoxParamVal9
            //
            this.TextBoxParamVal9.AcceptsReturn = true;
            this.TextBoxParamVal9.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal9.Location = new System.Drawing.Point(244, 240);
            this.TextBoxParamVal9.MaxLength = 0;
            this.TextBoxParamVal9.Name = "TextBoxParamVal9";
            this.TextBoxParamVal9.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal9.TabIndex = 9;
            this.TextBoxParamVal9.Visible = false;
            //
            //TextBoxParamVal10
            //
            this.TextBoxParamVal10.AcceptsReturn = true;
            this.TextBoxParamVal10.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal10.Location = new System.Drawing.Point(244, 264);
            this.TextBoxParamVal10.MaxLength = 0;
            this.TextBoxParamVal10.Name = "TextBoxParamVal10";
            this.TextBoxParamVal10.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal10.TabIndex = 10;
            this.TextBoxParamVal10.Visible = false;
            //
            //TextBoxParamVal11
            //
            this.TextBoxParamVal11.AcceptsReturn = true;
            this.TextBoxParamVal11.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal11.Location = new System.Drawing.Point(564, 24);
            this.TextBoxParamVal11.MaxLength = 0;
            this.TextBoxParamVal11.Name = "TextBoxParamVal11";
            this.TextBoxParamVal11.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal11.TabIndex = 11;
            this.TextBoxParamVal11.Visible = false;
            //
            //TextBoxParamVal12
            //
            this.TextBoxParamVal12.AcceptsReturn = true;
            this.TextBoxParamVal12.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal12.Location = new System.Drawing.Point(564, 48);
            this.TextBoxParamVal12.MaxLength = 0;
            this.TextBoxParamVal12.Name = "TextBoxParamVal12";
            this.TextBoxParamVal12.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal12.TabIndex = 12;
            this.TextBoxParamVal12.Visible = false;
            //
            //TextBoxParamVal13
            //
            this.TextBoxParamVal13.AcceptsReturn = true;
            this.TextBoxParamVal13.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal13.Location = new System.Drawing.Point(564, 72);
            this.TextBoxParamVal13.MaxLength = 0;
            this.TextBoxParamVal13.Name = "TextBoxParamVal13";
            this.TextBoxParamVal13.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal13.TabIndex = 13;
            this.TextBoxParamVal13.Visible = false;
            //
            //TextBoxParamVal14
            //
            this.TextBoxParamVal14.AcceptsReturn = true;
            this.TextBoxParamVal14.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal14.Location = new System.Drawing.Point(564, 96);
            this.TextBoxParamVal14.MaxLength = 0;
            this.TextBoxParamVal14.Name = "TextBoxParamVal14";
            this.TextBoxParamVal14.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal14.TabIndex = 14;
            this.TextBoxParamVal14.Visible = false;
            //
            //TextBoxParamVal15
            //
            this.TextBoxParamVal15.AcceptsReturn = true;
            this.TextBoxParamVal15.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal15.Location = new System.Drawing.Point(564, 120);
            this.TextBoxParamVal15.MaxLength = 0;
            this.TextBoxParamVal15.Name = "TextBoxParamVal15";
            this.TextBoxParamVal15.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal15.TabIndex = 15;
            this.TextBoxParamVal15.Visible = false;
            //
            //TextBoxParamVal16
            //
            this.TextBoxParamVal16.AcceptsReturn = true;
            this.TextBoxParamVal16.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal16.Location = new System.Drawing.Point(564, 144);
            this.TextBoxParamVal16.MaxLength = 0;
            this.TextBoxParamVal16.Name = "TextBoxParamVal16";
            this.TextBoxParamVal16.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal16.TabIndex = 16;
            this.TextBoxParamVal16.Visible = false;
            //
            //TextBoxParamVal17
            //
            this.TextBoxParamVal17.AcceptsReturn = true;
            this.TextBoxParamVal17.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal17.Location = new System.Drawing.Point(564, 168);
            this.TextBoxParamVal17.MaxLength = 0;
            this.TextBoxParamVal17.Name = "TextBoxParamVal17";
            this.TextBoxParamVal17.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal17.TabIndex = 17;
            this.TextBoxParamVal17.Visible = false;
            //
            //TextBoxParamVal18
            //
            this.TextBoxParamVal18.AcceptsReturn = true;
            this.TextBoxParamVal18.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal18.Location = new System.Drawing.Point(564, 192);
            this.TextBoxParamVal18.MaxLength = 0;
            this.TextBoxParamVal18.Name = "TextBoxParamVal18";
            this.TextBoxParamVal18.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal18.TabIndex = 18;
            this.TextBoxParamVal18.Visible = false;
            //
            //TextBoxParamVal19
            //
            this.TextBoxParamVal19.AcceptsReturn = true;
            this.TextBoxParamVal19.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal19.Location = new System.Drawing.Point(564, 216);
            this.TextBoxParamVal19.MaxLength = 0;
            this.TextBoxParamVal19.Name = "TextBoxParamVal19";
            this.TextBoxParamVal19.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal19.TabIndex = 19;
            this.TextBoxParamVal19.Visible = false;
            //
            //TextBoxParamVal20
            //
            this.TextBoxParamVal20.AcceptsReturn = true;
            this.TextBoxParamVal20.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal20.Location = new System.Drawing.Point(564, 240);
            this.TextBoxParamVal20.MaxLength = 0;
            this.TextBoxParamVal20.Name = "TextBoxParamVal20";
            this.TextBoxParamVal20.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal20.TabIndex = 20;
            this.TextBoxParamVal20.Visible = false;
            //
            //TextBoxParamVal21
            //
            this.TextBoxParamVal21.AcceptsReturn = true;
            this.TextBoxParamVal21.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxParamVal21.Location = new System.Drawing.Point(564, 264);
            this.TextBoxParamVal21.MaxLength = 0;
            this.TextBoxParamVal21.Name = "TextBoxParamVal21";
            this.TextBoxParamVal21.Size = new System.Drawing.Size(82, 20);
            this.TextBoxParamVal21.TabIndex = 21;
            this.TextBoxParamVal21.Visible = false;
            //
            //LabelChannel4
            //
            this.LabelChannel4.Location = new System.Drawing.Point(524, 336);
            this.LabelChannel4.Name = "LabelChannel4";
            this.LabelChannel4.Size = new System.Drawing.Size(49, 17);
            this.LabelChannel4.TabIndex = 433;
            this.LabelChannel4.Text = "Channel:";
            //
            //LabelDeviceCombo4
            //
            this.LabelDeviceCombo4.Location = new System.Drawing.Point(395, 336);
            this.LabelDeviceCombo4.Name = "LabelDeviceCombo4";
            this.LabelDeviceCombo4.Size = new System.Drawing.Size(89, 17);
            this.LabelDeviceCombo4.TabIndex = 427;
            this.LabelDeviceCombo4.Text = "Device:";
            //
            //LabelParameters0
            //
            this.LabelParameters0.Location = new System.Drawing.Point(8, 8);
            this.LabelParameters0.Name = "LabelParameters0";
            this.LabelParameters0.Size = new System.Drawing.Size(154, 13);
            this.LabelParameters0.TabIndex = 69;
            this.LabelParameters0.Text = "Parameter Name:";
            this.LabelParameters0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelValues0
            //
            this.LabelValues0.AutoSize = true;
            this.LabelValues0.Location = new System.Drawing.Point(168, 8);
            this.LabelValues0.Name = "LabelValues0";
            this.LabelValues0.Size = new System.Drawing.Size(37, 13);
            this.LabelValues0.TabIndex = 70;
            this.LabelValues0.Text = "Value:";
            //
            //LabelMod0
            //
            this.LabelMod0.AutoSize = true;
            this.LabelMod0.Location = new System.Drawing.Point(244, 8);
            this.LabelMod0.Name = "LabelMod0";
            this.LabelMod0.Size = new System.Drawing.Size(41, 13);
            this.LabelMod0.TabIndex = 71;
            this.LabelMod0.Text = "Modify:";
            //
            //LabelParameters1
            //
            this.LabelParameters1.Location = new System.Drawing.Point(327, 8);
            this.LabelParameters1.Name = "LabelParameters1";
            this.LabelParameters1.Size = new System.Drawing.Size(155, 16);
            this.LabelParameters1.TabIndex = 72;
            this.LabelParameters1.Text = "Parameter Name:";
            this.LabelParameters1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelValues1
            //
            this.LabelValues1.AutoSize = true;
            this.LabelValues1.Location = new System.Drawing.Point(488, 8);
            this.LabelValues1.Name = "LabelValues1";
            this.LabelValues1.Size = new System.Drawing.Size(37, 13);
            this.LabelValues1.TabIndex = 73;
            this.LabelValues1.Text = "Value:";
            //
            //LabelMod1
            //
            this.LabelMod1.AutoSize = true;
            this.LabelMod1.Location = new System.Drawing.Point(564, 8);
            this.LabelMod1.Name = "LabelMod1";
            this.LabelMod1.Size = new System.Drawing.Size(41, 13);
            this.LabelMod1.TabIndex = 74;
            this.LabelMod1.Text = "Modify:";
            //
            //TabPageInit
            //
            this.TabPageInit.Controls.Add(this.GroupBoxFastInit);
            this.TabPageInit.Controls.Add(this.GroupBox5BInit);
            this.TabPageInit.Controls.Add(this.LabelChannel5);
            this.TabPageInit.Controls.Add(this.LabelDeviceCombo5);
            this.TabPageInit.Controls.Add(this.ComboAvailableBoxLocator5);
            this.TabPageInit.Controls.Add(this.ComboAvailableChannelLocator5);
            this.TabPageInit.Location = new System.Drawing.Point(4, 22);
            this.TabPageInit.Name = "TabPageInit";
            this.TabPageInit.Size = new System.Drawing.Size(714, 395);
            this.TabPageInit.TabIndex = 5;
            this.TabPageInit.Text = "Init";
            //
            //GroupBoxFastInit
            //
            this.GroupBoxFastInit.Controls.Add(this.ButtonExecuteFastInit);
            this.GroupBoxFastInit.Controls.Add(this.TextBoxFIMessage);
            this.GroupBoxFastInit.Controls.Add(this.TextBoxFIFlags);
            this.GroupBoxFastInit.Controls.Add(this.LabelFIRXTitle);
            this.GroupBoxFastInit.Controls.Add(this.LabelFITXFlags);
            this.GroupBoxFastInit.Controls.Add(this.LabelFIRxStatus);
            this.GroupBoxFastInit.Controls.Add(this.LabelFIResponse);
            this.GroupBoxFastInit.Controls.Add(this.LabelFIMsgResp);
            this.GroupBoxFastInit.Controls.Add(this.LabelFIMsgData);
            this.GroupBoxFastInit.Location = new System.Drawing.Point(328, 24);
            this.GroupBoxFastInit.Name = "GroupBoxFastInit";
            this.GroupBoxFastInit.Size = new System.Drawing.Size(369, 297);
            this.GroupBoxFastInit.TabIndex = 1;
            this.GroupBoxFastInit.TabStop = false;
            this.GroupBoxFastInit.Text = "Fast Init";
            //
            //ButtonExecuteFastInit
            //
            this.ButtonExecuteFastInit.Location = new System.Drawing.Point(272, 104);
            this.ButtonExecuteFastInit.Name = "ButtonExecuteFastInit";
            this.ButtonExecuteFastInit.Size = new System.Drawing.Size(57, 25);
            this.ButtonExecuteFastInit.TabIndex = 0;
            this.ButtonExecuteFastInit.Text = "Execute";
            this.ButtonExecuteFastInit.UseVisualStyleBackColor = true;
            //
            //TextBoxFIMessage
            //
            this.TextBoxFIMessage.AcceptsReturn = true;
            this.TextBoxFIMessage.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFIMessage.Location = new System.Drawing.Point(16, 32);
            this.TextBoxFIMessage.MaxLength = 0;
            this.TextBoxFIMessage.Name = "TextBoxFIMessage";
            this.TextBoxFIMessage.Size = new System.Drawing.Size(225, 20);
            this.TextBoxFIMessage.TabIndex = 1;
            //
            //TextBoxFIFlags
            //
            this.TextBoxFIFlags.AcceptsReturn = true;
            this.TextBoxFIFlags.ContextMenuStrip = this.ContextMenuStripFlags;
            this.TextBoxFIFlags.Location = new System.Drawing.Point(248, 32);
            this.TextBoxFIFlags.MaxLength = 0;
            this.TextBoxFIFlags.Name = "TextBoxFIFlags";
            this.TextBoxFIFlags.Size = new System.Drawing.Size(81, 20);
            this.TextBoxFIFlags.TabIndex = 2;
            //
            //LabelFIRXTitle
            //
            this.LabelFIRXTitle.Location = new System.Drawing.Point(248, 136);
            this.LabelFIRXTitle.Name = "LabelFIRXTitle";
            this.LabelFIRXTitle.Size = new System.Drawing.Size(81, 17);
            this.LabelFIRXTitle.TabIndex = 6;
            this.LabelFIRXTitle.Text = "Rx Status:";
            //
            //LabelFITXFlags
            //
            this.LabelFITXFlags.Location = new System.Drawing.Point(248, 16);
            this.LabelFITXFlags.Name = "LabelFITXFlags";
            this.LabelFITXFlags.Size = new System.Drawing.Size(81, 17);
            this.LabelFITXFlags.TabIndex = 4;
            this.LabelFITXFlags.Text = "Tx Flags:";
            //
            //LabelFIRxStatus
            //
            this.LabelFIRxStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFIRxStatus.Location = new System.Drawing.Point(248, 153);
            this.LabelFIRxStatus.Name = "LabelFIRxStatus";
            this.LabelFIRxStatus.Size = new System.Drawing.Size(81, 19);
            this.LabelFIRxStatus.TabIndex = 8;
            //
            //LabelFIResponse
            //
            this.LabelFIResponse.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFIResponse.Location = new System.Drawing.Point(16, 153);
            this.LabelFIResponse.Name = "LabelFIResponse";
            this.LabelFIResponse.Size = new System.Drawing.Size(225, 91);
            this.LabelFIResponse.TabIndex = 7;
            //
            //LabelFIMsgResp
            //
            this.LabelFIMsgResp.Location = new System.Drawing.Point(16, 136);
            this.LabelFIMsgResp.Name = "LabelFIMsgResp";
            this.LabelFIMsgResp.Size = new System.Drawing.Size(186, 17);
            this.LabelFIMsgResp.TabIndex = 5;
            this.LabelFIMsgResp.Text = "Message Response:";
            //
            //LabelFIMsgData
            //
            this.LabelFIMsgData.Location = new System.Drawing.Point(16, 16);
            this.LabelFIMsgData.Name = "LabelFIMsgData";
            this.LabelFIMsgData.Size = new System.Drawing.Size(210, 17);
            this.LabelFIMsgData.TabIndex = 3;
            this.LabelFIMsgData.Text = "Message Data:";
            //
            //GroupBox5BInit
            //
            this.GroupBox5BInit.Controls.Add(this.TextBox5BInitECU);
            this.GroupBox5BInit.Controls.Add(this.ButtonExecute5BInit);
            this.GroupBox5BInit.Controls.Add(this.LabelKWlabel1);
            this.GroupBox5BInit.Controls.Add(this.LabelKWlabel0);
            this.GroupBox5BInit.Controls.Add(this.Label5BECU);
            this.GroupBox5BInit.Controls.Add(this.Label5BKeyWord0);
            this.GroupBox5BInit.Controls.Add(this.Label5BKeyWord1);
            this.GroupBox5BInit.Location = new System.Drawing.Point(8, 24);
            this.GroupBox5BInit.Name = "GroupBox5BInit";
            this.GroupBox5BInit.Size = new System.Drawing.Size(313, 297);
            this.GroupBox5BInit.TabIndex = 0;
            this.GroupBox5BInit.TabStop = false;
            this.GroupBox5BInit.Text = "Five Baud Init";
            //
            //TextBox5BInitECU
            //
            this.TextBox5BInitECU.AcceptsReturn = true;
            this.TextBox5BInitECU.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBox5BInitECU.Location = new System.Drawing.Point(32, 51);
            this.TextBox5BInitECU.MaxLength = 0;
            this.TextBox5BInitECU.Name = "TextBox5BInitECU";
            this.TextBox5BInitECU.Size = new System.Drawing.Size(57, 20);
            this.TextBox5BInitECU.TabIndex = 3;
            //
            //ButtonExecute5BInit
            //
            this.ButtonExecute5BInit.Location = new System.Drawing.Point(200, 75);
            this.ButtonExecute5BInit.Name = "ButtonExecute5BInit";
            this.ButtonExecute5BInit.Size = new System.Drawing.Size(57, 25);
            this.ButtonExecute5BInit.TabIndex = 0;
            this.ButtonExecute5BInit.Text = "Execute";
            this.ButtonExecute5BInit.UseVisualStyleBackColor = true;
            //
            //LabelKWlabel1
            //
            this.LabelKWlabel1.Location = new System.Drawing.Point(184, 32);
            this.LabelKWlabel1.Name = "LabelKWlabel1";
            this.LabelKWlabel1.Size = new System.Drawing.Size(73, 15);
            this.LabelKWlabel1.TabIndex = 6;
            this.LabelKWlabel1.Text = "Keyword 2:";
            //
            //LabelKWlabel0
            //
            this.LabelKWlabel0.Location = new System.Drawing.Point(104, 32);
            this.LabelKWlabel0.Name = "LabelKWlabel0";
            this.LabelKWlabel0.Size = new System.Drawing.Size(73, 15);
            this.LabelKWlabel0.TabIndex = 5;
            this.LabelKWlabel0.Text = "Keyword 1:";
            //
            //Label5BECU
            //
            this.Label5BECU.Location = new System.Drawing.Point(24, 32);
            this.Label5BECU.Name = "Label5BECU";
            this.Label5BECU.Size = new System.Drawing.Size(85, 15);
            this.Label5BECU.TabIndex = 4;
            this.Label5BECU.Text = "ECU Address:";
            //
            //Label5BKeyWord0
            //
            this.Label5BKeyWord0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Label5BKeyWord0.Location = new System.Drawing.Point(104, 51);
            this.Label5BKeyWord0.Name = "Label5BKeyWord0";
            this.Label5BKeyWord0.Size = new System.Drawing.Size(73, 19);
            this.Label5BKeyWord0.TabIndex = 1;
            //
            //Label5BKeyWord1
            //
            this.Label5BKeyWord1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Label5BKeyWord1.Location = new System.Drawing.Point(184, 51);
            this.Label5BKeyWord1.Name = "Label5BKeyWord1";
            this.Label5BKeyWord1.Size = new System.Drawing.Size(73, 19);
            this.Label5BKeyWord1.TabIndex = 2;
            //
            //LabelChannel5
            //
            this.LabelChannel5.Location = new System.Drawing.Point(524, 336);
            this.LabelChannel5.Name = "LabelChannel5";
            this.LabelChannel5.Size = new System.Drawing.Size(49, 17);
            this.LabelChannel5.TabIndex = 5;
            this.LabelChannel5.Text = "Channel:";
            //
            //LabelDeviceCombo5
            //
            this.LabelDeviceCombo5.Location = new System.Drawing.Point(395, 336);
            this.LabelDeviceCombo5.Name = "LabelDeviceCombo5";
            this.LabelDeviceCombo5.Size = new System.Drawing.Size(89, 17);
            this.LabelDeviceCombo5.TabIndex = 4;
            this.LabelDeviceCombo5.Text = "Device:";
            //
            //TabPageFunctionalMessages
            //
            this.TabPageFunctionalMessages.Controls.Add(this.ButtonClearAllFunctionalMessages);
            this.TabPageFunctionalMessages.Controls.Add(this.ButtonApplyFunctionalMessages);
            this.TabPageFunctionalMessages.Controls.Add(this.ButtonCancelFunctionalMessages);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete0);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete1);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete2);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete3);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete4);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete5);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete6);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete7);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete8);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete9);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete10);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete11);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete12);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete13);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete14);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete15);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete16);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete17);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete18);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete19);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete20);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete21);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete22);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete23);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete24);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete25);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete26);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete27);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete28);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete29);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete30);
            this.TabPageFunctionalMessages.Controls.Add(this.CheckBoxFunctionalMessageDelete31);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage0);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage1);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage2);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage3);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage4);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage5);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage6);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage7);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage8);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage9);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage10);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage11);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage12);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage13);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage14);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage15);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage16);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage17);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage18);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage19);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage20);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage21);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage22);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage23);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage24);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage25);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage26);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage27);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage28);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage29);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage30);
            this.TabPageFunctionalMessages.Controls.Add(this.TextBoxFunctionalMessage31);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelChannel6);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelDeviceCombo6);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessageModify);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessageValues);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId31);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId30);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId29);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId28);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId27);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId26);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId25);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId24);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId23);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId22);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId21);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId20);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId19);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId18);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId17);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId16);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId15);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId14);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId13);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId12);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId11);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId10);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId9);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId8);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId7);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId6);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId5);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId4);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId3);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId2);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId1);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFuncId0);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage0);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage1);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage2);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage3);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage4);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage5);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage6);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage7);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage8);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage9);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage10);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage11);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage12);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage13);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage14);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage15);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage16);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage17);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage18);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage19);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage20);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage21);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage22);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage23);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage24);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage25);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage26);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage27);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage28);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage29);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage30);
            this.TabPageFunctionalMessages.Controls.Add(this.LabelFunctionalMessage31);
            this.TabPageFunctionalMessages.Controls.Add(this.ComboAvailableBoxLocator6);
            this.TabPageFunctionalMessages.Controls.Add(this.ComboAvailableChannelLocator6);
            this.TabPageFunctionalMessages.Location = new System.Drawing.Point(4, 22);
            this.TabPageFunctionalMessages.Name = "TabPageFunctionalMessages";
            this.TabPageFunctionalMessages.Size = new System.Drawing.Size(714, 395);
            this.TabPageFunctionalMessages.TabIndex = 6;
            this.TabPageFunctionalMessages.Text = "Functional Messages";
            //
            //ButtonClearAllFunctionalMessages
            //
            this.ButtonClearAllFunctionalMessages.Location = new System.Drawing.Point(504, 304);
            this.ButtonClearAllFunctionalMessages.Name = "ButtonClearAllFunctionalMessages";
            this.ButtonClearAllFunctionalMessages.Size = new System.Drawing.Size(73, 25);
            this.ButtonClearAllFunctionalMessages.TabIndex = 32;
            this.ButtonClearAllFunctionalMessages.Text = "Clear All";
            this.ButtonClearAllFunctionalMessages.UseVisualStyleBackColor = true;
            //
            //ButtonApplyFunctionalMessages
            //
            this.ButtonApplyFunctionalMessages.Location = new System.Drawing.Point(639, 304);
            this.ButtonApplyFunctionalMessages.Name = "ButtonApplyFunctionalMessages";
            this.ButtonApplyFunctionalMessages.Size = new System.Drawing.Size(49, 25);
            this.ButtonApplyFunctionalMessages.TabIndex = 34;
            this.ButtonApplyFunctionalMessages.Text = "Apply";
            this.ButtonApplyFunctionalMessages.UseVisualStyleBackColor = true;
            //
            //ButtonCancelFunctionalMessages
            //
            this.ButtonCancelFunctionalMessages.Location = new System.Drawing.Point(583, 304);
            this.ButtonCancelFunctionalMessages.Name = "ButtonCancelFunctionalMessages";
            this.ButtonCancelFunctionalMessages.Size = new System.Drawing.Size(49, 25);
            this.ButtonCancelFunctionalMessages.TabIndex = 33;
            this.ButtonCancelFunctionalMessages.Text = "Cancel";
            this.ButtonCancelFunctionalMessages.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete0
            //
            this.CheckBoxFunctionalMessageDelete0.Location = new System.Drawing.Point(177, 48);
            this.CheckBoxFunctionalMessageDelete0.Name = "CheckBoxFunctionalMessageDelete0";
            this.CheckBoxFunctionalMessageDelete0.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete0.TabIndex = 63;
            this.CheckBoxFunctionalMessageDelete0.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete0.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete1
            //
            this.CheckBoxFunctionalMessageDelete1.Location = new System.Drawing.Point(177, 72);
            this.CheckBoxFunctionalMessageDelete1.Name = "CheckBoxFunctionalMessageDelete1";
            this.CheckBoxFunctionalMessageDelete1.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete1.TabIndex = 64;
            this.CheckBoxFunctionalMessageDelete1.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete1.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete2
            //
            this.CheckBoxFunctionalMessageDelete2.Location = new System.Drawing.Point(177, 96);
            this.CheckBoxFunctionalMessageDelete2.Name = "CheckBoxFunctionalMessageDelete2";
            this.CheckBoxFunctionalMessageDelete2.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete2.TabIndex = 65;
            this.CheckBoxFunctionalMessageDelete2.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete2.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete3
            //
            this.CheckBoxFunctionalMessageDelete3.Location = new System.Drawing.Point(177, 120);
            this.CheckBoxFunctionalMessageDelete3.Name = "CheckBoxFunctionalMessageDelete3";
            this.CheckBoxFunctionalMessageDelete3.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete3.TabIndex = 66;
            this.CheckBoxFunctionalMessageDelete3.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete3.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete4
            //
            this.CheckBoxFunctionalMessageDelete4.Location = new System.Drawing.Point(177, 144);
            this.CheckBoxFunctionalMessageDelete4.Name = "CheckBoxFunctionalMessageDelete4";
            this.CheckBoxFunctionalMessageDelete4.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete4.TabIndex = 67;
            this.CheckBoxFunctionalMessageDelete4.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete4.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete5
            //
            this.CheckBoxFunctionalMessageDelete5.Location = new System.Drawing.Point(177, 168);
            this.CheckBoxFunctionalMessageDelete5.Name = "CheckBoxFunctionalMessageDelete5";
            this.CheckBoxFunctionalMessageDelete5.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete5.TabIndex = 68;
            this.CheckBoxFunctionalMessageDelete5.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete5.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete6
            //
            this.CheckBoxFunctionalMessageDelete6.Location = new System.Drawing.Point(177, 192);
            this.CheckBoxFunctionalMessageDelete6.Name = "CheckBoxFunctionalMessageDelete6";
            this.CheckBoxFunctionalMessageDelete6.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete6.TabIndex = 69;
            this.CheckBoxFunctionalMessageDelete6.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete6.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete7
            //
            this.CheckBoxFunctionalMessageDelete7.Location = new System.Drawing.Point(177, 216);
            this.CheckBoxFunctionalMessageDelete7.Name = "CheckBoxFunctionalMessageDelete7";
            this.CheckBoxFunctionalMessageDelete7.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete7.TabIndex = 70;
            this.CheckBoxFunctionalMessageDelete7.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete7.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete8
            //
            this.CheckBoxFunctionalMessageDelete8.Location = new System.Drawing.Point(177, 240);
            this.CheckBoxFunctionalMessageDelete8.Name = "CheckBoxFunctionalMessageDelete8";
            this.CheckBoxFunctionalMessageDelete8.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete8.TabIndex = 71;
            this.CheckBoxFunctionalMessageDelete8.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete8.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete9
            //
            this.CheckBoxFunctionalMessageDelete9.Location = new System.Drawing.Point(177, 264);
            this.CheckBoxFunctionalMessageDelete9.Name = "CheckBoxFunctionalMessageDelete9";
            this.CheckBoxFunctionalMessageDelete9.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete9.TabIndex = 72;
            this.CheckBoxFunctionalMessageDelete9.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete9.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete10
            //
            this.CheckBoxFunctionalMessageDelete10.Location = new System.Drawing.Point(177, 288);
            this.CheckBoxFunctionalMessageDelete10.Name = "CheckBoxFunctionalMessageDelete10";
            this.CheckBoxFunctionalMessageDelete10.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete10.TabIndex = 73;
            this.CheckBoxFunctionalMessageDelete10.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete10.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete11
            //
            this.CheckBoxFunctionalMessageDelete11.Location = new System.Drawing.Point(385, 48);
            this.CheckBoxFunctionalMessageDelete11.Name = "CheckBoxFunctionalMessageDelete11";
            this.CheckBoxFunctionalMessageDelete11.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete11.TabIndex = 96;
            this.CheckBoxFunctionalMessageDelete11.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete11.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete12
            //
            this.CheckBoxFunctionalMessageDelete12.Location = new System.Drawing.Point(385, 72);
            this.CheckBoxFunctionalMessageDelete12.Name = "CheckBoxFunctionalMessageDelete12";
            this.CheckBoxFunctionalMessageDelete12.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete12.TabIndex = 97;
            this.CheckBoxFunctionalMessageDelete12.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete12.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete13
            //
            this.CheckBoxFunctionalMessageDelete13.Location = new System.Drawing.Point(385, 96);
            this.CheckBoxFunctionalMessageDelete13.Name = "CheckBoxFunctionalMessageDelete13";
            this.CheckBoxFunctionalMessageDelete13.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete13.TabIndex = 98;
            this.CheckBoxFunctionalMessageDelete13.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete13.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete14
            //
            this.CheckBoxFunctionalMessageDelete14.Location = new System.Drawing.Point(385, 120);
            this.CheckBoxFunctionalMessageDelete14.Name = "CheckBoxFunctionalMessageDelete14";
            this.CheckBoxFunctionalMessageDelete14.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete14.TabIndex = 99;
            this.CheckBoxFunctionalMessageDelete14.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete14.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete15
            //
            this.CheckBoxFunctionalMessageDelete15.Location = new System.Drawing.Point(385, 144);
            this.CheckBoxFunctionalMessageDelete15.Name = "CheckBoxFunctionalMessageDelete15";
            this.CheckBoxFunctionalMessageDelete15.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete15.TabIndex = 100;
            this.CheckBoxFunctionalMessageDelete15.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete15.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete16
            //
            this.CheckBoxFunctionalMessageDelete16.Location = new System.Drawing.Point(385, 168);
            this.CheckBoxFunctionalMessageDelete16.Name = "CheckBoxFunctionalMessageDelete16";
            this.CheckBoxFunctionalMessageDelete16.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete16.TabIndex = 101;
            this.CheckBoxFunctionalMessageDelete16.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete16.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete17
            //
            this.CheckBoxFunctionalMessageDelete17.Location = new System.Drawing.Point(385, 192);
            this.CheckBoxFunctionalMessageDelete17.Name = "CheckBoxFunctionalMessageDelete17";
            this.CheckBoxFunctionalMessageDelete17.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete17.TabIndex = 102;
            this.CheckBoxFunctionalMessageDelete17.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete17.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete18
            //
            this.CheckBoxFunctionalMessageDelete18.Location = new System.Drawing.Point(385, 216);
            this.CheckBoxFunctionalMessageDelete18.Name = "CheckBoxFunctionalMessageDelete18";
            this.CheckBoxFunctionalMessageDelete18.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete18.TabIndex = 103;
            this.CheckBoxFunctionalMessageDelete18.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete18.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete19
            //
            this.CheckBoxFunctionalMessageDelete19.Location = new System.Drawing.Point(385, 240);
            this.CheckBoxFunctionalMessageDelete19.Name = "CheckBoxFunctionalMessageDelete19";
            this.CheckBoxFunctionalMessageDelete19.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete19.TabIndex = 104;
            this.CheckBoxFunctionalMessageDelete19.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete19.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete20
            //
            this.CheckBoxFunctionalMessageDelete20.Location = new System.Drawing.Point(385, 264);
            this.CheckBoxFunctionalMessageDelete20.Name = "CheckBoxFunctionalMessageDelete20";
            this.CheckBoxFunctionalMessageDelete20.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete20.TabIndex = 105;
            this.CheckBoxFunctionalMessageDelete20.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete20.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete21
            //
            this.CheckBoxFunctionalMessageDelete21.Location = new System.Drawing.Point(385, 288);
            this.CheckBoxFunctionalMessageDelete21.Name = "CheckBoxFunctionalMessageDelete21";
            this.CheckBoxFunctionalMessageDelete21.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete21.TabIndex = 106;
            this.CheckBoxFunctionalMessageDelete21.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete21.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete22
            //
            this.CheckBoxFunctionalMessageDelete22.Location = new System.Drawing.Point(593, 48);
            this.CheckBoxFunctionalMessageDelete22.Name = "CheckBoxFunctionalMessageDelete22";
            this.CheckBoxFunctionalMessageDelete22.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete22.TabIndex = 127;
            this.CheckBoxFunctionalMessageDelete22.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete22.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete23
            //
            this.CheckBoxFunctionalMessageDelete23.Location = new System.Drawing.Point(593, 72);
            this.CheckBoxFunctionalMessageDelete23.Name = "CheckBoxFunctionalMessageDelete23";
            this.CheckBoxFunctionalMessageDelete23.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete23.TabIndex = 128;
            this.CheckBoxFunctionalMessageDelete23.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete23.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete24
            //
            this.CheckBoxFunctionalMessageDelete24.Location = new System.Drawing.Point(593, 96);
            this.CheckBoxFunctionalMessageDelete24.Name = "CheckBoxFunctionalMessageDelete24";
            this.CheckBoxFunctionalMessageDelete24.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete24.TabIndex = 129;
            this.CheckBoxFunctionalMessageDelete24.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete24.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete25
            //
            this.CheckBoxFunctionalMessageDelete25.Location = new System.Drawing.Point(593, 120);
            this.CheckBoxFunctionalMessageDelete25.Name = "CheckBoxFunctionalMessageDelete25";
            this.CheckBoxFunctionalMessageDelete25.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete25.TabIndex = 130;
            this.CheckBoxFunctionalMessageDelete25.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete25.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete26
            //
            this.CheckBoxFunctionalMessageDelete26.Location = new System.Drawing.Point(593, 144);
            this.CheckBoxFunctionalMessageDelete26.Name = "CheckBoxFunctionalMessageDelete26";
            this.CheckBoxFunctionalMessageDelete26.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete26.TabIndex = 131;
            this.CheckBoxFunctionalMessageDelete26.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete26.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete27
            //
            this.CheckBoxFunctionalMessageDelete27.Location = new System.Drawing.Point(593, 168);
            this.CheckBoxFunctionalMessageDelete27.Name = "CheckBoxFunctionalMessageDelete27";
            this.CheckBoxFunctionalMessageDelete27.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete27.TabIndex = 132;
            this.CheckBoxFunctionalMessageDelete27.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete27.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete28
            //
            this.CheckBoxFunctionalMessageDelete28.Location = new System.Drawing.Point(593, 192);
            this.CheckBoxFunctionalMessageDelete28.Name = "CheckBoxFunctionalMessageDelete28";
            this.CheckBoxFunctionalMessageDelete28.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete28.TabIndex = 133;
            this.CheckBoxFunctionalMessageDelete28.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete28.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete29
            //
            this.CheckBoxFunctionalMessageDelete29.Location = new System.Drawing.Point(593, 216);
            this.CheckBoxFunctionalMessageDelete29.Name = "CheckBoxFunctionalMessageDelete29";
            this.CheckBoxFunctionalMessageDelete29.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete29.TabIndex = 134;
            this.CheckBoxFunctionalMessageDelete29.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete29.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete30
            //
            this.CheckBoxFunctionalMessageDelete30.Location = new System.Drawing.Point(593, 240);
            this.CheckBoxFunctionalMessageDelete30.Name = "CheckBoxFunctionalMessageDelete30";
            this.CheckBoxFunctionalMessageDelete30.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete30.TabIndex = 135;
            this.CheckBoxFunctionalMessageDelete30.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete30.UseVisualStyleBackColor = true;
            //
            //CheckBoxFunctionalMessageDelete31
            //
            this.CheckBoxFunctionalMessageDelete31.Location = new System.Drawing.Point(593, 264);
            this.CheckBoxFunctionalMessageDelete31.Name = "CheckBoxFunctionalMessageDelete31";
            this.CheckBoxFunctionalMessageDelete31.Size = new System.Drawing.Size(57, 17);
            this.CheckBoxFunctionalMessageDelete31.TabIndex = 136;
            this.CheckBoxFunctionalMessageDelete31.Text = "Delete";
            this.CheckBoxFunctionalMessageDelete31.UseVisualStyleBackColor = true;
            //
            //TextBoxFunctionalMessage0
            //
            this.TextBoxFunctionalMessage0.AcceptsReturn = true;
            this.TextBoxFunctionalMessage0.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage0.Location = new System.Drawing.Point(113, 48);
            this.TextBoxFunctionalMessage0.MaxLength = 0;
            this.TextBoxFunctionalMessage0.Name = "TextBoxFunctionalMessage0";
            this.TextBoxFunctionalMessage0.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage0.TabIndex = 0;
            //
            //TextBoxFunctionalMessage1
            //
            this.TextBoxFunctionalMessage1.AcceptsReturn = true;
            this.TextBoxFunctionalMessage1.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage1.Location = new System.Drawing.Point(113, 72);
            this.TextBoxFunctionalMessage1.MaxLength = 0;
            this.TextBoxFunctionalMessage1.Name = "TextBoxFunctionalMessage1";
            this.TextBoxFunctionalMessage1.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage1.TabIndex = 1;
            //
            //TextBoxFunctionalMessage2
            //
            this.TextBoxFunctionalMessage2.AcceptsReturn = true;
            this.TextBoxFunctionalMessage2.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage2.Location = new System.Drawing.Point(113, 96);
            this.TextBoxFunctionalMessage2.MaxLength = 0;
            this.TextBoxFunctionalMessage2.Name = "TextBoxFunctionalMessage2";
            this.TextBoxFunctionalMessage2.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage2.TabIndex = 2;
            //
            //TextBoxFunctionalMessage3
            //
            this.TextBoxFunctionalMessage3.AcceptsReturn = true;
            this.TextBoxFunctionalMessage3.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage3.Location = new System.Drawing.Point(113, 120);
            this.TextBoxFunctionalMessage3.MaxLength = 0;
            this.TextBoxFunctionalMessage3.Name = "TextBoxFunctionalMessage3";
            this.TextBoxFunctionalMessage3.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage3.TabIndex = 3;
            //
            //TextBoxFunctionalMessage4
            //
            this.TextBoxFunctionalMessage4.AcceptsReturn = true;
            this.TextBoxFunctionalMessage4.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage4.Location = new System.Drawing.Point(113, 144);
            this.TextBoxFunctionalMessage4.MaxLength = 0;
            this.TextBoxFunctionalMessage4.Name = "TextBoxFunctionalMessage4";
            this.TextBoxFunctionalMessage4.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage4.TabIndex = 4;
            //
            //TextBoxFunctionalMessage5
            //
            this.TextBoxFunctionalMessage5.AcceptsReturn = true;
            this.TextBoxFunctionalMessage5.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage5.Location = new System.Drawing.Point(113, 168);
            this.TextBoxFunctionalMessage5.MaxLength = 0;
            this.TextBoxFunctionalMessage5.Name = "TextBoxFunctionalMessage5";
            this.TextBoxFunctionalMessage5.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage5.TabIndex = 5;
            //
            //TextBoxFunctionalMessage6
            //
            this.TextBoxFunctionalMessage6.AcceptsReturn = true;
            this.TextBoxFunctionalMessage6.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage6.Location = new System.Drawing.Point(113, 192);
            this.TextBoxFunctionalMessage6.MaxLength = 0;
            this.TextBoxFunctionalMessage6.Name = "TextBoxFunctionalMessage6";
            this.TextBoxFunctionalMessage6.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage6.TabIndex = 6;
            //
            //TextBoxFunctionalMessage7
            //
            this.TextBoxFunctionalMessage7.AcceptsReturn = true;
            this.TextBoxFunctionalMessage7.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage7.Location = new System.Drawing.Point(113, 216);
            this.TextBoxFunctionalMessage7.MaxLength = 0;
            this.TextBoxFunctionalMessage7.Name = "TextBoxFunctionalMessage7";
            this.TextBoxFunctionalMessage7.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage7.TabIndex = 7;
            //
            //TextBoxFunctionalMessage8
            //
            this.TextBoxFunctionalMessage8.AcceptsReturn = true;
            this.TextBoxFunctionalMessage8.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage8.Location = new System.Drawing.Point(113, 240);
            this.TextBoxFunctionalMessage8.MaxLength = 0;
            this.TextBoxFunctionalMessage8.Name = "TextBoxFunctionalMessage8";
            this.TextBoxFunctionalMessage8.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage8.TabIndex = 8;
            //
            //TextBoxFunctionalMessage9
            //
            this.TextBoxFunctionalMessage9.AcceptsReturn = true;
            this.TextBoxFunctionalMessage9.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage9.Location = new System.Drawing.Point(113, 264);
            this.TextBoxFunctionalMessage9.MaxLength = 0;
            this.TextBoxFunctionalMessage9.Name = "TextBoxFunctionalMessage9";
            this.TextBoxFunctionalMessage9.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage9.TabIndex = 9;
            //
            //TextBoxFunctionalMessage10
            //
            this.TextBoxFunctionalMessage10.AcceptsReturn = true;
            this.TextBoxFunctionalMessage10.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage10.Location = new System.Drawing.Point(113, 288);
            this.TextBoxFunctionalMessage10.MaxLength = 0;
            this.TextBoxFunctionalMessage10.Name = "TextBoxFunctionalMessage10";
            this.TextBoxFunctionalMessage10.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage10.TabIndex = 10;
            //
            //TextBoxFunctionalMessage11
            //
            this.TextBoxFunctionalMessage11.AcceptsReturn = true;
            this.TextBoxFunctionalMessage11.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage11.Location = new System.Drawing.Point(321, 48);
            this.TextBoxFunctionalMessage11.MaxLength = 0;
            this.TextBoxFunctionalMessage11.Name = "TextBoxFunctionalMessage11";
            this.TextBoxFunctionalMessage11.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage11.TabIndex = 11;
            //
            //TextBoxFunctionalMessage12
            //
            this.TextBoxFunctionalMessage12.AcceptsReturn = true;
            this.TextBoxFunctionalMessage12.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage12.Location = new System.Drawing.Point(321, 72);
            this.TextBoxFunctionalMessage12.MaxLength = 0;
            this.TextBoxFunctionalMessage12.Name = "TextBoxFunctionalMessage12";
            this.TextBoxFunctionalMessage12.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage12.TabIndex = 12;
            //
            //TextBoxFunctionalMessage13
            //
            this.TextBoxFunctionalMessage13.AcceptsReturn = true;
            this.TextBoxFunctionalMessage13.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage13.Location = new System.Drawing.Point(321, 96);
            this.TextBoxFunctionalMessage13.MaxLength = 0;
            this.TextBoxFunctionalMessage13.Name = "TextBoxFunctionalMessage13";
            this.TextBoxFunctionalMessage13.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage13.TabIndex = 13;
            //
            //TextBoxFunctionalMessage14
            //
            this.TextBoxFunctionalMessage14.AcceptsReturn = true;
            this.TextBoxFunctionalMessage14.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage14.Location = new System.Drawing.Point(321, 120);
            this.TextBoxFunctionalMessage14.MaxLength = 0;
            this.TextBoxFunctionalMessage14.Name = "TextBoxFunctionalMessage14";
            this.TextBoxFunctionalMessage14.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage14.TabIndex = 14;
            //
            //TextBoxFunctionalMessage15
            //
            this.TextBoxFunctionalMessage15.AcceptsReturn = true;
            this.TextBoxFunctionalMessage15.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage15.Location = new System.Drawing.Point(321, 144);
            this.TextBoxFunctionalMessage15.MaxLength = 0;
            this.TextBoxFunctionalMessage15.Name = "TextBoxFunctionalMessage15";
            this.TextBoxFunctionalMessage15.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage15.TabIndex = 15;
            //
            //TextBoxFunctionalMessage16
            //
            this.TextBoxFunctionalMessage16.AcceptsReturn = true;
            this.TextBoxFunctionalMessage16.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage16.Location = new System.Drawing.Point(321, 168);
            this.TextBoxFunctionalMessage16.MaxLength = 0;
            this.TextBoxFunctionalMessage16.Name = "TextBoxFunctionalMessage16";
            this.TextBoxFunctionalMessage16.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage16.TabIndex = 16;
            //
            //TextBoxFunctionalMessage17
            //
            this.TextBoxFunctionalMessage17.AcceptsReturn = true;
            this.TextBoxFunctionalMessage17.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage17.Location = new System.Drawing.Point(321, 192);
            this.TextBoxFunctionalMessage17.MaxLength = 0;
            this.TextBoxFunctionalMessage17.Name = "TextBoxFunctionalMessage17";
            this.TextBoxFunctionalMessage17.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage17.TabIndex = 17;
            //
            //TextBoxFunctionalMessage18
            //
            this.TextBoxFunctionalMessage18.AcceptsReturn = true;
            this.TextBoxFunctionalMessage18.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage18.Location = new System.Drawing.Point(321, 216);
            this.TextBoxFunctionalMessage18.MaxLength = 0;
            this.TextBoxFunctionalMessage18.Name = "TextBoxFunctionalMessage18";
            this.TextBoxFunctionalMessage18.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage18.TabIndex = 18;
            //
            //TextBoxFunctionalMessage19
            //
            this.TextBoxFunctionalMessage19.AcceptsReturn = true;
            this.TextBoxFunctionalMessage19.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage19.Location = new System.Drawing.Point(321, 240);
            this.TextBoxFunctionalMessage19.MaxLength = 0;
            this.TextBoxFunctionalMessage19.Name = "TextBoxFunctionalMessage19";
            this.TextBoxFunctionalMessage19.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage19.TabIndex = 19;
            //
            //TextBoxFunctionalMessage20
            //
            this.TextBoxFunctionalMessage20.AcceptsReturn = true;
            this.TextBoxFunctionalMessage20.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage20.Location = new System.Drawing.Point(321, 264);
            this.TextBoxFunctionalMessage20.MaxLength = 0;
            this.TextBoxFunctionalMessage20.Name = "TextBoxFunctionalMessage20";
            this.TextBoxFunctionalMessage20.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage20.TabIndex = 20;
            //
            //TextBoxFunctionalMessage21
            //
            this.TextBoxFunctionalMessage21.AcceptsReturn = true;
            this.TextBoxFunctionalMessage21.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage21.Location = new System.Drawing.Point(321, 288);
            this.TextBoxFunctionalMessage21.MaxLength = 0;
            this.TextBoxFunctionalMessage21.Name = "TextBoxFunctionalMessage21";
            this.TextBoxFunctionalMessage21.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage21.TabIndex = 21;
            //
            //TextBoxFunctionalMessage22
            //
            this.TextBoxFunctionalMessage22.AcceptsReturn = true;
            this.TextBoxFunctionalMessage22.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage22.Location = new System.Drawing.Point(529, 48);
            this.TextBoxFunctionalMessage22.MaxLength = 0;
            this.TextBoxFunctionalMessage22.Name = "TextBoxFunctionalMessage22";
            this.TextBoxFunctionalMessage22.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage22.TabIndex = 22;
            //
            //TextBoxFunctionalMessage23
            //
            this.TextBoxFunctionalMessage23.AcceptsReturn = true;
            this.TextBoxFunctionalMessage23.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage23.Location = new System.Drawing.Point(529, 72);
            this.TextBoxFunctionalMessage23.MaxLength = 0;
            this.TextBoxFunctionalMessage23.Name = "TextBoxFunctionalMessage23";
            this.TextBoxFunctionalMessage23.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage23.TabIndex = 23;
            //
            //TextBoxFunctionalMessage24
            //
            this.TextBoxFunctionalMessage24.AcceptsReturn = true;
            this.TextBoxFunctionalMessage24.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage24.Location = new System.Drawing.Point(529, 96);
            this.TextBoxFunctionalMessage24.MaxLength = 0;
            this.TextBoxFunctionalMessage24.Name = "TextBoxFunctionalMessage24";
            this.TextBoxFunctionalMessage24.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage24.TabIndex = 24;
            //
            //TextBoxFunctionalMessage25
            //
            this.TextBoxFunctionalMessage25.AcceptsReturn = true;
            this.TextBoxFunctionalMessage25.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage25.Location = new System.Drawing.Point(529, 120);
            this.TextBoxFunctionalMessage25.MaxLength = 0;
            this.TextBoxFunctionalMessage25.Name = "TextBoxFunctionalMessage25";
            this.TextBoxFunctionalMessage25.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage25.TabIndex = 25;
            //
            //TextBoxFunctionalMessage26
            //
            this.TextBoxFunctionalMessage26.AcceptsReturn = true;
            this.TextBoxFunctionalMessage26.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage26.Location = new System.Drawing.Point(529, 144);
            this.TextBoxFunctionalMessage26.MaxLength = 0;
            this.TextBoxFunctionalMessage26.Name = "TextBoxFunctionalMessage26";
            this.TextBoxFunctionalMessage26.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage26.TabIndex = 26;
            //
            //TextBoxFunctionalMessage27
            //
            this.TextBoxFunctionalMessage27.AcceptsReturn = true;
            this.TextBoxFunctionalMessage27.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage27.Location = new System.Drawing.Point(529, 168);
            this.TextBoxFunctionalMessage27.MaxLength = 0;
            this.TextBoxFunctionalMessage27.Name = "TextBoxFunctionalMessage27";
            this.TextBoxFunctionalMessage27.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage27.TabIndex = 27;
            //
            //TextBoxFunctionalMessage28
            //
            this.TextBoxFunctionalMessage28.AcceptsReturn = true;
            this.TextBoxFunctionalMessage28.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage28.Location = new System.Drawing.Point(529, 192);
            this.TextBoxFunctionalMessage28.MaxLength = 0;
            this.TextBoxFunctionalMessage28.Name = "TextBoxFunctionalMessage28";
            this.TextBoxFunctionalMessage28.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage28.TabIndex = 28;
            //
            //TextBoxFunctionalMessage29
            //
            this.TextBoxFunctionalMessage29.AcceptsReturn = true;
            this.TextBoxFunctionalMessage29.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage29.Location = new System.Drawing.Point(529, 216);
            this.TextBoxFunctionalMessage29.MaxLength = 0;
            this.TextBoxFunctionalMessage29.Name = "TextBoxFunctionalMessage29";
            this.TextBoxFunctionalMessage29.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage29.TabIndex = 29;
            //
            //TextBoxFunctionalMessage30
            //
            this.TextBoxFunctionalMessage30.AcceptsReturn = true;
            this.TextBoxFunctionalMessage30.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage30.Location = new System.Drawing.Point(529, 240);
            this.TextBoxFunctionalMessage30.MaxLength = 0;
            this.TextBoxFunctionalMessage30.Name = "TextBoxFunctionalMessage30";
            this.TextBoxFunctionalMessage30.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage30.TabIndex = 30;
            //
            //TextBoxFunctionalMessage31
            //
            this.TextBoxFunctionalMessage31.AcceptsReturn = true;
            this.TextBoxFunctionalMessage31.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxFunctionalMessage31.Location = new System.Drawing.Point(529, 264);
            this.TextBoxFunctionalMessage31.MaxLength = 0;
            this.TextBoxFunctionalMessage31.Name = "TextBoxFunctionalMessage31";
            this.TextBoxFunctionalMessage31.Size = new System.Drawing.Size(57, 20);
            this.TextBoxFunctionalMessage31.TabIndex = 31;
            //
            //LabelChannel6
            //
            this.LabelChannel6.Location = new System.Drawing.Point(524, 336);
            this.LabelChannel6.Name = "LabelChannel6";
            this.LabelChannel6.Size = new System.Drawing.Size(49, 17);
            this.LabelChannel6.TabIndex = 36;
            this.LabelChannel6.Text = "Channel:";
            //
            //LabelDeviceCombo6
            //
            this.LabelDeviceCombo6.Location = new System.Drawing.Point(395, 336);
            this.LabelDeviceCombo6.Name = "LabelDeviceCombo6";
            this.LabelDeviceCombo6.Size = new System.Drawing.Size(89, 17);
            this.LabelDeviceCombo6.TabIndex = 35;
            this.LabelDeviceCombo6.Text = "Device:";
            //
            //LabelFunctionalMessageModify
            //
            this.LabelFunctionalMessageModify.Location = new System.Drawing.Point(113, 32);
            this.LabelFunctionalMessageModify.Name = "LabelFunctionalMessageModify";
            this.LabelFunctionalMessageModify.Size = new System.Drawing.Size(57, 13);
            this.LabelFunctionalMessageModify.TabIndex = 40;
            this.LabelFunctionalMessageModify.Text = "Modify:";
            //
            //LabelFunctionalMessageValues
            //
            this.LabelFunctionalMessageValues.Location = new System.Drawing.Point(65, 32);
            this.LabelFunctionalMessageValues.Name = "LabelFunctionalMessageValues";
            this.LabelFunctionalMessageValues.Size = new System.Drawing.Size(42, 16);
            this.LabelFunctionalMessageValues.TabIndex = 39;
            this.LabelFunctionalMessageValues.Text = "Addr:";
            //
            //LabelFuncId31
            //
            this.LabelFuncId31.Location = new System.Drawing.Point(456, 265);
            this.LabelFuncId31.Name = "LabelFuncId31";
            this.LabelFuncId31.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId31.TabIndex = 116;
            this.LabelFuncId31.Text = "31:";
            this.LabelFuncId31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId30
            //
            this.LabelFuncId30.Location = new System.Drawing.Point(456, 241);
            this.LabelFuncId30.Name = "LabelFuncId30";
            this.LabelFuncId30.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId30.TabIndex = 115;
            this.LabelFuncId30.Text = "30:";
            this.LabelFuncId30.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId29
            //
            this.LabelFuncId29.Location = new System.Drawing.Point(456, 217);
            this.LabelFuncId29.Name = "LabelFuncId29";
            this.LabelFuncId29.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId29.TabIndex = 114;
            this.LabelFuncId29.Text = "29:";
            this.LabelFuncId29.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId28
            //
            this.LabelFuncId28.Location = new System.Drawing.Point(456, 193);
            this.LabelFuncId28.Name = "LabelFuncId28";
            this.LabelFuncId28.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId28.TabIndex = 113;
            this.LabelFuncId28.Text = "28:";
            this.LabelFuncId28.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId27
            //
            this.LabelFuncId27.Location = new System.Drawing.Point(456, 169);
            this.LabelFuncId27.Name = "LabelFuncId27";
            this.LabelFuncId27.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId27.TabIndex = 112;
            this.LabelFuncId27.Text = "27:";
            this.LabelFuncId27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId26
            //
            this.LabelFuncId26.Location = new System.Drawing.Point(456, 145);
            this.LabelFuncId26.Name = "LabelFuncId26";
            this.LabelFuncId26.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId26.TabIndex = 111;
            this.LabelFuncId26.Text = "26:";
            this.LabelFuncId26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId25
            //
            this.LabelFuncId25.Location = new System.Drawing.Point(456, 121);
            this.LabelFuncId25.Name = "LabelFuncId25";
            this.LabelFuncId25.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId25.TabIndex = 110;
            this.LabelFuncId25.Text = "25:";
            this.LabelFuncId25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId24
            //
            this.LabelFuncId24.Location = new System.Drawing.Point(456, 97);
            this.LabelFuncId24.Name = "LabelFuncId24";
            this.LabelFuncId24.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId24.TabIndex = 109;
            this.LabelFuncId24.Text = "24:";
            this.LabelFuncId24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId23
            //
            this.LabelFuncId23.Location = new System.Drawing.Point(456, 73);
            this.LabelFuncId23.Name = "LabelFuncId23";
            this.LabelFuncId23.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId23.TabIndex = 108;
            this.LabelFuncId23.Text = "23:";
            this.LabelFuncId23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId22
            //
            this.LabelFuncId22.Location = new System.Drawing.Point(456, 49);
            this.LabelFuncId22.Name = "LabelFuncId22";
            this.LabelFuncId22.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId22.TabIndex = 107;
            this.LabelFuncId22.Text = "22:";
            this.LabelFuncId22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId21
            //
            this.LabelFuncId21.Location = new System.Drawing.Point(248, 289);
            this.LabelFuncId21.Name = "LabelFuncId21";
            this.LabelFuncId21.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId21.TabIndex = 84;
            this.LabelFuncId21.Text = "21:";
            this.LabelFuncId21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId20
            //
            this.LabelFuncId20.Location = new System.Drawing.Point(248, 265);
            this.LabelFuncId20.Name = "LabelFuncId20";
            this.LabelFuncId20.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId20.TabIndex = 83;
            this.LabelFuncId20.Text = "20:";
            this.LabelFuncId20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId19
            //
            this.LabelFuncId19.Location = new System.Drawing.Point(248, 241);
            this.LabelFuncId19.Name = "LabelFuncId19";
            this.LabelFuncId19.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId19.TabIndex = 82;
            this.LabelFuncId19.Text = "19:";
            this.LabelFuncId19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId18
            //
            this.LabelFuncId18.Location = new System.Drawing.Point(248, 217);
            this.LabelFuncId18.Name = "LabelFuncId18";
            this.LabelFuncId18.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId18.TabIndex = 81;
            this.LabelFuncId18.Text = "18:";
            this.LabelFuncId18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId17
            //
            this.LabelFuncId17.Location = new System.Drawing.Point(248, 193);
            this.LabelFuncId17.Name = "LabelFuncId17";
            this.LabelFuncId17.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId17.TabIndex = 80;
            this.LabelFuncId17.Text = "17:";
            this.LabelFuncId17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId16
            //
            this.LabelFuncId16.Location = new System.Drawing.Point(248, 169);
            this.LabelFuncId16.Name = "LabelFuncId16";
            this.LabelFuncId16.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId16.TabIndex = 79;
            this.LabelFuncId16.Text = "16:";
            this.LabelFuncId16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId15
            //
            this.LabelFuncId15.Location = new System.Drawing.Point(248, 145);
            this.LabelFuncId15.Name = "LabelFuncId15";
            this.LabelFuncId15.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId15.TabIndex = 78;
            this.LabelFuncId15.Text = "15:";
            this.LabelFuncId15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId14
            //
            this.LabelFuncId14.Location = new System.Drawing.Point(248, 121);
            this.LabelFuncId14.Name = "LabelFuncId14";
            this.LabelFuncId14.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId14.TabIndex = 77;
            this.LabelFuncId14.Text = "14:";
            this.LabelFuncId14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId13
            //
            this.LabelFuncId13.Location = new System.Drawing.Point(248, 97);
            this.LabelFuncId13.Name = "LabelFuncId13";
            this.LabelFuncId13.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId13.TabIndex = 76;
            this.LabelFuncId13.Text = "13:";
            this.LabelFuncId13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId12
            //
            this.LabelFuncId12.Location = new System.Drawing.Point(248, 73);
            this.LabelFuncId12.Name = "LabelFuncId12";
            this.LabelFuncId12.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId12.TabIndex = 75;
            this.LabelFuncId12.Text = "12:";
            this.LabelFuncId12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId11
            //
            this.LabelFuncId11.Location = new System.Drawing.Point(248, 49);
            this.LabelFuncId11.Name = "LabelFuncId11";
            this.LabelFuncId11.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId11.TabIndex = 74;
            this.LabelFuncId11.Text = "11:";
            this.LabelFuncId11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId10
            //
            this.LabelFuncId10.Location = new System.Drawing.Point(40, 289);
            this.LabelFuncId10.Name = "LabelFuncId10";
            this.LabelFuncId10.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId10.TabIndex = 51;
            this.LabelFuncId10.Text = "10:";
            this.LabelFuncId10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId9
            //
            this.LabelFuncId9.Location = new System.Drawing.Point(40, 265);
            this.LabelFuncId9.Name = "LabelFuncId9";
            this.LabelFuncId9.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId9.TabIndex = 50;
            this.LabelFuncId9.Text = "9:";
            this.LabelFuncId9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId8
            //
            this.LabelFuncId8.Location = new System.Drawing.Point(40, 241);
            this.LabelFuncId8.Name = "LabelFuncId8";
            this.LabelFuncId8.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId8.TabIndex = 49;
            this.LabelFuncId8.Text = "8:";
            this.LabelFuncId8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId7
            //
            this.LabelFuncId7.Location = new System.Drawing.Point(40, 217);
            this.LabelFuncId7.Name = "LabelFuncId7";
            this.LabelFuncId7.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId7.TabIndex = 48;
            this.LabelFuncId7.Text = "7:";
            this.LabelFuncId7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId6
            //
            this.LabelFuncId6.Location = new System.Drawing.Point(40, 193);
            this.LabelFuncId6.Name = "LabelFuncId6";
            this.LabelFuncId6.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId6.TabIndex = 47;
            this.LabelFuncId6.Text = "6:";
            this.LabelFuncId6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId5
            //
            this.LabelFuncId5.Location = new System.Drawing.Point(40, 169);
            this.LabelFuncId5.Name = "LabelFuncId5";
            this.LabelFuncId5.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId5.TabIndex = 46;
            this.LabelFuncId5.Text = "5:";
            this.LabelFuncId5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId4
            //
            this.LabelFuncId4.Location = new System.Drawing.Point(40, 145);
            this.LabelFuncId4.Name = "LabelFuncId4";
            this.LabelFuncId4.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId4.TabIndex = 45;
            this.LabelFuncId4.Text = "4:";
            this.LabelFuncId4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId3
            //
            this.LabelFuncId3.Location = new System.Drawing.Point(40, 121);
            this.LabelFuncId3.Name = "LabelFuncId3";
            this.LabelFuncId3.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId3.TabIndex = 44;
            this.LabelFuncId3.Text = "3:";
            this.LabelFuncId3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId2
            //
            this.LabelFuncId2.Location = new System.Drawing.Point(40, 97);
            this.LabelFuncId2.Name = "LabelFuncId2";
            this.LabelFuncId2.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId2.TabIndex = 43;
            this.LabelFuncId2.Text = "2:";
            this.LabelFuncId2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId1
            //
            this.LabelFuncId1.Location = new System.Drawing.Point(40, 73);
            this.LabelFuncId1.Name = "LabelFuncId1";
            this.LabelFuncId1.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId1.TabIndex = 42;
            this.LabelFuncId1.Text = "1:";
            this.LabelFuncId1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFuncId0
            //
            this.LabelFuncId0.Location = new System.Drawing.Point(40, 49);
            this.LabelFuncId0.Name = "LabelFuncId0";
            this.LabelFuncId0.Size = new System.Drawing.Size(22, 17);
            this.LabelFuncId0.TabIndex = 41;
            this.LabelFuncId0.Text = "0:";
            this.LabelFuncId0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelFunctionalMessage0
            //
            this.LabelFunctionalMessage0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage0.Location = new System.Drawing.Point(65, 48);
            this.LabelFunctionalMessage0.Name = "LabelFunctionalMessage0";
            this.LabelFunctionalMessage0.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage0.TabIndex = 52;
            //
            //LabelFunctionalMessage1
            //
            this.LabelFunctionalMessage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage1.Location = new System.Drawing.Point(65, 72);
            this.LabelFunctionalMessage1.Name = "LabelFunctionalMessage1";
            this.LabelFunctionalMessage1.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage1.TabIndex = 53;
            //
            //LabelFunctionalMessage2
            //
            this.LabelFunctionalMessage2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage2.Location = new System.Drawing.Point(65, 96);
            this.LabelFunctionalMessage2.Name = "LabelFunctionalMessage2";
            this.LabelFunctionalMessage2.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage2.TabIndex = 54;
            //
            //LabelFunctionalMessage3
            //
            this.LabelFunctionalMessage3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage3.Location = new System.Drawing.Point(65, 120);
            this.LabelFunctionalMessage3.Name = "LabelFunctionalMessage3";
            this.LabelFunctionalMessage3.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage3.TabIndex = 55;
            //
            //LabelFunctionalMessage4
            //
            this.LabelFunctionalMessage4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage4.Location = new System.Drawing.Point(65, 144);
            this.LabelFunctionalMessage4.Name = "LabelFunctionalMessage4";
            this.LabelFunctionalMessage4.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage4.TabIndex = 56;
            //
            //LabelFunctionalMessage5
            //
            this.LabelFunctionalMessage5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage5.Location = new System.Drawing.Point(65, 168);
            this.LabelFunctionalMessage5.Name = "LabelFunctionalMessage5";
            this.LabelFunctionalMessage5.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage5.TabIndex = 57;
            //
            //LabelFunctionalMessage6
            //
            this.LabelFunctionalMessage6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage6.Location = new System.Drawing.Point(65, 192);
            this.LabelFunctionalMessage6.Name = "LabelFunctionalMessage6";
            this.LabelFunctionalMessage6.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage6.TabIndex = 58;
            //
            //LabelFunctionalMessage7
            //
            this.LabelFunctionalMessage7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage7.Location = new System.Drawing.Point(65, 216);
            this.LabelFunctionalMessage7.Name = "LabelFunctionalMessage7";
            this.LabelFunctionalMessage7.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage7.TabIndex = 59;
            //
            //LabelFunctionalMessage8
            //
            this.LabelFunctionalMessage8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage8.Location = new System.Drawing.Point(65, 240);
            this.LabelFunctionalMessage8.Name = "LabelFunctionalMessage8";
            this.LabelFunctionalMessage8.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage8.TabIndex = 60;
            //
            //LabelFunctionalMessage9
            //
            this.LabelFunctionalMessage9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage9.Location = new System.Drawing.Point(65, 264);
            this.LabelFunctionalMessage9.Name = "LabelFunctionalMessage9";
            this.LabelFunctionalMessage9.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage9.TabIndex = 61;
            //
            //LabelFunctionalMessage10
            //
            this.LabelFunctionalMessage10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage10.Location = new System.Drawing.Point(65, 288);
            this.LabelFunctionalMessage10.Name = "LabelFunctionalMessage10";
            this.LabelFunctionalMessage10.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage10.TabIndex = 62;
            //
            //LabelFunctionalMessage11
            //
            this.LabelFunctionalMessage11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage11.Location = new System.Drawing.Point(273, 48);
            this.LabelFunctionalMessage11.Name = "LabelFunctionalMessage11";
            this.LabelFunctionalMessage11.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage11.TabIndex = 85;
            //
            //LabelFunctionalMessage12
            //
            this.LabelFunctionalMessage12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage12.Location = new System.Drawing.Point(273, 72);
            this.LabelFunctionalMessage12.Name = "LabelFunctionalMessage12";
            this.LabelFunctionalMessage12.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage12.TabIndex = 86;
            //
            //LabelFunctionalMessage13
            //
            this.LabelFunctionalMessage13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage13.Location = new System.Drawing.Point(273, 96);
            this.LabelFunctionalMessage13.Name = "LabelFunctionalMessage13";
            this.LabelFunctionalMessage13.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage13.TabIndex = 87;
            //
            //LabelFunctionalMessage14
            //
            this.LabelFunctionalMessage14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage14.Location = new System.Drawing.Point(273, 120);
            this.LabelFunctionalMessage14.Name = "LabelFunctionalMessage14";
            this.LabelFunctionalMessage14.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage14.TabIndex = 88;
            //
            //LabelFunctionalMessage15
            //
            this.LabelFunctionalMessage15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage15.Location = new System.Drawing.Point(273, 144);
            this.LabelFunctionalMessage15.Name = "LabelFunctionalMessage15";
            this.LabelFunctionalMessage15.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage15.TabIndex = 89;
            //
            //LabelFunctionalMessage16
            //
            this.LabelFunctionalMessage16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage16.Location = new System.Drawing.Point(273, 168);
            this.LabelFunctionalMessage16.Name = "LabelFunctionalMessage16";
            this.LabelFunctionalMessage16.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage16.TabIndex = 90;
            //
            //LabelFunctionalMessage17
            //
            this.LabelFunctionalMessage17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage17.Location = new System.Drawing.Point(273, 192);
            this.LabelFunctionalMessage17.Name = "LabelFunctionalMessage17";
            this.LabelFunctionalMessage17.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage17.TabIndex = 91;
            //
            //LabelFunctionalMessage18
            //
            this.LabelFunctionalMessage18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage18.Location = new System.Drawing.Point(273, 216);
            this.LabelFunctionalMessage18.Name = "LabelFunctionalMessage18";
            this.LabelFunctionalMessage18.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage18.TabIndex = 92;
            //
            //LabelFunctionalMessage19
            //
            this.LabelFunctionalMessage19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage19.Location = new System.Drawing.Point(273, 240);
            this.LabelFunctionalMessage19.Name = "LabelFunctionalMessage19";
            this.LabelFunctionalMessage19.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage19.TabIndex = 93;
            //
            //LabelFunctionalMessage20
            //
            this.LabelFunctionalMessage20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage20.Location = new System.Drawing.Point(273, 264);
            this.LabelFunctionalMessage20.Name = "LabelFunctionalMessage20";
            this.LabelFunctionalMessage20.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage20.TabIndex = 94;
            //
            //LabelFunctionalMessage21
            //
            this.LabelFunctionalMessage21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage21.Location = new System.Drawing.Point(273, 288);
            this.LabelFunctionalMessage21.Name = "LabelFunctionalMessage21";
            this.LabelFunctionalMessage21.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage21.TabIndex = 95;
            //
            //LabelFunctionalMessage22
            //
            this.LabelFunctionalMessage22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage22.Location = new System.Drawing.Point(481, 48);
            this.LabelFunctionalMessage22.Name = "LabelFunctionalMessage22";
            this.LabelFunctionalMessage22.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage22.TabIndex = 117;
            //
            //LabelFunctionalMessage23
            //
            this.LabelFunctionalMessage23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage23.Location = new System.Drawing.Point(481, 72);
            this.LabelFunctionalMessage23.Name = "LabelFunctionalMessage23";
            this.LabelFunctionalMessage23.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage23.TabIndex = 118;
            //
            //LabelFunctionalMessage24
            //
            this.LabelFunctionalMessage24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage24.Location = new System.Drawing.Point(481, 96);
            this.LabelFunctionalMessage24.Name = "LabelFunctionalMessage24";
            this.LabelFunctionalMessage24.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage24.TabIndex = 119;
            //
            //LabelFunctionalMessage25
            //
            this.LabelFunctionalMessage25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage25.Location = new System.Drawing.Point(481, 120);
            this.LabelFunctionalMessage25.Name = "LabelFunctionalMessage25";
            this.LabelFunctionalMessage25.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage25.TabIndex = 120;
            //
            //LabelFunctionalMessage26
            //
            this.LabelFunctionalMessage26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage26.Location = new System.Drawing.Point(481, 144);
            this.LabelFunctionalMessage26.Name = "LabelFunctionalMessage26";
            this.LabelFunctionalMessage26.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage26.TabIndex = 121;
            //
            //LabelFunctionalMessage27
            //
            this.LabelFunctionalMessage27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage27.Location = new System.Drawing.Point(481, 168);
            this.LabelFunctionalMessage27.Name = "LabelFunctionalMessage27";
            this.LabelFunctionalMessage27.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage27.TabIndex = 122;
            //
            //LabelFunctionalMessage28
            //
            this.LabelFunctionalMessage28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage28.Location = new System.Drawing.Point(481, 192);
            this.LabelFunctionalMessage28.Name = "LabelFunctionalMessage28";
            this.LabelFunctionalMessage28.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage28.TabIndex = 123;
            //
            //LabelFunctionalMessage29
            //
            this.LabelFunctionalMessage29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage29.Location = new System.Drawing.Point(481, 216);
            this.LabelFunctionalMessage29.Name = "LabelFunctionalMessage29";
            this.LabelFunctionalMessage29.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage29.TabIndex = 124;
            //
            //LabelFunctionalMessage30
            //
            this.LabelFunctionalMessage30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage30.Location = new System.Drawing.Point(481, 240);
            this.LabelFunctionalMessage30.Name = "LabelFunctionalMessage30";
            this.LabelFunctionalMessage30.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage30.TabIndex = 125;
            //
            //LabelFunctionalMessage31
            //
            this.LabelFunctionalMessage31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelFunctionalMessage31.Location = new System.Drawing.Point(481, 264);
            this.LabelFunctionalMessage31.Name = "LabelFunctionalMessage31";
            this.LabelFunctionalMessage31.Size = new System.Drawing.Size(41, 19);
            this.LabelFunctionalMessage31.TabIndex = 126;
            //
            //TabPageAnalog
            //
            this.TabPageAnalog.Controls.Add(this.ComboBoxAnalogChannel);
            this.TabPageAnalog.Controls.Add(this.GroupBoxAnalog);
            this.TabPageAnalog.Controls.Add(this.GroupBoxBattVoltage);
            this.TabPageAnalog.Controls.Add(this.GroupBoxProgVoltage);
            this.TabPageAnalog.Controls.Add(this.LabelAnalogChannel);
            this.TabPageAnalog.Controls.Add(this.LabelDeviceCombo7);
            this.TabPageAnalog.Controls.Add(this.ComboAvailableBoxLocator7);
            this.TabPageAnalog.Controls.Add(this.ComboAvailableChannelLocator7);
            this.TabPageAnalog.Location = new System.Drawing.Point(4, 22);
            this.TabPageAnalog.Name = "TabPageAnalog";
            this.TabPageAnalog.Size = new System.Drawing.Size(714, 395);
            this.TabPageAnalog.TabIndex = 7;
            this.TabPageAnalog.Text = "IO && Programming Voltages";
            //
            //ComboBoxAnalogChannel
            //
            this.ComboBoxAnalogChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxAnalogChannel.Location = new System.Drawing.Point(527, 353);
            this.ComboBoxAnalogChannel.Name = "ComboBoxAnalogChannel";
            this.ComboBoxAnalogChannel.Size = new System.Drawing.Size(161, 21);
            this.ComboBoxAnalogChannel.TabIndex = 399;
            //
            //GroupBoxAnalog
            //
            this.GroupBoxAnalog.Controls.Add(this.PanelAnalog);
            this.GroupBoxAnalog.Controls.Add(this.LabelAnalogLow1);
            this.GroupBoxAnalog.Controls.Add(this.LabelAnalogHigh1);
            this.GroupBoxAnalog.Controls.Add(this.LabelAnalogHigh0);
            this.GroupBoxAnalog.Controls.Add(this.LabelAnalogLow0);
            this.GroupBoxAnalog.Controls.Add(this.LabelAnalogReading);
            this.GroupBoxAnalog.Location = new System.Drawing.Point(8, 3);
            this.GroupBoxAnalog.Name = "GroupBoxAnalog";
            this.GroupBoxAnalog.Size = new System.Drawing.Size(357, 322);
            this.GroupBoxAnalog.TabIndex = 0;
            this.GroupBoxAnalog.TabStop = false;
            this.GroupBoxAnalog.Text = "Analog IO";
            //
            //PanelAnalog
            //
            this.PanelAnalog.AutoScroll = true;
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH0);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH1);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH2);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH3);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH4);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH5);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH6);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH7);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH8);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH9);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH10);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH11);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH12);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH13);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH14);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH15);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH16);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH17);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH18);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH19);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH20);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH21);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH22);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH23);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH24);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH25);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH26);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH27);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH28);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH29);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH30);
            this.PanelAnalog.Controls.Add(this.LabelAnalogCH31);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead0);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead1);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead2);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead3);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead4);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead5);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead6);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead7);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead8);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead9);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead10);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead11);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead12);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead13);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead14);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead15);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead16);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead17);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead18);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead19);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead20);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead21);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead22);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead23);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead24);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead25);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead26);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead27);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead28);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead29);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead30);
            this.PanelAnalog.Controls.Add(this.LabelAnalogRead31);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog0);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog1);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog2);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog3);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog4);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog5);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog6);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog7);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog8);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog9);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog10);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog11);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog12);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog13);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog14);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog15);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog16);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog17);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog18);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog19);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog20);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog21);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog22);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog23);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog24);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog25);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog26);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog27);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog28);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog29);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog30);
            this.PanelAnalog.Controls.Add(this.ProgressBarAnalog31);
            this.PanelAnalog.Location = new System.Drawing.Point(6, 37);
            this.PanelAnalog.Name = "PanelAnalog";
            this.PanelAnalog.Size = new System.Drawing.Size(345, 247);
            this.PanelAnalog.TabIndex = 0;
            //
            //LabelAnalogCH0
            //
            this.LabelAnalogCH0.Location = new System.Drawing.Point(2, 10);
            this.LabelAnalogCH0.Name = "LabelAnalogCH0";
            this.LabelAnalogCH0.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH0.TabIndex = 0;
            this.LabelAnalogCH0.Text = "Channel 1:";
            this.LabelAnalogCH0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH1
            //
            this.LabelAnalogCH1.Location = new System.Drawing.Point(2, 34);
            this.LabelAnalogCH1.Name = "LabelAnalogCH1";
            this.LabelAnalogCH1.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH1.TabIndex = 1;
            this.LabelAnalogCH1.Text = "Channel 2:";
            this.LabelAnalogCH1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH2
            //
            this.LabelAnalogCH2.Location = new System.Drawing.Point(2, 58);
            this.LabelAnalogCH2.Name = "LabelAnalogCH2";
            this.LabelAnalogCH2.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH2.TabIndex = 2;
            this.LabelAnalogCH2.Text = "Channel 3:";
            this.LabelAnalogCH2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH3
            //
            this.LabelAnalogCH3.Location = new System.Drawing.Point(2, 82);
            this.LabelAnalogCH3.Name = "LabelAnalogCH3";
            this.LabelAnalogCH3.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH3.TabIndex = 3;
            this.LabelAnalogCH3.Text = "Channel 4:";
            this.LabelAnalogCH3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH4
            //
            this.LabelAnalogCH4.Location = new System.Drawing.Point(2, 106);
            this.LabelAnalogCH4.Name = "LabelAnalogCH4";
            this.LabelAnalogCH4.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH4.TabIndex = 4;
            this.LabelAnalogCH4.Text = "Channel 5:";
            this.LabelAnalogCH4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH5
            //
            this.LabelAnalogCH5.Location = new System.Drawing.Point(2, 130);
            this.LabelAnalogCH5.Name = "LabelAnalogCH5";
            this.LabelAnalogCH5.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH5.TabIndex = 5;
            this.LabelAnalogCH5.Text = "Channel 6:";
            this.LabelAnalogCH5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH6
            //
            this.LabelAnalogCH6.Location = new System.Drawing.Point(2, 154);
            this.LabelAnalogCH6.Name = "LabelAnalogCH6";
            this.LabelAnalogCH6.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH6.TabIndex = 6;
            this.LabelAnalogCH6.Text = "Channel 7:";
            this.LabelAnalogCH6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH7
            //
            this.LabelAnalogCH7.Location = new System.Drawing.Point(2, 178);
            this.LabelAnalogCH7.Name = "LabelAnalogCH7";
            this.LabelAnalogCH7.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH7.TabIndex = 7;
            this.LabelAnalogCH7.Text = "Channel 8:";
            this.LabelAnalogCH7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH8
            //
            this.LabelAnalogCH8.Location = new System.Drawing.Point(2, 202);
            this.LabelAnalogCH8.Name = "LabelAnalogCH8";
            this.LabelAnalogCH8.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH8.TabIndex = 8;
            this.LabelAnalogCH8.Text = "Channel 9:";
            this.LabelAnalogCH8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH9
            //
            this.LabelAnalogCH9.Location = new System.Drawing.Point(2, 226);
            this.LabelAnalogCH9.Name = "LabelAnalogCH9";
            this.LabelAnalogCH9.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH9.TabIndex = 9;
            this.LabelAnalogCH9.Text = "Channel 10:";
            this.LabelAnalogCH9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH10
            //
            this.LabelAnalogCH10.Location = new System.Drawing.Point(2, 250);
            this.LabelAnalogCH10.Name = "LabelAnalogCH10";
            this.LabelAnalogCH10.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH10.TabIndex = 10;
            this.LabelAnalogCH10.Text = "Channel 11:";
            this.LabelAnalogCH10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH11
            //
            this.LabelAnalogCH11.Location = new System.Drawing.Point(2, 274);
            this.LabelAnalogCH11.Name = "LabelAnalogCH11";
            this.LabelAnalogCH11.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH11.TabIndex = 11;
            this.LabelAnalogCH11.Text = "Channel 12:";
            this.LabelAnalogCH11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH12
            //
            this.LabelAnalogCH12.Location = new System.Drawing.Point(2, 298);
            this.LabelAnalogCH12.Name = "LabelAnalogCH12";
            this.LabelAnalogCH12.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH12.TabIndex = 12;
            this.LabelAnalogCH12.Text = "Channel 13:";
            this.LabelAnalogCH12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH13
            //
            this.LabelAnalogCH13.Location = new System.Drawing.Point(2, 322);
            this.LabelAnalogCH13.Name = "LabelAnalogCH13";
            this.LabelAnalogCH13.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH13.TabIndex = 13;
            this.LabelAnalogCH13.Text = "Channel 14:";
            this.LabelAnalogCH13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH14
            //
            this.LabelAnalogCH14.Location = new System.Drawing.Point(2, 346);
            this.LabelAnalogCH14.Name = "LabelAnalogCH14";
            this.LabelAnalogCH14.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH14.TabIndex = 14;
            this.LabelAnalogCH14.Text = "Channel 15:";
            this.LabelAnalogCH14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH15
            //
            this.LabelAnalogCH15.Location = new System.Drawing.Point(2, 370);
            this.LabelAnalogCH15.Name = "LabelAnalogCH15";
            this.LabelAnalogCH15.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH15.TabIndex = 15;
            this.LabelAnalogCH15.Text = "Channel 16:";
            this.LabelAnalogCH15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH16
            //
            this.LabelAnalogCH16.Location = new System.Drawing.Point(2, 394);
            this.LabelAnalogCH16.Name = "LabelAnalogCH16";
            this.LabelAnalogCH16.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH16.TabIndex = 382;
            this.LabelAnalogCH16.Text = "Channel 17:";
            this.LabelAnalogCH16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH17
            //
            this.LabelAnalogCH17.Location = new System.Drawing.Point(2, 418);
            this.LabelAnalogCH17.Name = "LabelAnalogCH17";
            this.LabelAnalogCH17.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH17.TabIndex = 382;
            this.LabelAnalogCH17.Text = "Channel 18:";
            this.LabelAnalogCH17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH18
            //
            this.LabelAnalogCH18.Location = new System.Drawing.Point(2, 442);
            this.LabelAnalogCH18.Name = "LabelAnalogCH18";
            this.LabelAnalogCH18.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH18.TabIndex = 382;
            this.LabelAnalogCH18.Text = "Channel 19:";
            this.LabelAnalogCH18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH19
            //
            this.LabelAnalogCH19.Location = new System.Drawing.Point(2, 466);
            this.LabelAnalogCH19.Name = "LabelAnalogCH19";
            this.LabelAnalogCH19.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH19.TabIndex = 382;
            this.LabelAnalogCH19.Text = "Channel 20:";
            this.LabelAnalogCH19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH20
            //
            this.LabelAnalogCH20.Location = new System.Drawing.Point(2, 490);
            this.LabelAnalogCH20.Name = "LabelAnalogCH20";
            this.LabelAnalogCH20.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH20.TabIndex = 377;
            this.LabelAnalogCH20.Text = "Channel 21:";
            this.LabelAnalogCH20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH21
            //
            this.LabelAnalogCH21.Location = new System.Drawing.Point(2, 514);
            this.LabelAnalogCH21.Name = "LabelAnalogCH21";
            this.LabelAnalogCH21.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH21.TabIndex = 378;
            this.LabelAnalogCH21.Text = "Channel 22:";
            this.LabelAnalogCH21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH22
            //
            this.LabelAnalogCH22.Location = new System.Drawing.Point(2, 538);
            this.LabelAnalogCH22.Name = "LabelAnalogCH22";
            this.LabelAnalogCH22.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH22.TabIndex = 379;
            this.LabelAnalogCH22.Text = "Channel 23:";
            this.LabelAnalogCH22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH23
            //
            this.LabelAnalogCH23.Location = new System.Drawing.Point(2, 562);
            this.LabelAnalogCH23.Name = "LabelAnalogCH23";
            this.LabelAnalogCH23.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH23.TabIndex = 380;
            this.LabelAnalogCH23.Text = "Channel 24:";
            this.LabelAnalogCH23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH24
            //
            this.LabelAnalogCH24.Location = new System.Drawing.Point(2, 586);
            this.LabelAnalogCH24.Name = "LabelAnalogCH24";
            this.LabelAnalogCH24.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH24.TabIndex = 381;
            this.LabelAnalogCH24.Text = "Channel 25:";
            this.LabelAnalogCH24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH25
            //
            this.LabelAnalogCH25.Location = new System.Drawing.Point(2, 610);
            this.LabelAnalogCH25.Name = "LabelAnalogCH25";
            this.LabelAnalogCH25.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH25.TabIndex = 382;
            this.LabelAnalogCH25.Text = "Channel 26:";
            this.LabelAnalogCH25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH26
            //
            this.LabelAnalogCH26.Location = new System.Drawing.Point(2, 634);
            this.LabelAnalogCH26.Name = "LabelAnalogCH26";
            this.LabelAnalogCH26.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH26.TabIndex = 382;
            this.LabelAnalogCH26.Text = "Channel 27:";
            this.LabelAnalogCH26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH27
            //
            this.LabelAnalogCH27.Location = new System.Drawing.Point(2, 658);
            this.LabelAnalogCH27.Name = "LabelAnalogCH27";
            this.LabelAnalogCH27.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH27.TabIndex = 382;
            this.LabelAnalogCH27.Text = "Channel 28:";
            this.LabelAnalogCH27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH28
            //
            this.LabelAnalogCH28.Location = new System.Drawing.Point(2, 682);
            this.LabelAnalogCH28.Name = "LabelAnalogCH28";
            this.LabelAnalogCH28.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH28.TabIndex = 382;
            this.LabelAnalogCH28.Text = "Channel 29:";
            this.LabelAnalogCH28.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH29
            //
            this.LabelAnalogCH29.Location = new System.Drawing.Point(2, 706);
            this.LabelAnalogCH29.Name = "LabelAnalogCH29";
            this.LabelAnalogCH29.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH29.TabIndex = 382;
            this.LabelAnalogCH29.Text = "Channel 30:";
            this.LabelAnalogCH29.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH30
            //
            this.LabelAnalogCH30.Location = new System.Drawing.Point(2, 730);
            this.LabelAnalogCH30.Name = "LabelAnalogCH30";
            this.LabelAnalogCH30.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH30.TabIndex = 377;
            this.LabelAnalogCH30.Text = "Channel 31:";
            this.LabelAnalogCH30.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogCH31
            //
            this.LabelAnalogCH31.Location = new System.Drawing.Point(2, 754);
            this.LabelAnalogCH31.Name = "LabelAnalogCH31";
            this.LabelAnalogCH31.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogCH31.TabIndex = 378;
            this.LabelAnalogCH31.Text = "Channel 32:";
            this.LabelAnalogCH31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogRead0
            //
            this.LabelAnalogRead0.Location = new System.Drawing.Point(72, 10);
            this.LabelAnalogRead0.Name = "LabelAnalogRead0";
            this.LabelAnalogRead0.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead0.TabIndex = 16;
            this.LabelAnalogRead0.Text = "LabelAnalogRead(0)";
            //
            //LabelAnalogRead1
            //
            this.LabelAnalogRead1.Location = new System.Drawing.Point(72, 34);
            this.LabelAnalogRead1.Name = "LabelAnalogRead1";
            this.LabelAnalogRead1.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead1.TabIndex = 17;
            this.LabelAnalogRead1.Text = "LabelAnalogRead(1)";
            //
            //LabelAnalogRead2
            //
            this.LabelAnalogRead2.Location = new System.Drawing.Point(72, 58);
            this.LabelAnalogRead2.Name = "LabelAnalogRead2";
            this.LabelAnalogRead2.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead2.TabIndex = 18;
            this.LabelAnalogRead2.Text = "LabelAnalogRead(2)";
            //
            //LabelAnalogRead3
            //
            this.LabelAnalogRead3.Location = new System.Drawing.Point(72, 82);
            this.LabelAnalogRead3.Name = "LabelAnalogRead3";
            this.LabelAnalogRead3.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead3.TabIndex = 19;
            this.LabelAnalogRead3.Text = "LabelAnalogRead(3)";
            //
            //LabelAnalogRead4
            //
            this.LabelAnalogRead4.Location = new System.Drawing.Point(72, 106);
            this.LabelAnalogRead4.Name = "LabelAnalogRead4";
            this.LabelAnalogRead4.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead4.TabIndex = 20;
            this.LabelAnalogRead4.Text = "LabelAnalogRead(4)";
            //
            //LabelAnalogRead5
            //
            this.LabelAnalogRead5.Location = new System.Drawing.Point(72, 130);
            this.LabelAnalogRead5.Name = "LabelAnalogRead5";
            this.LabelAnalogRead5.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead5.TabIndex = 21;
            this.LabelAnalogRead5.Text = "LabelAnalogRead(5)";
            //
            //LabelAnalogRead6
            //
            this.LabelAnalogRead6.Location = new System.Drawing.Point(72, 154);
            this.LabelAnalogRead6.Name = "LabelAnalogRead6";
            this.LabelAnalogRead6.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead6.TabIndex = 22;
            this.LabelAnalogRead6.Text = "LabelAnalogRead(6)";
            //
            //LabelAnalogRead7
            //
            this.LabelAnalogRead7.Location = new System.Drawing.Point(72, 178);
            this.LabelAnalogRead7.Name = "LabelAnalogRead7";
            this.LabelAnalogRead7.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead7.TabIndex = 23;
            this.LabelAnalogRead7.Text = "LabelAnalogRead(7)";
            //
            //LabelAnalogRead8
            //
            this.LabelAnalogRead8.Location = new System.Drawing.Point(72, 202);
            this.LabelAnalogRead8.Name = "LabelAnalogRead8";
            this.LabelAnalogRead8.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead8.TabIndex = 24;
            this.LabelAnalogRead8.Text = "LabelAnalogRead(8)";
            //
            //LabelAnalogRead9
            //
            this.LabelAnalogRead9.Location = new System.Drawing.Point(72, 226);
            this.LabelAnalogRead9.Name = "LabelAnalogRead9";
            this.LabelAnalogRead9.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead9.TabIndex = 25;
            this.LabelAnalogRead9.Text = "LabelAnalogRead(9)";
            //
            //LabelAnalogRead10
            //
            this.LabelAnalogRead10.Location = new System.Drawing.Point(72, 250);
            this.LabelAnalogRead10.Name = "LabelAnalogRead10";
            this.LabelAnalogRead10.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead10.TabIndex = 26;
            this.LabelAnalogRead10.Text = "LabelAnalogRead(10)";
            //
            //LabelAnalogRead11
            //
            this.LabelAnalogRead11.Location = new System.Drawing.Point(72, 274);
            this.LabelAnalogRead11.Name = "LabelAnalogRead11";
            this.LabelAnalogRead11.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead11.TabIndex = 27;
            this.LabelAnalogRead11.Text = "LabelAnalogRead(11)";
            //
            //LabelAnalogRead12
            //
            this.LabelAnalogRead12.Location = new System.Drawing.Point(72, 298);
            this.LabelAnalogRead12.Name = "LabelAnalogRead12";
            this.LabelAnalogRead12.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead12.TabIndex = 28;
            this.LabelAnalogRead12.Text = "LabelAnalogRead(12)";
            //
            //LabelAnalogRead13
            //
            this.LabelAnalogRead13.Location = new System.Drawing.Point(72, 322);
            this.LabelAnalogRead13.Name = "LabelAnalogRead13";
            this.LabelAnalogRead13.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead13.TabIndex = 29;
            this.LabelAnalogRead13.Text = "LabelAnalogRead(13)";
            //
            //LabelAnalogRead14
            //
            this.LabelAnalogRead14.Location = new System.Drawing.Point(72, 346);
            this.LabelAnalogRead14.Name = "LabelAnalogRead14";
            this.LabelAnalogRead14.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead14.TabIndex = 30;
            this.LabelAnalogRead14.Text = "LabelAnalogRead(14)";
            //
            //LabelAnalogRead15
            //
            this.LabelAnalogRead15.Location = new System.Drawing.Point(72, 370);
            this.LabelAnalogRead15.Name = "LabelAnalogRead15";
            this.LabelAnalogRead15.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead15.TabIndex = 31;
            this.LabelAnalogRead15.Text = "LabelAnalogRead(15)";
            //
            //LabelAnalogRead16
            //
            this.LabelAnalogRead16.Location = new System.Drawing.Point(72, 394);
            this.LabelAnalogRead16.Name = "LabelAnalogRead16";
            this.LabelAnalogRead16.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead16.TabIndex = 388;
            this.LabelAnalogRead16.Text = "LabelAnalogRead(16)";
            //
            //LabelAnalogRead17
            //
            this.LabelAnalogRead17.Location = new System.Drawing.Point(72, 418);
            this.LabelAnalogRead17.Name = "LabelAnalogRead17";
            this.LabelAnalogRead17.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead17.TabIndex = 388;
            this.LabelAnalogRead17.Text = "LabelAnalogRead(17)";
            //
            //LabelAnalogRead18
            //
            this.LabelAnalogRead18.Location = new System.Drawing.Point(72, 442);
            this.LabelAnalogRead18.Name = "LabelAnalogRead18";
            this.LabelAnalogRead18.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead18.TabIndex = 388;
            this.LabelAnalogRead18.Text = "LabelAnalogRead(18)";
            //
            //LabelAnalogRead19
            //
            this.LabelAnalogRead19.Location = new System.Drawing.Point(72, 466);
            this.LabelAnalogRead19.Name = "LabelAnalogRead19";
            this.LabelAnalogRead19.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead19.TabIndex = 388;
            this.LabelAnalogRead19.Text = "LabelAnalogRead(19)";
            //
            //LabelAnalogRead20
            //
            this.LabelAnalogRead20.Location = new System.Drawing.Point(72, 490);
            this.LabelAnalogRead20.Name = "LabelAnalogRead20";
            this.LabelAnalogRead20.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead20.TabIndex = 383;
            this.LabelAnalogRead20.Text = "LabelAnalogRead(20)";
            //
            //LabelAnalogRead21
            //
            this.LabelAnalogRead21.Location = new System.Drawing.Point(72, 514);
            this.LabelAnalogRead21.Name = "LabelAnalogRead21";
            this.LabelAnalogRead21.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead21.TabIndex = 384;
            this.LabelAnalogRead21.Text = "LabelAnalogRead(21)";
            //
            //LabelAnalogRead22
            //
            this.LabelAnalogRead22.Location = new System.Drawing.Point(72, 538);
            this.LabelAnalogRead22.Name = "LabelAnalogRead22";
            this.LabelAnalogRead22.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead22.TabIndex = 385;
            this.LabelAnalogRead22.Text = "LabelAnalogRead(22)";
            //
            //LabelAnalogRead23
            //
            this.LabelAnalogRead23.Location = new System.Drawing.Point(72, 562);
            this.LabelAnalogRead23.Name = "LabelAnalogRead23";
            this.LabelAnalogRead23.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead23.TabIndex = 386;
            this.LabelAnalogRead23.Text = "LabelAnalogRead(23)";
            //
            //LabelAnalogRead24
            //
            this.LabelAnalogRead24.Location = new System.Drawing.Point(72, 586);
            this.LabelAnalogRead24.Name = "LabelAnalogRead24";
            this.LabelAnalogRead24.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead24.TabIndex = 387;
            this.LabelAnalogRead24.Text = "LabelAnalogRead(24)";
            //
            //LabelAnalogRead25
            //
            this.LabelAnalogRead25.Location = new System.Drawing.Point(72, 610);
            this.LabelAnalogRead25.Name = "LabelAnalogRead25";
            this.LabelAnalogRead25.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead25.TabIndex = 388;
            this.LabelAnalogRead25.Text = "LabelAnalogRead(25)";
            //
            //LabelAnalogRead26
            //
            this.LabelAnalogRead26.Location = new System.Drawing.Point(72, 634);
            this.LabelAnalogRead26.Name = "LabelAnalogRead26";
            this.LabelAnalogRead26.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead26.TabIndex = 388;
            this.LabelAnalogRead26.Text = "LabelAnalogRead(26)";
            //
            //LabelAnalogRead27
            //
            this.LabelAnalogRead27.Location = new System.Drawing.Point(72, 658);
            this.LabelAnalogRead27.Name = "LabelAnalogRead27";
            this.LabelAnalogRead27.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead27.TabIndex = 388;
            this.LabelAnalogRead27.Text = "LabelAnalogRead(27)";
            //
            //LabelAnalogRead28
            //
            this.LabelAnalogRead28.Location = new System.Drawing.Point(72, 682);
            this.LabelAnalogRead28.Name = "LabelAnalogRead28";
            this.LabelAnalogRead28.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead28.TabIndex = 388;
            this.LabelAnalogRead28.Text = "LabelAnalogRead(28)";
            //
            //LabelAnalogRead29
            //
            this.LabelAnalogRead29.Location = new System.Drawing.Point(72, 706);
            this.LabelAnalogRead29.Name = "LabelAnalogRead29";
            this.LabelAnalogRead29.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead29.TabIndex = 388;
            this.LabelAnalogRead29.Text = "LabelAnalogRead(29)";
            //
            //LabelAnalogRead30
            //
            this.LabelAnalogRead30.Location = new System.Drawing.Point(72, 730);
            this.LabelAnalogRead30.Name = "LabelAnalogRead30";
            this.LabelAnalogRead30.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead30.TabIndex = 383;
            this.LabelAnalogRead30.Text = "LabelAnalogRead(30)";
            //
            //LabelAnalogRead31
            //
            this.LabelAnalogRead31.Location = new System.Drawing.Point(72, 754);
            this.LabelAnalogRead31.Name = "LabelAnalogRead31";
            this.LabelAnalogRead31.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogRead31.TabIndex = 384;
            this.LabelAnalogRead31.Text = "LabelAnalogRead(31)";
            //
            //ProgressBarAnalog0
            //
            this.ProgressBarAnalog0.Location = new System.Drawing.Point(150, 12);
            this.ProgressBarAnalog0.Name = "ProgressBarAnalog0";
            this.ProgressBarAnalog0.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog0.TabIndex = 32;
            //
            //ProgressBarAnalog1
            //
            this.ProgressBarAnalog1.Location = new System.Drawing.Point(150, 36);
            this.ProgressBarAnalog1.Name = "ProgressBarAnalog1";
            this.ProgressBarAnalog1.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog1.TabIndex = 33;
            //
            //ProgressBarAnalog2
            //
            this.ProgressBarAnalog2.Location = new System.Drawing.Point(150, 60);
            this.ProgressBarAnalog2.Name = "ProgressBarAnalog2";
            this.ProgressBarAnalog2.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog2.TabIndex = 34;
            //
            //ProgressBarAnalog3
            //
            this.ProgressBarAnalog3.Location = new System.Drawing.Point(150, 84);
            this.ProgressBarAnalog3.Name = "ProgressBarAnalog3";
            this.ProgressBarAnalog3.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog3.TabIndex = 35;
            //
            //ProgressBarAnalog4
            //
            this.ProgressBarAnalog4.Location = new System.Drawing.Point(150, 108);
            this.ProgressBarAnalog4.Name = "ProgressBarAnalog4";
            this.ProgressBarAnalog4.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog4.TabIndex = 36;
            //
            //ProgressBarAnalog5
            //
            this.ProgressBarAnalog5.Location = new System.Drawing.Point(150, 132);
            this.ProgressBarAnalog5.Name = "ProgressBarAnalog5";
            this.ProgressBarAnalog5.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog5.TabIndex = 37;
            //
            //ProgressBarAnalog6
            //
            this.ProgressBarAnalog6.Location = new System.Drawing.Point(150, 156);
            this.ProgressBarAnalog6.Name = "ProgressBarAnalog6";
            this.ProgressBarAnalog6.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog6.TabIndex = 38;
            //
            //ProgressBarAnalog7
            //
            this.ProgressBarAnalog7.Location = new System.Drawing.Point(150, 180);
            this.ProgressBarAnalog7.Name = "ProgressBarAnalog7";
            this.ProgressBarAnalog7.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog7.TabIndex = 39;
            //
            //ProgressBarAnalog8
            //
            this.ProgressBarAnalog8.Location = new System.Drawing.Point(150, 204);
            this.ProgressBarAnalog8.Name = "ProgressBarAnalog8";
            this.ProgressBarAnalog8.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog8.TabIndex = 40;
            //
            //ProgressBarAnalog9
            //
            this.ProgressBarAnalog9.Location = new System.Drawing.Point(150, 228);
            this.ProgressBarAnalog9.Name = "ProgressBarAnalog9";
            this.ProgressBarAnalog9.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog9.TabIndex = 41;
            //
            //ProgressBarAnalog10
            //
            this.ProgressBarAnalog10.Location = new System.Drawing.Point(150, 252);
            this.ProgressBarAnalog10.Name = "ProgressBarAnalog10";
            this.ProgressBarAnalog10.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog10.TabIndex = 42;
            //
            //ProgressBarAnalog11
            //
            this.ProgressBarAnalog11.Location = new System.Drawing.Point(150, 276);
            this.ProgressBarAnalog11.Name = "ProgressBarAnalog11";
            this.ProgressBarAnalog11.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog11.TabIndex = 43;
            //
            //ProgressBarAnalog12
            //
            this.ProgressBarAnalog12.Location = new System.Drawing.Point(150, 300);
            this.ProgressBarAnalog12.Name = "ProgressBarAnalog12";
            this.ProgressBarAnalog12.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog12.TabIndex = 44;
            //
            //ProgressBarAnalog13
            //
            this.ProgressBarAnalog13.Location = new System.Drawing.Point(150, 324);
            this.ProgressBarAnalog13.Name = "ProgressBarAnalog13";
            this.ProgressBarAnalog13.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog13.TabIndex = 45;
            //
            //ProgressBarAnalog14
            //
            this.ProgressBarAnalog14.Location = new System.Drawing.Point(150, 348);
            this.ProgressBarAnalog14.Name = "ProgressBarAnalog14";
            this.ProgressBarAnalog14.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog14.TabIndex = 46;
            //
            //ProgressBarAnalog15
            //
            this.ProgressBarAnalog15.Location = new System.Drawing.Point(150, 372);
            this.ProgressBarAnalog15.Name = "ProgressBarAnalog15";
            this.ProgressBarAnalog15.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog15.TabIndex = 47;
            //
            //ProgressBarAnalog16
            //
            this.ProgressBarAnalog16.Location = new System.Drawing.Point(150, 396);
            this.ProgressBarAnalog16.Name = "ProgressBarAnalog16";
            this.ProgressBarAnalog16.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog16.TabIndex = 376;
            //
            //ProgressBarAnalog17
            //
            this.ProgressBarAnalog17.Location = new System.Drawing.Point(150, 420);
            this.ProgressBarAnalog17.Name = "ProgressBarAnalog17";
            this.ProgressBarAnalog17.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog17.TabIndex = 376;
            //
            //ProgressBarAnalog18
            //
            this.ProgressBarAnalog18.Location = new System.Drawing.Point(150, 444);
            this.ProgressBarAnalog18.Name = "ProgressBarAnalog18";
            this.ProgressBarAnalog18.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog18.TabIndex = 376;
            //
            //ProgressBarAnalog19
            //
            this.ProgressBarAnalog19.Location = new System.Drawing.Point(150, 468);
            this.ProgressBarAnalog19.Name = "ProgressBarAnalog19";
            this.ProgressBarAnalog19.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog19.TabIndex = 376;
            //
            //ProgressBarAnalog20
            //
            this.ProgressBarAnalog20.Location = new System.Drawing.Point(150, 492);
            this.ProgressBarAnalog20.Name = "ProgressBarAnalog20";
            this.ProgressBarAnalog20.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog20.TabIndex = 371;
            //
            //ProgressBarAnalog21
            //
            this.ProgressBarAnalog21.Location = new System.Drawing.Point(150, 516);
            this.ProgressBarAnalog21.Name = "ProgressBarAnalog21";
            this.ProgressBarAnalog21.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog21.TabIndex = 372;
            //
            //ProgressBarAnalog22
            //
            this.ProgressBarAnalog22.Location = new System.Drawing.Point(150, 540);
            this.ProgressBarAnalog22.Name = "ProgressBarAnalog22";
            this.ProgressBarAnalog22.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog22.TabIndex = 373;
            //
            //ProgressBarAnalog23
            //
            this.ProgressBarAnalog23.Location = new System.Drawing.Point(150, 564);
            this.ProgressBarAnalog23.Name = "ProgressBarAnalog23";
            this.ProgressBarAnalog23.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog23.TabIndex = 374;
            //
            //ProgressBarAnalog24
            //
            this.ProgressBarAnalog24.Location = new System.Drawing.Point(150, 588);
            this.ProgressBarAnalog24.Name = "ProgressBarAnalog24";
            this.ProgressBarAnalog24.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog24.TabIndex = 375;
            //
            //ProgressBarAnalog25
            //
            this.ProgressBarAnalog25.Location = new System.Drawing.Point(150, 612);
            this.ProgressBarAnalog25.Name = "ProgressBarAnalog25";
            this.ProgressBarAnalog25.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog25.TabIndex = 376;
            //
            //ProgressBarAnalog26
            //
            this.ProgressBarAnalog26.Location = new System.Drawing.Point(150, 636);
            this.ProgressBarAnalog26.Name = "ProgressBarAnalog26";
            this.ProgressBarAnalog26.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog26.TabIndex = 376;
            //
            //ProgressBarAnalog27
            //
            this.ProgressBarAnalog27.Location = new System.Drawing.Point(150, 660);
            this.ProgressBarAnalog27.Name = "ProgressBarAnalog27";
            this.ProgressBarAnalog27.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog27.TabIndex = 376;
            //
            //ProgressBarAnalog28
            //
            this.ProgressBarAnalog28.Location = new System.Drawing.Point(150, 684);
            this.ProgressBarAnalog28.Name = "ProgressBarAnalog28";
            this.ProgressBarAnalog28.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog28.TabIndex = 376;
            //
            //ProgressBarAnalog29
            //
            this.ProgressBarAnalog29.Location = new System.Drawing.Point(150, 708);
            this.ProgressBarAnalog29.Name = "ProgressBarAnalog29";
            this.ProgressBarAnalog29.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog29.TabIndex = 376;
            //
            //ProgressBarAnalog30
            //
            this.ProgressBarAnalog30.Location = new System.Drawing.Point(150, 732);
            this.ProgressBarAnalog30.Name = "ProgressBarAnalog30";
            this.ProgressBarAnalog30.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog30.TabIndex = 371;
            //
            //ProgressBarAnalog31
            //
            this.ProgressBarAnalog31.Location = new System.Drawing.Point(150, 756);
            this.ProgressBarAnalog31.Name = "ProgressBarAnalog31";
            this.ProgressBarAnalog31.Size = new System.Drawing.Size(161, 9);
            this.ProgressBarAnalog31.TabIndex = 372;
            //
            //LabelAnalogLow1
            //
            this.LabelAnalogLow1.Location = new System.Drawing.Point(156, 287);
            this.LabelAnalogLow1.Name = "LabelAnalogLow1";
            this.LabelAnalogLow1.Size = new System.Drawing.Size(43, 17);
            this.LabelAnalogLow1.TabIndex = 398;
            this.LabelAnalogLow1.Text = "0v";
            //
            //LabelAnalogHigh1
            //
            this.LabelAnalogHigh1.Location = new System.Drawing.Point(256, 287);
            this.LabelAnalogHigh1.Name = "LabelAnalogHigh1";
            this.LabelAnalogHigh1.Size = new System.Drawing.Size(61, 17);
            this.LabelAnalogHigh1.TabIndex = 1;
            this.LabelAnalogHigh1.Text = "27v";
            this.LabelAnalogHigh1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogHigh0
            //
            this.LabelAnalogHigh0.Location = new System.Drawing.Point(253, 21);
            this.LabelAnalogHigh0.Name = "LabelAnalogHigh0";
            this.LabelAnalogHigh0.Size = new System.Drawing.Size(64, 17);
            this.LabelAnalogHigh0.TabIndex = 4;
            this.LabelAnalogHigh0.Text = "27v";
            this.LabelAnalogHigh0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogLow0
            //
            this.LabelAnalogLow0.Location = new System.Drawing.Point(156, 21);
            this.LabelAnalogLow0.Name = "LabelAnalogLow0";
            this.LabelAnalogLow0.Size = new System.Drawing.Size(43, 17);
            this.LabelAnalogLow0.TabIndex = 3;
            this.LabelAnalogLow0.Text = "0v";
            //
            //LabelAnalogReading
            //
            this.LabelAnalogReading.Location = new System.Drawing.Point(78, 21);
            this.LabelAnalogReading.Name = "LabelAnalogReading";
            this.LabelAnalogReading.Size = new System.Drawing.Size(72, 17);
            this.LabelAnalogReading.TabIndex = 2;
            this.LabelAnalogReading.Text = "Reading:";
            //
            //GroupBoxBattVoltage
            //
            this.GroupBoxBattVoltage.Controls.Add(this.ButtonReadBatt);
            this.GroupBoxBattVoltage.Controls.Add(this.LabelBattRead);
            this.GroupBoxBattVoltage.Location = new System.Drawing.Point(371, 252);
            this.GroupBoxBattVoltage.Name = "GroupBoxBattVoltage";
            this.GroupBoxBattVoltage.Size = new System.Drawing.Size(333, 73);
            this.GroupBoxBattVoltage.TabIndex = 2;
            this.GroupBoxBattVoltage.TabStop = false;
            this.GroupBoxBattVoltage.Text = "Battery Voltage - OBDII pin 16";
            //
            //ButtonReadBatt
            //
            this.ButtonReadBatt.Location = new System.Drawing.Point(32, 32);
            this.ButtonReadBatt.Name = "ButtonReadBatt";
            this.ButtonReadBatt.Size = new System.Drawing.Size(49, 25);
            this.ButtonReadBatt.TabIndex = 0;
            this.ButtonReadBatt.Text = "Read";
            this.ButtonReadBatt.UseVisualStyleBackColor = true;
            //
            //LabelBattRead
            //
            this.LabelBattRead.Location = new System.Drawing.Point(89, 40);
            this.LabelBattRead.Name = "LabelBattRead";
            this.LabelBattRead.Size = new System.Drawing.Size(113, 17);
            this.LabelBattRead.TabIndex = 1;
            this.LabelBattRead.Text = "0v";
            //
            //GroupBoxProgVoltage
            //
            this.GroupBoxProgVoltage.Controls.Add(this.GroupBoxVolt);
            this.GroupBoxProgVoltage.Controls.Add(this.GroupBoxPin);
            this.GroupBoxProgVoltage.Location = new System.Drawing.Point(371, 3);
            this.GroupBoxProgVoltage.Name = "GroupBoxProgVoltage";
            this.GroupBoxProgVoltage.Size = new System.Drawing.Size(333, 242);
            this.GroupBoxProgVoltage.TabIndex = 1;
            this.GroupBoxProgVoltage.TabStop = false;
            this.GroupBoxProgVoltage.Text = "Programming Voltage";
            //
            //GroupBoxVolt
            //
            this.GroupBoxVolt.Controls.Add(this.LabelVoltWarning);
            this.GroupBoxVolt.Controls.Add(this.ButtonSetVoltage);
            this.GroupBoxVolt.Controls.Add(this.ButtonReadVolt);
            this.GroupBoxVolt.Controls.Add(this.TextBoxVoltSetting);
            this.GroupBoxVolt.Controls.Add(this.optSet);
            this.GroupBoxVolt.Controls.Add(this.optShortToGround);
            this.GroupBoxVolt.Controls.Add(this.optVoltOff);
            this.GroupBoxVolt.Controls.Add(this.LabelVoltRead);
            this.GroupBoxVolt.Controls.Add(this.LabelMV);
            this.GroupBoxVolt.Location = new System.Drawing.Point(142, 19);
            this.GroupBoxVolt.Name = "GroupBoxVolt";
            this.GroupBoxVolt.Size = new System.Drawing.Size(185, 217);
            this.GroupBoxVolt.TabIndex = 1;
            this.GroupBoxVolt.TabStop = false;
            this.GroupBoxVolt.Text = "Voltage:";
            //
            //LabelVoltWarning
            //
            this.LabelVoltWarning.AutoSize = true;
            this.LabelVoltWarning.Location = new System.Drawing.Point(7, 172);
            this.LabelVoltWarning.Name = "LabelVoltWarning";
            this.LabelVoltWarning.Size = new System.Drawing.Size(172, 39);
            this.LabelVoltWarning.TabIndex = 8;
            this.LabelVoltWarning.Text = "Caution: Do not apply programming" + "\r" + "\n" + "voltage to a pin that is in use by a" + "\r" + "\n" + "J2534 pr" +
        "otocol.";
            //
            //ButtonSetVoltage
            //
            this.ButtonSetVoltage.Location = new System.Drawing.Point(17, 100);
            this.ButtonSetVoltage.Name = "ButtonSetVoltage";
            this.ButtonSetVoltage.Size = new System.Drawing.Size(49, 25);
            this.ButtonSetVoltage.TabIndex = 5;
            this.ButtonSetVoltage.Text = "Set";
            this.ButtonSetVoltage.UseVisualStyleBackColor = true;
            //
            //ButtonReadVolt
            //
            this.ButtonReadVolt.Location = new System.Drawing.Point(17, 131);
            this.ButtonReadVolt.Name = "ButtonReadVolt";
            this.ButtonReadVolt.Size = new System.Drawing.Size(49, 25);
            this.ButtonReadVolt.TabIndex = 0;
            this.ButtonReadVolt.Text = "Read";
            this.ButtonReadVolt.UseVisualStyleBackColor = true;
            //
            //TextBoxVoltSetting
            //
            this.TextBoxVoltSetting.AcceptsReturn = true;
            this.TextBoxVoltSetting.ContextMenuStrip = this.ContextMenuStripTextBox;
            this.TextBoxVoltSetting.Location = new System.Drawing.Point(56, 69);
            this.TextBoxVoltSetting.MaxLength = 0;
            this.TextBoxVoltSetting.Name = "TextBoxVoltSetting";
            this.TextBoxVoltSetting.Size = new System.Drawing.Size(65, 20);
            this.TextBoxVoltSetting.TabIndex = 4;
            //
            //optSet
            //
            this.optSet.AutoSize = true;
            this.optSet.Location = new System.Drawing.Point(17, 70);
            this.optSet.Name = "optSet";
            this.optSet.Size = new System.Drawing.Size(44, 17);
            this.optSet.TabIndex = 3;
            this.optSet.TabStop = true;
            this.optSet.Text = "Set:";
            this.optSet.UseVisualStyleBackColor = true;
            //
            //optShortToGround
            //
            this.optShortToGround.AutoSize = true;
            this.optShortToGround.Location = new System.Drawing.Point(17, 46);
            this.optShortToGround.Name = "optShortToGround";
            this.optShortToGround.Size = new System.Drawing.Size(104, 17);
            this.optShortToGround.TabIndex = 2;
            this.optShortToGround.TabStop = true;
            this.optShortToGround.Text = "Short To Ground";
            this.optShortToGround.UseVisualStyleBackColor = true;
            //
            //optVoltOff
            //
            this.optVoltOff.AutoSize = true;
            this.optVoltOff.Location = new System.Drawing.Point(17, 24);
            this.optVoltOff.Name = "optVoltOff";
            this.optVoltOff.Size = new System.Drawing.Size(78, 17);
            this.optVoltOff.TabIndex = 1;
            this.optVoltOff.TabStop = true;
            this.optVoltOff.Text = "Voltage Off";
            this.optVoltOff.UseVisualStyleBackColor = true;
            //
            //LabelVoltRead
            //
            this.LabelVoltRead.Location = new System.Drawing.Point(72, 137);
            this.LabelVoltRead.Name = "LabelVoltRead";
            this.LabelVoltRead.Size = new System.Drawing.Size(65, 17);
            this.LabelVoltRead.TabIndex = 7;
            this.LabelVoltRead.Text = "0v";
            //
            //LabelMV
            //
            this.LabelMV.Location = new System.Drawing.Point(127, 72);
            this.LabelMV.Name = "LabelMV";
            this.LabelMV.Size = new System.Drawing.Size(28, 17);
            this.LabelMV.TabIndex = 6;
            this.LabelMV.Text = "mV";
            //
            //GroupBoxPin
            //
            this.GroupBoxPin.Controls.Add(this.RadioButtonPin15);
            this.GroupBoxPin.Controls.Add(this.RadioButtonPin14);
            this.GroupBoxPin.Controls.Add(this.RadioButtonPin13);
            this.GroupBoxPin.Controls.Add(this.RadioButtonPin12);
            this.GroupBoxPin.Controls.Add(this.RadioButtonPin11);
            this.GroupBoxPin.Controls.Add(this.RadioButtonPin9);
            this.GroupBoxPin.Controls.Add(this.RadioButtonPin6);
            this.GroupBoxPin.Controls.Add(this.RadioButtonPin0);
            this.GroupBoxPin.Controls.Add(this.LabelPin15);
            this.GroupBoxPin.Controls.Add(this.LabelPin14);
            this.GroupBoxPin.Controls.Add(this.LabelPin13);
            this.GroupBoxPin.Controls.Add(this.LabelPin12);
            this.GroupBoxPin.Controls.Add(this.LabelPin11);
            this.GroupBoxPin.Controls.Add(this.LabelPin9);
            this.GroupBoxPin.Controls.Add(this.LabelPin6);
            this.GroupBoxPin.Controls.Add(this.LabelPin0);
            this.GroupBoxPin.Location = new System.Drawing.Point(9, 19);
            this.GroupBoxPin.Name = "GroupBoxPin";
            this.GroupBoxPin.Size = new System.Drawing.Size(127, 217);
            this.GroupBoxPin.TabIndex = 0;
            this.GroupBoxPin.TabStop = false;
            this.GroupBoxPin.Text = "Pin:";
            //
            //RadioButtonPin15
            //
            this.RadioButtonPin15.Location = new System.Drawing.Point(56, 185);
            this.RadioButtonPin15.Name = "RadioButtonPin15";
            this.RadioButtonPin15.Size = new System.Drawing.Size(57, 17);
            this.RadioButtonPin15.TabIndex = 7;
            this.RadioButtonPin15.TabStop = true;
            this.RadioButtonPin15.Text = "Pin 15";
            this.RadioButtonPin15.UseVisualStyleBackColor = true;
            //
            //RadioButtonPin14
            //
            this.RadioButtonPin14.Location = new System.Drawing.Point(56, 162);
            this.RadioButtonPin14.Name = "RadioButtonPin14";
            this.RadioButtonPin14.Size = new System.Drawing.Size(57, 17);
            this.RadioButtonPin14.TabIndex = 6;
            this.RadioButtonPin14.TabStop = true;
            this.RadioButtonPin14.Text = "Pin 14";
            this.RadioButtonPin14.UseVisualStyleBackColor = true;
            //
            //RadioButtonPin13
            //
            this.RadioButtonPin13.Location = new System.Drawing.Point(56, 139);
            this.RadioButtonPin13.Name = "RadioButtonPin13";
            this.RadioButtonPin13.Size = new System.Drawing.Size(57, 17);
            this.RadioButtonPin13.TabIndex = 5;
            this.RadioButtonPin13.TabStop = true;
            this.RadioButtonPin13.Text = "Pin 13";
            this.RadioButtonPin13.UseVisualStyleBackColor = true;
            //
            //RadioButtonPin12
            //
            this.RadioButtonPin12.Location = new System.Drawing.Point(56, 116);
            this.RadioButtonPin12.Name = "RadioButtonPin12";
            this.RadioButtonPin12.Size = new System.Drawing.Size(57, 17);
            this.RadioButtonPin12.TabIndex = 4;
            this.RadioButtonPin12.TabStop = true;
            this.RadioButtonPin12.Text = "Pin 12";
            this.RadioButtonPin12.UseVisualStyleBackColor = true;
            //
            //RadioButtonPin11
            //
            this.RadioButtonPin11.Location = new System.Drawing.Point(56, 93);
            this.RadioButtonPin11.Name = "RadioButtonPin11";
            this.RadioButtonPin11.Size = new System.Drawing.Size(57, 17);
            this.RadioButtonPin11.TabIndex = 3;
            this.RadioButtonPin11.TabStop = true;
            this.RadioButtonPin11.Text = "Pin 11";
            this.RadioButtonPin11.UseVisualStyleBackColor = true;
            //
            //RadioButtonPin9
            //
            this.RadioButtonPin9.Location = new System.Drawing.Point(56, 69);
            this.RadioButtonPin9.Name = "RadioButtonPin9";
            this.RadioButtonPin9.Size = new System.Drawing.Size(57, 17);
            this.RadioButtonPin9.TabIndex = 2;
            this.RadioButtonPin9.TabStop = true;
            this.RadioButtonPin9.Text = "Pin 9";
            this.RadioButtonPin9.UseVisualStyleBackColor = true;
            //
            //RadioButtonPin6
            //
            this.RadioButtonPin6.Location = new System.Drawing.Point(56, 47);
            this.RadioButtonPin6.Name = "RadioButtonPin6";
            this.RadioButtonPin6.Size = new System.Drawing.Size(57, 17);
            this.RadioButtonPin6.TabIndex = 1;
            this.RadioButtonPin6.TabStop = true;
            this.RadioButtonPin6.Text = "Pin 6";
            this.RadioButtonPin6.UseVisualStyleBackColor = true;
            //
            //RadioButtonPin0
            //
            this.RadioButtonPin0.Location = new System.Drawing.Point(56, 24);
            this.RadioButtonPin0.Name = "RadioButtonPin0";
            this.RadioButtonPin0.Size = new System.Drawing.Size(57, 17);
            this.RadioButtonPin0.TabIndex = 0;
            this.RadioButtonPin0.TabStop = true;
            this.RadioButtonPin0.Text = "Aux";
            this.RadioButtonPin0.UseVisualStyleBackColor = true;
            //
            //LabelPin15
            //
            this.LabelPin15.Location = new System.Drawing.Point(21, 187);
            this.LabelPin15.Name = "LabelPin15";
            this.LabelPin15.Size = new System.Drawing.Size(30, 17);
            this.LabelPin15.TabIndex = 15;
            this.LabelPin15.Text = "LabelPin(15)";
            this.LabelPin15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelPin14
            //
            this.LabelPin14.Location = new System.Drawing.Point(21, 164);
            this.LabelPin14.Name = "LabelPin14";
            this.LabelPin14.Size = new System.Drawing.Size(30, 17);
            this.LabelPin14.TabIndex = 14;
            this.LabelPin14.Text = "LabelPin(14)";
            this.LabelPin14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelPin13
            //
            this.LabelPin13.Location = new System.Drawing.Point(21, 141);
            this.LabelPin13.Name = "LabelPin13";
            this.LabelPin13.Size = new System.Drawing.Size(30, 17);
            this.LabelPin13.TabIndex = 13;
            this.LabelPin13.Text = "LabelPin(13)";
            this.LabelPin13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelPin12
            //
            this.LabelPin12.Location = new System.Drawing.Point(21, 118);
            this.LabelPin12.Name = "LabelPin12";
            this.LabelPin12.Size = new System.Drawing.Size(30, 17);
            this.LabelPin12.TabIndex = 12;
            this.LabelPin12.Text = "LabelPin(12)";
            this.LabelPin12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelPin11
            //
            this.LabelPin11.Location = new System.Drawing.Point(21, 95);
            this.LabelPin11.Name = "LabelPin11";
            this.LabelPin11.Size = new System.Drawing.Size(30, 17);
            this.LabelPin11.TabIndex = 11;
            this.LabelPin11.Text = "LabelPin(11)";
            this.LabelPin11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelPin9
            //
            this.LabelPin9.Location = new System.Drawing.Point(21, 72);
            this.LabelPin9.Name = "LabelPin9";
            this.LabelPin9.Size = new System.Drawing.Size(30, 17);
            this.LabelPin9.TabIndex = 10;
            this.LabelPin9.Text = "LabelPin(9)";
            this.LabelPin9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelPin6
            //
            this.LabelPin6.Location = new System.Drawing.Point(21, 49);
            this.LabelPin6.Name = "LabelPin6";
            this.LabelPin6.Size = new System.Drawing.Size(30, 17);
            this.LabelPin6.TabIndex = 9;
            this.LabelPin6.Text = "LabelPin(6)";
            this.LabelPin6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelPin0
            //
            this.LabelPin0.Location = new System.Drawing.Point(21, 26);
            this.LabelPin0.Name = "LabelPin0";
            this.LabelPin0.Size = new System.Drawing.Size(30, 17);
            this.LabelPin0.TabIndex = 8;
            this.LabelPin0.Text = "LabelPin(0)";
            this.LabelPin0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            //LabelAnalogChannel
            //
            this.LabelAnalogChannel.Location = new System.Drawing.Point(524, 336);
            this.LabelAnalogChannel.Name = "LabelAnalogChannel";
            this.LabelAnalogChannel.Size = new System.Drawing.Size(49, 17);
            this.LabelAnalogChannel.TabIndex = 4;
            this.LabelAnalogChannel.Text = "Channel:";
            //
            //LabelDeviceCombo7
            //
            this.LabelDeviceCombo7.Location = new System.Drawing.Point(395, 336);
            this.LabelDeviceCombo7.Name = "LabelDeviceCombo7";
            this.LabelDeviceCombo7.Size = new System.Drawing.Size(89, 17);
            this.LabelDeviceCombo7.TabIndex = 3;
            this.LabelDeviceCombo7.Text = "Device:";
            //
            //TabPageResults
            //
            this.TabPageResults.Controls.Add(this.ListViewResults);
            this.TabPageResults.Location = new System.Drawing.Point(4, 22);
            this.TabPageResults.Name = "TabPageResults";
            this.TabPageResults.Size = new System.Drawing.Size(714, 395);
            this.TabPageResults.TabIndex = 8;
            this.TabPageResults.Text = "Results";
            //
            //ListViewResults
            //
            this.ListViewResults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {this.ListViewResultsColumnHeader1, this.ListViewResultsColumnHeader2});
            this.ListViewResults.ContextMenuStrip = this.ContextMenuStripResults;
            this.ListViewResults.FullRowSelect = true;
            this.ListViewResults.Location = new System.Drawing.Point(8, 15);
            this.ListViewResults.Name = "ListViewResults";
            this.ListViewResults.Size = new System.Drawing.Size(696, 361);
            this.ListViewResults.TabIndex = 0;
            this.ListViewResults.UseCompatibleStateImageBehavior = false;
            this.ListViewResults.View = System.Windows.Forms.View.Details;
            //
            //ListViewResultsColumnHeader1
            //
            this.ListViewResultsColumnHeader1.Text = "Device";
            this.ListViewResultsColumnHeader1.Width = 134;
            //
            //ListViewResultsColumnHeader2
            //
            this.ListViewResultsColumnHeader2.Text = "Result";
            this.ListViewResultsColumnHeader2.Width = 527;
            //
            //ContextMenuStripResults
            //
            this.ContextMenuStripResults.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuResultsCopyLine, this.MenuResultsDelete, this.MenuResultsSelectAll, this.MenuResultsSpace1, this.MenuResultsClear});
            this.ContextMenuStripResults.Name = "ContextMenuStripMsgIn";
            this.ContextMenuStripResults.Size = new System.Drawing.Size(128, 98);
            //
            //MenuResultsCopyLine
            //
            this.MenuResultsCopyLine.Enabled = false;
            this.MenuResultsCopyLine.Name = "MenuResultsCopyLine";
            this.MenuResultsCopyLine.Size = new System.Drawing.Size(127, 22);
            this.MenuResultsCopyLine.Text = "Copy Line";
            //
            //MenuResultsDelete
            //
            this.MenuResultsDelete.Name = "MenuResultsDelete";
            this.MenuResultsDelete.Size = new System.Drawing.Size(127, 22);
            this.MenuResultsDelete.Text = "Delete";
            //
            //MenuResultsSelectAll
            //
            this.MenuResultsSelectAll.Enabled = false;
            this.MenuResultsSelectAll.Name = "MenuResultsSelectAll";
            this.MenuResultsSelectAll.Size = new System.Drawing.Size(127, 22);
            this.MenuResultsSelectAll.Text = "Select All";
            //
            //MenuResultsSpace1
            //
            this.MenuResultsSpace1.Name = "MenuResultsSpace1";
            this.MenuResultsSpace1.Size = new System.Drawing.Size(124, 6);
            //
            //MenuResultsClear
            //
            this.MenuResultsClear.Enabled = false;
            this.MenuResultsClear.Name = "MenuResultsClear";
            this.MenuResultsClear.Size = new System.Drawing.Size(127, 22);
            this.MenuResultsClear.Text = "Clear";
            //
            //ContextMenuStripMessageIn
            //
            this.ContextMenuStripMessageIn.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuMsgInCopyLine, this.MenuMsgInCopyData, this.MenuMsgInMakeFilter, this.MenuMsgInRxStatus, this.MenuMsgInSelectAll, this.MenuMsgInSpace1, this.MenuMsgInSaveSelected, this.MenuMsgInSaveAll, this.MenuMsgInClear});
            this.ContextMenuStripMessageIn.Name = "ContextMenuStripMsgIn";
            this.ContextMenuStripMessageIn.Size = new System.Drawing.Size(146, 186);
            //
            //ContextMenuStripMessageOut
            //
            this.ContextMenuStripMessageOut.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {this.MenuMsgOutDelete, this.MenuMsgOutEditMessage, this.MenuMsgOutAddMessage, this.MenuMsgOutCopyToScratchPad, this.MenuMsgOutMakePeriodicMessage, this.MenuMsgOutSpace1, this.MenuMsgOutSave, this.MenuMsgOutLoad, this.MenuMsgOutClear});
            this.ContextMenuStripMessageOut.Name = "ContextMenuStripMsgOut";
            this.ContextMenuStripMessageOut.Size = new System.Drawing.Size(199, 186);
            //
            //FormJ2534
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(720, 466);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.TabControl1);
            this.Controls.Add(this.MenuStripMain);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.Location = new System.Drawing.Point(88, 149);
            this.MaximizeBox = false;
            this.Name = "FormJ2534";
            this.Text = "FormJ2534";
            this.MenuStripMain.ResumeLayout(false);
            this.MenuStripMain.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.TabControl1.ResumeLayout(false);
            this.TabPageConnect.ResumeLayout(false);
            this.GroupBoxDevices.ResumeLayout(false);
            this.GroupBoxDevices.PerformLayout();
            this.GroupBoxAPIs.ResumeLayout(false);
            this.GroupBoxAPIs.PerformLayout();
            this.ContextMenuStripTextBox.ResumeLayout(false);
            this.GroupBoxConnect.ResumeLayout(false);
            this.GroupBoxConnect.PerformLayout();
            this.ContextMenuStripFlags.ResumeLayout(false);
            this.GroupBoxJ2534Info.ResumeLayout(false);
            this.GroupBoxJ2534Info.PerformLayout();
            this.TabPageMessages.ResumeLayout(false);
            this.TabPageMessages.PerformLayout();
            this.ContextMenuStripScratchPad.ResumeLayout(false);
            this.TabPagePeriodicMessages.ResumeLayout(false);
            this.TabPagePeriodicMessages.PerformLayout();
            this.TabPageFilters.ResumeLayout(false);
            this.TabPageFilters.PerformLayout();
            this.TabPageConfig.ResumeLayout(false);
            this.TabPageConfig.PerformLayout();
            this.GroupBoxAnalogConfig.ResumeLayout(false);
            this.GroupBoxAnalogConfig.PerformLayout();
            this.PanelAudioChannel.ResumeLayout(false);
            this.PanelAudioChannel.PerformLayout();
            this.TabPageInit.ResumeLayout(false);
            this.GroupBoxFastInit.ResumeLayout(false);
            this.GroupBoxFastInit.PerformLayout();
            this.GroupBox5BInit.ResumeLayout(false);
            this.GroupBox5BInit.PerformLayout();
            this.TabPageFunctionalMessages.ResumeLayout(false);
            this.TabPageFunctionalMessages.PerformLayout();
            this.TabPageAnalog.ResumeLayout(false);
            this.GroupBoxAnalog.ResumeLayout(false);
            this.PanelAnalog.ResumeLayout(false);
            this.GroupBoxBattVoltage.ResumeLayout(false);
            this.GroupBoxProgVoltage.ResumeLayout(false);
            this.GroupBoxVolt.ResumeLayout(false);
            this.GroupBoxVolt.PerformLayout();
            this.GroupBoxPin.ResumeLayout(false);
            this.TabPageResults.ResumeLayout(false);
            this.ContextMenuStripResults.ResumeLayout(false);
            this.ContextMenuStripMessageIn.ResumeLayout(false);
            this.ContextMenuStripMessageOut.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

            ButtonOpenBox.Click += new System.EventHandler(ButtonOpenBox_Click);
            ButtonCloseBox.Click += new System.EventHandler(ButtonCloseBox_Click);
            ButtonConnect.Click += new System.EventHandler(ButtonConnect_Click);
            ButtonDisconnect.Click += new System.EventHandler(ButtonDisconnect_Click);
            ButtonClearRx.Click += new System.EventHandler(ButtonClearRx_Click);
            ButtonClearTx.Click += new System.EventHandler(ButtonClearTx_Click);
            ButtonExecute5BInit.Click += new System.EventHandler(ButtonExecute5BInit_Click);
            ButtonExecuteFastInit.Click += new System.EventHandler(ButtonExecuteFastInit_Click);
            ButtonApplyFilter.Click += new System.EventHandler(ButtonApplyFilter_Click);
            ButtonCancelFilter.Click += new System.EventHandler(ButtonCancelFilter_Click);
            ButtonClearAllFilter.Click += new System.EventHandler(ButtonClearAllFilter_Click);
            ButtonCreatePassFilter.Click += new System.EventHandler(ButtonCreatePassFilter_Click);
            ButtonApplyFunctionalMessages.Click += new System.EventHandler(ButtonApplyFunctionalMessages_Click);
            ButtonCancelFunctionalMessages.Click += new System.EventHandler(ButtonCancelFunctionalMessages_Click);
            ButtonClearAllFunctionalMessages.Click += new System.EventHandler(ButtonClearAllFunctionalMessagesClick);
            ButtonLoadDLL.Click += new System.EventHandler(ButtonLoadDLL_Click);
            ButtonApplyPeriodicMessages.Click += new System.EventHandler(ButtonApplyPeriodicMessages_Click);
            ButtonCancelPeriodicMessages.Click += new System.EventHandler(ButtonCancelPeriodicMessages_Click);
            ButtonClearAllPeriodicMessages.Click += new System.EventHandler(ButtonClearAllPeriodicMessages_Click);
            ButtonReceive.Click += new System.EventHandler(ButtonReceive_Click);
            ButtonClearList.Click += new System.EventHandler(ButtonClearList_Click);
            ButtonSend.Click += new System.EventHandler(ButtonSend_Click);
            ButtonClaimJ1939Address.Click += new System.EventHandler(ButtonClaimJ1939Address_Click);
            ButtonSetConfig.Click += new System.EventHandler(ButtonSetConfig_Click);
            ButtonClearConfig.Click += new System.EventHandler(ButtonClearConfig_Click);
            ButtonSetVoltage.Click += new System.EventHandler(ButtonSetVoltage_Click);
            ButtonReadVolt.Click += new System.EventHandler(ButtonReadVolt_Click);
            ButtonReadBatt.Click += new System.EventHandler(ButtonReadBatt_Click);
            ComboBoxAnalogChannel.SelectionChangeCommitted += new System.EventHandler(ComboBoxAnalogChannel_SelectionChangeCommitted);
            ComboBoxAvailableChannel.SelectionChangeCommitted += new System.EventHandler(ComboBoxAvailableChannel_SelectionChangeCommitted);
            ComboBoxAvailableDevice.SelectionChangeCommitted += new System.EventHandler(ComboBoxAvailableDevice_SelectionChangeCommitted);
            ComboBoxMessageChannel.SelectedIndexChanged += new System.EventHandler(ComboBoxMessageChannel_SelectedIndexChanged);
            ComboBoxDevice.SelectedIndexChanged += new System.EventHandler(ComboBoxDevice_SelectedIndexChanged);
            ComboBoxDevice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(ComboBoxDevice_KeyPress);
            ComboBoxDevice.TextChanged += new System.EventHandler(ComboBoxDevice_TextChanged);
            ComboBoxAPI.SelectedIndexChanged += new System.EventHandler(ComboBoxAPI_SelectedIndexChanged);
            ComboBoxBaudRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(ComboBoxBaudRate_KeyPress);
            ComboBoxConnectChannel.SelectionChangeCommitted += new System.EventHandler(ComboBoxConnectChannel_SelectionChangeCommitted);
            ComboBoxConnector.SelectedIndexChanged += new System.EventHandler(ComboBoxConnector_SelectedIndexChanged);
            ComboBoxAnalogRateMode.SelectionChangeCommitted += new System.EventHandler(ComboBoxAnalogRateMode_SelectionChangeCommitted);
            MenuEdit.DropDownOpening += new System.EventHandler(MenuEdit_DropDownOpening);
            MenuEditClear.Click += new System.EventHandler(MenuEditClear_Click);
            MenuEditCopy.Click += new System.EventHandler(MenuEditCopy_Click);
            MenuEditCut.Click += new System.EventHandler(MenuEditCut_Click);
            MenuEditDelete.Click += new System.EventHandler(MenuEditDelete_Click);
            MenuEditFlagsClear.Click += new System.EventHandler(MenuEditFlagsClear_Click);
            MenuEditFlagsSetDefault.Click += new System.EventHandler(MenuEditFlagsSetDefault_Click);
            MenuEditFlagsEdit.Click += new System.EventHandler(MenuEditFlagsEdit_Click);
            MenuEditMsgInCopyData.Click += new System.EventHandler(MenuEditMsgInCopyData_Click);
            MenuEditCopyLine.Click += new System.EventHandler(MenuEditCopyLine_Click);
            MenuEditMsgInMakeFilter.Click += new System.EventHandler(MenuEditMsgInMakeFilter_Click);
            MenuEditMsgOutAddMessage.Click += new System.EventHandler(MenuEditMsgOutAddMessage_Click);
            MenuEditMsgOutEditMessage.Click += new System.EventHandler(MenuEditMsgOutEditMessage_Click);
            MenuEditMsgOutCopyToScratchPad.Click += new System.EventHandler(MenuEditMsgOutCopyToScratchPad_Click);
            MenuEditMsgOutMakePeriodicMessage.Click += new System.EventHandler(MenuEditMsgOutMakePeriodicMessage_Click);
            MenuEditScratchAddToOutgoingMessageSet.Click += new System.EventHandler(MenuEditScratchAddToOutgoingMessageSet_Click);
            MenuEditPaste.Click += new System.EventHandler(MenuEditPaste_Click);
            MenuEditRxStatus.Click += new System.EventHandler(MenuEditRxStatus_Click);
            MenuEditSelectAll.Click += new System.EventHandler(MenuEditSelectAll_Click);
            MenuEditUndo.Click += new System.EventHandler(MenuEditUndo_Click);
            MenuFile.DropDownOpening += new System.EventHandler(MenuFile_DropDownOpening);
            MenuFileMsgInSaveAll.Click += new System.EventHandler(MenuFileMsgInSaveAll_Click);
            MenuFileMsgInSaveSelected.Click += new System.EventHandler(MenuFileMsgInSaveSelected_Click);
            MenuFileMsgOutLoad.Click += new System.EventHandler(MenuFileMsgOutLoad_Click);
            MenuFileMsgOutSave.Click += new System.EventHandler(MenuFileMsgOutSave_Click);
            MenuFileFilterLoad.Click += new System.EventHandler(MenuFileFilterLoad_Click);
            MenuFileFilterSave.Click += new System.EventHandler(MenuFileFilterSave_Click);
            MenuFilePeriodicMessageLoad.Click += new System.EventHandler(MenuFilePeriodicMessageLoad_Click);
            MenuFilePeriodicMessageSave.Click += new System.EventHandler(MenuFilePeriodicMessageSave_Click);
            MenuFileExit.Click += new System.EventHandler(MenuFileExit_Click);
            ContextMenuStripFlags.Opening += new System.ComponentModel.CancelEventHandler(ContextMenuStripFlags_Opening);
            MenuFlagsClear.Click += new System.EventHandler(MenuFlagsClear_Click);
            MenuFlagsSetDefault.Click += new System.EventHandler(MenuFlagsSetDefault_Click);
            MenuFlagsEdit.Click += new System.EventHandler(MenuFlagsEdit_Click);
            MenuFlagsCopy.Click += new System.EventHandler(MenuFlagsCopy_Click);
            MenuFlagsCut.Click += new System.EventHandler(MenuFlagsCut_Click);
            MenuFlagsDelete.Click += new System.EventHandler(MenuFlagsDelete_Click);
            MenuFlagsPaste.Click += new System.EventHandler(MenuFlagsPaste_Click);
            MenuFlagsSelectAll.Click += new System.EventHandler(MenuFlagsSelectAll_Click);
            MenuFlagsUndo.Click += new System.EventHandler(MenuFlagsUndo_Click);
            MenuHelpAbout.Click += new System.EventHandler(MenuHelpAbout_Click);
            ContextMenuStripMessageIn.Opening += new System.ComponentModel.CancelEventHandler(ContextMenuStripMsgIn_Opening);
            MenuMsgInClear.Click += new System.EventHandler(MenuMsgInClear_Click);
            MenuMsgInCopyData.Click += new System.EventHandler(MenuMsgInCopyData_Click);
            MenuMsgInCopyLine.Click += new System.EventHandler(MenuMsgInCopyLine_Click);
            MenuMsgInMakeFilter.Click += new System.EventHandler(MenuMsgInMakeFilter_Click);
            MenuMsgInRxStatus.Click += new System.EventHandler(MenuMsgInRxStatus_Click);
            MenuMsgInSaveAll.Click += new System.EventHandler(MenuMsgInSaveAll_Click);
            MenuMsgInSaveSelected.Click += new System.EventHandler(MenuMsgInSaveSelected_Click);
            MenuMsgInSelectAll.Click += new System.EventHandler(MenuMsgInSelectAll_Click);
            ContextMenuStripMessageOut.Opening += new System.ComponentModel.CancelEventHandler(ContextMenuStripMsgOut_Opening);
            MenuMsgOutClear.Click += new System.EventHandler(MenuMsgOutClear_Click);
            MenuMsgOutDelete.Click += new System.EventHandler(MenuMsgOutDelete_Click);
            MenuMsgOutAddMessage.Click += new System.EventHandler(MenuMsgOutAddMessage_Click);
            MenuMsgOutLoad.Click += new System.EventHandler(MenuMsgOutLoad_Click);
            MenuMsgOutEditMessage.Click += new System.EventHandler(MenuMsgOutEditMessage_Click);
            MenuMsgOutMakePeriodicMessage.Click += new System.EventHandler(MenuMsgOutMakePeriodicMessage_Click);
            MenuMsgOutSave.Click += new System.EventHandler(MenuMsgOutSave_Click);
            MenuMsgOutCopyToScratchPad.Click += new System.EventHandler(MenuMsgOutCopyToScratchPad_Click);
            ContextMenuStripScratchPad.Opening += new System.ComponentModel.CancelEventHandler(ContextMenuStripScratchPad_Opening);
            MenuScratchPadCopy.Click += new System.EventHandler(MenuScratchPadCopy_Click);
            MenuScratchPadCut.Click += new System.EventHandler(MenuScratchPadCut_Click);
            MenuScratchPadDelete.Click += new System.EventHandler(MenuScratchPadDelete_Click);
            MenuScratchPadPaste.Click += new System.EventHandler(MenuScratchPadPaste_Click);
            MenuScratchPadSelectAll.Click += new System.EventHandler(MenuScratchPadSelectAll_Click);
            MenuScratchPadUndo.Click += new System.EventHandler(MenuScratchPadUndo_Click);
            MenuScratchPadAddToOutgoingMessageSet.Click += new System.EventHandler(MenuScratchPadAddToOutgoingMessageSet_Click);
            ContextMenuStripTextBox.Opening += new System.ComponentModel.CancelEventHandler(ContextMenuStripTextBox_Opening);
            MenuTextBoxCopy.Click += new System.EventHandler(MenuTextBoxCopy_Click);
            MenuTextBoxCut.Click += new System.EventHandler(MenuTextBoxCut_Click);
            MenuTextBoxDelete.Click += new System.EventHandler(MenuTextBoxDelete_Click);
            MenuTextBoxPaste.Click += new System.EventHandler(MenuTextBoxPaste_Click);
            MenuTextBoxSelectAll.Click += new System.EventHandler(MenuTextBoxSelectAll_Click);
            MenuTextBoxUndo.Click += new System.EventHandler(MenuTextBoxUndo_Click);
            ContextMenuStripResults.Opening += new System.ComponentModel.CancelEventHandler(ContextMenuStripResults_Opening);
            MenuResultsCopyLine.Click += new System.EventHandler(MenuResultsCopyLine_Click);
            MenuResultsDelete.Click += new System.EventHandler(MenuResultsDelete_Click);
            MenuResultsSelectAll.Click += new System.EventHandler(MenuResultsSelectAll_Click);
            MenuResultsClear.Click += new System.EventHandler(MenuResultsClear_Click);
            TabControl1.SelectedIndexChanged += new System.EventHandler(TabControl1_SelectedIndexChanged);
            TextBox5BInitECU.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBox5BInit_KeyPress);
            TextBoxFIFlags.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxFIFlags_KeyPress);
            TextBoxFIMessage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxFIMessage_KeyPress);
            TextBoxConnectFlags.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxConnectFlags_KeyPress);
            TextBoxOutFlags.Enter += new System.EventHandler(TextBoxOutFlags_Enter);
            TextBoxOutFlags.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxOutFlags_KeyPress);
            TextBoxOutFlags.Leave += new System.EventHandler(TextBoxOutFlags_Leave);
            TextBoxMessageOut.Enter += new System.EventHandler(TextBoxMessageOut_Enter);
            TextBoxMessageOut.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxMessageOut_KeyPress);
            TextBoxMessageOut.Leave += new System.EventHandler(TextBoxMessageOut_Leave);
            TextBoxAnalogRate.TextChanged += new System.EventHandler(TextBoxAnalogRate_TextChanged);
            TextBoxReadRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxReadRate_KeyPress);
            TextBoxTimeOut.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxTimeOut_KeyPress);
            TextBoxVoltSetting.KeyPress += new System.Windows.Forms.KeyPressEventHandler(TextBoxVoltSetting_KeyPress);
            TextBoxVoltSetting.Leave += new System.EventHandler(TextBoxVoltSetting_Leave);
        }

        private void InitializeComboAvailableChannelLocator()
        {
            ComboAvailableChannelLocator = new Label[8];
            this.ComboAvailableChannelLocator[0] = ComboAvailableChannelLocator0;
            this.ComboAvailableChannelLocator[1] = ComboAvailableChannelLocator1;
            this.ComboAvailableChannelLocator[2] = ComboAvailableChannelLocator2;
            this.ComboAvailableChannelLocator[3] = ComboAvailableChannelLocator3;
            this.ComboAvailableChannelLocator[4] = ComboAvailableChannelLocator4;
            this.ComboAvailableChannelLocator[5] = ComboAvailableChannelLocator5;
            this.ComboAvailableChannelLocator[6] = ComboAvailableChannelLocator6;
            this.ComboAvailableChannelLocator[7] = ComboAvailableChannelLocator7;
        }

        private void InitializeComboAvailableBoxLocator()
        {
            ComboAvailableBoxLocator = new Label[8];
            this.ComboAvailableBoxLocator[0] = ComboAvailableBoxLocator0;
            this.ComboAvailableBoxLocator[1] = ComboAvailableBoxLocator1;
            this.ComboAvailableBoxLocator[2] = ComboAvailableBoxLocator2;
            this.ComboAvailableBoxLocator[3] = ComboAvailableBoxLocator3;
            this.ComboAvailableBoxLocator[4] = ComboAvailableBoxLocator4;
            this.ComboAvailableBoxLocator[5] = ComboAvailableBoxLocator5;
            this.ComboAvailableBoxLocator[6] = ComboAvailableBoxLocator6;
            this.ComboAvailableBoxLocator[7] = ComboAvailableBoxLocator7;
        }

        private void InitializeTextBoxPeriodicMessage()
        {
            TextBoxPeriodicMessage = new TextBox[10];
            this.TextBoxPeriodicMessage[0] = TextBoxPeriodicMessage0;
            this.TextBoxPeriodicMessage[1] = TextBoxPeriodicMessage1;
            this.TextBoxPeriodicMessage[2] = TextBoxPeriodicMessage2;
            this.TextBoxPeriodicMessage[3] = TextBoxPeriodicMessage3;
            this.TextBoxPeriodicMessage[4] = TextBoxPeriodicMessage4;
            this.TextBoxPeriodicMessage[5] = TextBoxPeriodicMessage5;
            this.TextBoxPeriodicMessage[6] = TextBoxPeriodicMessage6;
            this.TextBoxPeriodicMessage[7] = TextBoxPeriodicMessage7;
            this.TextBoxPeriodicMessage[8] = TextBoxPeriodicMessage8;
            this.TextBoxPeriodicMessage[9] = TextBoxPeriodicMessage9;
        }

        private void InitializeTextBoxPeriodicMessageInterval()
        {
            TextBoxPeriodicMessageInterval = new TextBox[10];
            this.TextBoxPeriodicMessageInterval[0] = TextBoxPeriodicMessageInterval0;
            this.TextBoxPeriodicMessageInterval[1] = TextBoxPeriodicMessageInterval1;
            this.TextBoxPeriodicMessageInterval[2] = TextBoxPeriodicMessageInterval2;
            this.TextBoxPeriodicMessageInterval[3] = TextBoxPeriodicMessageInterval3;
            this.TextBoxPeriodicMessageInterval[4] = TextBoxPeriodicMessageInterval4;
            this.TextBoxPeriodicMessageInterval[5] = TextBoxPeriodicMessageInterval5;
            this.TextBoxPeriodicMessageInterval[6] = TextBoxPeriodicMessageInterval6;
            this.TextBoxPeriodicMessageInterval[7] = TextBoxPeriodicMessageInterval7;
            this.TextBoxPeriodicMessageInterval[8] = TextBoxPeriodicMessageInterval8;
            this.TextBoxPeriodicMessageInterval[9] = TextBoxPeriodicMessageInterval9;
        }

        private void InitializeTextBoxPeriodicMessageFlags()
        {
            TextBoxPeriodicMessageFlags = new TextBox[10];
            this.TextBoxPeriodicMessageFlags[0] = TextBoxPeriodicMessageFlags0;
            this.TextBoxPeriodicMessageFlags[1] = TextBoxPeriodicMessageFlags1;
            this.TextBoxPeriodicMessageFlags[2] = TextBoxPeriodicMessageFlags2;
            this.TextBoxPeriodicMessageFlags[3] = TextBoxPeriodicMessageFlags3;
            this.TextBoxPeriodicMessageFlags[4] = TextBoxPeriodicMessageFlags4;
            this.TextBoxPeriodicMessageFlags[5] = TextBoxPeriodicMessageFlags5;
            this.TextBoxPeriodicMessageFlags[6] = TextBoxPeriodicMessageFlags6;
            this.TextBoxPeriodicMessageFlags[7] = TextBoxPeriodicMessageFlags7;
            this.TextBoxPeriodicMessageFlags[8] = TextBoxPeriodicMessageFlags8;
            this.TextBoxPeriodicMessageFlags[9] = TextBoxPeriodicMessageFlags9;
        }

        private void InitializeTextBoxFunctionalMessage()
        {
            TextBoxFunctionalMessage = new TextBox[32];
            this.TextBoxFunctionalMessage[0] = TextBoxFunctionalMessage0;
            this.TextBoxFunctionalMessage[1] = TextBoxFunctionalMessage1;
            this.TextBoxFunctionalMessage[2] = TextBoxFunctionalMessage2;
            this.TextBoxFunctionalMessage[3] = TextBoxFunctionalMessage3;
            this.TextBoxFunctionalMessage[4] = TextBoxFunctionalMessage4;
            this.TextBoxFunctionalMessage[5] = TextBoxFunctionalMessage5;
            this.TextBoxFunctionalMessage[6] = TextBoxFunctionalMessage6;
            this.TextBoxFunctionalMessage[7] = TextBoxFunctionalMessage7;
            this.TextBoxFunctionalMessage[8] = TextBoxFunctionalMessage8;
            this.TextBoxFunctionalMessage[9] = TextBoxFunctionalMessage9;
            this.TextBoxFunctionalMessage[10] = TextBoxFunctionalMessage10;
            this.TextBoxFunctionalMessage[11] = TextBoxFunctionalMessage11;
            this.TextBoxFunctionalMessage[12] = TextBoxFunctionalMessage12;
            this.TextBoxFunctionalMessage[13] = TextBoxFunctionalMessage13;
            this.TextBoxFunctionalMessage[14] = TextBoxFunctionalMessage14;
            this.TextBoxFunctionalMessage[15] = TextBoxFunctionalMessage15;
            this.TextBoxFunctionalMessage[16] = TextBoxFunctionalMessage16;
            this.TextBoxFunctionalMessage[17] = TextBoxFunctionalMessage17;
            this.TextBoxFunctionalMessage[18] = TextBoxFunctionalMessage18;
            this.TextBoxFunctionalMessage[19] = TextBoxFunctionalMessage19;
            this.TextBoxFunctionalMessage[20] = TextBoxFunctionalMessage20;
            this.TextBoxFunctionalMessage[21] = TextBoxFunctionalMessage21;
            this.TextBoxFunctionalMessage[22] = TextBoxFunctionalMessage22;
            this.TextBoxFunctionalMessage[23] = TextBoxFunctionalMessage23;
            this.TextBoxFunctionalMessage[24] = TextBoxFunctionalMessage24;
            this.TextBoxFunctionalMessage[25] = TextBoxFunctionalMessage25;
            this.TextBoxFunctionalMessage[26] = TextBoxFunctionalMessage26;
            this.TextBoxFunctionalMessage[27] = TextBoxFunctionalMessage27;
            this.TextBoxFunctionalMessage[28] = TextBoxFunctionalMessage28;
            this.TextBoxFunctionalMessage[29] = TextBoxFunctionalMessage29;
            this.TextBoxFunctionalMessage[30] = TextBoxFunctionalMessage30;
            this.TextBoxFunctionalMessage[31] = TextBoxFunctionalMessage31;
        }

        private void InitializeTextBoxFilterPatt()
        {
            TextBoxFilterPatt = new TextBox[10];
            this.TextBoxFilterPatt[0] = TextBoxFilterPatt0;
            this.TextBoxFilterPatt[1] = TextBoxFilterPatt1;
            this.TextBoxFilterPatt[2] = TextBoxFilterPatt2;
            this.TextBoxFilterPatt[3] = TextBoxFilterPatt3;
            this.TextBoxFilterPatt[4] = TextBoxFilterPatt4;
            this.TextBoxFilterPatt[5] = TextBoxFilterPatt5;
            this.TextBoxFilterPatt[6] = TextBoxFilterPatt6;
            this.TextBoxFilterPatt[7] = TextBoxFilterPatt7;
            this.TextBoxFilterPatt[8] = TextBoxFilterPatt8;
            this.TextBoxFilterPatt[9] = TextBoxFilterPatt9;
        }

        private void InitializeTextBoxFilterMask()
        {
            TextBoxFilterMask = new TextBox[10];
            this.TextBoxFilterMask[9] = TextBoxFilterMask9;
            this.TextBoxFilterMask[8] = TextBoxFilterMask8;
            this.TextBoxFilterMask[7] = TextBoxFilterMask7;
            this.TextBoxFilterMask[6] = TextBoxFilterMask6;
            this.TextBoxFilterMask[5] = TextBoxFilterMask5;
            this.TextBoxFilterMask[4] = TextBoxFilterMask4;
            this.TextBoxFilterMask[3] = TextBoxFilterMask3;
            this.TextBoxFilterMask[2] = TextBoxFilterMask2;
            this.TextBoxFilterMask[1] = TextBoxFilterMask1;
            this.TextBoxFilterMask[0] = TextBoxFilterMask0;
        }

        private void InitializeTextBoxFilterFlow()
        {
            TextBoxFilterFlow = new TextBox[10];
            this.TextBoxFilterFlow[0] = TextBoxFilterFlow0;
            this.TextBoxFilterFlow[1] = TextBoxFilterFlow1;
            this.TextBoxFilterFlow[2] = TextBoxFilterFlow2;
            this.TextBoxFilterFlow[3] = TextBoxFilterFlow3;
            this.TextBoxFilterFlow[4] = TextBoxFilterFlow4;
            this.TextBoxFilterFlow[5] = TextBoxFilterFlow5;
            this.TextBoxFilterFlow[6] = TextBoxFilterFlow6;
            this.TextBoxFilterFlow[7] = TextBoxFilterFlow7;
            this.TextBoxFilterFlow[8] = TextBoxFilterFlow8;
            this.TextBoxFilterFlow[9] = TextBoxFilterFlow9;
        }

        private void InitializeTextBoxFilterFlags()
        {
            TextBoxFilterFlags = new TextBox[10];
            this.TextBoxFilterFlags[0] = TextBoxFilterFlags0;
            this.TextBoxFilterFlags[1] = TextBoxFilterFlags1;
            this.TextBoxFilterFlags[2] = TextBoxFilterFlags2;
            this.TextBoxFilterFlags[3] = TextBoxFilterFlags3;
            this.TextBoxFilterFlags[4] = TextBoxFilterFlags4;
            this.TextBoxFilterFlags[5] = TextBoxFilterFlags5;
            this.TextBoxFilterFlags[6] = TextBoxFilterFlags6;
            this.TextBoxFilterFlags[7] = TextBoxFilterFlags7;
            this.TextBoxFilterFlags[8] = TextBoxFilterFlags8;
            this.TextBoxFilterFlags[9] = TextBoxFilterFlags9;
        }

        private void InitializeProgressBarAnalog()
        {
            ProgressBarAnalog = new ProgressBar[32];
            this.ProgressBarAnalog[0] = ProgressBarAnalog0;
            this.ProgressBarAnalog[1] = ProgressBarAnalog1;
            this.ProgressBarAnalog[2] = ProgressBarAnalog2;
            this.ProgressBarAnalog[3] = ProgressBarAnalog3;
            this.ProgressBarAnalog[4] = ProgressBarAnalog4;
            this.ProgressBarAnalog[5] = ProgressBarAnalog5;
            this.ProgressBarAnalog[6] = ProgressBarAnalog6;
            this.ProgressBarAnalog[7] = ProgressBarAnalog7;
            this.ProgressBarAnalog[8] = ProgressBarAnalog8;
            this.ProgressBarAnalog[9] = ProgressBarAnalog9;
            this.ProgressBarAnalog[10] = ProgressBarAnalog10;
            this.ProgressBarAnalog[11] = ProgressBarAnalog11;
            this.ProgressBarAnalog[12] = ProgressBarAnalog12;
            this.ProgressBarAnalog[13] = ProgressBarAnalog13;
            this.ProgressBarAnalog[14] = ProgressBarAnalog14;
            this.ProgressBarAnalog[15] = ProgressBarAnalog15;
            this.ProgressBarAnalog[16] = ProgressBarAnalog16;
            this.ProgressBarAnalog[17] = ProgressBarAnalog17;
            this.ProgressBarAnalog[18] = ProgressBarAnalog18;
            this.ProgressBarAnalog[19] = ProgressBarAnalog19;
            this.ProgressBarAnalog[20] = ProgressBarAnalog20;
            this.ProgressBarAnalog[21] = ProgressBarAnalog21;
            this.ProgressBarAnalog[22] = ProgressBarAnalog22;
            this.ProgressBarAnalog[23] = ProgressBarAnalog23;
            this.ProgressBarAnalog[24] = ProgressBarAnalog24;
            this.ProgressBarAnalog[25] = ProgressBarAnalog25;
            this.ProgressBarAnalog[26] = ProgressBarAnalog26;
            this.ProgressBarAnalog[27] = ProgressBarAnalog27;
            this.ProgressBarAnalog[28] = ProgressBarAnalog28;
            this.ProgressBarAnalog[29] = ProgressBarAnalog29;
            this.ProgressBarAnalog[30] = ProgressBarAnalog30;
            this.ProgressBarAnalog[31] = ProgressBarAnalog31;
        }

        private void InitializeRadioButtonPin()
        {
            RadioButtonPin = new RadioButton[16];
            this.RadioButtonPin[0] = RadioButtonPin0;
            this.RadioButtonPin[6] = RadioButtonPin6;
            this.RadioButtonPin[9] = RadioButtonPin9;
            this.RadioButtonPin[11] = RadioButtonPin11;
            this.RadioButtonPin[12] = RadioButtonPin12;
            this.RadioButtonPin[13] = RadioButtonPin13;
            this.RadioButtonPin[14] = RadioButtonPin14;
            this.RadioButtonPin[15] = RadioButtonPin15;
        }

        private void InitializeLabelChannel()
        {
            LabelChannel = new Label[7];
            this.LabelChannel[0] = null;
            this.LabelChannel[1] = LabelChannel1;
            this.LabelChannel[2] = LabelChannel2;
            this.LabelChannel[3] = LabelChannel3;
            this.LabelChannel[4] = LabelChannel4;
            this.LabelChannel[5] = LabelChannel5;
            this.LabelChannel[6] = LabelChannel6;
        }

        private void InitializeLabelPin()
        {
            LabelPin = new Label[16];
            this.LabelPin[0] = LabelPin0;
            this.LabelPin[6] = LabelPin6;
            this.LabelPin[9] = LabelPin9;
            this.LabelPin[11] = LabelPin11;
            this.LabelPin[12] = LabelPin12;
            this.LabelPin[13] = LabelPin13;
            this.LabelPin[14] = LabelPin14;
            this.LabelPin[15] = LabelPin15;
        }

        private void InitializeLabelParameterName()
        {
            LabelParameterName = new Label[22];
            this.LabelParameterName[0] = LabelParameterName0;
            this.LabelParameterName[1] = LabelParameterName1;
            this.LabelParameterName[2] = LabelParameterName2;
            this.LabelParameterName[3] = LabelParameterName3;
            this.LabelParameterName[4] = LabelParameterName4;
            this.LabelParameterName[5] = LabelParameterName5;
            this.LabelParameterName[6] = LabelParameterName6;
            this.LabelParameterName[7] = LabelParameterName7;
            this.LabelParameterName[8] = LabelParameterName8;
            this.LabelParameterName[9] = LabelParameterName9;
            this.LabelParameterName[10] = LabelParameterName10;
            this.LabelParameterName[11] = LabelParameterName11;
            this.LabelParameterName[12] = LabelParameterName12;
            this.LabelParameterName[13] = LabelParameterName13;
            this.LabelParameterName[14] = LabelParameterName14;
            this.LabelParameterName[15] = LabelParameterName15;
            this.LabelParameterName[16] = LabelParameterName16;
            this.LabelParameterName[17] = LabelParameterName17;
            this.LabelParameterName[18] = LabelParameterName18;
            this.LabelParameterName[19] = LabelParameterName19;
            this.LabelParameterName[20] = LabelParameterName20;
            this.LabelParameterName[21] = LabelParameterName21;
        }

        private void InitializeLabelParamVal()
        {
            LabelParamVal = new Label[22];
            this.LabelParamVal[0] = LabelParamVal0;
            this.LabelParamVal[1] = LabelParamVal1;
            this.LabelParamVal[2] = LabelParamVal2;
            this.LabelParamVal[3] = LabelParamVal3;
            this.LabelParamVal[4] = LabelParamVal4;
            this.LabelParamVal[5] = LabelParamVal5;
            this.LabelParamVal[6] = LabelParamVal6;
            this.LabelParamVal[7] = LabelParamVal7;
            this.LabelParamVal[8] = LabelParamVal8;
            this.LabelParamVal[9] = LabelParamVal9;
            this.LabelParamVal[10] = LabelParamVal10;
            this.LabelParamVal[11] = LabelParamVal11;
            this.LabelParamVal[12] = LabelParamVal12;
            this.LabelParamVal[13] = LabelParamVal13;
            this.LabelParamVal[14] = LabelParamVal14;
            this.LabelParamVal[15] = LabelParamVal15;
            this.LabelParamVal[16] = LabelParamVal16;
            this.LabelParamVal[17] = LabelParamVal17;
            this.LabelParamVal[18] = LabelParamVal18;
            this.LabelParamVal[19] = LabelParamVal19;
            this.LabelParamVal[20] = LabelParamVal20;
            this.LabelParamVal[21] = LabelParamVal21;
        }

        private void InitializeTextBoxParamVal()
        {
            TextBoxParamVal = new TextBox[22];
            this.TextBoxParamVal[0] = TextBoxParamVal0;
            this.TextBoxParamVal[1] = TextBoxParamVal1;
            this.TextBoxParamVal[2] = TextBoxParamVal2;
            this.TextBoxParamVal[3] = TextBoxParamVal3;
            this.TextBoxParamVal[4] = TextBoxParamVal4;
            this.TextBoxParamVal[5] = TextBoxParamVal5;
            this.TextBoxParamVal[6] = TextBoxParamVal6;
            this.TextBoxParamVal[7] = TextBoxParamVal7;
            this.TextBoxParamVal[8] = TextBoxParamVal8;
            this.TextBoxParamVal[9] = TextBoxParamVal9;
            this.TextBoxParamVal[10] = TextBoxParamVal10;
            this.TextBoxParamVal[11] = TextBoxParamVal11;
            this.TextBoxParamVal[12] = TextBoxParamVal12;
            this.TextBoxParamVal[13] = TextBoxParamVal13;
            this.TextBoxParamVal[14] = TextBoxParamVal14;
            this.TextBoxParamVal[15] = TextBoxParamVal15;
            this.TextBoxParamVal[16] = TextBoxParamVal16;
            this.TextBoxParamVal[17] = TextBoxParamVal17;
            this.TextBoxParamVal[18] = TextBoxParamVal18;
            this.TextBoxParamVal[19] = TextBoxParamVal19;
            this.TextBoxParamVal[20] = TextBoxParamVal20;
            this.TextBoxParamVal[21] = TextBoxParamVal21;
        }

        private void InitializeLabelPeriodicMessageId()
        {
            LabelPeriodicMessageId = new Label[10];
            this.LabelPeriodicMessageId[0] = LabelPeriodicMessageId0;
            this.LabelPeriodicMessageId[1] = LabelPeriodicMessageId1;
            this.LabelPeriodicMessageId[2] = LabelPeriodicMessageId2;
            this.LabelPeriodicMessageId[3] = LabelPeriodicMessageId3;
            this.LabelPeriodicMessageId[4] = LabelPeriodicMessageId4;
            this.LabelPeriodicMessageId[5] = LabelPeriodicMessageId5;
            this.LabelPeriodicMessageId[6] = LabelPeriodicMessageId6;
            this.LabelPeriodicMessageId[7] = LabelPeriodicMessageId7;
            this.LabelPeriodicMessageId[8] = LabelPeriodicMessageId8;
            this.LabelPeriodicMessageId[9] = LabelPeriodicMessageId9;
        }

        private void InitializeLabelPeriodicMessage()
        {
            LabelPeriodicMessage = new Label[10];
            this.LabelPeriodicMessage[0] = LabelPeriodicMessage0;
            this.LabelPeriodicMessage[1] = LabelPeriodicMessage1;
            this.LabelPeriodicMessage[2] = LabelPeriodicMessage2;
            this.LabelPeriodicMessage[3] = LabelPeriodicMessage3;
            this.LabelPeriodicMessage[4] = LabelPeriodicMessage4;
            this.LabelPeriodicMessage[5] = LabelPeriodicMessage5;
            this.LabelPeriodicMessage[6] = LabelPeriodicMessage6;
            this.LabelPeriodicMessage[7] = LabelPeriodicMessage7;
            this.LabelPeriodicMessage[8] = LabelPeriodicMessage8;
            this.LabelPeriodicMessage[9] = LabelPeriodicMessage9;
        }

        private void InitializeLabelFunctionalMessage()
        {
            LabelFunctionalMessage = new Label[32];
            this.LabelFunctionalMessage[0] = LabelFunctionalMessage0;
            this.LabelFunctionalMessage[1] = LabelFunctionalMessage1;
            this.LabelFunctionalMessage[2] = LabelFunctionalMessage2;
            this.LabelFunctionalMessage[3] = LabelFunctionalMessage3;
            this.LabelFunctionalMessage[4] = LabelFunctionalMessage4;
            this.LabelFunctionalMessage[5] = LabelFunctionalMessage5;
            this.LabelFunctionalMessage[6] = LabelFunctionalMessage6;
            this.LabelFunctionalMessage[7] = LabelFunctionalMessage7;
            this.LabelFunctionalMessage[8] = LabelFunctionalMessage8;
            this.LabelFunctionalMessage[9] = LabelFunctionalMessage9;
            this.LabelFunctionalMessage[10] = LabelFunctionalMessage10;
            this.LabelFunctionalMessage[11] = LabelFunctionalMessage11;
            this.LabelFunctionalMessage[12] = LabelFunctionalMessage12;
            this.LabelFunctionalMessage[13] = LabelFunctionalMessage13;
            this.LabelFunctionalMessage[14] = LabelFunctionalMessage14;
            this.LabelFunctionalMessage[15] = LabelFunctionalMessage15;
            this.LabelFunctionalMessage[16] = LabelFunctionalMessage16;
            this.LabelFunctionalMessage[17] = LabelFunctionalMessage17;
            this.LabelFunctionalMessage[18] = LabelFunctionalMessage18;
            this.LabelFunctionalMessage[19] = LabelFunctionalMessage19;
            this.LabelFunctionalMessage[20] = LabelFunctionalMessage20;
            this.LabelFunctionalMessage[21] = LabelFunctionalMessage21;
            this.LabelFunctionalMessage[22] = LabelFunctionalMessage22;
            this.LabelFunctionalMessage[23] = LabelFunctionalMessage23;
            this.LabelFunctionalMessage[24] = LabelFunctionalMessage24;
            this.LabelFunctionalMessage[25] = LabelFunctionalMessage25;
            this.LabelFunctionalMessage[26] = LabelFunctionalMessage26;
            this.LabelFunctionalMessage[27] = LabelFunctionalMessage27;
            this.LabelFunctionalMessage[28] = LabelFunctionalMessage28;
            this.LabelFunctionalMessage[29] = LabelFunctionalMessage29;
            this.LabelFunctionalMessage[30] = LabelFunctionalMessage30;
            this.LabelFunctionalMessage[31] = LabelFunctionalMessage31;
        }

        private void InitializeLabelFuncId()
        {
            LabelFuncId = new Label[32];
            this.LabelFuncId[0] = LabelFuncId0;
            this.LabelFuncId[1] = LabelFuncId1;
            this.LabelFuncId[2] = LabelFuncId2;
            this.LabelFuncId[3] = LabelFuncId3;
            this.LabelFuncId[4] = LabelFuncId4;
            this.LabelFuncId[5] = LabelFuncId5;
            this.LabelFuncId[6] = LabelFuncId6;
            this.LabelFuncId[7] = LabelFuncId7;
            this.LabelFuncId[8] = LabelFuncId8;
            this.LabelFuncId[9] = LabelFuncId9;
            this.LabelFuncId[10] = LabelFuncId10;
            this.LabelFuncId[11] = LabelFuncId11;
            this.LabelFuncId[12] = LabelFuncId12;
            this.LabelFuncId[13] = LabelFuncId13;
            this.LabelFuncId[14] = LabelFuncId14;
            this.LabelFuncId[15] = LabelFuncId15;
            this.LabelFuncId[16] = LabelFuncId16;
            this.LabelFuncId[17] = LabelFuncId17;
            this.LabelFuncId[18] = LabelFuncId18;
            this.LabelFuncId[19] = LabelFuncId19;
            this.LabelFuncId[20] = LabelFuncId20;
            this.LabelFuncId[21] = LabelFuncId21;
            this.LabelFuncId[22] = LabelFuncId22;
            this.LabelFuncId[23] = LabelFuncId23;
            this.LabelFuncId[24] = LabelFuncId24;
            this.LabelFuncId[25] = LabelFuncId25;
            this.LabelFuncId[26] = LabelFuncId26;
            this.LabelFuncId[27] = LabelFuncId27;
            this.LabelFuncId[28] = LabelFuncId28;
            this.LabelFuncId[29] = LabelFuncId29;
            this.LabelFuncId[30] = LabelFuncId30;
            this.LabelFuncId[31] = LabelFuncId31;
        }

        private void InitializeLabelFilterId()
        {
            LabelFilterId = new Label[10];
            this.LabelFilterId[0] = LabelFilterId0;
            this.LabelFilterId[1] = LabelFilterId1;
            this.LabelFilterId[2] = LabelFilterId2;
            this.LabelFilterId[3] = LabelFilterId3;
            this.LabelFilterId[4] = LabelFilterId4;
            this.LabelFilterId[5] = LabelFilterId5;
            this.LabelFilterId[6] = LabelFilterId6;
            this.LabelFilterId[7] = LabelFilterId7;
            this.LabelFilterId[8] = LabelFilterId8;
            this.LabelFilterId[9] = LabelFilterId9;
        }

        private void InitializeLabelFilter()
        {
            LabelFilter = new Label[10];
            this.LabelFilter[0] = LabelFilter0;
            this.LabelFilter[1] = LabelFilter1;
            this.LabelFilter[2] = LabelFilter2;
            this.LabelFilter[3] = LabelFilter3;
            this.LabelFilter[4] = LabelFilter4;
            this.LabelFilter[5] = LabelFilter5;
            this.LabelFilter[6] = LabelFilter6;
            this.LabelFilter[7] = LabelFilter7;
            this.LabelFilter[8] = LabelFilter8;
            this.LabelFilter[9] = LabelFilter9;
        }

        private void InitializeLabelDeviceCombo()
        {
            LabelDeviceCombo = new Label[8];
            this.LabelDeviceCombo[0] = LabelDeviceCombo0;
            this.LabelDeviceCombo[1] = LabelDeviceCombo1;
            this.LabelDeviceCombo[2] = LabelDeviceCombo2;
            this.LabelDeviceCombo[3] = LabelDeviceCombo3;
            this.LabelDeviceCombo[4] = LabelDeviceCombo4;
            this.LabelDeviceCombo[5] = LabelDeviceCombo5;
            this.LabelDeviceCombo[6] = LabelDeviceCombo6;
            this.LabelDeviceCombo[7] = LabelDeviceCombo7;
        }

        private void InitializeLabelAnalogRead()
        {
            LabelAnalogRead = new Label[32];
            this.LabelAnalogRead[0] = LabelAnalogRead0;
            this.LabelAnalogRead[1] = LabelAnalogRead1;
            this.LabelAnalogRead[2] = LabelAnalogRead2;
            this.LabelAnalogRead[3] = LabelAnalogRead3;
            this.LabelAnalogRead[4] = LabelAnalogRead4;
            this.LabelAnalogRead[5] = LabelAnalogRead5;
            this.LabelAnalogRead[6] = LabelAnalogRead6;
            this.LabelAnalogRead[7] = LabelAnalogRead7;
            this.LabelAnalogRead[8] = LabelAnalogRead8;
            this.LabelAnalogRead[9] = LabelAnalogRead9;
            this.LabelAnalogRead[10] = LabelAnalogRead10;
            this.LabelAnalogRead[11] = LabelAnalogRead11;
            this.LabelAnalogRead[12] = LabelAnalogRead12;
            this.LabelAnalogRead[13] = LabelAnalogRead13;
            this.LabelAnalogRead[14] = LabelAnalogRead14;
            this.LabelAnalogRead[15] = LabelAnalogRead15;
            this.LabelAnalogRead[16] = LabelAnalogRead16;
            this.LabelAnalogRead[17] = LabelAnalogRead17;
            this.LabelAnalogRead[18] = LabelAnalogRead18;
            this.LabelAnalogRead[19] = LabelAnalogRead19;
            this.LabelAnalogRead[20] = LabelAnalogRead20;
            this.LabelAnalogRead[21] = LabelAnalogRead21;
            this.LabelAnalogRead[22] = LabelAnalogRead22;
            this.LabelAnalogRead[23] = LabelAnalogRead23;
            this.LabelAnalogRead[24] = LabelAnalogRead24;
            this.LabelAnalogRead[25] = LabelAnalogRead25;
            this.LabelAnalogRead[26] = LabelAnalogRead26;
            this.LabelAnalogRead[27] = LabelAnalogRead27;
            this.LabelAnalogRead[28] = LabelAnalogRead28;
            this.LabelAnalogRead[29] = LabelAnalogRead29;
            this.LabelAnalogRead[30] = LabelAnalogRead30;
            this.LabelAnalogRead[31] = LabelAnalogRead31;
        }

        private void InitializeLabelAnalogCH()
        {
            LabelAnalogCH = new Label[32];
            this.LabelAnalogCH[0] = LabelAnalogCH0;
            this.LabelAnalogCH[1] = LabelAnalogCH1;
            this.LabelAnalogCH[2] = LabelAnalogCH2;
            this.LabelAnalogCH[3] = LabelAnalogCH3;
            this.LabelAnalogCH[4] = LabelAnalogCH4;
            this.LabelAnalogCH[5] = LabelAnalogCH5;
            this.LabelAnalogCH[6] = LabelAnalogCH6;
            this.LabelAnalogCH[7] = LabelAnalogCH7;
            this.LabelAnalogCH[8] = LabelAnalogCH8;
            this.LabelAnalogCH[9] = LabelAnalogCH9;
            this.LabelAnalogCH[10] = LabelAnalogCH10;
            this.LabelAnalogCH[11] = LabelAnalogCH11;
            this.LabelAnalogCH[12] = LabelAnalogCH12;
            this.LabelAnalogCH[13] = LabelAnalogCH13;
            this.LabelAnalogCH[14] = LabelAnalogCH14;
            this.LabelAnalogCH[15] = LabelAnalogCH15;
            this.LabelAnalogCH[16] = LabelAnalogCH16;
            this.LabelAnalogCH[17] = LabelAnalogCH17;
            this.LabelAnalogCH[18] = LabelAnalogCH18;
            this.LabelAnalogCH[19] = LabelAnalogCH19;
            this.LabelAnalogCH[20] = LabelAnalogCH20;
            this.LabelAnalogCH[21] = LabelAnalogCH21;
            this.LabelAnalogCH[22] = LabelAnalogCH22;
            this.LabelAnalogCH[23] = LabelAnalogCH23;
            this.LabelAnalogCH[24] = LabelAnalogCH24;
            this.LabelAnalogCH[25] = LabelAnalogCH25;
            this.LabelAnalogCH[26] = LabelAnalogCH26;
            this.LabelAnalogCH[27] = LabelAnalogCH27;
            this.LabelAnalogCH[28] = LabelAnalogCH28;
            this.LabelAnalogCH[29] = LabelAnalogCH29;
            this.LabelAnalogCH[30] = LabelAnalogCH30;
            this.LabelAnalogCH[31] = LabelAnalogCH31;
        }

        private void InitializeCheckBoxPeriodicMessageEnable()
        {
            CheckBoxPeriodicMessageEnable = new CheckBox[10];
            this.CheckBoxPeriodicMessageEnable[0] = CheckBoxPeriodicMessageEnable0;
            this.CheckBoxPeriodicMessageEnable[1] = CheckBoxPeriodicMessageEnable1;
            this.CheckBoxPeriodicMessageEnable[2] = CheckBoxPeriodicMessageEnable2;
            this.CheckBoxPeriodicMessageEnable[3] = CheckBoxPeriodicMessageEnable3;
            this.CheckBoxPeriodicMessageEnable[4] = CheckBoxPeriodicMessageEnable4;
            this.CheckBoxPeriodicMessageEnable[5] = CheckBoxPeriodicMessageEnable5;
            this.CheckBoxPeriodicMessageEnable[6] = CheckBoxPeriodicMessageEnable6;
            this.CheckBoxPeriodicMessageEnable[7] = CheckBoxPeriodicMessageEnable7;
            this.CheckBoxPeriodicMessageEnable[8] = CheckBoxPeriodicMessageEnable8;
            this.CheckBoxPeriodicMessageEnable[9] = CheckBoxPeriodicMessageEnable9;
        }

        private void InitializeCheckBoxFunctionalMessageDelete()
        {
            CheckBoxFunctionalMessageDelete = new CheckBox[32];
            this.CheckBoxFunctionalMessageDelete[0] = CheckBoxFunctionalMessageDelete0;
            this.CheckBoxFunctionalMessageDelete[1] = CheckBoxFunctionalMessageDelete1;
            this.CheckBoxFunctionalMessageDelete[2] = CheckBoxFunctionalMessageDelete2;
            this.CheckBoxFunctionalMessageDelete[3] = CheckBoxFunctionalMessageDelete3;
            this.CheckBoxFunctionalMessageDelete[4] = CheckBoxFunctionalMessageDelete4;
            this.CheckBoxFunctionalMessageDelete[5] = CheckBoxFunctionalMessageDelete5;
            this.CheckBoxFunctionalMessageDelete[6] = CheckBoxFunctionalMessageDelete6;
            this.CheckBoxFunctionalMessageDelete[7] = CheckBoxFunctionalMessageDelete7;
            this.CheckBoxFunctionalMessageDelete[8] = CheckBoxFunctionalMessageDelete8;
            this.CheckBoxFunctionalMessageDelete[9] = CheckBoxFunctionalMessageDelete9;
            this.CheckBoxFunctionalMessageDelete[10] = CheckBoxFunctionalMessageDelete10;
            this.CheckBoxFunctionalMessageDelete[11] = CheckBoxFunctionalMessageDelete11;
            this.CheckBoxFunctionalMessageDelete[12] = CheckBoxFunctionalMessageDelete12;
            this.CheckBoxFunctionalMessageDelete[13] = CheckBoxFunctionalMessageDelete13;
            this.CheckBoxFunctionalMessageDelete[14] = CheckBoxFunctionalMessageDelete14;
            this.CheckBoxFunctionalMessageDelete[15] = CheckBoxFunctionalMessageDelete15;
            this.CheckBoxFunctionalMessageDelete[16] = CheckBoxFunctionalMessageDelete16;
            this.CheckBoxFunctionalMessageDelete[17] = CheckBoxFunctionalMessageDelete17;
            this.CheckBoxFunctionalMessageDelete[18] = CheckBoxFunctionalMessageDelete18;
            this.CheckBoxFunctionalMessageDelete[19] = CheckBoxFunctionalMessageDelete19;
            this.CheckBoxFunctionalMessageDelete[20] = CheckBoxFunctionalMessageDelete20;
            this.CheckBoxFunctionalMessageDelete[21] = CheckBoxFunctionalMessageDelete21;
            this.CheckBoxFunctionalMessageDelete[22] = CheckBoxFunctionalMessageDelete22;
            this.CheckBoxFunctionalMessageDelete[23] = CheckBoxFunctionalMessageDelete23;
            this.CheckBoxFunctionalMessageDelete[24] = CheckBoxFunctionalMessageDelete24;
            this.CheckBoxFunctionalMessageDelete[25] = CheckBoxFunctionalMessageDelete25;
            this.CheckBoxFunctionalMessageDelete[26] = CheckBoxFunctionalMessageDelete26;
            this.CheckBoxFunctionalMessageDelete[27] = CheckBoxFunctionalMessageDelete27;
            this.CheckBoxFunctionalMessageDelete[28] = CheckBoxFunctionalMessageDelete28;
            this.CheckBoxFunctionalMessageDelete[29] = CheckBoxFunctionalMessageDelete29;
            this.CheckBoxFunctionalMessageDelete[30] = CheckBoxFunctionalMessageDelete30;
            this.CheckBoxFunctionalMessageDelete[31] = CheckBoxFunctionalMessageDelete31;
        }

        private void InitializeComboBoxPeriodicMessageChannel()
        {
            ComboBoxPeriodicMessageChannel = new ComboBox[10];
            this.ComboBoxPeriodicMessageChannel[0] = ComboBoxPeriodicMessageChannel0;
            this.ComboBoxPeriodicMessageChannel[1] = ComboBoxPeriodicMessageChannel1;
            this.ComboBoxPeriodicMessageChannel[2] = ComboBoxPeriodicMessageChannel2;
            this.ComboBoxPeriodicMessageChannel[3] = ComboBoxPeriodicMessageChannel3;
            this.ComboBoxPeriodicMessageChannel[4] = ComboBoxPeriodicMessageChannel4;
            this.ComboBoxPeriodicMessageChannel[5] = ComboBoxPeriodicMessageChannel5;
            this.ComboBoxPeriodicMessageChannel[6] = ComboBoxPeriodicMessageChannel6;
            this.ComboBoxPeriodicMessageChannel[7] = ComboBoxPeriodicMessageChannel7;
            this.ComboBoxPeriodicMessageChannel[8] = ComboBoxPeriodicMessageChannel8;
            this.ComboBoxPeriodicMessageChannel[9] = ComboBoxPeriodicMessageChannel9;
        }

        private void InitializeComboBoxFilterType()
        {
            ComboBoxFilterType = new ComboBox[10];
            this.ComboBoxFilterType[0] = ComboBoxFilterType0;
            this.ComboBoxFilterType[1] = ComboBoxFilterType1;
            this.ComboBoxFilterType[2] = ComboBoxFilterType2;
            this.ComboBoxFilterType[3] = ComboBoxFilterType3;
            this.ComboBoxFilterType[4] = ComboBoxFilterType4;
            this.ComboBoxFilterType[5] = ComboBoxFilterType5;
            this.ComboBoxFilterType[6] = ComboBoxFilterType6;
            this.ComboBoxFilterType[7] = ComboBoxFilterType7;
            this.ComboBoxFilterType[8] = ComboBoxFilterType8;
            this.ComboBoxFilterType[9] = ComboBoxFilterType9;
        }

        private void InitializeCheckBoxCH()
        {
            CheckBoxCH = new CheckBox[32];
            this.CheckBoxCH[0] = CheckBoxCH0;
            this.CheckBoxCH[1] = CheckBoxCH1;
            this.CheckBoxCH[2] = CheckBoxCH2;
            this.CheckBoxCH[3] = CheckBoxCH3;
            this.CheckBoxCH[4] = CheckBoxCH4;
            this.CheckBoxCH[5] = CheckBoxCH5;
            this.CheckBoxCH[6] = CheckBoxCH6;
            this.CheckBoxCH[7] = CheckBoxCH7;
            this.CheckBoxCH[8] = CheckBoxCH8;
            this.CheckBoxCH[9] = CheckBoxCH9;
            this.CheckBoxCH[10] = CheckBoxCH10;
            this.CheckBoxCH[11] = CheckBoxCH11;
            this.CheckBoxCH[12] = CheckBoxCH12;
            this.CheckBoxCH[13] = CheckBoxCH13;
            this.CheckBoxCH[14] = CheckBoxCH14;
            this.CheckBoxCH[15] = CheckBoxCH15;
            this.CheckBoxCH[16] = CheckBoxCH16;
            this.CheckBoxCH[17] = CheckBoxCH17;
            this.CheckBoxCH[18] = CheckBoxCH18;
            this.CheckBoxCH[19] = CheckBoxCH19;
            this.CheckBoxCH[20] = CheckBoxCH20;
            this.CheckBoxCH[21] = CheckBoxCH21;
            this.CheckBoxCH[22] = CheckBoxCH22;
            this.CheckBoxCH[23] = CheckBoxCH23;
            this.CheckBoxCH[24] = CheckBoxCH24;
            this.CheckBoxCH[25] = CheckBoxCH25;
            this.CheckBoxCH[26] = CheckBoxCH26;
            this.CheckBoxCH[27] = CheckBoxCH27;
            this.CheckBoxCH[28] = CheckBoxCH28;
            this.CheckBoxCH[29] = CheckBoxCH29;
            this.CheckBoxCH[30] = CheckBoxCH30;
            this.CheckBoxCH[31] = CheckBoxCH31;
        }

        private CheckBox[] CheckBoxCH;
        private CheckBox CheckBoxCH0;
        private CheckBox CheckBoxCH1;
        private CheckBox CheckBoxCH2;
        private CheckBox CheckBoxCH3;
        private CheckBox CheckBoxCH4;
        private CheckBox CheckBoxCH5;
        private CheckBox CheckBoxCH6;
        private CheckBox CheckBoxCH7;
        private CheckBox CheckBoxCH8;
        private CheckBox CheckBoxCH9;
        private CheckBox CheckBoxCH10;
        private CheckBox CheckBoxCH11;
        private CheckBox CheckBoxCH12;
        private CheckBox CheckBoxCH13;
        private CheckBox CheckBoxCH14;
        private CheckBox CheckBoxCH15;
        private CheckBox CheckBoxCH16;
        private CheckBox CheckBoxCH17;
        private CheckBox CheckBoxCH18;
        private CheckBox CheckBoxCH19;
        private CheckBox CheckBoxCH20;
        private CheckBox CheckBoxCH21;
        private CheckBox CheckBoxCH22;
        private CheckBox CheckBoxCH23;
        private CheckBox CheckBoxCH24;
        private CheckBox CheckBoxCH25;
        private CheckBox CheckBoxCH26;
        private CheckBox CheckBoxCH27;
        private CheckBox CheckBoxCH28;
        private CheckBox CheckBoxCH29;
        private CheckBox CheckBoxCH30;
        private CheckBox CheckBoxCH31;

        private CheckBox[] CheckBoxFunctionalMessageDelete;
        private CheckBox CheckBoxFunctionalMessageDelete0;
        private CheckBox CheckBoxFunctionalMessageDelete1;
        private CheckBox CheckBoxFunctionalMessageDelete2;
        private CheckBox CheckBoxFunctionalMessageDelete3;
        private CheckBox CheckBoxFunctionalMessageDelete4;
        private CheckBox CheckBoxFunctionalMessageDelete5;
        private CheckBox CheckBoxFunctionalMessageDelete6;
        private CheckBox CheckBoxFunctionalMessageDelete7;
        private CheckBox CheckBoxFunctionalMessageDelete8;
        private CheckBox CheckBoxFunctionalMessageDelete9;
        private CheckBox CheckBoxFunctionalMessageDelete10;
        private CheckBox CheckBoxFunctionalMessageDelete11;
        private CheckBox CheckBoxFunctionalMessageDelete12;
        private CheckBox CheckBoxFunctionalMessageDelete13;
        private CheckBox CheckBoxFunctionalMessageDelete14;
        private CheckBox CheckBoxFunctionalMessageDelete15;
        private CheckBox CheckBoxFunctionalMessageDelete16;
        private CheckBox CheckBoxFunctionalMessageDelete17;
        private CheckBox CheckBoxFunctionalMessageDelete18;
        private CheckBox CheckBoxFunctionalMessageDelete19;
        private CheckBox CheckBoxFunctionalMessageDelete20;
        private CheckBox CheckBoxFunctionalMessageDelete21;
        private CheckBox CheckBoxFunctionalMessageDelete22;
        private CheckBox CheckBoxFunctionalMessageDelete23;
        private CheckBox CheckBoxFunctionalMessageDelete24;
        private CheckBox CheckBoxFunctionalMessageDelete25;
        private CheckBox CheckBoxFunctionalMessageDelete26;
        private CheckBox CheckBoxFunctionalMessageDelete27;
        private CheckBox CheckBoxFunctionalMessageDelete28;
        private CheckBox CheckBoxFunctionalMessageDelete29;
        private CheckBox CheckBoxFunctionalMessageDelete30;
        private CheckBox CheckBoxFunctionalMessageDelete31;

        private CheckBox[] CheckBoxPeriodicMessageEnable;
        private CheckBox CheckBoxPeriodicMessageEnable0;
        private CheckBox CheckBoxPeriodicMessageEnable1;
        private CheckBox CheckBoxPeriodicMessageEnable2;
        private CheckBox CheckBoxPeriodicMessageEnable3;
        private CheckBox CheckBoxPeriodicMessageEnable4;
        private CheckBox CheckBoxPeriodicMessageEnable5;
        private CheckBox CheckBoxPeriodicMessageEnable6;
        private CheckBox CheckBoxPeriodicMessageEnable7;
        private CheckBox CheckBoxPeriodicMessageEnable8;
        private CheckBox CheckBoxPeriodicMessageEnable9;

        private Label[] ComboAvailableBoxLocator;
        private Label[] ComboAvailableChannelLocator;
        private Label ComboAvailableBoxLocator0;
        private Label ComboAvailableChannelLocator0;
        private Label ComboAvailableBoxLocator1;
        private Label ComboAvailableChannelLocator1;
        private Label ComboAvailableBoxLocator2;
        private Label ComboAvailableChannelLocator2;
        private Label ComboAvailableBoxLocator3;
        private Label ComboAvailableChannelLocator3;
        private Label ComboAvailableBoxLocator4;
        private Label ComboAvailableChannelLocator4;
        private Label ComboAvailableBoxLocator5;
        private Label ComboAvailableChannelLocator5;
        private Label ComboAvailableBoxLocator6;
        private Label ComboAvailableChannelLocator6;
        private Label ComboAvailableBoxLocator7;
        private Label ComboAvailableChannelLocator7;

        private ComboBox[] ComboBoxFilterType;
        private ComboBox ComboBoxFilterType0;
        private ComboBox ComboBoxFilterType1;
        private ComboBox ComboBoxFilterType2;
        private ComboBox ComboBoxFilterType3;
        private ComboBox ComboBoxFilterType4;
        private ComboBox ComboBoxFilterType5;
        private ComboBox ComboBoxFilterType6;
        private ComboBox ComboBoxFilterType7;
        private ComboBox ComboBoxFilterType8;
        private ComboBox ComboBoxFilterType9;

        private ComboBox[] ComboBoxPeriodicMessageChannel;
        private ComboBox ComboBoxPeriodicMessageChannel0;
        private ComboBox ComboBoxPeriodicMessageChannel1;
        private ComboBox ComboBoxPeriodicMessageChannel2;
        private ComboBox ComboBoxPeriodicMessageChannel3;
        private ComboBox ComboBoxPeriodicMessageChannel4;
        private ComboBox ComboBoxPeriodicMessageChannel5;
        private ComboBox ComboBoxPeriodicMessageChannel6;
        private ComboBox ComboBoxPeriodicMessageChannel7;
        private ComboBox ComboBoxPeriodicMessageChannel8;
        private ComboBox ComboBoxPeriodicMessageChannel9;

        private Label[] LabelAnalogCH;
        private Label LabelAnalogCH0;
        private Label LabelAnalogCH1;
        private Label LabelAnalogCH2;
        private Label LabelAnalogCH3;
        private Label LabelAnalogCH4;
        private Label LabelAnalogCH5;
        private Label LabelAnalogCH6;
        private Label LabelAnalogCH7;
        private Label LabelAnalogCH8;
        private Label LabelAnalogCH9;
        private Label LabelAnalogCH10;
        private Label LabelAnalogCH11;
        private Label LabelAnalogCH12;
        private Label LabelAnalogCH13;
        private Label LabelAnalogCH14;
        private Label LabelAnalogCH15;
        private Label LabelAnalogCH16;
        private Label LabelAnalogCH17;
        private Label LabelAnalogCH18;
        private Label LabelAnalogCH19;
        private Label LabelAnalogCH20;
        private Label LabelAnalogCH21;
        private Label LabelAnalogCH22;
        private Label LabelAnalogCH23;
        private Label LabelAnalogCH24;
        private Label LabelAnalogCH25;
        private Label LabelAnalogCH26;
        private Label LabelAnalogCH27;
        private Label LabelAnalogCH28;
        private Label LabelAnalogCH29;
        private Label LabelAnalogCH30;
        private Label LabelAnalogCH31;

        private Label[] LabelAnalogRead;
        private Label LabelAnalogRead0;
        private Label LabelAnalogRead1;
        private Label LabelAnalogRead2;
        private Label LabelAnalogRead3;
        private Label LabelAnalogRead4;
        private Label LabelAnalogRead5;
        private Label LabelAnalogRead6;
        private Label LabelAnalogRead7;
        private Label LabelAnalogRead8;
        private Label LabelAnalogRead9;
        private Label LabelAnalogRead10;
        private Label LabelAnalogRead11;
        private Label LabelAnalogRead12;
        private Label LabelAnalogRead13;
        private Label LabelAnalogRead14;
        private Label LabelAnalogRead15;
        private Label LabelAnalogRead16;
        private Label LabelAnalogRead17;
        private Label LabelAnalogRead18;
        private Label LabelAnalogRead19;
        private Label LabelAnalogRead20;
        private Label LabelAnalogRead21;
        private Label LabelAnalogRead22;
        private Label LabelAnalogRead23;
        private Label LabelAnalogRead24;
        private Label LabelAnalogRead25;
        private Label LabelAnalogRead26;
        private Label LabelAnalogRead27;
        private Label LabelAnalogRead28;
        private Label LabelAnalogRead29;
        private Label LabelAnalogRead30;
        private Label LabelAnalogRead31;

        private Label[] LabelDeviceCombo;
        private Label LabelDeviceCombo0;
        private Label LabelDeviceCombo1;
        private Label LabelDeviceCombo2;
        private Label LabelDeviceCombo3;
        private Label LabelDeviceCombo4;
        private Label LabelDeviceCombo5;
        private Label LabelDeviceCombo6;
        private Label LabelDeviceCombo7;

        private Label[] LabelFilter;
        private Label LabelFilter0;
        private Label LabelFilter1;
        private Label LabelFilter2;
        private Label LabelFilter3;
        private Label LabelFilter4;
        private Label LabelFilter5;
        private Label LabelFilter6;
        private Label LabelFilter7;
        private Label LabelFilter8;
        private Label LabelFilter9;

        private Label[] LabelFilterId;
        private Label LabelFilterId0;
        private Label LabelFilterId1;
        private Label LabelFilterId2;
        private Label LabelFilterId3;
        private Label LabelFilterId4;
        private Label LabelFilterId5;
        private Label LabelFilterId6;
        private Label LabelFilterId7;
        private Label LabelFilterId8;
        private Label LabelFilterId9;

        private Label[] LabelFuncId;
        private Label LabelFuncId0;
        private Label LabelFuncId1;
        private Label LabelFuncId2;
        private Label LabelFuncId3;
        private Label LabelFuncId4;
        private Label LabelFuncId5;
        private Label LabelFuncId6;
        private Label LabelFuncId7;
        private Label LabelFuncId8;
        private Label LabelFuncId9;
        private Label LabelFuncId10;
        private Label LabelFuncId11;
        private Label LabelFuncId12;
        private Label LabelFuncId13;
        private Label LabelFuncId14;
        private Label LabelFuncId15;
        private Label LabelFuncId16;
        private Label LabelFuncId17;
        private Label LabelFuncId18;
        private Label LabelFuncId19;
        private Label LabelFuncId20;
        private Label LabelFuncId21;
        private Label LabelFuncId22;
        private Label LabelFuncId23;
        private Label LabelFuncId24;
        private Label LabelFuncId25;
        private Label LabelFuncId26;
        private Label LabelFuncId27;
        private Label LabelFuncId28;
        private Label LabelFuncId29;
        private Label LabelFuncId30;
        private Label LabelFuncId31;

        private Label[] LabelFunctionalMessage;
        private Label LabelFunctionalMessage0;
        private Label LabelFunctionalMessage1;
        private Label LabelFunctionalMessage2;
        private Label LabelFunctionalMessage3;
        private Label LabelFunctionalMessage4;
        private Label LabelFunctionalMessage5;
        private Label LabelFunctionalMessage6;
        private Label LabelFunctionalMessage7;
        private Label LabelFunctionalMessage8;
        private Label LabelFunctionalMessage9;
        private Label LabelFunctionalMessage10;
        private Label LabelFunctionalMessage11;
        private Label LabelFunctionalMessage12;
        private Label LabelFunctionalMessage13;
        private Label LabelFunctionalMessage14;
        private Label LabelFunctionalMessage15;
        private Label LabelFunctionalMessage16;
        private Label LabelFunctionalMessage17;
        private Label LabelFunctionalMessage18;
        private Label LabelFunctionalMessage19;
        private Label LabelFunctionalMessage20;
        private Label LabelFunctionalMessage21;
        private Label LabelFunctionalMessage22;
        private Label LabelFunctionalMessage23;
        private Label LabelFunctionalMessage24;
        private Label LabelFunctionalMessage25;
        private Label LabelFunctionalMessage26;
        private Label LabelFunctionalMessage27;
        private Label LabelFunctionalMessage28;
        private Label LabelFunctionalMessage29;
        private Label LabelFunctionalMessage30;
        private Label LabelFunctionalMessage31;

        private Label[] LabelParameterName;
        private Label LabelParameterName0;
        private Label LabelParameterName1;
        private Label LabelParameterName2;
        private Label LabelParameterName3;
        private Label LabelParameterName4;
        private Label LabelParameterName5;
        private Label LabelParameterName6;
        private Label LabelParameterName7;
        private Label LabelParameterName8;
        private Label LabelParameterName9;
        private Label LabelParameterName10;
        private Label LabelParameterName11;
        private Label LabelParameterName12;
        private Label LabelParameterName13;
        private Label LabelParameterName14;
        private Label LabelParameterName15;
        private Label LabelParameterName16;
        private Label LabelParameterName17;
        private Label LabelParameterName18;
        private Label LabelParameterName19;
        private Label LabelParameterName20;
        private Label LabelParameterName21;

        private Label[] LabelParamVal;
        private Label LabelParamVal0;
        private Label LabelParamVal1;
        private Label LabelParamVal2;
        private Label LabelParamVal3;
        private Label LabelParamVal4;
        private Label LabelParamVal5;
        private Label LabelParamVal6;
        private Label LabelParamVal7;
        private Label LabelParamVal8;
        private Label LabelParamVal9;
        private Label LabelParamVal10;
        private Label LabelParamVal11;
        private Label LabelParamVal12;
        private Label LabelParamVal13;
        private Label LabelParamVal14;
        private Label LabelParamVal15;
        private Label LabelParamVal16;
        private Label LabelParamVal17;
        private Label LabelParamVal18;
        private Label LabelParamVal19;
        private Label LabelParamVal20;
        private Label LabelParamVal21;

        private Label[] LabelPin;
        private Label LabelPin0;
        private Label LabelPin6;
        private Label LabelPin9;
        private Label LabelPin11;
        private Label LabelPin12;
        private Label LabelPin13;
        private Label LabelPin14;
        private Label LabelPin15;

        private Label[] LabelPeriodicMessageId;
        private Label LabelPeriodicMessageId0;
        private Label LabelPeriodicMessageId1;
        private Label LabelPeriodicMessageId2;
        private Label LabelPeriodicMessageId3;
        private Label LabelPeriodicMessageId4;
        private Label LabelPeriodicMessageId5;
        private Label LabelPeriodicMessageId6;
        private Label LabelPeriodicMessageId7;
        private Label LabelPeriodicMessageId8;
        private Label LabelPeriodicMessageId9;

        private Label[] LabelPeriodicMessage;
        private Label LabelPeriodicMessage0;
        private Label LabelPeriodicMessage1;
        private Label LabelPeriodicMessage2;
        private Label LabelPeriodicMessage3;
        private Label LabelPeriodicMessage4;
        private Label LabelPeriodicMessage5;
        private Label LabelPeriodicMessage6;
        private Label LabelPeriodicMessage7;
        private Label LabelPeriodicMessage8;
        private Label LabelPeriodicMessage9;

        private Label[] LabelChannel;
        private Label LabelChannel1;
        private Label LabelChannel2;
        private Label LabelChannel3;
        private Label LabelChannel4;
        private Label LabelChannel5;
        private Label LabelChannel6;

        private ProgressBar[] ProgressBarAnalog;
        private ProgressBar ProgressBarAnalog0;
        private ProgressBar ProgressBarAnalog1;
        private ProgressBar ProgressBarAnalog2;
        private ProgressBar ProgressBarAnalog3;
        private ProgressBar ProgressBarAnalog4;
        private ProgressBar ProgressBarAnalog5;
        private ProgressBar ProgressBarAnalog6;
        private ProgressBar ProgressBarAnalog7;
        private ProgressBar ProgressBarAnalog8;
        private ProgressBar ProgressBarAnalog9;
        private ProgressBar ProgressBarAnalog10;
        private ProgressBar ProgressBarAnalog11;
        private ProgressBar ProgressBarAnalog12;
        private ProgressBar ProgressBarAnalog13;
        private ProgressBar ProgressBarAnalog14;
        private ProgressBar ProgressBarAnalog15;
        private ProgressBar ProgressBarAnalog16;
        private ProgressBar ProgressBarAnalog17;
        private ProgressBar ProgressBarAnalog18;
        private ProgressBar ProgressBarAnalog19;
        private ProgressBar ProgressBarAnalog20;
        private ProgressBar ProgressBarAnalog21;
        private ProgressBar ProgressBarAnalog22;
        private ProgressBar ProgressBarAnalog23;
        private ProgressBar ProgressBarAnalog24;
        private ProgressBar ProgressBarAnalog25;
        private ProgressBar ProgressBarAnalog26;
        private ProgressBar ProgressBarAnalog27;
        private ProgressBar ProgressBarAnalog28;
        private ProgressBar ProgressBarAnalog29;
        private ProgressBar ProgressBarAnalog30;
        private ProgressBar ProgressBarAnalog31;

        private RadioButton[] RadioButtonPin;
        private RadioButton RadioButtonPin0;
        private RadioButton RadioButtonPin6;
        private RadioButton RadioButtonPin9;
        private RadioButton RadioButtonPin11;
        private RadioButton RadioButtonPin12;
        private RadioButton RadioButtonPin13;
        private RadioButton RadioButtonPin14;
        private RadioButton RadioButtonPin15;

        private TextBox[] TextBoxFilterFlow;
        private TextBox TextBoxFilterFlow0;
        private TextBox TextBoxFilterFlow1;
        private TextBox TextBoxFilterFlow2;
        private TextBox TextBoxFilterFlow3;
        private TextBox TextBoxFilterFlow4;
        private TextBox TextBoxFilterFlow5;
        private TextBox TextBoxFilterFlow6;
        private TextBox TextBoxFilterFlow7;
        private TextBox TextBoxFilterFlow8;
        private TextBox TextBoxFilterFlow9;

        private TextBox[] TextBoxFilterFlags;
        private TextBox TextBoxFilterFlags0;
        private TextBox TextBoxFilterFlags1;
        private TextBox TextBoxFilterFlags2;
        private TextBox TextBoxFilterFlags3;
        private TextBox TextBoxFilterFlags4;
        private TextBox TextBoxFilterFlags5;
        private TextBox TextBoxFilterFlags6;
        private TextBox TextBoxFilterFlags7;
        private TextBox TextBoxFilterFlags8;
        private TextBox TextBoxFilterFlags9;

        private TextBox[] TextBoxFilterPatt;
        private TextBox TextBoxFilterPatt0;
        private TextBox TextBoxFilterPatt1;
        private TextBox TextBoxFilterPatt2;
        private TextBox TextBoxFilterPatt3;
        private TextBox TextBoxFilterPatt4;
        private TextBox TextBoxFilterPatt5;
        private TextBox TextBoxFilterPatt6;
        private TextBox TextBoxFilterPatt7;
        private TextBox TextBoxFilterPatt8;
        private TextBox TextBoxFilterPatt9;

        private TextBox[] TextBoxFilterMask;
        private TextBox TextBoxFilterMask0;
        private TextBox TextBoxFilterMask1;
        private TextBox TextBoxFilterMask2;
        private TextBox TextBoxFilterMask3;
        private TextBox TextBoxFilterMask4;
        private TextBox TextBoxFilterMask5;
        private TextBox TextBoxFilterMask6;
        private TextBox TextBoxFilterMask7;
        private TextBox TextBoxFilterMask8;
        private TextBox TextBoxFilterMask9;

        private TextBox[] TextBoxFunctionalMessage;
        private TextBox TextBoxFunctionalMessage0;
        private TextBox TextBoxFunctionalMessage1;
        private TextBox TextBoxFunctionalMessage2;
        private TextBox TextBoxFunctionalMessage3;
        private TextBox TextBoxFunctionalMessage4;
        private TextBox TextBoxFunctionalMessage5;
        private TextBox TextBoxFunctionalMessage6;
        private TextBox TextBoxFunctionalMessage7;
        private TextBox TextBoxFunctionalMessage8;
        private TextBox TextBoxFunctionalMessage9;
        private TextBox TextBoxFunctionalMessage10;
        private TextBox TextBoxFunctionalMessage11;
        private TextBox TextBoxFunctionalMessage12;
        private TextBox TextBoxFunctionalMessage13;
        private TextBox TextBoxFunctionalMessage14;
        private TextBox TextBoxFunctionalMessage15;
        private TextBox TextBoxFunctionalMessage16;
        private TextBox TextBoxFunctionalMessage17;
        private TextBox TextBoxFunctionalMessage18;
        private TextBox TextBoxFunctionalMessage19;
        private TextBox TextBoxFunctionalMessage20;
        private TextBox TextBoxFunctionalMessage21;
        private TextBox TextBoxFunctionalMessage22;
        private TextBox TextBoxFunctionalMessage23;
        private TextBox TextBoxFunctionalMessage24;
        private TextBox TextBoxFunctionalMessage25;
        private TextBox TextBoxFunctionalMessage26;
        private TextBox TextBoxFunctionalMessage27;
        private TextBox TextBoxFunctionalMessage28;
        private TextBox TextBoxFunctionalMessage29;
        private TextBox TextBoxFunctionalMessage30;
        private TextBox TextBoxFunctionalMessage31;

        private TextBox[] TextBoxParamVal;
        private TextBox TextBoxParamVal0;
        private TextBox TextBoxParamVal1;
        private TextBox TextBoxParamVal2;
        private TextBox TextBoxParamVal3;
        private TextBox TextBoxParamVal4;
        private TextBox TextBoxParamVal5;
        private TextBox TextBoxParamVal6;
        private TextBox TextBoxParamVal7;
        private TextBox TextBoxParamVal8;
        private TextBox TextBoxParamVal9;
        private TextBox TextBoxParamVal10;
        private TextBox TextBoxParamVal11;
        private TextBox TextBoxParamVal12;
        private TextBox TextBoxParamVal13;
        private TextBox TextBoxParamVal14;
        private TextBox TextBoxParamVal15;
        private TextBox TextBoxParamVal16;
        private TextBox TextBoxParamVal17;
        private TextBox TextBoxParamVal18;
        private TextBox TextBoxParamVal19;
        private TextBox TextBoxParamVal20;
        private TextBox TextBoxParamVal21;

        private TextBox[] TextBoxPeriodicMessageFlags;
        private TextBox TextBoxPeriodicMessageFlags0;
        private TextBox TextBoxPeriodicMessageFlags1;
        private TextBox TextBoxPeriodicMessageFlags2;
        private TextBox TextBoxPeriodicMessageFlags3;
        private TextBox TextBoxPeriodicMessageFlags4;
        private TextBox TextBoxPeriodicMessageFlags5;
        private TextBox TextBoxPeriodicMessageFlags6;
        private TextBox TextBoxPeriodicMessageFlags7;
        private TextBox TextBoxPeriodicMessageFlags8;
        private TextBox TextBoxPeriodicMessageFlags9;

        private TextBox[] TextBoxPeriodicMessage;
        private TextBox TextBoxPeriodicMessage0;
        private TextBox TextBoxPeriodicMessage1;
        private TextBox TextBoxPeriodicMessage2;
        private TextBox TextBoxPeriodicMessage3;
        private TextBox TextBoxPeriodicMessage4;
        private TextBox TextBoxPeriodicMessage5;
        private TextBox TextBoxPeriodicMessage6;
        private TextBox TextBoxPeriodicMessage7;
        private TextBox TextBoxPeriodicMessage8;
        private TextBox TextBoxPeriodicMessage9;

        private TextBox[] TextBoxPeriodicMessageInterval;
        private TextBox TextBoxPeriodicMessageInterval9;
        private TextBox TextBoxPeriodicMessageInterval8;
        private TextBox TextBoxPeriodicMessageInterval7;
        private TextBox TextBoxPeriodicMessageInterval6;
        private TextBox TextBoxPeriodicMessageInterval5;
        private TextBox TextBoxPeriodicMessageInterval4;
        private TextBox TextBoxPeriodicMessageInterval3;
        private TextBox TextBoxPeriodicMessageInterval2;
        private TextBox TextBoxPeriodicMessageInterval1;
        private TextBox TextBoxPeriodicMessageInterval0;

        private ToolStripStatusLabel ToolStripStatusLabel2;
        private ContextMenuStrip ContextMenuStripScratchPad;
        private ToolStripMenuItem MenuScratchPadUndo;
        private ToolStripSeparator MenuScratchPadSeparator1;
        private ToolStripMenuItem MenuScratchPadCut;
        private ToolStripMenuItem MenuScratchPadCopy;
        private ToolStripMenuItem MenuScratchPadPaste;
        private ToolStripSeparator MenuScratchPadSeparator2;
        private ToolStripMenuItem MenuScratchPadSelectAll;
        private ToolStripSeparator MenuScratchPadSeparator3;
        private ToolStripMenuItem MenuFileMsgOutLoad;
        private ToolStripMenuItem MenuScratchPadAddToOutgoingMessageSet;
        private ToolStripMenuItem MenuFlagsSetDefault;
        private ToolStripMenuItem MenuEditScratchAddToOutgoingMessageSet;
        private ToolStripMenuItem MenuFlagsDelete;
        private ToolStripMenuItem MenuScratchPadDelete;
        private ContextMenuStrip ContextMenuStripTextBox;
        private ToolStripMenuItem MenuTextBoxUndo;
        private ToolStripSeparator MenuTextBoxSeparator1;
        private ToolStripMenuItem MenuTextBoxCut;
        private ToolStripMenuItem MenuTextBoxCopy;
        private ToolStripMenuItem MenuTextBoxPaste;
        private ToolStripMenuItem MenuTextBoxDelete;
        private ToolStripSeparator MenuTextBoxSeparator2;
        private ToolStripMenuItem MenuTextBoxSelectAll;
        private ComboBox ComboBoxAnalogChannel;
        private ToolTip ToolTipMessageIn;
        private ToolStripMenuItem MenuFileFilterLoad;
        private ToolStripMenuItem MenuFileFilterSave;
        private ToolStripMenuItem MenuFilePeriodicMessageSave;
        private Panel PanelAnalog;
        private ContextMenuStrip ContextMenuStripResults;
        private ToolStripMenuItem MenuResultsCopyLine;
        private ToolStripMenuItem MenuResultsDelete;
        private ToolStripMenuItem MenuResultsSelectAll;
        private ToolStripSeparator MenuResultsSpace1;
        private ToolStripMenuItem MenuResultsClear;
        private GroupBox GroupBoxAnalogConfig;
        private TextBox TextBoxAnalogRate;
        private Panel PanelAudioChannel;
        private ComboBox ComboBoxAnalogRateMode;
        private Button ButtonClaimJ1939Address;
        private ToolStripMenuItem MenuFilePeriodicMessageLoad;
        private ToolStripSeparator MenuFileSpace1;
        private ToolStripMenuItem MenuFileMsgInSaveSelected;
        private ToolStripMenuItem MenuFileMsgInSaveAll;
        private ToolStripMenuItem MenuFileMsgOutSave;
        private ToolStripSeparator MenuFileSpace2;
        private ToolStripMenuItem MenuFileExit;
        private ToolStripMenuItem MenuFile;
        private ToolStripMenuItem MenuEditUndo;
        private ToolStripSeparator MenuEditSpace1;
        private ToolStripMenuItem MenuEditCut;
        private ToolStripMenuItem MenuEditCopy;
        private ToolStripMenuItem MenuEditCopyLine;
        private ToolStripMenuItem MenuEditMsgInCopyData;
        private ToolStripMenuItem MenuEditPaste;
        private ToolStripMenuItem MenuEditDelete;
        private ToolStripSeparator MenuEditSpace2;
        private ToolStripMenuItem MenuEditMsgInMakeFilter;
        private ToolStripMenuItem MenuEditFlagsEdit;
        private ToolStripMenuItem MenuEditRxStatus;
        private ToolStripMenuItem MenuEditFlagsClear;
        private ToolStripMenuItem MenuEditFlagsSetDefault;
        private ToolStripMenuItem MenuEditMsgOutEditMessage;
        private ToolStripMenuItem MenuEditMsgOutAddMessage;
        private ToolStripMenuItem MenuEditMsgOutCopyToScratchPad;
        private ToolStripMenuItem MenuEditMsgOutMakePeriodicMessage;
        private ToolStripMenuItem MenuEditSelectAll;
        private ToolStripMenuItem MenuEditClear;
        private ToolStripMenuItem MenuEdit;
        private ToolStripMenuItem MenuHelpContents;
        private ToolStripMenuItem MenuHelpAbout;
        private ToolStripMenuItem MenuHelp;
        private ToolStripMenuItem MenuMsgInCopyLine;
        private ToolStripMenuItem MenuMsgInCopyData;
        private ToolStripMenuItem MenuMsgInMakeFilter;
        private ToolStripMenuItem MenuMsgInRxStatus;
        private ToolStripMenuItem MenuMsgInSelectAll;
        private ToolStripSeparator MenuMsgInSpace1;
        private ToolStripMenuItem MenuMsgInSaveSelected;
        private ToolStripMenuItem MenuMsgInSaveAll;
        private ToolStripMenuItem MenuMsgInClear;

        private ToolStripMenuItem MenuMsgOutDelete;
        private ToolStripMenuItem MenuMsgOutEditMessage;
        private ToolStripMenuItem MenuMsgOutAddMessage;
        private ToolStripMenuItem MenuMsgOutCopyToScratchPad;
        private ToolStripMenuItem MenuMsgOutMakePeriodicMessage;
        private ToolStripSeparator MenuMsgOutSpace1;
        private ToolStripMenuItem MenuMsgOutSave;
        private ToolStripMenuItem MenuMsgOutLoad;
        private ToolStripMenuItem MenuMsgOutClear;

        private ToolStripMenuItem MenuFlagsUndo;
        private ToolStripSeparator MenuFlagsSpace1;
        private ToolStripMenuItem MenuFlagsCut;
        private ToolStripMenuItem MenuFlagsCopy;
        private ToolStripMenuItem MenuFlagsPaste;
        private ToolStripSeparator MenuFlagsSpace2;
        private ToolStripMenuItem MenuFlagsSelectAll;
        private ToolStripSeparator MenuFlagsSpace3;
        private ToolStripMenuItem MenuFlagsEdit;
        private ToolStripMenuItem MenuFlagsClear;

        private MenuStrip MenuStripMain;
        private ToolStripStatusLabel ToolStripStatusLabel1;
        private StatusStrip StatusStrip1;
        private ComboBox ComboBoxDevice;
        private Button ButtonDiscover;
        private Button ButtonCloseBox;
        private Button ButtonOpenBox;
        private GroupBox GroupBoxDevices;
        private ComboBox ComboBoxAPI;
        private Button ButtonLoadDLL;
        private TextBox TextBoxDllPath;

        private GroupBox GroupBoxAPIs;
        private ComboBox ComboBoxPins;
        private ComboBox ComboBoxConnector;
        private CheckBox CheckBoxMixedMode;
        private TextBox TextBoxConnectFlags;
        private ComboBox ComboBoxBaudRate;
        private Button ButtonConnect;
        private Button ButtonDisconnect;
        private ComboBox ComboBoxConnectChannel;

        private GroupBox GroupBoxConnect;

        private GroupBox GroupBoxJ2534Info;
        private ComboBox ComboBoxMessageChannel;
        private CheckBox CheckBoxPadMessage;
        private TextBox TextBoxTimeOut;
        private OpenFileDialog DialogFileOpen;
        private SaveFileDialog DialogFileSave;
        private TextBox TextBoxOutFlags;
        private Button ButtonClearRx;
        private Button ButtonClearTx;
        private TextBox TextBoxMessageOut;
        private Button ButtonSend;
        private Button ButtonReceive;
        private TextBox TextBoxReadRate;
        private Button ButtonClearList;
        private Button ButtonClearAllPeriodicMessages;
        private Button ButtonCancelPeriodicMessages;
        private Button ButtonApplyPeriodicMessages;

        private Label LabelPeriodicMessageChannel;
        private Label LabelPeriodicMessageIds;
        private Label LabelPeriodicMessageFlags;
        private Label LabelPMMessage;
        private Label LvInLocator;
        private Label LvOutLocator;
        private Label LabelPeriodicMessageInterval;
        private Label LabelPeriodicMessageDelete;
        private Label LabelScratchPad;
        private Label LabelTO;
        private Label LabelReadRate;
        private Label LabelProtSupport;
        private Label LabelJ2534Info;
        private Label LabelPins;
        private Label LabelConn;
        private Label LabelConnectFlags;
        private Label LabelComboDevice;
        private Label LabelAPI;
        private Label LabelVendor;
        private Label LabelDevice;
        private Label LabelDllPath;
        private Label LabelDllName;
        private Label LabelConnectChannel;
        private Label LabelAnalogChannel;
        private Label LabelBaud;
        private Label LabelDeviceInfo;
        private Label LabelMsgFlags;
        private Label LabelVoltWarning;
        private Label LabelMaxSample;
        private Label LabelAnalogRate;

        private Button ButtonCreatePassFilter;
        private Button ButtonClearAllFilter;
        private Button ButtonCancelFilter;
        private Button ButtonApplyFilter;

        private Label LabelFilterIds;
        private Label LabelFilterType;
        private Label LabelFilterFlags;
        private Label LabelFilterFlow;
        private Label LabelFilterPatt;
        private Label LabelFilterMask;

        private ComboBox ComboBoxAvailableDevice;
        private ComboBox ComboBoxAvailableChannel;
        private Button ButtonSetConfig;
        private Button ButtonClearConfig;

        private Label LabelParameters0;
        private Label LabelValues0;
        private Label LabelMod0;
        private Label LabelParameters1;
        private Label LabelValues1;
        private Label LabelMod1;
        private Button ButtonExecuteFastInit;
        private TextBox TextBoxFIMessage;
        private TextBox TextBoxFIFlags;
        private Label LabelFIRXTitle;
        private Label LabelFITXFlags;
        private Label LabelFIRxStatus;
        private Label LabelFIResponse;
        private Label LabelFIMsgResp;
        private Label LabelFIMsgData;
        private GroupBox GroupBoxFastInit;
        private TextBox TextBox5BInitECU;
        private Button ButtonExecute5BInit;
        private Label LabelKWlabel1;
        private Label LabelKWlabel0;
        private Label Label5BECU;
        private Label Label5BKeyWord0;
        private Label Label5BKeyWord1;
        private GroupBox GroupBox5BInit;
        private Button ButtonApplyFunctionalMessages;
        private Button ButtonCancelFunctionalMessages;
        private Button ButtonClearAllFunctionalMessages;
        private Label LabelFunctionalMessageModify;
        private Label LabelFunctionalMessageValues;
        private Label LabelAnalogLow1;
        private Label LabelAnalogHigh1;
        private Label LabelAnalogHigh0;
        private Label LabelAnalogLow0;
        private Label LabelAnalogReading;
        private GroupBox GroupBoxAnalog;
        private Button ButtonReadBatt;
        private Label LabelBattRead;
        private GroupBox GroupBoxBattVoltage;
        private Button ButtonSetVoltage;
        private Button ButtonReadVolt;
        private TextBox TextBoxVoltSetting;
        private RadioButton optSet;
        private RadioButton optShortToGround;
        private RadioButton optVoltOff;
        private Label LabelVoltRead;
        private Label LabelMV;
        private GroupBox GroupBoxVolt;

        private GroupBox GroupBoxPin;
        private GroupBox GroupBoxProgVoltage;

        private ColumnHeader ListViewResultsColumnHeader1;
        private ColumnHeader ListViewResultsColumnHeader2;
        private ListView ListViewResults;

        private TabControl TabControl1;
        private TabPage TabPageConnect;
        private TabPage TabPageMessages;
        private TabPage TabPagePeriodicMessages;
        private TabPage TabPageFilters;
        private TabPage TabPageConfig;
        private TabPage TabPageInit;
        private TabPage TabPageFunctionalMessages;
        private TabPage TabPageAnalog;
        private TabPage TabPageResults;

        private ContextMenuStrip ContextMenuStripMessageIn;
        private ContextMenuStrip ContextMenuStripMessageOut;
        private ContextMenuStrip ContextMenuStripFlags;


        private static FormJ2534 _DefaultInstance;
        public static FormJ2534 DefaultInstance
        {
            get
            {
                if (_DefaultInstance == null || _DefaultInstance.IsDisposed)
                    _DefaultInstance = new FormJ2534();

                return _DefaultInstance;
            }
        }
    }
}