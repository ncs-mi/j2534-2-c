﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace NCS_J2534_2_Tool
{
    public class J2534_API : IDisposable
    {
        private IntPtr dllPointer;

        public PassThruOpenDelegate PassThruOpen;
        public PassThruCloseDelegate PassThruClose;
        public PassThruConnectDelegate PassThruConnect;
        public PassThruDisconnectDelegate PassThruDisconnect;
        public PassThruReadMsgsDelegate PassThruReadMsgs;
        public PassThruWriteMsgsDelegate PassThruWriteMsgs;
        public PassThruStartPeriodicMsgDelegate PassThruStartPeriodicMsg;
        public PassThruStopPeriodicMsgDelegate PassThruStopPeriodicMsg;
        public PassThruStartMsgFilterDelegate PassThruStartMsgFilter;
        public PassThruStopMsgFilterDelegate PassThruStopMsgFilter;
        public PassThruSetProgrammingVoltageDelegate PassThruSetProgrammingVoltage;
        public PassThruReadVersionDelegate PassThruReadVersion;
        public PassThruGetLastErrorDelegate PassThruGetLastError;
        public PassThruIoctlDelegate PassThruIoctl;

        public string KeyName {get; set;}
        public string VendorName {get; set;}
        public string DeviceName {get; set;}
        public string LibraryName {get; set;}
        public string ConfigAppPath {get; set;}
        public DeviceType DeviceType {get; set;}
        public bool DllIsLoaded
        {
            get
            {
                return dllPointer != IntPtr.Zero;
            }
        }

        public const int DLL_NO_ERROR = 0x0;
        public const int ERR_NO_PTOPEN = 0x1;
        public const int ERR_NO_PTCLOSE = 0x2;
        public const int ERR_NO_PTCONNECT = 0x4;
        public const int ERR_NO_PTDISCONNECT = 0x8;
        public const int ERR_NO_PTREADMSGS = 0x10;
        public const int ERR_NO_PTWRITEMSGS = 0x20;
        public const int ERR_NO_PTSTARTPERIODICMSG = 0x40;
        public const int ERR_NO_PTSTOPPERIODICMSG = 0x80;
        public const int ERR_NO_PTSTARTMSGFILTER = 0x100;
        public const int ERR_NO_PTSTOPMSGFILTER = 0x200;
        public const int ERR_NO_PTSETPROGRAMMINGVOLTAGE = 0x400;
        public const int ERR_NO_PTREADVERSION = 0x800;
        public const int ERR_NO_PTGETLASTERROR = 0x1000;
        public const int ERR_NO_PTIOCTL = 0x2000;
        public const int ERR_NO_FUNCTIONS = 0x3FFF;
        public const int ERR_NO_DLL = -1;
        public const int ERR_WRONG_DLL_VER = -2;
        public const int ERR_FUNC_MISSING = -3;

        public int LoadJ2534dll()
        {
            int result = ERR_NO_DLL;
            if (string.IsNullOrWhiteSpace(this.LibraryName))
            {
                return result;
            }
            if (dllPointer != IntPtr.Zero)
            {
                return result;
            }

            dllPointer = NativeMethods.LoadLibrary(this.LibraryName);
            if (dllPointer == IntPtr.Zero)
            {
                return result;
            }

            IntPtr passThruOpenPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruOpen");
            IntPtr passThruClosePointer = NativeMethods.GetProcAddress(dllPointer, "PassThruClose");
            IntPtr passThruConnectPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruConnect");
            IntPtr passThruDisconnectPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruDisconnect");
            IntPtr passThruReadMsgsPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruReadMsgs");
            IntPtr passThruWriteMsgsPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruWriteMsgs");
            IntPtr passThruStartPeriodicMsgPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruStartPeriodicMsg");
            IntPtr passThruStopPeriodicMsgPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruStopPeriodicMsg");
            IntPtr passThruStartMsgFilterPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruStartMsgFilter");
            IntPtr passThruStopMsgFilterPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruStopMsgFilter");
            IntPtr passThruSetProgrammingVoltagePointer = NativeMethods.GetProcAddress(dllPointer, "PassThruSetProgrammingVoltage");
            IntPtr passThruReadVersionPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruReadVersion");
            IntPtr passThruGetLastErrorPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruGetLastError");
            IntPtr passThruIoctlPointer = NativeMethods.GetProcAddress(dllPointer, "PassThruIoctl");
            result = DLL_NO_ERROR;

            if (passThruOpenPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTOPEN;
            }
            if (passThruClosePointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTCLOSE;
            }
            if (passThruConnectPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTCONNECT;
            }
            if (passThruDisconnectPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTDISCONNECT;
            }
            if (passThruReadMsgsPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTREADMSGS;
            }
            if (passThruWriteMsgsPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTWRITEMSGS;
            }
            if (passThruStartPeriodicMsgPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTSTARTPERIODICMSG;
            }
            if (passThruStopPeriodicMsgPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTSTOPPERIODICMSG;
            }
            if (passThruStartMsgFilterPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTSTARTMSGFILTER;
            }
            if (passThruStopMsgFilterPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTSTOPMSGFILTER;
            }
            if (passThruSetProgrammingVoltagePointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTSETPROGRAMMINGVOLTAGE;
            }
            if (passThruReadVersionPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTREADVERSION;
            }
            if (passThruGetLastErrorPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTGETLASTERROR;
            }
            if (passThruIoctlPointer == IntPtr.Zero)
            {
                result |= ERR_NO_PTIOCTL;
            }
            if (result != DLL_NO_ERROR)
            {
                return result;
            }

            PassThruOpen = (PassThruOpenDelegate)Marshal.GetDelegateForFunctionPointer(passThruOpenPointer, typeof(PassThruOpenDelegate));
            PassThruClose = (PassThruCloseDelegate)Marshal.GetDelegateForFunctionPointer(passThruClosePointer, typeof(PassThruCloseDelegate));
            PassThruConnect = (PassThruConnectDelegate)Marshal.GetDelegateForFunctionPointer(passThruConnectPointer, typeof(PassThruConnectDelegate));
            PassThruDisconnect = (PassThruDisconnectDelegate)Marshal.GetDelegateForFunctionPointer(passThruDisconnectPointer, typeof(PassThruDisconnectDelegate));
            PassThruReadMsgs = (PassThruReadMsgsDelegate)Marshal.GetDelegateForFunctionPointer(passThruReadMsgsPointer, typeof(PassThruReadMsgsDelegate));
            PassThruWriteMsgs = (PassThruWriteMsgsDelegate)Marshal.GetDelegateForFunctionPointer(passThruWriteMsgsPointer, typeof(PassThruWriteMsgsDelegate));
            PassThruStartPeriodicMsg = (PassThruStartPeriodicMsgDelegate)Marshal.GetDelegateForFunctionPointer(passThruStartPeriodicMsgPointer, typeof(PassThruStartPeriodicMsgDelegate));
            PassThruStopPeriodicMsg = (PassThruStopPeriodicMsgDelegate)Marshal.GetDelegateForFunctionPointer(passThruStopPeriodicMsgPointer, typeof(PassThruStopPeriodicMsgDelegate));
            PassThruStartMsgFilter = (PassThruStartMsgFilterDelegate)Marshal.GetDelegateForFunctionPointer(passThruStartMsgFilterPointer, typeof(PassThruStartMsgFilterDelegate));
            PassThruStopMsgFilter = (PassThruStopMsgFilterDelegate)Marshal.GetDelegateForFunctionPointer(passThruStopMsgFilterPointer, typeof(PassThruStopMsgFilterDelegate));
            PassThruSetProgrammingVoltage = (PassThruSetProgrammingVoltageDelegate)Marshal.GetDelegateForFunctionPointer(passThruSetProgrammingVoltagePointer, typeof(PassThruSetProgrammingVoltageDelegate));
            PassThruReadVersion = (PassThruReadVersionDelegate)Marshal.GetDelegateForFunctionPointer(passThruReadVersionPointer, typeof(PassThruReadVersionDelegate));
            PassThruGetLastError = (PassThruGetLastErrorDelegate)Marshal.GetDelegateForFunctionPointer(passThruGetLastErrorPointer, typeof(PassThruGetLastErrorDelegate));
            PassThruIoctl = (PassThruIoctlDelegate)Marshal.GetDelegateForFunctionPointer(passThruIoctlPointer, typeof(PassThruIoctlDelegate));

            return result;
        }

        public int UnloadJ2534Dll()
        {
            int result = 0;
            for (int i = 0; i <= short.MaxValue; i++)
            {
                result = NativeMethods.FreeLibrary(dllPointer);
                if (result == 0)
                {
                    break;
                }
            }
            dllPointer = IntPtr.Zero;
            PassThruOpen = null;
            PassThruClose = null;
            PassThruConnect = null;
            PassThruDisconnect = null;
            PassThruReadMsgs = null;
            PassThruWriteMsgs = null;
            PassThruStartPeriodicMsg = null;
            PassThruStopPeriodicMsg = null;
            PassThruStartMsgFilter = null;
            PassThruStopMsgFilter = null;
            PassThruSetProgrammingVoltage = null;
            PassThruReadVersion = null;
            PassThruGetLastError = null;
            PassThruIoctl = null;
            return result;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return this.KeyName;
        }

        private bool disposedValue; // To detect redundant calls
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //dispose managed state (managed objects).
                }
                UnloadJ2534Dll();
            }
            disposedValue = true;
        }

        ~J2534_API()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }


}