﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace NCS_J2534_2_Tool
{
    public partial class FormSplash
    {
        public FormSplash()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            LabelVersion.Text = string.Concat("Version ", My.MyApplication.Application.Info.Version.Major, ".", My.MyApplication.Application.Info.Version.Minor.ToString("00"));
            if (PublicDeclarations.betaVersion > 0)
            {
                LabelVersion.Text = string.Concat(LabelVersion.Text, " (ß", PublicDeclarations.betaVersion.ToString(), ")");
            }
            LabelProductName.Text = My.MyApplication.Application.Info.ProductName;
            LabelCompany.Text = My.MyApplication.Application.Info.CompanyName;
            LabelCopyright.Text = My.MyApplication.Application.Info.Copyright;
            TextBoxLicense.Select(0, 0);
            base.OnLoad(e);
        }

        private void FormSplash_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormSplash_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == KeyChars.Escape)
            {
                this.Close();
            }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            this.Close();
        }

    }
}