﻿// NCS J2534-2 Tool
// Copyright © 2017, 2018 National Control Systems, Inc.
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// National Control Systems, Inc.
// 10737 Hamburg Rd
// Hamburg, MI 48139
// 810-231-2901
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Collections.Concurrent;
using static NCS_J2534_2_Tool.J2534ConfigParameterId;

namespace NCS_J2534_2_Tool
{
    public static class J2534Constants
    {
        public const int MAX_FILTER = 9;
        public const int MAX_PM = 9;
        public const int MAX_FUNC_MSG = 31;
        public const int MAX_ANALOG_CH = 31;

        public const uint MIN_CH = 1;
        public const uint MAX_CH = 128;
        public const uint MAX_IN = 32;
    }

    public class J2534Collections
    {
        public static J2534Filter[] FilterEditBuffer = new J2534Filter[J2534Constants.MAX_FILTER + 1];
        public static J2534PeriodicMessage[] PeriodicMessageEditBuffer = new J2534PeriodicMessage[J2534Constants.MAX_PM + 1];
        public static List<J2534_API> J2534_APIs = new List<J2534_API>();
        public static ConcurrentDictionary<int, J2534Device> J2534Devices = new ConcurrentDictionary<int, J2534Device>();
        private J2534Collections()
        { 
        }
    }

    public class ChannelSetup
    { 
        public static readonly ChannelSetupParameters J1850VPW = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.J1850VPW_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.J1850VPW_PS_J1962,
            BaseId = J2534ProtocolId.J1850VPW,
            PinSwitchedId = J2534ProtocolId.J1850VPW_PS,
            FirstCHxId = J2534ProtocolId.J1850VPW_CH1
        };

        public static readonly ChannelSetupParametersK J1850PWM = new ChannelSetupParametersK
        {
            SupportParameter = J2534GetDeviceInfoParameter.J1850PWM_SUPPORTED,
            J1962_K_PinsParameter = J2534GetDeviceInfoParameter.J1850PWM_PS_J1962,
            BaseId = J2534ProtocolId.J1850PWM,
            PinSwitchedId = J2534ProtocolId.J1850PWM_PS,
            FirstCHxId = J2534ProtocolId.J1850PWM_CH1
        };

        public static readonly ChannelSetupParametersK ISO9141 = new ChannelSetupParametersK
        {
            SupportParameter = J2534GetDeviceInfoParameter.ISO9141_SUPPORTED,
            J1962_K_PinsParameter = J2534GetDeviceInfoParameter.ISO9141_PS_K_LINE_J1962,
            J1962_L_PinsParameter = J2534GetDeviceInfoParameter.ISO9141_PS_L_LINE_J1962,
            J1939_K_PinsParameter = J2534GetDeviceInfoParameter.ISO9141_PS_K_LINE_J1939,
            J1939_L_PinsParameter = J2534GetDeviceInfoParameter.ISO9141_PS_L_LINE_J1939,
            BaseId = J2534ProtocolId.ISO9141,
            PinSwitchedId = J2534ProtocolId.ISO9141_PS,
            FirstCHxId = J2534ProtocolId.ISO9141_CH1
        };

        public static readonly ChannelSetupParametersK ISO14230 = new ChannelSetupParametersK
        {
            SupportParameter = J2534GetDeviceInfoParameter.ISO14230_SUPPORTED,
            J1962_K_PinsParameter = J2534GetDeviceInfoParameter.ISO14230_PS_K_LINE_J1962,
            J1962_L_PinsParameter = J2534GetDeviceInfoParameter.ISO14230_PS_L_LINE_J1962,
            J1939_K_PinsParameter = J2534GetDeviceInfoParameter.ISO14230_PS_K_LINE_J1939,
            J1939_L_PinsParameter = J2534GetDeviceInfoParameter.ISO14230_PS_L_LINE_J1939,
            BaseId = J2534ProtocolId.ISO14230,
            PinSwitchedId = J2534ProtocolId.ISO14230_PS,
            FirstCHxId = J2534ProtocolId.ISO14230_CH1
        };

        public static readonly ChannelSetupParameters CAN = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.CAN_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.CAN_PS_J1962,
            BaseId = J2534ProtocolId.CAN,
            PinSwitchedId = J2534ProtocolId.CAN_PS,
            FirstCHxId = J2534ProtocolId.CAN_CH1
        };

        public static readonly ChannelSetupParameters SW_CAN = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.SW_CAN_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.SW_CAN_PS_J1962,
            PinSwitchedId = J2534ProtocolId.SW_CAN_PS,
            FirstCHxId = J2534ProtocolId.SW_CAN_CAN_CH1
        };

        public static readonly ChannelSetupParameters FT_CAN = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.FT_CAN_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.FT_CAN_PS_J1962,
            PinSwitchedId = J2534ProtocolId.FT_CAN_PS,
            FirstCHxId = J2534ProtocolId.FT_CAN_CH1
        };

        public static readonly ChannelSetupParameters ISO15765 = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.ISO15765_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.ISO15765_PS_J1962,
            BaseId = J2534ProtocolId.ISO15765,
            PinSwitchedId = J2534ProtocolId.ISO15765_PS,
            FirstCHxId = J2534ProtocolId.ISO15765_CH1
        };

        public static readonly ChannelSetupParameters SW_ISO15765 = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.SW_ISO15765_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.SW_ISO15765_PS_J1962,
            PinSwitchedId = J2534ProtocolId.SW_ISO15765_PS,
            FirstCHxId = J2534ProtocolId.SW_CAN_ISO15765_CH1
        };

        public static readonly ChannelSetupParameters FT_ISO15765 = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.FT_ISO15765_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.FT_ISO15765_PS_J1962,
            PinSwitchedId = J2534ProtocolId.FT_ISO15765_PS,
            FirstCHxId = J2534ProtocolId.FT_ISO15765_CH1
        };

        public static readonly ChannelSetupParameters SCI_A_ENGINE = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.SCI_A_ENGINE_SUPPORTED,
            BaseId = J2534ProtocolId.SCI_A_ENGINE
        };

        public static readonly ChannelSetupParameters SCI_A_TRANS = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.SCI_A_TRANS_SUPPORTED,
            BaseId = J2534ProtocolId.SCI_A_TRANS
        };

        public static readonly ChannelSetupParameters SCI_B_ENGINE = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.SCI_B_ENGINE_SUPPORTED,
            BaseId = J2534ProtocolId.SCI_B_ENGINE
        };

        public static readonly ChannelSetupParameters SCI_B_TRANS = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.SCI_B_TRANS_SUPPORTED,
            BaseId = J2534ProtocolId.SCI_B_TRANS
        };

        public static readonly ChannelSetupParameters J2610 = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.J2610_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.J2610_PS_J1962,
            PinSwitchedId = J2534ProtocolId.J2610_PS,
            FirstCHxId = J2534ProtocolId.J2610_CH1
        };

        public static readonly ChannelSetupParameters GM_UART = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.GM_UART_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.GM_UART_PS_J1962,
            PinSwitchedId = J2534ProtocolId.GM_UART_PS,
            FirstCHxId = J2534ProtocolId.GM_UART_CH1
        };

        public static readonly ChannelSetupParameters ECHO_BYTE = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.UART_ECHO_BYTE_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.UART_ECHO_BYTE_PS_J1962,
            PinSwitchedId = J2534ProtocolId.UART_ECHO_BYTE_PS,
            FirstCHxId = J2534ProtocolId.ECHO_BYTE_CH1
        };

        public static readonly ChannelSetupParameters HONDA_DIAGH = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.HONDA_DIAGH_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.HONDA_DIAGH_PS_J1962,
            PinSwitchedId = J2534ProtocolId.HONDA_DIAGH_PS,
            FirstCHxId = J2534ProtocolId.HONDA_DIAGH_CH1
        };

        public static readonly ChannelSetupParameters J1939 = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.J1939_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.J1939_PS_J1962,
            J1939PinsParameter = J2534GetDeviceInfoParameter.J1939_PS_J1939,
            PinSwitchedId = J2534ProtocolId.J1939_PS,
            FirstCHxId = J2534ProtocolId.J1939_CH1
        };

        public static readonly ChannelSetupParameters J1708 = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.J1708_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.J1708_PS_J1962,
            J1939PinsParameter = J2534GetDeviceInfoParameter.J1708_PS_J1939,
            J1708PinsParameter = J2534GetDeviceInfoParameter.J1708_PS_J1708,
            PinSwitchedId = J2534ProtocolId.J1708_PS,
            FirstCHxId = J2534ProtocolId.J1708_CH1
        };

        public static readonly ChannelSetupParameters TP2_0 = new ChannelSetupParameters
        {
            SupportParameter = J2534GetDeviceInfoParameter.TP2_0_SUPPORTED,
            J1962PinsParameter = J2534GetDeviceInfoParameter.TP2_0_PS_J1962,
            PinSwitchedId = J2534ProtocolId.TP2_0_PS,
            FirstCHxId = J2534ProtocolId.TP2_0_CH1
        };

        public static readonly ChannelSetupParametersAnalog Analog = new ChannelSetupParametersAnalog
        {
            SupportParameter = J2534GetDeviceInfoParameter.ANALOG_IN_SUPPORTED,
            FirstCHxId = J2534ProtocolId.ANALOG_IN_1
        };

        private ChannelSetup()
        {
        }
    }

    public class J2534BaudRates
    { 
        public static uint[] J1850VPW()
        {
            uint[] result = new uint[2];
            result[0] = 10400;
            result[1] = 41600;
            return result;
        }

        public static uint[] J1850PWM()
        {
            uint[] result = new uint[2];
            result[0] = 41600;
            result[1] = 83300;
            return result;
        }

        public static uint[] ISO9141_14230()
        {
            uint[] result = new uint[17];
            result[0] = 4800;
            result[1] = 9600;
            result[2] = 9615;
            result[3] = 9800;
            result[4] = 10000;
            result[5] = 10400;
            result[6] = 10870;
            result[7] = 11905;
            result[8] = 12500;
            result[9] = 13158;
            result[10] = 13889;
            result[11] = 14706;
            result[12] = 15625;
            result[13] = 19200;
            result[14] = 38400;
            result[15] = 57600;
            result[16] = 115200;
            return result;
        }

        public static uint[] CAN_ISO()
        {
            uint[] result = new uint[10];
            result[0] = 31250;
            result[1] = 50000; //DG Netbridge
            result[2] = 62500;
            result[3] = 83333;
            result[4] = 95200;
            result[5] = 125000;
            result[6] = 250000;
            result[7] = 500000;
            result[8] = 666667; //DG Netbridge
            result[9] = 1000000;
            return result;
        }

        public static uint[] SW_CAN_ISO()
        {
            uint[] result = new uint[2];
            result[0] = 33333;
            result[1] = 83333;
            return result;
        }

        public static uint[] FT_CAN_ISO()
        {
            uint[] result = new uint[3];
            result[0] = 50000;
            result[1] = 95238;
            result[2] = 125000;
            return result;
        }

        public static uint[] J2610()
        {
            uint[] result = new uint[3];
            result[0] = 7812;
            result[1] = 62500;
            result[2] = 125000;
            return result;
        }

        public static uint[] GM_UART()
        {
            uint[] result = new uint[1];
            result[0] = 8192;
            return result;
        }

        public static uint[] UART_ECHO_BYTE()
        {
            uint[] result = new uint[1];
            result[0] = 9600;
            return result;
        }

        public static uint[] HONDA_DIAGH()
        {
            uint[] result = new uint[1];
            result[0] = 9600;
            return result;
        }

        public static uint[] J1939()
        {
            uint[] result = new uint[4];
            result[0] = 125000;
            result[1] = 250000;
            result[2] = 500000;
            result[3] = 666667;
            return result;
        }

        public static uint[] J1708()
        {
            uint[] result = new uint[2];
            result[0] = 9600;
            result[1] = 19200;
            return result;
        }

        public static uint[] TP2_0()
        {
            uint[] result = new uint[6];
            result[0] = 83333;
            result[1] = 125000;
            result[2] = 250000;
            result[3] = 500000;
            result[4] = 666667; //DG Netbridge
            result[5] = 1000000;
            return result;
        }

        public static uint[] LIN()
        {
            uint[] result = new uint[4]; //DG
            result[0] = 4800;
            result[1] = 9600;
            result[2] = 19200;
            result[3] = 20000;
            return result;
        }

        private J2534BaudRates()
        {
        }
    }

    public class J2534ConfigParameters
    { 
        public static J2534ConfigParameter[] J1850VPW()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] J1850PWM()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(NODE_ADDRESS),
                new J2534ConfigParameter(NETWORK_LINE)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] ISO9141()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(P1_MAX),
                new J2534ConfigParameter(P3_MIN),
                new J2534ConfigParameter(P4_MIN),
                new J2534ConfigParameter(W0),
                new J2534ConfigParameter(W1),
                new J2534ConfigParameter(W2),
                new J2534ConfigParameter(W3),
                new J2534ConfigParameter(W4),
                new J2534ConfigParameter(TIDLE),
                new J2534ConfigParameter(TINIL),
                new J2534ConfigParameter(TWUP),
                new J2534ConfigParameter(PARITY),
                new J2534ConfigParameter(DATA_BITS),
                new J2534ConfigParameter(FIVE_BAUD_MOD)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] ISO14230()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(P1_MAX),
                new J2534ConfigParameter(P3_MIN),
                new J2534ConfigParameter(P4_MIN),
                new J2534ConfigParameter(W0),
                new J2534ConfigParameter(W1),
                new J2534ConfigParameter(W2),
                new J2534ConfigParameter(W3),
                new J2534ConfigParameter(W4),
                new J2534ConfigParameter(W5),
                new J2534ConfigParameter(TIDLE),
                new J2534ConfigParameter(TINIL),
                new J2534ConfigParameter(TWUP),
                new J2534ConfigParameter(PARITY),
                new J2534ConfigParameter(DATA_BITS),
                new J2534ConfigParameter(FIVE_BAUD_MOD)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] CAN()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(BIT_SAMPLE_POINT),
                new J2534ConfigParameter(SYNC_JUMP_WIDTH)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] ISO15765()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(BIT_SAMPLE_POINT),
                new J2534ConfigParameter(SYNC_JUMP_WIDTH),
                new J2534ConfigParameter(ISO15765_BS),
                new J2534ConfigParameter(ISO15765_STMIN),
                new J2534ConfigParameter(BS_TX),
                new J2534ConfigParameter(STMIN_TX),
                new J2534ConfigParameter(ISO15765_WFT_MAX),
                new J2534ConfigParameter(CAN_MIXED_FORMAT)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] SW_CAN()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(BIT_SAMPLE_POINT),
                new J2534ConfigParameter(SYNC_JUMP_WIDTH),
                new J2534ConfigParameter(SW_CAN_HS_DATA_RATE),
                new J2534ConfigParameter(SW_CAN_SPEEDCHANGE_ENABLE),
                new J2534ConfigParameter(SW_CAN_RES_SWITCH)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] FT_CAN()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(BIT_SAMPLE_POINT),
                new J2534ConfigParameter(SYNC_JUMP_WIDTH)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] SW_ISO15765()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(BIT_SAMPLE_POINT),
                new J2534ConfigParameter(SYNC_JUMP_WIDTH),
                new J2534ConfigParameter(ISO15765_BS),
                new J2534ConfigParameter(ISO15765_STMIN),
                new J2534ConfigParameter(BS_TX),
                new J2534ConfigParameter(STMIN_TX),
                new J2534ConfigParameter(ISO15765_WFT_MAX),
                new J2534ConfigParameter(CAN_MIXED_FORMAT),
                new J2534ConfigParameter(SW_CAN_HS_DATA_RATE),
                new J2534ConfigParameter(SW_CAN_SPEEDCHANGE_ENABLE),
                new J2534ConfigParameter(SW_CAN_RES_SWITCH)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] FT_ISO15765()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(BIT_SAMPLE_POINT),
                new J2534ConfigParameter(SYNC_JUMP_WIDTH),
                new J2534ConfigParameter(ISO15765_BS),
                new J2534ConfigParameter(ISO15765_STMIN),
                new J2534ConfigParameter(BS_TX),
                new J2534ConfigParameter(STMIN_TX),
                new J2534ConfigParameter(ISO15765_WFT_MAX),
                new J2534ConfigParameter(CAN_MIXED_FORMAT)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] J2610()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(T1_MAX),
                new J2534ConfigParameter(T2_MAX),
                new J2534ConfigParameter(T3_MAX),
                new J2534ConfigParameter(T4_MAX),
                new J2534ConfigParameter(T5_MAX)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] GM_UART()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] UART_ECHO_BYTE()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(UEB_T0_MIN),
                new J2534ConfigParameter(UEB_T1_MAX),
                new J2534ConfigParameter(UEB_T2_MAX),
                new J2534ConfigParameter(UEB_T3_MAX),
                new J2534ConfigParameter(UEB_T4_MIN),
                new J2534ConfigParameter(UEB_T5_MAX),
                new J2534ConfigParameter(UEB_T6_MAX),
                new J2534ConfigParameter(UEB_T7_MIN),
                new J2534ConfigParameter(UEB_T7_MAX),
                new J2534ConfigParameter(UEB_T9_MIN)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] HONDA_DIAGH()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(P1_MAX),
                new J2534ConfigParameter(P3_MIN),
                new J2534ConfigParameter(P4_MIN)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] J1939()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(BIT_SAMPLE_POINT),
                new J2534ConfigParameter(SYNC_JUMP_WIDTH),
                new J2534ConfigParameter(J1939_T1),
                new J2534ConfigParameter(J1939_T2),
                new J2534ConfigParameter(J1939_T3),
                new J2534ConfigParameter(J1939_T4),
                new J2534ConfigParameter(J1939_BRDCST_MIN_DELAY)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] J1708()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] TP2_0()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(BIT_SAMPLE_POINT),
                new J2534ConfigParameter(SYNC_JUMP_WIDTH),
                new J2534ConfigParameter(TP2_0_T_BR_INT),
                new J2534ConfigParameter(TP2_0_T_E),
                new J2534ConfigParameter(TP2_0_MNTC),
                new J2534ConfigParameter(TP2_0_T_CTA),
                new J2534ConfigParameter(TP2_0_MNCT),
                new J2534ConfigParameter(TP2_0_MNTB),
                new J2534ConfigParameter(TP2_0_MNT),
                new J2534ConfigParameter(TP2_0_T_WAIT),
                new J2534ConfigParameter(TP2_0_T1),
                new J2534ConfigParameter(TP2_0_T3),
                new J2534ConfigParameter(TP2_0_IDENTIFER),
                new J2534ConfigParameter(TP2_0_RXIDPASSIVE)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] Analog()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(ACTIVE_CHANNELS, false),
                new J2534ConfigParameter(SAMPLE_RATE, false),
                new J2534ConfigParameter(SAMPLES_PER_READING, false),
                new J2534ConfigParameter(READINGS_PER_MSG, false),
                new J2534ConfigParameter(AVERAGING_METHOD, false),
                new J2534ConfigParameter(SAMPLE_RESOLUTION, true),
                new J2534ConfigParameter(INPUT_RANGE_LOW, true),
                new J2534ConfigParameter(INPUT_RANGE_HIGH, true)
            };
            return result.ToArray();
        }

        public static J2534ConfigParameter[] LIN()
        {
            List<J2534ConfigParameter> result = new List<J2534ConfigParameter>()
            {
                new J2534ConfigParameter(DATA_RATE),
                new J2534ConfigParameter(LOOPBACK),
                new J2534ConfigParameter(LIN_VERSION)
            };
            return result.ToArray();
        }

        private J2534ConfigParameters()
        {
        }
    }

    public delegate J2534Result PassThruOpenDelegate(Int32 pName, ref UInt32 DeviceId);
    public delegate J2534Result PassThruCloseDelegate(UInt32 deviceId);
    public delegate J2534Result PassThruConnectDelegate(UInt32 deviceId, J2534ProtocolId protocolId, J2534ConnectFlags flags, UInt32 baudRate, ref UInt32 channelId);
    public delegate J2534Result PassThruDisconnectDelegate(UInt32 channelId);
    public delegate J2534Result PassThruReadMsgsDelegate(UInt32 channelId, Int32 pMsg, ref Int32 pNumMsgs, UInt32 timeout);
    public delegate J2534Result PassThruWriteMsgsDelegate(UInt32 channelId, Int32 pMsg, ref Int32 pNumMsgs, UInt32 timeout);
    public delegate J2534Result PassThruStartPeriodicMsgDelegate(UInt32 channelId, Int32 pMsg, ref UInt32 MsgId, UInt32 timeInterval);
    public delegate J2534Result PassThruStopPeriodicMsgDelegate(UInt32 channelId, UInt32 MsgId);
    public delegate J2534Result PassThruStartMsgFilterDelegate(UInt32 channelId, J2534FilterType filterType, Int32 pMaskMsg, Int32 pPattMsg, Int32 pFlowControlMsg, ref UInt32 MsgId);
    public delegate J2534Result PassThruStopMsgFilterDelegate(UInt32 channelId, UInt32 MsgId);
    public delegate J2534Result PassThruSetProgrammingVoltageDelegate(UInt32 deviceId, UInt32 Pin, UInt32 Voltage);
    public delegate J2534Result PassThruReadVersionDelegate(UInt32 deviceId, byte[] pFWVer, byte[] pDLLVer, byte[] pAPIVer);
    public delegate J2534Result PassThruGetLastErrorDelegate(byte[] rrrStr);
    public delegate J2534Result PassThruIoctlDelegate(UInt32 channelId, J2534IoCtlId ioctlId, Int32 pInput, Int32 pOutput);

    [StructLayout(LayoutKind.Explicit, Pack=1)]
    public struct SPARAM //Basically a J2534-2 SPARAM with explicit values for GET_DEVICE_INFO outputs
    {
        [FieldOffset(0)] public uint Parameter;
        [FieldOffset(4)] public byte SS;
        [FieldOffset(5)] public byte RR;
        [FieldOffset(6)] public byte QQ;
        [FieldOffset(7)] public byte PP;
        [FieldOffset(4)] public uint Value;
        [FieldOffset(8)] public uint Supported;
    }

    [StructLayout(LayoutKind.Explicit, Pack=1)]
    public struct PinMap
    {
        [FieldOffset(0)] public uint Value;
        [FieldOffset(0)] public ushort LLLL;
        [FieldOffset(2)] public ushort HHHH;
        [FieldOffset(0)] public byte LoPin;
        [FieldOffset(1)] public byte HiPin;
    }

    // This enum can be expanded to support features particular to a given box.
    public enum DeviceType
    {
        Unknown = 0,
        Other = 1,
        Netbridge = 2 //DG
    }

    [Flags]
    public enum MessageCompareResults
    {
        None = 0,
        ProtocolMismatch = 1,
        RxStatusMismatch = 2,
        TxFlagsMismatch = 4,
        TimestampMismatch = 8,
        DataSizeMismatch = 16,
        ExtraDataMismatch = 32,
        DataMismatch = 64
    }

    [Flags]
    public enum ConfigChanges
    {
        None = 0,
        ValueMismatch = 1,
        TextExists = 2
    }

    public class J2534MessageText
    {
        public string ProtocolName {get; set;}
        public string RxStatus {get; set;}
        public string TxFlags {get; set;}
        public string Timestamp {get; set;}
        public string Data {get; set;}
        public string ExtraData {get; set;}
        public string DataASCII {get; set;}
        public string Comment {get; set;}
        public bool Tx {get; set;}
        public J2534MessageText Clone()
        {
            J2534MessageText result = (J2534MessageText)MemberwiseClone();
            return result;
        }
    }

    public class J2534FilterText
    {
        public J2534MessageText MaskMessageText {get; set;} = new J2534MessageText();
        public J2534MessageText PatternMessageText {get; set;} = new J2534MessageText();
        public J2534MessageText FlowMessageText {get; set;} = new J2534MessageText();
        public uint MessageId {get; set;}
        public string FilterType {get; set;} = string.Empty;
        public bool Enabled {get; set;}
    }

    public class J2534Filter
    {
        public PASSTHRU_MSG MaskMessage = PASSTHRU_MSG.CreateInstance();
        public PASSTHRU_MSG PatternMessage = PASSTHRU_MSG.CreateInstance();
        public PASSTHRU_MSG FlowMessage = PASSTHRU_MSG.CreateInstance();
        public uint MessageId {get; set;}
        public J2534FilterType FilterType {get; set;}
        public bool Enabled {get; set;}

        public bool IsEmpty()
        {
            bool result = false;
            if (MaskMessage.DataSize == 0 && PatternMessage.DataSize == 0 && FlowMessage.DataSize == 0 && FilterType == 0 && !Enabled) //filter is empty
            {
                result = true;
            }
            return result;
        }

        public J2534Filter Clone()
        {
            J2534Filter result = (J2534Filter)MemberwiseClone();
            result.MaskMessage = MaskMessage.Clone();
            result.PatternMessage = PatternMessage.Clone();
            result.FlowMessage = FlowMessage.Clone();
            return result;
        }
    }

    public class J2534PeriodicMessageText
    {
        public J2534MessageText MessageText {get; set;} = new J2534MessageText();
        public uint Interval {get; set;}
        public uint MessageId {get; set;}
        public bool Enabled {get; set;}
    }

    public class J2534PeriodicMessage
    {
        public PASSTHRU_MSG Message = PASSTHRU_MSG.CreateInstance();
        public uint Interval {get; set;}
        public uint MessageId {get; set;}
        public bool Enabled {get; set;}

        public bool IsEmpty()
        {
            bool result = false;
            if (Message.DataSize == 0 && !Enabled)
            {
                result = true;
            }
            return result;
        }

        public J2534PeriodicMessage Clone()
        {
            J2534PeriodicMessage result = (J2534PeriodicMessage)MemberwiseClone();
            result.Message = Message.Clone();
            return result;
        }
    }

    public class J2534ConfigParameter
    {
        public J2534ConfigParameterId Id {get; set;}
        public uint Value {get; set;}
        public string NewText {get; set;}
        public bool IsReadOnly {get; set;}

        public J2534ConfigParameter(J2534ConfigParameterId id) : this(id, false)
        {
        }
        public J2534ConfigParameter(J2534ConfigParameterId id, bool readOnly)
        {
            Id = id;
            IsReadOnly = readOnly;
        }
    }

    public class J2534FunctionalMessage
    {
        public byte Value {get; set;}
        public string NewText {get; set;} = string.Empty;
    }

    public class J2534Setup
    {
        public string SelectedChannelName {get; set;}
        public string MessageChannelName {get; set;}
        public string ConnectChannelName {get; set;}
        public string TextBoxOutFlags {get; set;}
        public string TextBoxMessageOut {get; set;}
        public bool GetMessages {get; set;}
        public int MessageReadRate {get; set;}
    }

    public class J2534ConnectOptions
    {
        public J2534Channel Channel {get; set;}
        public uint BaudRate {get; set;}
        public uint Connector {get; set;}
        public uint Pins {get; set;}
        public J2534ConnectFlags ConnectFlags {get; set;}
        public bool MixedMode {get; set;}
    }

    public class PinInfo
    {
        public bool Selected {get; set;}
        public bool CanBeShorted {get; set;}
        public bool CanBeSet {get; set;}
        public PinMode Mode {get; set;}
    }

    public enum PinMode
    {
        Open = 0,
        Volt = 1,
        Shorted = 2
    }

    public class J2534AnalogSubsystem
    {
        public int[] AnalogIn {get; set;}
        public int RangeLow {get; set;}
        public int RangeHigh {get; set;}
        public uint ActiveChannels {get; set;}
        public uint ReadingsPerMessage {get; set;}
        public uint MaxChannel {get; set;}
        public uint MaxSampleRate {get; set;}
        public J2534AnalogSubsystem()
        {
            AnalogIn = new int[32];
        }
    }

    public class J2534Analog
    {
        public PinInfo[] Pin {get; set;}
        public uint PinReading {get; set;}
        public uint BatteryReading {get; set;}
        public uint PinSetting {get; set;}
        public J2534Analog()
        {
            Pin = new PinInfo[16];
        }
    }

    public class J1939AddressClaimSetup
    {
        public byte Address {get; set;}
        public uint Identity {get; set;}
        public uint MfgCode {get; set;}
        public byte ECUInstance {get; set;}
        public byte FunctionInstance {get; set;}
        public byte FunctionId {get; set;}
        public byte VehicleSystemId {get; set;}
        public byte VehicleSystemInstance {get; set;}
        public byte IndustryGroup {get; set;}
        public bool ArbitraryCapable {get; set;}

        //masks for J1939 address claim
        public const uint J1939_NAME_MASK_IDENTITY = 0x00_1F_FF_FF;
        public const uint J1939_NAME_MASK_MFG_CODE = 0xFF_E0_00_00U; //unused
        public const byte J1939_NAME_MASK_ECU_INST = 0b0000_0111;
        // public const byte J1939_NAME_MASK_FUNC_INST = 0b1111_1000
        // public const byte J1939_NAME_MASK_VEH_SYS = 0b1111_1110
        public const byte J1939_NAME_MASK_VEH_SYS_INST = 0b0000_1111;
        public const byte J1939_NAME_MASK_IND_GRP = 0b0111_0000;
        public const byte J1939_NAME_MASK_ARB = 0b1000_0000;
    }

    public class ChannelSetupParameters
    {
        public J2534GetDeviceInfoParameter SupportParameter {get; set;} = 0;
        public J2534GetDeviceInfoParameter? J1962PinsParameter {get; set;}
        public J2534GetDeviceInfoParameter? J1939PinsParameter {get; set;}
        public J2534GetDeviceInfoParameter? J1708PinsParameter {get; set;}
        public J2534ProtocolId? BaseId {get; set;}
        public J2534ProtocolId? PinSwitchedId {get; set;}
        public J2534ProtocolId? FirstCHxId {get; set;}
    }

    public class ChannelSetupParametersK
    {
        public J2534GetDeviceInfoParameter SupportParameter {get; set;} = 0;
        public J2534GetDeviceInfoParameter? J1962_K_PinsParameter {get; set;}
        public J2534GetDeviceInfoParameter? J1962_L_PinsParameter {get; set;}
        public J2534GetDeviceInfoParameter? J1939_K_PinsParameter {get; set;}
        public J2534GetDeviceInfoParameter? J1939_L_PinsParameter {get; set;}
        public J2534ProtocolId? BaseId {get; set;}
        public J2534ProtocolId? PinSwitchedId {get; set;}
        public J2534ProtocolId? FirstCHxId {get; set;}
    }

    public class ChannelSetupParametersAnalog
    {
        public J2534GetDeviceInfoParameter SupportParameter {get; set;} = 0;
        public J2534ProtocolId? FirstCHxId {get; set;}
    }
}